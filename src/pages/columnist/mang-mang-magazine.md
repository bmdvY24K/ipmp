---
templateKey: columnist
columnist_title: 莽莽杂志
slug: mang-mang-magazine
cover: /img/mm-logo.png
order: 11
description: '《莽莽》，一本涉猎时政、社会、文化等议题的独立中文杂志。成员为身在海外、热爱自由的一群人，既致力于现实行动，亦想捍卫以中文自由写作的权利，用母语来记录、对抗遗忘和发起辩论。
联络方式：
投稿/联系:  info@mangmang.run
Instagram/Telegram频道：@mangmang_editorial
Matters：莽莽杂志（@mm2022matter）
网站：https://www.mangmang.run/'
---

