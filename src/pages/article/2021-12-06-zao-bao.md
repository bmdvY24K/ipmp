---
templateKey: article
title: 12.6 N记早报
slug: zao-bao
date: 2021-12-06T04:45:44Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: T

---
**1. 律师视频会见李翘楚，称其幻听加重，伴有头晕、乏力等症状**

据[FreeLiqiaochu](https://www.facebook.com/110153281109553/posts/298130578978488/?d = n)消息，代理律师于 12 月 3 日视频会见李翘楚，称她的幻听在抗抑郁药物调整后加重，并出现头晕、乏力等症状。李翘楚母亲再次提出取保候审申请书，希望她能回北京治疗。

李翘楚长期关注公民运动、女权和劳工问题，曾投身于 MeToo 和反“北京切除”等事件。2019 年 12 月 31 日，李翘楚因其男友许志永参加“[厦门公民聚会案](https://www.hrichina.org/cht/sha-men-ju-hui)”而被连带抓捕，后被指定居所监视居住 120 天。2021 年，李翘楚因涉嫌“煽动颠覆国家政权罪”被起诉。 #李翘楚

**2. 深圳市卫建委被网民投诉低俗博眼球**

据[澎湃新闻](https://m.weibo.cn/5044281310/4711170340552996)报道，有网民在人民网“领导留言板”留言称，深圳市卫建委微信公众号“使用标题党吸引人眼球博得流量。建议进行整改。”

12 月 5 日，深圳市卫健委回复网友称，微信公众号”会适当收缩开放的推文尺度”，保持专业、中性、客观的风格，做到雅俗共赏、老少皆宜，“把握正确价值导向，积极传递正能量”。 #举报

**3. 缅甸军车冲撞仰光示威人群，士兵开枪，或致十人受伤**

据[路透社](https://www.reuters.com/world/asia-pacific/myanmar-security-forces-ram-car-into-protest-yangon-deaths-feared-2021-12-05/)报道，周日上午，缅甸军方一辆货车冲撞仰光“快闪”抗议示威人群，士兵向人群开枪，致现场还有数十人受伤，至少 15 人被捕。

一位匿名幸存者接受路透社采访表示，“我被撞到，倒在一辆货车前面。一个士兵用步枪打我，我反抗将他推了回去。然后我沿​​环形交叉路线逃跑，他立刻向我开枪。幸运的是，我逃脱了。”

路透社引述另外两名目击者称，士兵开车从后面撞向人群，并对逃散的示威者逮捕、殴打。有人头部严重受伤，失去知觉。

缅甸军方自 2 月 1 日政变以来，多次以开枪等暴力手段镇压抗议，已至少有 1300 人在镇压中死亡。 #缅甸

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
