---
templateKey: article
title: 6.20 N记早报
slug: zao-bao
date: 2022-06-20T05:33:26Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: NL

---
**1. 今年 5 月全国财政收入同比下降 32.5%**

据[财新](https://economy.caixin.com/2022-06-16/101900165.html)报道，今年 5 月全国财政收入即一般公共预算收入较上年同期大幅下降 32.5%，其中，税收收入同比锐减 38.1%。税收收入下降与 2022 年 4 月开始实施的增值税留抵退税政策有关。财政部称，今年 4-5 月集中退还中小微企业的存量留抵税款，扣除该留抵退税因素后，5 月财政收入同比下降 5.7%。同时，5 月财政支出同比增长 5.6%。财政收入锐减、支出锐增反映了疫情对中国经济的冲击仍在持续：企业产能下降，居民消费意愿下降，疫情管制对公共支出造成显著负担。 #经济

**2. 香港鲜浪潮电影节影片未过审，导演现场朗读剧本**

香港鲜浪潮国际短片节 6 月 17 日开幕，其中一部以失踪少女“燕琳”为背景的《Time, and Time Again》未获得香港[“电影、报刊及物品管理办事处”](https://www.ofnaa.gov.hk/tc/home/index.html)（电检处）的上映“核准证明书”，需取消两场放映。6 月 19 日，在放映单元“生者如斯”中，参展影片《防止自杀手册》导演王嘉诺和《余烬》导演潘逸敏现场朗读《Time, and Time Again》[第一至三场的剧本](https://www.facebook.com/469003650507942/posts/pfbid0tTMHmtLxX2zj2JBwGD7THhC4CkmBJDvrc3o1deF6SGBMQtZ48D4yFNe92fwKaCE9l/)。

另一部参展影片《群鼠》则是在 6 月 17 日傍晚才获发“核准证明书”，评为三级获批放映。短片节 11 位参展导演[联名声明](https://www.inmediahk.net/node/%E6%96%87%E8%97%9D/%E9%AE%AE%E6%B5%AA%E6%BD%AE11%E5%90%8D%E6%9C%AC%E5%9C%B0%E5%B0%8E%E6%BC%94%E8%81%AF%E7%BD%B2-%E6%89%B9%E9%9B%BB%E6%AA%A2%E6%8B%96%E5%BB%B6%E5%AF%A9%E6%89%B9%E5%A6%82%E5%90%8C%E7%84%A1%E7%90%86%E7%A6%81%E6%92%AD)，批评电检处不止一次拖延发放审批“无疑等同无需理由地禁播”，认为电检处做法严重影响创作自由。 #香港

**3. 中国、印度增加采购俄罗斯原油和煤碳，伊朗、印度、俄罗斯启用新运输通道**

欧美主要西方国家因俄罗斯入侵乌克兰而对俄罗斯采取的经济制裁仍在进行，中国和印度则大举增加对俄罗斯能源产品的购买量。据[日经报道](https://zh.cn.nikkei.com/industry/ienvironment/48814-2022-06-15-05-00-10.html)，今年 5 月，中国对俄罗斯原油的海上进口量（不含管线）达到每日 80 万桶，比 1 月增加 4 成以上。印度的俄罗斯原油海上进口量在 1 月时为零，5 月则达到每日近 70 万桶。

据[路透社](https://www.reuters.com/markets/commodities/exclusive-indias-russian-coal-buying-spikes-traders-offer-steep-discounts-2022-06-18/)报道，近几周，俄罗斯卖家向印度提供最高 30% 的煤矿销售折扣，同时允许印度用卢比、阿联酋迪拉姆和美元等货币进行支付。俄乌战争爆发后的三个月里，印度对俄罗斯煤炭的日均购买量为 771 万美元，近三周以来，这个数字翻番至 1655 万美元。

近期，印度、伊朗和俄罗斯签订了[“国际南北运输通道”（INSTC）](https://economictimes.indiatimes.com/news/economy/foreign-trade/instc-operationalised-as-russia-sends-consignments-for-indian-port/articleshow/92189350.cms)，伊朗允许印度和俄罗斯使用伊朗港口和伊朗国营船运公司运输贸易货物。这条通道将取代原先必须经由地中海-苏伊士运河的欧洲贸易通道，为俄罗斯规避西方制裁提供了新的出口。 #俄罗斯 #制裁

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
