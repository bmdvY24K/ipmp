---
templateKey: article
title: 2020:"共享富裕，共享恐惧"(二月)
slug: qu-nian-jin-ri-feb
date: 2021-03-13T03:21:57.265Z
description: null
featuredpost: true
featuredimage: /img/0217.jpg
trending: 9
isbrief: false
typora-copy-images-to: ../../../static/img
columnist: 2020：“共享富裕，共享恐惧”
tags:
  - "2020"
  - 去年今日
  - covid-19
  - 武汉
  - 李文亮
contributors:
  - type: 插图
    name: 糖糖
  - type: 作者
    name: lizi
  - type: 作者
    name: laitek
typora-root-url: ../../../static

---

![](/img/0206.JPG)

![](/img/0209.JPG)

![](/img/0212.JPG)

![](/img/0213.JPG)

![](/img/0216.JPG)

![](/img/0217.jpg)

![](/img/0218.JPG)

![](/img/0221.JPG)

![](/img/0224.JPG)

![](/img/0225.JPG)

![](/img/0226.JPG)

![](/img/0227.JPG)

![](/img/0229.JPG)

