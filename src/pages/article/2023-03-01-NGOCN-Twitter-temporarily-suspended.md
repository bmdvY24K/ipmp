---
templateKey: article
title: '告读者：NGOCN推特账户突遭暂时停用'
slug: 'NGOCN-Twitter-temporarily-suspended'
date: 2023-03-01T03:21:57.265Z
description: '近日，有人假冒记者接近白纸运动行动者或者相关组织，从中获取他们个人信息或者关键资料。结合目前个案我们讲讲如何辨别真假记者以便安全地接受媒体采访。'
featuredpost: false
featuredimage: /img/hua-1.png
trending: 1
isbrief: false
columnist: 告读者
contributors:
  - type: '作者'
    name: 'NGOCN'
tags:
  - "推特"
  - "停用"
  - "恶意举报"
  - "NGOCN"
  - "言论自由"
---

近日NGOCN推特账户突然被告知“永久冻结”。面对这种突发情况，我们采取了如下措施和应对想法：

1. 立即通知团队全体成员，并且提升整体安全措施，确保团队安全。

2. 通过推特的机制提出申诉，要求尽快恢复账号。

3. 认真分析发布的内容。我们的推特主要发布报道链接，倒数第二条发布的内容是武汉医保改革抗争的深度报道[《武汉医改示威者：“三年了，这是存活下来的人在反抗”》](https://ngocn2.org/article/2023-02-22-Wuhan-Medical-Insurance-Reform-Protest-2/)；最后一条内容是转发巴丢草的推特，提醒大家[注意识别假冒记者](https://ngocn2.org/article/2022-12-13-safe-for-media-interviews/)。我们相信NGOCN推特账户发布的内容并没有违反任何相关推特的政策，账号被冻结，极有可能来自恶意举报。在此强烈谴责这种通过不正当手段，干涉、侵扰言论自由的做法。我们将努力向推特申诉，并且做好准备应对一切可能出现的挑战和风险。

4. 一直以来，由于人力有限，我们都优先把资源投入到内容生产，对于运营一直比较佛系。但这次突发事件让我们意识到两点。第一，无论是关注中国现状的深度报道，还是提醒大家行动时的安全注意，这些内容都很有价值，今后我们会继续加强相关内容产出，以更优质的内容回馈读者。第二，我们也重新反思了运营策略，在保证人员安全和可应付的情况下，我们将投入更多精力进行运营和传播，包括探索更多去中心化的传播方式，将优质的报道内容带给更广泛的读者。

最后，我们感谢读者的及时反馈，如果申诉有任何进展，我们会及时告知。感谢大家一路的支持，我们会继续努力，与大家并肩开创属于我们的未来。