---
templateKey: article
title: 8.19 N记早报
slug: zao-bao
date: 2022-08-19T13:35:31Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: T

---
**1. 阿里巴巴等多家互联网公司向政府备案演算法服务**

据中国网信办发布“互联网信息服务算法备案信息”之[公告](http://www.cac.gov.cn/2022-08/12/c_1661927474338504.htm)，境内互联网信息服务算法可透过演算法[备案系统](https://beian.cac.gov.cn)查询。据境内互联网信息服务算法备案清单（2022 年 8 月）显示，阿里巴巴、腾讯、字节跳动、美团、百度等互联网企业旗下的 30 个应用程序算法均已上报备案。东华政法大学竞争法研究中心执行主任翟巍[认为](https://www.zaobao.com.sg/realtime/china/story20220815-1303267)，公司提交给网信办的信息会“包含一些不宜公开于众的商业秘密”。 #演算法 #网信办

**2. 四川省政府要求“让电于民”，部分工业用电企业停工停产 6 日**

8 月 16 日，四川省人民政府官网发布《[全力保障民生用电 四川启动三级保供电调控措施](https://www.sc.gov.cn/10462/10464/13722/2022/8/16/24bf8f6ec55044338064b77b4e67dd15.shtml)》，称今年因“极端高温干旱天气”，电力通道皆满载运行，为保障民生用电需进行三级调控，将从 8 月 15 日 00:00 起对部分高载工业用电企业实施停产、“让电于民”，至明日（20 日）24:00 结束。

据中国气象局[评估](https://mp.weixin.qq.com/s/ZMCvczkldjDSkTnaAIYtVQ)，从今年 6 月 13 日起区域性高温的强度，已达到 1961 年有完整气象记录以来最强；高温持续的时间亦为有记录以来最长。而全球范围内的极端天气，亦将造成新的[风险不平等](https://www.washingtonpost.com/climate-environment/interactive/2022/extreme-heat-risk-map-us/)问题。 #环境 #限电

**3. 沙特女权活动者因转发推特、声援异见者被判 34 年监禁**

在英国攻读博士学位的沙特阿拉伯女性 Salma al-Shehab，因曾在推特上为争取女性驾驶权、后被捕入狱的 Loujain al-Hathloul 发声（及转发文章），在去年 1 月被沙特当局以“[透过社交媒体扰乱公共秩序、破坏国家安全](https://www.google.com/amp/s/amp.theguardian.com/world/2022/aug/16/saudi-woman-given-34-year-prison-sentence-for-using-twitter)”的罪名拘捕监禁。今年 8 月 8 日，沙特法院判处其 34 年监禁及 34 年旅游限制，据 NGO 组织自由倡议（The Freedom Initiative）[统计](https://thefreedomi.org/statements/saudi-womens-rights-activist-and-mother-of-two-sentenced-to-34-years-in-jail/)，这是目前沙特阿拉伯已知被判刑时间最长的女权活动家。

Salma al-Shehab 目前有两个小孩，系什叶派穆斯林，在以逊尼派穆斯林为主的沙特属于少数。 #沙特阿拉伯 #女权

**4. 以色列政府强制关闭多家巴勒斯坦非政府组织**

据路透社[报道](https://www.reuters.com/world/middle-east/israeli-forces-kill-palestinian-west-bank-clashes-medics-say-2022-08-18/)，当地时间 8 月 18 日（四），以色列安全部队突袭西岸人权纪录监督组织（Al-Haq）、巴勒斯坦妇女联合会（UPWC）等 7 个巴勒斯坦 NGO 办公室，沒收其电脑设备后封锁了入口。以色列国防部部长 Benny Gantz 认为，这些 NGO 为解放巴勒斯坦人民陣線（PFLP）旗下的组织，而该阵线系“对以色列人进行攻击的恐怖组织”。联合国批评此行动为任意、“没有任何证据可证明其合理性”的；美国国务院则[表示](https://www.reuters.com/world/middle-east/israel-provide-information-us-basis-ngo-closures-state-dept-2022-08-18/)，将根据以色列后续提供的信息再进行判断。 #以色列 #巴勒斯坦

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
