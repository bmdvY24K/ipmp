---
templateKey: article
title: This Week China Society-Henan Floods, Kris Wu and Nanjing Outbreak|NGOCN
slug: weekly-newsletter-27-july
date: 2021-07-27T01:28:20.370Z
description: Happy Tuesday. This is NGOCN’s third weekly newsletter. We are covering the catastrophic floods in Henan province, the opinions war over pop idol Kris Wu’s alleged sexual misconduct, and other news that happened in Chinese society.
featuredpost: true
trending: 7
isbrief: false
columnist: '英文周报Weekly Newsletter'
contributors:
  - type: Author
    name: Ming
  - type: Editor
    name: 小戌


---

Happy Tuesday. This is NGOCN’s third weekly newsletter. We are covering the catastrophic floods in Henan province, the opinions war over pop idol Kris Wu’s alleged sexual misconduct, and other news that happened in Chinese society.



**People mourned the 12 who died in floods that submerged Zhengzhou subway**

A record-breaking rainfall hit Zhengzhou and submerged the city of 10 million population last week. Floods burst into the subway tunnel of Line 5, trapped about 500 people and killed 12, according to the government statement. 

In videos and pictures shared online, passengers stood in neck-high floodwaters while clinging to handrails inside the carriage.

“Call the police,” one man in a video begged for help.

“The water reached my chest,” a survivor wrote on Weibo. “I was scared. The oxygen was low. I was so desperate when the water kept rising.”

Media reported that passengers near the drivers’ cab left the carriage, trying to reach the subway station through the tunnel. Some missed on the way. The rescue team arrived around three hours later while 12 were found dead. 

On July 26, local residents came to lay flowers and light candles outside the B1 Entrance of Shakoulu station to commemorate people who died in the subway. It was one week since the incident, a traditional time period for mourning. 

A postcard, surrounded by flowers, reads: “Sometimes, unfortunate things happen to kind people.”

![210727](/Users/LIQI/ipmp/static/img/210727.png)

Flowers and cards commemorating victims of Zhengzhou subway outside Shakoulu Station. Source: [熊阿姨](http://www.douban.com/people/auntbear/status/3527948253/)



**Pop idol Kris Wu at the center of #MeToo Movement**

Du Meizhu, a 19-year-old college student, accused pop idol Kris Wu Yifan of sexual assault that Wu lured multiple teenage girls for sex. 

In an [interview](https://www.163.com/ent/article/GF6K4UV200038ADR.html) with NetEase on July 18, Du said Wu’s agent asked her to go to Wu’s house for an audition where she was pressured to play board games and drink alcohol until losing consciousness. She woke up the next day, finding herself in his bed.

According to a statement from police in Beijing, Wu and Du had sex that night, proving Wu lied about his relationship with Du in his previous denial.

Du also claimed that she knew of at least eight victims and some are underage.

After Du’s accusation, at least 25 women have stood out and shared similar experiences on Weibo. Some posted screenshots of WeChat conversations with Wu, exposing his predatory behaviors. 

Du and other women have met with outpouring support. Their allegations ignited widespread outrage on social media. Hashtag calling Wu to quit the music business topped trending topics on Weibo for days. Meanwhile, numerous global luxury brands, such as Louis Vuitton and Porsche, have cut ties with Kris Wu.



**MEANWHILE**

1. Nine workers at Nanjing Lukou International Airport tested positive for coronavirus. As of July 26, the total cases in the city’s cluster went to 88, resulting in a new spike of Covid-19. Nanjing has started a second round of mass testing for all residents. However, the government’s zero-tolerance policy has met with criticism and people complained that mass gatherings may lead to further infections.

2. On July 22, seven defendants were adjudicated for the Yuen Long attack with a maximum sentence of seven years. On July 21, 2019, more than 100 armed men indiscriminately attacked passengers, protesters, journalists in Yuen Long subway station during the pro-democracy protest. 

 