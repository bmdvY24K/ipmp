---
templateKey: article
title: 'This Week China Society - Another trending MeToo case in China: Alibaba’s rape scandal | NGOCN'
slug: 'weekly-newsletter-10-aug'
date: '2021-08-10T01:28:20.370Z'
description: 'This is NGOCN’s fifth weekly newsletter. We are covering the corruption charges against singer and activist Anthony Wong Yiu-ming, Alibaba’s sex scandal, and other news that happened in Chinese society.'
featuredpost: true
featuredimage: /img/Wong.png
trending: 7
isbrief: false
columnist: '英文周报Weekly Newsletter'
contributors:
  - type: Author
    name: Ming
  - type: Editor
    name: Laitek


---



Happy Tuesday. This is NGOCN’s fifth weekly newsletter. We are covering the corruption charges against singer and activist Anthony Wong Yiu-ming, Alibaba’s sex scandal, and other news that happened in Chinese society.



**Another trending #MeToo case in China: Alibaba’s rape scandal**

Technology giant Alibaba has fired a manager who allegedly raped a female employee after days of widespread outrage over the company’s handling of the case.

On August 7, a female employee of Alibaba posted a detailed [account](https://finance.sina.com.cn/chanjing/gsnews/2021-08-08/doc-ikqcfncc1597568.shtml) on the company’s intranet, in which she said her supervisor Quyi had sexually assaulted her on a business trip on the night of July 27.

The woman said she had reported the case to police on July 28 and to Alibaba’s human resources department on August 2, but both of them failed to take action against Quyi.

After being stonewalled by the police and Alibaba, she had made flyers and handed them out at the company’s staff canteen. 

Screenshots of the woman’s account soon leaked to social media and went viral on Saturday, fueling widespread anger. The incident has become another trending #MeToo case in China following pop idol Kris Wu’s sex scandal several weeks ago. Hashtags related to the incident ranked among the top-trending on Weibo for days.

More than 6,000 Alibaba employees formed a support group and posted a joint statement-[“Aliren help Aliren”](https://baijiahao.baidu.com/s?id=1707531486308383339&wfr=spider&for=pc), demanding justice for the victim, and calling on action to establish policies to prevent sexual harassment.

![Aliren](/Users/LIQI/ipmp/static/img/Aliren.jpeg)The joint statement declared by 6,000 Alibaba employees: "Aliren help Aliren"  *(Source: Weibo)*



On August 9, Chief Executive Zhang Yong [said](https://www.yicai.com/news/101134668.html) that “Quyi was fired and will never be rehired,” adding that the City Retail unit’s president and human resources head had resigned, and Alibaba’s Chief People Officer will be given an official demerit. 

Many comments focus on the culture of sexual harassment in some companies in China. From sexual-related games in occasions such as orientation, team building, and ice breaking to job ads that use women to lure men, sexism seems to be an internal culture in current business industry.



**Hong Kongers continue to sing and hang on: the arrest of singer and activist Anthony Wong** 

Hong Kong singer and activist Anthony Wong Yiu-ming has been dropped a charge of “corrupt conduct” over an entertainment performance at an election rally by the Independent Commission Against Corruption (ICAC) on August 5.

ICAC said on August 2 that Wong had provided “entertainment to induce others to vote” for pro-democracy candidate Au Nok-hin in a 2018 election rally. 

“Wong performed two songs at the rally. At the end of the rally, he appealed to the audience to vote for Au,” the ICAC wrote in its official statement.

Being one of the most popular singers in Hong Kong music industry, Wong is also an outspoken advocate for Hong Kong democracy and LGBT rights. He is a strong supporter for [the 2014 “Umbrella” Movement](https://zh.wikipedia.org/zh/雨傘革命) and [2019 Anti-Extradition Bill Movement](https://zh.wikipedia.org/zh-hans/反對逃犯條例修訂草案運動).

A prosecutor for the ICAC said the agency had agreed to not pursue the charge against Wong by giving him a bind-over order, which requires Wong maintain good behavior and not violate any law related to election conduct for 18 months.

“No matter if I have a hundred rights, or a thousand wrongs, I will accept the consequence wholeheartedly. Facing everything in the world, I will preserve my true self,” Wong gave a performance of his song *Ask Me* after the hearing outside the court, encouraging Hong Kongers to “continue to sing and hang on.”

![Wong](/Users/LIQI/ipmp/static/img/Wong.png)

Anthony Wong appearing outside the court on August 5 *(Source: LIHKG)*



**MEANWHILE**

1. The University of Hong Kong stated on August 4 that students who have participated in a student council meeting last month, which expressed sympathy for a man who [killed himself after stabbing a police officer](https://zh.wikipedia.org/wiki/銅鑼灣刺警案), were not allowed to enter the campus and utilize any facilities and services.

2. Lawyer [Zhou Xiaoyun](https://baike.baidu.com/item/周筱赟/5532846) has been arrested on suspicion of “picking quarrels and provoking trouble” on July 29. Zhou has been vocal in exposing corruption within large institutions. He seems to have been detained over a courtroom video of prosecutors admitting errors. In the video, prosecutor Sun Wang says: “Accepting bribes and not getting the work done maintains the moral borderline of the judicial system.”