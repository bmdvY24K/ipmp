---
templateKey: article
title: 'This Week China Society - China releases the founders of an open-resource website that archives censored articles'
slug: 'weekly-newsletter-16-august'
date: 2021-08-16T01:28:20.370Z
description: 'This is NGOCN’s sixth weekly newsletter. We are covering the verdict of the founders of Terminums2049, an open-resource website that archived censored articles, and other news that happened in Chinese society.'
featuredpost: true
trending: 7
isbrief: false
columnist: '英文周报Weekly Newsletter'
contributors:
  - type: 'Author'
    name: '小戌'
  - type: 'Editor'
    name: 'Samuel'

---



Happy Monday. This is NGOCN’s sixth weekly newsletter. We are covering the verdict of the founders of Terminums2049, an open-resource website that archived censored articles, and other news that happened in Chinese society.

  

**Founders of Terminums2049, the website that archives censored articles, released after 482 days in jail** 

 

China released the two founders who were jailed for 482 days because of archiving censored news and articles on the open-resource website Terminums2049.

 

The two detainees, Chen Mei and Cai Wei, were released on Aug 15, according to Chen’s brother Chen Kun and Cai’s father. 

 

Chen Kun said the Wenyuhe Court in Beijing called his mother on Aug 13 to announce the verdict that Chen Mei and Cai We were sentenced for one year and three months, of which their time in the detention center since April 2020 could be converted to, according to Chinese Criminal Procedure Law. It resulted in their freedom two days after the verdict. 

 

Neither the trial nor the verdict announcement was public, according to Chen Kun. NGOCN was unable to find the verdict on the court schedule.

 

“We’ve been waiting for 480 days, and I'm so happy to hear that he's finally coming home,” Chen Kun said in an interview with NGOCN on Aug 13. “But at the same time, I feel very sad he suffered nearly 500 days in prison for nothing! He was completely innocent.”

 

He said couldn’t wait to see his brother. “When seeing my brother, the most important thing I want to say is that I hope he can have a good rest, recover both physically and mentally, and start his life again,” he said.

 

Cai Wei’s father told NGOCN on Aug 13 that he was finally relieved. “At least we have some sort of result. My only hope is to see my son after 480 days of waiting,” he required anonymity because of the fear of repercussion by the authorities.

 

He [said on Twitter](https://twitter.com/caiweifather/status/1426037364141420544?s=20) that the judge asked him whether he was satisfied that he could see his son soon, and he said, “It’s not about the satisfaction or not. My son’s life is already ruined. My only hope is that he could go home healthily.” 

 

“The past is the past. We have to try hard to face what happened, and live well in the future,” he told NGOCN.

 

Chen Kun, who currently studies at the Université de Paris, has been vocal online to call for the release of Chen Mei and Cai Wei. “I’m worried about my safety as well, but because I know family members have to be vocal, otherwise people in jail would suffer more,” he said. “Of course, he is my little brother and the kinship kept me up the fight.”

 

Chen Kun and Cai Wei established Terminus2049 on open-resource GitHub on April 22, 2018, which called on the public to participate in the resistance to China’s internet censorship and ensure the availability of news and articles. 

 

The site contained more than 600 articles, including China’s #MeToo Movement, 

Hong Kong pro-democracy protest, Beijing's deportation of low-income migrant workers and more. During the outbreak of the Covid-19 in Wuhan, the site archived dozens of investigations, breaking news and features that were taken down by the government.

 

When China soon managed to maintain the outbreak, Chen Mei and Cai Wei faced the repercussion. On April 19, 2020, they were taken away by the police and put under “residential confinement at designated residences,” a more arbitrary way of detention that is [not completely constitutional](http://pkulaw.cn/fulltext_form.aspx?Db=qikan&Gid=2f648ba2ad8e31a8bc306497145fe108bdfb&EncodingName=) and [often generated inhuman treatments.](http://pkulaw.cn/fulltext_form.aspx?Db=qikan&Gid=bf4aeaaa728176e1051732ed67f8216bbdfb&keyword=&EncodingName=&Search_Mode=&Search_IsTitle=0)

 

They were officially charged on suspicion of "picking quarrels and provoking trouble" on June 12, 2020, a vague catch-all crime often used for politically sensitive cases.

 

“The law and judicial system in China is controlled by politics, and always have been,” Chen Kun said.

 

When the case was brought to the court on May 11, 2021, the prosecutor said on trial that there was a lot of misinformation on Terminus2049, which instigated people, insulted the country's leaders and had a negative impact on China’s image, according to Chen Kun’s Twitter citing his mother’s court transcripts. 

 

International human rights organizations paid close attention to the case. Human Rights Watch issued a statement in April 2020, calling on Chinese authorities to release Chen Mei and Cai Wei. One month later, Amnesty International issued a statement saying that Chen and Cai were at risk of being tortured. 

 

![terminus2049](/Users/samuelyeung/Documents/GitHub/ipmp/static/img/terminus2049_0816.png)

Chen Mei (left) and Cai Wei. Courtesy to Terminums2049 Concern Group.

 

MEANWHILE

1. Jianjiao Buluo (literally translated as Pepper Tribe), a media focused on female     labor rights in China, closed its accounts on Aug 10. Establishing in 2014, the media published more than 1400 articles about female workers’ rights, many of which were written by workers themselves.

 

2. Hong Kong’s largest protest coalition The Civil Human Rights Front, which organized     some of the biggest pro-democracy demonstrations in Hong Kong’s history, disbanded following pressure from the authorities.

 

3. Prominent human rights activists Xu Zhiyong and Ding Jiaxi were officially charged     with subversion and “gathering crowds to disturb social order” with cases being brought to court. Xu and Ding were core members of the pro-democracy New Citizen Movement and were detained after meeting with other human rights activists in Xiamen in December 2019.

 

 

 