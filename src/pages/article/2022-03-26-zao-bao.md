---
templateKey: article
title: 3.25 N记早报
slug: zao-bao
date: 2022-03-25T18:42:33Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: Giulietta
---

**1. 上海疫情防控期间一护士突发哮喘，被医院拒诊后延误病情离世**

据网络社交媒体[热议](https://s.weibo.com/weibo?q =%23%E4%B8%8A%E6%B5%B7%E4%B8%9C%E6%96%B9%E5%8C%BB%E9%99%A2%E6%8A%A4%E5%A3%AB%23)， 23 日上海市东方医院生殖中心护士周盛妮在家中哮喘发作，先后就诊于浦南医院与自己工作的东方医院，但均因医院不接诊致使病情延误，4 个小时后，该护士在仁济医院去世。

25 日凌晨上海东方医院发布通告确认其曾在 23 日夜间 19 时许由其家属载至该院求诊，[回应称](https://www.easthospital.cn/ArticleDetail?CombId = 1f2d532b-515c-4aa0-9a0c-2de0e20b7fa8%2C1f2d532b-515c-4aa0-9a0c-2de0e20b7fa8%2C55e5a59d-097c-49ee-b43d-1c35ffd24eca)：“我院南院急诊部因疫情防控需要，正暂时关闭，进行环境采样和消毒，家属遂将病人送到仁济医院东院救治。23 时许，周盛妮同志因抢救无效去世。”

在 25 日上午 10:00 上海市举行的疫情防控工作新闻发布会上，市卫生健康委主任邬惊雷[表示](https://mp.weixin.qq.com/s/gr3v84F17T9AZB4VgVO6HA)：“今天凌晨，东方医院发布情况通报，一名护士因病不幸去世，对此我们表示深切哀悼，对家属表示慰问。”

据[丁香园报道转引此前网传朋友圈信息](https://mp.weixin.qq.com/s/ux0UgcJf8vv5pOFNkE_Ekw)，逝者此前为产科门诊护士，发病后曾进行自救但无法缓解，救护车无法及时调配到位，周盛妮最终辗转多家医院。东方医院承担了巨大的核酸采样工作，但临床工作无法停止，医护人员在院坚守岗位承担了大部分压力。但当丁香园打电话核实时，东方医院的职工均表示无法透露相关信息。#上海防疫

**2. 东航 MU5735 第二个黑匣子暂未找到，FAA 称有信心得到该架波音 737-800 客机坠毁的“根本原因”**

据[央视新闻报道](https://weibo.com/2656274875/LloGtwMI5?refer_flag = 1001030103_)，25 日在“3·21”东航飞行事故现场主要撞击点为中心展开的拉网式搜寻核心区发现的橙色碎片，并非此前失事的东航 MU5735 的第二个黑匣子。此前，23 日发现的第一个黑匣子，为驾驶舱语音记录器，通常至少可保留最后两小时的录音，根据 25 日国家应急处置指挥部举行新闻发布会[介绍](https://weibo.com/2656274875/LlnLSjhOy?refer_flag = 1001030103_)：其数据分析所需时间暂不能确定。

根据路透社当地时间 25 日[报道](https://www.reuters.com/business/aerospace-defense/us-faa-chief-confident-finding-root-cause-china-737-800-crash-2022-03-25/?taid = 623dc18ba25f70000123fda8&utm_campaign = trueAnthem:+ Trending + Content&utm_medium = trueAnthem&utm_source = twitter)，波音及其他航空行业监管机构美国联邦航空管理局（the Federal Aviation Administration, FAA）局长史蒂夫·迪克森(Steve Dickson)对美国消费者新闻与商业频道 CNBC 表示，调查“要求我们不要对事故原因进行猜测”，并补充在失事飞机型号波音 737-800 所属的波音 737NG 机群“是有史以来在商业运营中生产的最安全的飞机之一。”“所有迹象表明，这是一架适合飞行的飞机，但我们必须根据事实找到根本原因。”

美国交通部长皮特·布蒂吉格(Pete Buttigieg)周三表示，中国当局已邀请美国国家运输安全委员会(NTSB)参与调查，并补充说，他对前往中国现场的邀请感到非常鼓舞。然而，美国国家运输安全委员会后来表示，尚未决定调查人员是否会根据签证的情况和检疫要求前往中国。

随着 MU5735 搜救和调查的展开，波音 737 客机的安全性也在中国网络平台引发热议，据[澎湃新闻梳理](https://m.thepaper.cn/newsDetail_forward_17226455)“3·21”东航失事的波音 737-800 机型过去 6 年在全球发生 4 次严重事故。除 MU5735 所属的波音 737NG 机群外，2019 年，因印尼狮子航空 610 号班机、埃塞俄比亚航空 302 号班机（机型均为波音 737MAX8）两次空难事件，中国、新加坡、印度尼西亚、墨西哥、澳大利亚等多国宣布禁止波音 737 Max 系列的飞行。根据经济学人商业板块 2019 年[报道](https://www.economist.com/business/2019/03/23/regulatory-capture-may-be-responsible-for-boeings-recent-problems)，FAA 在上述两起事故发生后未遵循惯例，在其他大型监管机构均宣布停飞波音 737 Max 机型之后，才宣布停飞此机型。FAA 的地位也因此急转直下。FAA 现任局长史蒂夫也为此前特朗普政府在波音 737 Max 型号空难后任命的，而史蒂夫也将于下周辞去美国联邦航空局局长一职。根据[新京报报道](https://view.inews.qq.com/a/20220325A0DPO200?refer=wx_hot)，波音公司也对“3·21”东航飞行事故作出回应表示，正与东航开展合作，技术专家已经为协助中国民航局开展调查做好了准备。 #MU5735

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2