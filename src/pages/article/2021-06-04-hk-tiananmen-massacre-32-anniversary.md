---
templateKey: article
title: 亲历香港维园六四32周年纪念：国安法下烛光长燃、人民没有忘记
slug: hk-tiananmen-massacre-32-anniversary
date: 2021-06-05T21:21:28.132Z
description: 这是《香港国安法》推行后的第一个六四。每年维园的六四烛光悼念，承载了香港人过去31年的集体回忆。但今年，警方第二次向主办方“支联会”发出反对通知书。特区政府保安局声明称，六四悼念均属非法集会，最高可判处5年监禁。重新开放的六四纪念馆也被迫关闭。
featuredpost: true
featuredimage: /img/BKXkc87.jpg
trending: 10
isbrief: false

tags:
  - 六四
  - 天安门
  - 香港
  - 港区国安法
contributors:
  - type: 报道
    name: 小冲
  - type: 摄影
    name: 小冲
  - type: 贡献
    name: 128K, Samuel, 小糖, 莱特
typora-root-url: ../../../static
typora-copy-images-to: ../../../static/img
---

6月4日晚上9点，刘姨姨在维多利亚公园的封锁线外，轻声哼唱：“也许我倒下，将不再起来…你不要悲哀，共和国的旗帜上有我们血染的风采。”

她因在反修例运动时给被家里赶出来的学生送饭，被年轻人亲切地喊作“刘姨姨”。1989年，她在海南大学，这首《血染的风采》，是包围省政府时和同学们一起唱的歌。2003年，她嫁到香港，每年参加六四悼念，亲历者们挥舞蜡烛，熟悉的旋律在维园飘荡。

今年，刘姨姨伫立于维园角落的黑暗里，独自歌唱。她哽咽，歌声戛然而止，微弱的路灯照亮眼里的泪光。“今年特别难过，你看，广场里空荡荡的，什么人都没有。”她说。



![BKXkc87](/img/BKXkc87.jpg)

<center style="font-size:14px;color:#C0C0C0">傍晚的维多利亚公园内空无一人，只有警察驻守</center>



这是《香港国安法》推行后的第一个六四。每年维园的六四烛光悼念，承载了香港人过去31年的集体回忆。但今年，警方第二次向主办方“支联会”发出反对通知书。特区政府保安局[声明](https://www.info.gov.hk/gia/general/202105/29/P2021052900395.htm)称，六四悼念均属非法集会，最高可判处5年监禁。重新开放的六四纪念馆也被迫[关闭](https://news.rthk.hk/rthk/ch/component/k2/1593875-20210602.htm)。

据多家媒体[报道](https://www.hk01.com/突發/633476/六四32-7000警力戒備-警方或引-公安條例-封閉維園部分地方)，约有4000警察驻守维园，警方于[全港拘捕6人](https://news.rthk.hk/rthk/ch/component/k2/1594386-20210605.htm)，先后亮蓝、黄、[紫](https://twitter.com/hkcnews_com/status/1400792325094137858)旗。蓝旗警告民众涉嫌非法集会，黄旗指集会者可能面临刑事检控，紫旗则警告民众涉嫌违反国安法。在铜锣湾，警方一度亮出[橙旗](https://twitter.com/hkfp/status/1400995936621367296?s=28)（警告民众速离，否则开枪），2秒后收回。



![YQWyNLe](/img/YQWyNLe.jpg)

<center style="font-size:14px;color:#C0C0C0">警察举黄旗，警告民众可能被刑事检控</center>



![AuAUbYJ](/img/AuAUbYJ.jpg)

<center style="font-size:14px;color:#C0C0C0">晚上八点，警察驱散市民，封锁维园周边，大量警察驻守现场</center>



![C66dXsR](/img/C66dXsR.jpg)

<center style="font-size:14px;color:#C0C0C0">晚上八点，警察驱散市民，封锁维园周边，大量警察驻守现场</center>



即使有国安法高压，且维园广场内部被封锁，但仍有上万民众来维园周边悼念。晚上8点，原定的悼念时间到来，人们一起打开手机闪光灯、高举蜡烛，烛光照亮了年轻的、或饱经风霜的面庞，寂静的维园被烛海与星光环绕。

“香港没有沉默。有这样的人心，我觉得香港没有死。”韩东方说。在他身后，银色的手机闪光灯星星点点，环绕着身穿蓝色制服的警察。这里是维园正门临街的十字路口，人群在警察大声的驱赶下慢悠悠地向外撤离。他曾是火车电工，1989年作为北京工人自治联会发言人，在天安门组织工人罢工游行。1993年被驱逐出境，来港创办中国劳工通讯组织，继续为中国工人运动发声。



![RSPyfjM](/img/RSPyfjM.jpg)

<center style="font-size:14px;color:#C0C0C0">大量市民举起手机灯光，以示悼念</center>



![9fSuDlN](/img/9fSuDlN.jpg)

<center style="font-size:14px;color:#C0C0C0">路旁举起烛光与手机灯光的市民</center>



![tdEh7r1](/img/tdEh7r1.jpg)

<center style="font-size:14px;color:#C0C0C0">路旁举起烛光与手机灯光的市民</center>



![NxaPpyT](/img/NxaPpyT.jpg)

<center style="font-size:14px;color:#C0C0C0">放置于路边的天安门母亲电子烛光</center>



**国安法未能熄灭烛火，但在近十年来香港政治的风云变化下，烛光已然渐弱。**

支联会全称“香港市民支援爱国民主运动联合会”，于1989年声援北京学生的百万大游行中成立。数十年来，天安门成为了香港老一代泛民主派守护的集体记忆，在一期一会的烛光里，重申“建设民主中国”的信仰。但随着香港本土思潮崛起，年轻人对爱国纲领缺乏认同，批评六四悼念“行礼如仪”。雨伞运动后，维园集会人数下降近[30%](https://theinitium.com/article/20210603-photo-victoriapark-june-4-candlelightvigil/)，各大学另起炉灶纪念六四，学联退出支联会。

但国安法带来的政治高压，却让本土派与支联会重新结盟。2019年反修例运动以来，年轻的香港众志成员被捕、流亡、组织分崩离析，7所大学的学生会内阁遭校方“腰斩”，而支联会14常委中，2人在囚，6人被检控，副主席邹幸彤更于今年六四清晨被捕，后获[保释](https://news.rthk.hk/rthk/ch/component/k2/1594451-20210605.htm)。国安法广阔而模糊的红线，挤压整个泛民光谱上的新老抗争者共同的生存空间。



![yFO42dU_mosaic](/img/yFO42dU_mosaic.png)

<center style="font-size:14px;color:#C0C0C0">晚上八点半，警察进一步扩大封锁区域。一男子身穿“风雨飘摇，勿忘初心”的黑衣，带两名小男孩离开</center>



几天前，港中大本土派学生会外务秘书袁德智[表示](https://beta.thestandnews.com/politics/我曾杯葛支聯會六四集會-但今年我決定會悼念六四)重新悼念六四，称后国安法时代，香港民主抗争不应拘泥于派系、世代和身份认同。而支联会也在年度集会主题中呼应中港两地民运：“为自由、共命运、同抗争。”

六四集会成为了大家共同守护的底线。维园边，能听到“香港人，加油！”也能看到“浩气中华，英灵不息”的标语；有稚气未脱的中学生，也有两鬓斑白的老人。人们出于不同的原因来到维园，或悼念死难者，或反抗极权，亦或是为香港的本土抗争提升士气，但大家手里拿着相同的蜡烛，在第32年薪火相传。



![C0jeBnG](/img/C0jeBnG.jpg)

<center style="font-size:14px;color:#C0C0C0">一位市民手持“浩气中华、英灵不息”的标语，后于警察发生冲突</center>



![34zoOlZ](/img/34zoOlZ.jpg)

<center style="font-size:14px;color:#C0C0C0">与小朋友前来参与纪念活动的市民</center>



![j27Tivp](/img/j27Tivp.jpg)

<center style="font-size:14px;color:#C0C0C0">悼念的市民</center>



![PulvHlT](/img/PulvHlT.jpg)

<center style="font-size:14px;color:#C0C0C0">市民举起烛光，纪念死难者</center>



![4LUyguZ](/img/4LUyguZ.jpg)

<center style="font-size:14px;color:#C0C0C0">市民举起烛光，纪念死难者</center>



高登新闻记者胡家辉，在维园门口举起1989年6月6日的《大公报》头版，印有“全国掀起抗议镇压怒潮”的标题。他称《大公报》30年来立场翻转，从支持悼念六四到批评邹幸彤“为乱港分子辩护”，反映香港的“白色恐怖”已经来临。

“我想看32年前的《大公报》和今天的大公报究竟在说什么。我怕过两个月后，谈论六四就已经是违反国安法。”胡家辉说。

“如果下一个国安法（禁止的）是公开讨论六四，那我就进国安法里了，”他说。“其实做人是做自己，在有能力的时候，做自己认为应该做的事情就好。”



![E94lIt0](/img/E94lIt0.jpg)

<center style="font-size:14px;color:#C0C0C0">高登新闻记者胡家辉向民众展示《大公报》1989年对六四的报道</center>



![Wah1ivI](/img/Wah1ivI.jpg)

<center style="font-size:14px;color:#C0C0C0">维园侧面墙上的喷的标语，“不要让64成为禁词”</center>



65岁的王凤瑶，因在反修例运动中举着标志性的小黄伞冲在第一线，被人们尊称为“王婆婆”。今年六四，她在维园和时代广场继续撑伞，声援天安门母亲，纪念刘晓波和遇难学生。

5月30日，她独自一人来到中联办门口声援六四，因“非法集结”而被[逮捕](https://www.hk01.com/社會新聞/631637/六四32-遊行遭禁-王婆婆攜黃傘示威標語獨自現身-警總附近被捕)，两天后被释放。

“我没法担心自己的安全，没法担心自己的身体。那么多死在广场，死在狱中的人，我怎么能忘记？”王凤瑶说。“谁不想安度晚年啊？但是我在家待着，我心就不安。”



![mFTze7I](/img/mFTze7I.jpg)

<center style="font-size:14px;color:#C0C0C0">王婆婆在时代广场悼念六四</center>



19岁的邓嘉欣（化名）在初中时参加雨伞运动，在高中时参与反送中，每年都参加六四烛光悼念集会。她和男友身穿黑衣，手持电子蜡烛，故意大摇大摆地在一群警察面前走过。

她不赞成本土派抵制六四集会。“一些香港年轻人认为自己不是中国人，大多因为厌恶中国政权，但现在打压香港社运的政权，不就是32年前在天安门镇压学生的政权吗？”她说。

“把坦克开上天安门，镇压学生，是人类的罪行，无论你是香港人还是中国人，这个罪行都不会改变，”她说。“如果你支持民主与人权，就应该支持纪念六四。”

她提到，大陆的言论审查仍然存在，因此“香港人在还能发声的时候，就应该发声。”她说，“了解历史，传播真相，是我们的责任。我们有责任坚守和保存这份历史记忆，然后站出来告诉香港和大陆的年轻人，中国政府犯了什么罪。”



![Ks5ELEn](/img/Ks5ELEn.jpg)

<center style="font-size:14px;color:#C0C0C0">手持烛光、参与纪念活动的市民</center>



![AoXd3HK](/img/AoXd3HK.jpg)

<center style="font-size:14px;color:#C0C0C0">一个女孩在维园门口阅读题为《抗争》的宣传册</center>



36岁的陈小姐这十年来都会前来参加烛光悼念。今年六四，当她怀抱白花，徐徐走向维园大门，迎着警察把花放在栏杆上时，警察呵斥她立刻把花拿走。争执中，她拿出手机给栏杆上的白花拍了照，然后在记者簇拥下抱花离开。

“一束花都容纳不了的香港，还有言论自由吗？我只想悼念在天安门死去的年轻人，我没有违反法律，难道我连悼念的权利也没有了吗？”她说。

“六四就是我一辈子不会原谅的事。我是香港人，香港人不会忘记。”她说。



![hQuzCSD](/img/hQuzCSD.jpg)

<center style="font-size:14px;color:#C0C0C0">陈小姐将白花放在维园栏杆上，被警察制止</center>



![9vxBXeT](/img/9vxBXeT.jpg)

<center style="font-size:14px;color:#C0C0C0">陈小姐将白花放在维园栏杆上，被警察制止</center>



支联会筹办六四悼念集会，32年风雨无改，但无人知晓第33年该如何纪念。去年警方首次反对集会却默许民众入园悼念，然而今年已是上千警力清场封锁维园，自由空间步步坍缩。而支联会常委平均年龄超过50岁，三分之二身负刑事指控，以年轻化为目标而成立的“支青组”运营失败，学联退出，交棒下一代的历史传承前路未明。

今年六四前，邹幸彤在Facebook上发表[声明](https://www.facebook.com/505789300/posts/10159843428114301/?d=n)，愿以个人名义遵守这份32年的约定，点起烛光。但六四当天早上7点，警方在家中将她逮捕。

“尽管打压是那么严重，大家还没有放弃，”逮捕前，她在[自由亚洲电台](https://twitter.com/RFA_Chinese/status/1400712722623328256?s=20)说。

支联会每年选一次常委，选举前，候选人需要提交一份[政纲](https://beta.thestandnews.com/politics/六四-32-周年-最後的信仰-危難中堅持的支聯會-14-常委)。现任老一辈常委初次当选时，尚在90年代，政纲有“推动支联会五大纲领”、培养香港和大陆的“民气”、发展民主教育等等。

**2020年，他们选举的纲领都用了一个词：坚持。**



![LR5CFYs](/img/LR5CFYs.jpg)

<center style="font-size:14px;color:#C0C0C0">身穿黑衣、与小朋友前往集会现场的市民</center>



![wpkHrIy](/img/wpkHrIy.jpg)

<center style="font-size:14px;color:#C0C0C0">市民黄小姐怀抱白花，于维园门口悼念六四，鞠躬默哀</center>



![S8ygfxb](/img/S8ygfxb.jpg)

<center style="font-size:14px;color:#C0C0C0">市民手举今年警方反对六四集会相关报道</center>



![8CoLsdl](/img/8CoLsdl.jpg)

<center style="font-size:14px;color:#C0C0C0">陈先生，六月联合成员，在维园门前举起一把吉他，吉他盒贴着标语“不要到维园点起蜡烛”</center>



![PnQmQus](/img/PnQmQus.jpg)

<center style="font-size:14px;color:#C0C0C0">陈先生，六月联合成员，在维园门前举起一把吉他，吉他盒贴着标语“不要到维园点起蜡烛”</center>



![DjxMsm7](/img/DjxMsm7.jpg)

<center style="font-size:14px;color:#C0C0C0">陈先生，六月联合成员，在维园门前举起一把吉他，吉他盒贴着标语“不要到维园点起蜡烛”</center>



![ESe0euG](/img/ESe0euG.jpg)

<center style="font-size:14px;color:#C0C0C0">以打火机火焰代替烛光纪念死难者的市民”</center>



![HfPLG7A](/img/HfPLG7A.jpg)

<center style="font-size:14px;color:#C0C0C0">使用手机拍摄在场戒备警察的与会市民</center>



![teTRMID](/img/teTRMID.jpg)

<center style="font-size:14px;color:#C0C0C0">被遗留在栏杆上的电子烛光</center>