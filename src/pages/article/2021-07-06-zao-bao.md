---
templateKey: article
title: 7.6 N记早报
slug: zao-bao
date: 2021-07-06T07:40:55Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: Fukaya

---
**1. 光城者涉嫌发动炸弹袭击，国安处拘九人包括中学生**

据[众新闻](https://reurl.cc/6abKgk)，7 月 5 日，香港警方以香港国安法第 24 条“串谋策划使用炸弹、意图造成社会危害活动罪”拘捕 5 男 4 女，年龄介乎 15 至 39 岁，其中 6 名为中学生。据[香港 01 报道](https://reurl.cc/9rb3kV)，警务处国家安全处高级警司李桂华指，被捕人士属港独组织“光城者”，涉嫌制造土制炸药 TATP，于海底隧道、铁路、法庭及垃圾桶等地发动炸弹袭击，意图造成人命伤亡。李形容此为“由港独组织策划的一个恐怖袭击”。

7 月 6 日，行政长官林郑月娥出席行政会议前见记者表示，这些行动并非偶然，证明“黑色暴力”从群众变成隐蔽式，执法部门将针对网上有人煽惑他人杀警作相关调查。

**2. 海航集团两万余名员工集体上书举报董事长，请求中纪委彻查**

据[向爷杂谈](https://mp.weixin.qq.com/s/JvqJqIGrkyyOI7X5LIZElQ)，6 月 30 日，海航集团两万余名员工集体上书举报董事长，其中列出多项罪名，包括“暗箱操作私自兑付集资款、贪心妄想把海航变为家族企业、利用职权拉帮结派中饱私囊”等。据称，2018 年至 2020 年期间，时任海航集团主要负责人的陈峰，利用手中职权私自动用近百亿资金，优先给自己、亲朋好友以及部分嫡系集团高管购买的集资产品进行了本金及其利息的兑付。其子陈晓峰在未经集团任何合法合规手续的情况下“空降”海航董事局，引发员工不满。

[综合新闻网](https://udn.com/news/story/7333/5580529)在报道中指出，海南航空控股股份有限公司为中国大陆第一家 A 股和 B 股同时上市的航空公司，也是中国大陆第四大航空公司。 今年六月，路透社曾援引匿名消息报道，海航集团申报了总计人民币 1.2 万亿元的债权，是中国大陆最大的破产案。

**3. 日本副首相称中国武力犯台就行使“集体自卫权”，防卫副大臣转推**

据[ETtoday 新闻云](https://www.ettoday.net/news/20210705/2023497.htm)，7 月 5 日，日本副首相麻生太郎在东京演讲时表示，如果中国入侵台湾，将承认其受到符合法律规范的“生存危机”，并行使有限度的“集体自卫权”。相关新闻也被防卫副大臣中山泰秀，转贴到推特上。

据[时事通信社](https://reurl.cc/GmGndd)，“生存危机”指的是其他与日本密切关系的国家，明显受到攻击，或有受到攻击危险时的情况，导致日本的生存受到威胁。当台湾发生大问题时，日本有卷入生存危机的可能性，即构成行使“集体自卫权”的条件之一。麻生太郎称，这一情况下，日本就必须和美国共同“防卫台湾”。

**4. 以色列大批辉瑞疫苗月底到期，探讨将疫苗捐赠他国**

据[路透社 7 月 4 日讯](https://reurl.cc/2rl6VE)，以色列正在与其他国家商谈一项协议，以赠送其多余的辉瑞疫苗。据悉，这些疫苗的剂量将在本月底到期。以色列卫生部总干事赫兹·列维在接受 103 FM 电台采访时表示，任何交易都必须获得辉瑞的批准。他并未明确指出交易计量，以色列《国土报》认为，剂量的数量约为 100 万。上月，巴勒斯坦人拒绝了以色列约 100 万剂冠病疫苗，并指疫苗离过期日太近。

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
