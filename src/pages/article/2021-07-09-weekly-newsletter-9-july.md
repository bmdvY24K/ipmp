---
templateKey: article
title: July 9 - This Week China Society
slug: weekly-newsletter-9-july
date: 2021-07-09T21:21:28.132Z
description: Weekly Briefing (Week 5-9 July)
featuredpost: true
trending: 5
isbrief: false
columnist: '英文周报Weekly Newsletter'
contributors:
  - type: Author
    name: 小戌
  - type: Editor
    name: Anonymous

---



Happy weekend. This is NGOCN’s first weekly newsletter. We’re covering China’s massive crackdown on student LGBT organizations, nine arrested in Hong Kong over alleged terrorist plots to bomb streets, and other news that happened in Chinese society.

 

## Dozens of student LGBT organizations’ accounts shut down on social media 

 

The WeChat accounts of about 20 student LGBT organizations across China were [permanently shut down](https://www.chinalgbt.org/banned-voices) on July 6, showing a new wave of repression on gender diversity and civil association.

 

The name of those accounts was changed to “unnamed account,” with notifications saying “After receiving relevant complaints, this account has violated the regulation on the management of online public accounts. This account has been put out of service, all content has been blocked.”

 

The targeted accounts included those from top universities, such as Peking University ColorsWorld, Tsinghua University Wudaokou Purple and Fudan University Zhihe Society.

 

In the past, these organizations actively advocated for gender diversity and gender equality, holding Pride Month activities, providing sex education and supporting the #MeToo movement.

 

The sweeping crackdown, following the [cancellation ](https://supchina.com/2020/08/13/the-end-of-shanghaipride-organizers-cancel-all-upcoming-activities/)of Shanghai Pride 2021, China’s biggest LGBT festival, showed the increasing challenges for the LGBT community.

 

Despite China decriminalized homosexuality in 1997 and removed it from its official list of mental disorders in 2001, same-sex marriage is illegal and authorities banned presenting homosexuality on media in 2016. 

 

## 9 arrested in Hong Kong over alleged terrorist plot to bomb streets, courts and public transportation 

 

Hong Kong’s national security police have [arrested](https://www.scmp.com/news/hong-kong/law-and-crime/article/3139977/national-security-law-hong-kong-police-arrest-9-over) nine people on July 5, including six high school students, over an alleged terrorist plot to bomb streets, courts and tunnels in the coming week.

 

The police said they seized trace amounts of explosives, two bottles of liquid chemicals and laboratory equipments required to produce triacetone triperoxide, a highly unstable and powerful explosive known as TATP.

 

The suspects included four boys and two girls aged between 15 and 18, recruited by “Returning Valiant”, a group that promoted Hong Kong independence on its website.

 

[The New York Times](https://www.nytimes.com/2021/07/06/world/asia/hong-kong-bomb-violence.html) analyzed that “the case reopened an uncomfortable debate within the pro-democracy movement: whether it condones or supports violence, in the name of the underdog fight against the powerful Chinese government.”

 

It also further divided Hong Kong, as The Times analyzed that authorities underscored the necessity of imposing the National Security Law, while democracy activists have accused the government of creating an environment which left residents radicalized.

 


## MEANWHILE

 

Zhou Xiaoxuan, known as “Xianzi”, the pioneer #Metoo activist who sued famous state media host Zhu Jun for sexual assault, found her social media Weibo account [suspended for one year](https://m.weibo.cn/status/4656392751026773) until July 8, 2022. Previously, her account was deactivated for 15 days after publishing a post about the second hearing of her trial.

 

Zhang Tao, the party secretary and chairman of China Aerospace Investment Holdings, [brutally attacked](https://weibo.com/ttarticle/p/show?id=2309404654941200580963) two prominent scientists after they refused his request to be recommended for the membership of the International Academy of Astronautics. Wu Meirong, 85, suffered a fractured spine, while 55-year-old Wang Jinnian had several broken ribs and soft tissue injuries all over his body.

 

Two Hong Kong high school students were [punished](https://www.scmp.com/news/hong-kong/education/article/3140306/hong-kong-pupils-punished-singing-dear-jane-hit-which) for performing the song “Galactic Repairman.” According to the school, the lyrics of this Canton-pop hit contained “politically sensitive information,” such as “in this terrible situation, we can only confront it”, and “repairing this world in turmoil”.

 

That’s the newsletter for this week. See you next time. 

 

 

 

 

 

 