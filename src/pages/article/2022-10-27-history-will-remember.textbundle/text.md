---
templateKey: article
title: '历史会记住这一夜(文字+海报）'
slug: 'history-will-remember'
date: 2022-11-27T03:21:57.265Z
description: '这一个个看似微弱又十分勇敢的行动，都为自己争取到了一分获得自由的机会.不自由毋宁死!'
featuredpost: true
featuredimage: /img/wufeng.jpg
trending: 10
isbrief: false
contributors:
  - type: '作者'
    name: '悼念乌鲁木齐火灾逝者'
  - type: '设计'  
    name: '不自由毋宁死'
tags:
  - "乌鲁木齐"
  - "抗议"
  - "学生"
  - "民众"
---

今夜，
<br>无论你是站在上海的乌鲁木齐中路还是其他地方手举白纸；

<br>冲着对面的警察或者保安表达诉求，高叫口号；

<br>还是在校园或者社区的某个角落涂鸦，贴海报；

<br>站在认识或者不认识的人身边，拉着他们不被警察带走；

<br>还是把宿舍门口堵住，拒绝被转运的命运；

<br>自行推开小区大门，为自己解封；

<br>或是播放一首你认为需要在此刻被播出的歌曲；

<br>在全球各地手持蜡烛或者标语悼念乌鲁木齐火灾逝者；

<br>……

<br>这一个个看似微弱又十分勇敢的行动，
<br>都为自己争取到了一分获得自由的机会；
<br>不自由毋宁死！

> <br>深切悼念乌鲁木齐火灾逝者
> <br>声援所有在行动中被捕的中国公民
> <br>我不是境外势力，我是中国公民

**深切悼念乌鲁木齐火灾逝者海报**

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu7.jpg)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu8.png)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu9.jpg)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu11.jpg)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu5.jpg)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu6.png)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu12.png)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/3.png)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu4.png)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu1.jpg)

![22](/Users/admin/Documents/GitHub/ipmp/static/img/wu2.jpg)

