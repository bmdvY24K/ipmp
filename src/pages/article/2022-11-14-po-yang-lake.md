---
templateKey: article
title: '鄱阳湖大旱：疏离与忧虑之下的民生图景'
slug: 'po-yang-lake'
date: 2022-11-14T03:21:57.265Z
description: '在极端天气显著的2022年，鄱阳湖的干旱是具有象征意义的事件，而干旱最直接的影响，还是那些世代依湖而居的人们的生活。'
featuredpost: true
featuredimage: /img/po-yang1.jpg
trending: 10
isbrief: false
contributors:
  - type: '作者'
    name: 'Dale(文/图)'
  - type: '编辑'  
    name: 'Echo'
tags:
  - "鄱阳湖"
  - "干旱"
  - "民生"
  - "极端天气"
---


*极端天气频发的2022年，曾经烟波浩渺的鄱阳湖提前100余天进入枯水期，湖底变“草海”，鱼类和鸟类死亡，土地干裂，触目惊心。 极旱背景下，当地民众的生活也受到影响，加之疫情之下的生活艰难，对未来的忧虑困扰着他们。请看来自一线的记录。*

![22](/Users/admin/Documents/GitHub/ipmp/static/img/po-yang1.jpg)
<center><font color=gray>鄱阳湖干裂的湖底</font></center>

9月中旬的一天，火车驶过鄱阳湖铁路特大桥，车厢里好几个乘客忽然站起来，举起手机向窗外拍摄。车窗外只有一眼看不到尽头的“泥地”，以及一些细小的水流。

“怎么湖都干了！”有乘客喊叫了起来。他是对的。这一片巨大的“泥地”，就是原本烟波浩渺的鄱阳湖湖底。

此刻列车经过的地方，就是鄱阳湖入长江口的上方。这条铁路特大桥全长超过五公里，横跨鄱阳湖——中国第一大淡水湖。在本应属于丰水期的8月，鄱阳湖的水体面积却一直在缩小，并提前进入枯水期。据财新传媒报道，到9月6日，鄱阳湖正式进入极枯水期，比平均时间提前115天。

在中国媒体上，鄱阳湖汛期反枯被简单归因于今年江西省的极端干旱天气。据江西省气象局消息，截至10月30日，当地重旱已持续111天。在6月至8月的夏季，高温持续有69日，39℃高温覆盖范围占全省的94%，是1961年以来高温持续时间最长的记录，与此同时，6月23日至9月6日，全省无雨日数达到60.6天，蒸发量是降水量的2.8倍。

在极端天气显著的2022年，鄱阳湖的干旱是具有象征意义的事件，而干旱最直接的影响，还是那些世代依湖而居的人们的生活。

### 湖中岛乡，芝麻也被旱死

“我母亲今年90多岁了，也从未见过像今年这样的干旱。”鄱阳县莲湖乡的农民汪贵生站在田埂上说。年过六旬的他戴着一顶草帽，正午阳光炙热，汗水随着草帽的绳子流下，他抬头看天，连云都没有，更别谈下雨了。

![22](/Users/admin/Documents/GitHub/ipmp/static/img/po-yang5.jpg)
<center><font color=gray>鄱阳湖区域的农民抽水浇灌田地</font></center>

莲湖乡是鄱阳湖中的一个岛乡，有超过六万亩的耕地，以种植水稻为主。在汪贵生的记忆里，这里一直是一个水气充足的好地方。

当汪贵生重复地说“这里土地很好，靠近鄱阳湖，引水方便”时，他脚边却是一排旱死的芝麻杆子。芝麻收割之后，只剩下一小节留在地上，可以清晰地看到，有的杆子是深褐色的，他说这些都是旱死的芝麻。至于尚能收成的芝麻，也长得不好，只有稀稀落落的三四片叶子。

汪贵生介绍，芝麻已经是非常耐旱的作物了，他种的棉花今年长得还没到平时一半的高度，花生则多是空苞。这些作物原本不需要格外灌溉，正常的年份，大约十天下一次雨，水分都够了，今年的干旱则是“从未有过”。

多名居住在湖边的居民说到，从未见过夏天的湖水水位降到这么低，甚至在往年的枯水期也是少见的。汪贵生记得，过去在湖边，一整年都能抓到鱼虾，现在湖水只能没到脚踝。

“从未有过”的大旱的另一面，是缺乏应对经验。在鄱阳湖一带，农户解决灌溉问题的方式，大多是用抽水机从水塘抽水，再喷洒到田地里，或者由政府组织挖引水渠。

在都昌县的一片菜地里，农妇段友珍说起，最近的“农活”便是抽水。她们每天早上在田地附近找水塘——有的水塘还会用于生活和养殖用水，都要协调，找到水源后，便是架机器、驳管子到田间，等到能抽水灌溉时，往往已经过了大半天。抽水机还不时有故障，有时一天光是换零件和维修，就要花去两三个小时。有时，水抽上来了，水管接驳处却断开，又必须关掉机器重新驳上，直到太阳快下山，抽起的水才撒到田地里。段友珍感叹：“一整天都在弄这机器。”

鄱阳湖周边一些县市主要从湖中取水，旱情也影响了部分居民的生活。一名在九江市参与旱情调研的志愿者则表示，真正的饮水困难，大多发生在没有通自来水的山区村庄。当地解决饮水困难的办法，一般就是开消防车供水，以及打水井，前者是应急操作，后者能相对长期地应对旱情。

“从未有过”的旱情也体现在数据上。据江西省防汛抗旱指挥部最新统计，截至9月29日下午4时，干旱灾害已造成全省481.4万人受灾，因旱需要生活救助的超20万人，因旱饮水困难需救助近1.88万人，农作物受灾面积达992.2万亩，绝收达114.3万亩。在应对方面，则投入了机电井7.43万眼、泵站2.27万处、机动设备42.62万台套、运水车辆2.15万辆次。

### 农民的困境：种地的成本已到了天花板

江西省是全国水稻重要产区，而鄱阳湖平原则是江西主要的农业生产区，长期以来，此处的农耕依托着鄱阳湖周边水土肥沃的优势。然而，今年鄱阳湖平原的农田却是另一番景象——随时可见枯死的玉米秆，水稻根部枯黄，未长出作物的土地都干裂了。

极端干旱也使得人们把目光聚焦到当地农业的影响上，然而在农户看来，农业其实一直都很困难，这个夏天只是雪上加霜。

“现在（种地）成本已经冲破天花板。”都昌县的种植大户范大伟抱怨道。

![22](/Users/admin/Documents/GitHub/ipmp/static/img/po-yang2.jpg)
<center><font color=gray>田地里干枯的玉米</font></center>

范大伟是都昌县本地人，原本在当地做家具生意，2010年他觉得国家越来越重视农业，这一领域有发展前景，便转型承包土地做农业。现在范大伟承包有800亩水稻田。8月以来，他像
很多农户一样，过上了每天开车“找水”的日子。做了12年农业，他从没遇到过这样的旱情，只一个劲地重复着“没有经验”、“没办法”。

到了9月，他已经彻底放弃“找水”，毕竟水稻预期收成并不好，抽水可能连油费都亏了。他打开县里种植户的微信群，看到地方在组织挖引水渠，他点开图片看了一会，觉得就算挖好，效果也有限。“高温蒸发太快了，往年水库放一次水，能保十天半个月，现在三天就蒸发完了。”范大伟说。

范大伟估计，今年必定是亏本的一年，不仅因为旱情导致收成差，还有成本徒然上涨。他试着计算了种植的成本，包括化肥、农药、机器油费、工人劳务全都在上涨，例如今年汽油费用涨到了4000元每吨，平均下来，一亩田至少要150斤汽油；工人的劳务也涨到一两百元每天。

可是，水稻的收购价格却并未跟随成本上涨，范大伟透露，最近两年他卖出稻米的价格都比2016年低，种植成本却是翻倍。目前国内稻谷主产区实行最低收购价政策，每年都会制定出稻米的最低收购价格，在范大伟提到的2016年，则针对最低收购价格进行过下调，2020年以来虽然持续在上调，但目前的最低收购价格仍然低于2016年。

### 两年之内，已历两次大涝大旱

问及今年的干旱是否会影响日后的种植计划，甚至再次转行，范大伟首先想到的不是反复的旱涝，而是种植成本，比起这种极端天气，他更在意的是种水稻的投入产出比。

![22](/Users/admin/Documents/GitHub/ipmp/static/img/po-yang6.jpg)
<center><font color=gray>穿越鄱阳湖的铁路大桥</font></center>

干旱固然令农户受损，可是包括范大伟在内，多数农户会把今年“从未有过”的干旱看作少数极端事件，认为以后未必会再发生。相比之下，化肥价格、油价和人工的上涨，则是能明确预期的，并且成本似乎将继续上涨。

莲湖乡的农民汪贵生在网络上看到过“气候变暖”的说法。极端天气的反复也实际地影响着他的生活，仅仅在两年前，江西发大洪水，水位超过98年的历史高度，莲湖乡大面积绝收，汪贵生的田地也全被淹了。短短两年里，汪贵生便经历过两次极端的涝旱，然而他并不把“气候变暖”作为一个新危机来理解，在他看来，农民从来就是“看天吃饭”的，当“天”在变时，人是没办法、难以调整的。

回忆起两年前的大洪水，汪贵生觉得遇到这种情况就彻底没有办法了，只能靠政府补助和储蓄度日。事实上，靠农活养家的模式也已经不可行了。

值得注意的是，早在“天”变化之前，人就变化了，务农早已不是鄱阳湖周边村民的首选项。汪贵生说，在湖边的村庄里，只能看到老人和小孩，青壮年都出去打工了，种地的也不是小户农民，而是承包大户。这种变化既缘于整个社会经济模式的变化，也与务农成本和风险高，收益少有关。

“现在越来越不指望靠天吃饭，靠天吃饭已经不太可能了，收成好就收了多一点，就算不好，我们有打工的收入，也不会影响多大。对于小农民来说，继续靠天吃饭已经不太可能，大家赚钱都是靠往外打工。”汪贵生说道，他自家的三个孩子都在沿海地区打工。

### 禁渔之后：人与湖的疏离

如同汪贵生所说的，过去“靠天吃饭”、“靠水吃水”的生活方式，在过去二十多年里已经被改变了。它带来的一个结果，则是本地人与湖泊的关系变得疏离。

在九江市一个渔村里，60多岁的罗萍笑着说：“现在又不捉鱼，又不从（鄱阳）湖里取水，都用自来水了，干了也没有影响。”罗萍的家紧邻鄱阳湖，在三楼就能看见湖——现在湖水全干了，河床长出了大片的草。

![22](/Users/admin/Documents/GitHub/ipmp/static/img/po-yang4.jpg)
<center><font color=gray>湖水干涸之后，有船搁置在“湖”中</font></center>

鄱阳湖过早进入枯水期，并没有使得自出生以来便没离开过湖边的罗萍苦恼。去年1月开始，鄱阳湖基于鱼类保育的原因，实行10年禁渔，当地渔民也改变了延续数十年的谋生方式。

罗萍原本也是一名渔民，几年前先于禁渔政策退休了。她回忆起从前在湖里捕鱼的日子，初期大家都会摆“迷魂阵”，“迷魂阵”把大鱼小鱼一并捞走，不利于渔业资源的持续性，后来“迷魂阵”被禁了，但捕鱼船越来越多，鱼类仍在减少，捕鱼也变得难了，渔民要把船开到更远的地方。

现在邻湖的老房子只有罗萍和老伴带着孙子们住，她的两个儿子都在九江市区工作，并且买了新房子，对于罗萍一家来说，“靠水吃水”的日子已经彻底过去了，现在扭开水龙头便有自来水，湖水干枯也“没有影响”。

在都昌县的另一个渔村，转型成鱼类养殖户的洪韬表示，禁渔之后，青壮年的渔民大部分都到外地打工了，虽然政府鼓励渔民转型做养殖，但是养殖要投入成本，也有风险，外出务工才是更多普通渔民的选择。

这种生活方式的转变，对农民来说则来得更早。《鄱阳湖地区耕地变化及其驱动力研究》一文统计得出，1999年至2002年，鄱阳湖耕地迅速减少，与此同时，当地开始招商引资，第二、第三产业迅速发展， 许多人外出务工，种地的积极性有所下降，出现耕地被撂荒的现象。另一篇期刊论文《2001—2020年鄱阳湖平原耕地复种时空变化研究》则指出，工业化和城市化进程加快，鄱阳湖地区已成为城市发展与粮食生产竞争的典型区域，另外由于农业成本上升、农业比较效益下降，使农户种粮积极性下降，种植结构发生较大改变。

###  “下一次的极端干旱会在什么时候？”

正如一名曾在九江市经营生态农业的商人表示的：在大的鄱阳湖区域，田地撂荒现象非常普遍。主要原因是农民种地难以赚钱，而且土地形态破碎，难以机械化作业，人力成本相对高，对于农业种植商人来说，也不好持续。都昌县的种植大户杨海则表示，在2018年前后，村里的田地大多撂荒，后来村委以优惠政策吸纳了外省种植户入驻，杨海则是跟外省的种植户学习后，在2020年承包起2000亩地，开始做种植。

近十年以来，基于保障农业和粮食产量的目的，地方会有鼓励农业的政策或举措，这种模式会吸引到像杨海、范大伟这类原本并不是农户出身，但有兴趣参与农业经营的种植户。

![22](/Users/admin/Documents/GitHub/ipmp/static/img/po-yang3.jpg)
<center><font color=gray>干涸的土地</font></center>

在农民汪贵生看来，现在做农业的绝大部分都是这类承包的种植户，因为规模化生产才能有收益，至于像他这样的小农，几乎已经绝迹了。汪贵生自成年起便在生产队里做农活，到后来包产到户，也一直都在种地，早些年是种水稻，近年则以种芝麻和棉花为主。

其实现在汪贵生种的东西已经不再出售，种地也只是延续一种生活习惯。另外，就是他舍不得看着土地撂荒。“自己种的东西，吃着就更香。像芝麻炒了蘸来吃，或者做成芝麻糖。”他拍拍手里的芝麻粒说。

这种人与湖水、土地关系的疏离，在城市里则更为明显。鄱阳湖位于九江市濂溪大道的一则，只需要驾车10分钟，周边的景象已经从干枯的玉米杆变成高楼，商铺招牌都是花花绿绿的灯饰，干旱预警并不会影响经营，一个商户店主说：“说实话，（干旱）真的没什么感觉。”网络上，鄱阳湖有了新名称——草海——湖水干枯后，湖底长出了茂密的草，它成了社交媒体里的新晋打卡点，甚至连官方宣传都以草海为主题。

10月中旬，在降雨和调水后，鄱阳湖的水位稍有回升，江西的旱应急响应也调整降级。这场在人们口中从未遇过的干旱，似乎终于到了尾声。但下一次，这种极端的干旱还会不会再次发生？

在政府机关的留言板上，有一则8月的江西赣州的村民留言，表示其所在村严重缺水，全组只有两三口水井，留言写着：“看趋势以后也有可能会很旱，虽然政府出钱打过水井，但是大旱年解决不了问题，我家甚至打过一口一百多米的基井也是没水……”

（应受访者要求，文中使用了化名）
