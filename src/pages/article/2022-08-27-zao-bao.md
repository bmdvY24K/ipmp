---
templateKey: article
title: 8.27 N记早报
slug: zao-bao
date: 2022-08-26T17:19:11Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: 小咚

---
**1. 江西鹰潭贵溪防疫静默期超过半个月，防疫政策遭到质疑**

根据[环球日报](https://china.huanqiu.com/article/49IyOoa41DV)消息，江西鹰潭贵溪市自 8 月 9 日开始出现新冠阳性病例并不断扩散。根据[网友呈现贵溪疫情防控指挥部发布的公告](https://ishare.ifeng.com/c/s/v004BvTeTQDhzytpNG2G-_ytB1YIdOPyu3ow2DexFC5BThW4__?spss = np&channelId =&aman = 54Ub50u271i770Gac5T98ew194E074fcaewb66Q0e5&gud = 3b260A107z019H50080007004)，贵溪城区自 8 月 10 日开始静默，至 8 月 14 日，贵溪全市开始实施 3 天静默管理。当地防疫指挥部于 8 月 18 日、20 日发公告不断延长静默时间，至今仍未解封。

尽管已经静默超过半个月，在 8 月 25 日[江西日报的数据通报中](https://weibo.com/1806128454/M2Nugjtkl?refer_flag = 1001030103_)，鹰潭贵溪仍新增 20 例无症状感染。根据[凤凰网于 8 月 25 日的报道](https://weibo.com/2060112437/M2J6A0llD?refer_flag = 1001030103_)，鹰潭贵溪方舱中目前有上百人居住其中，但方舱条件极为糟糕，从当地居民上传视频中可看出，大量民众只能用木板及塑料袋打地铺，垃圾堆积，卫生状况堪忧。

另外，根据[江西鹰潭信江公安于 8 月 23 日发布的公告](https://ishare.ifeng.com/c/s/v004BvTeTQDhzytpNG2G-_ytB1YIdOPyu3ow2DexFC5BThW4__?spss = np&channelId =&aman = 54Ub50u271i770Gac5T98ew194E074fcaewb66Q0e5&gud = 3b260A107z019H50080007004)，8 月 16 日，当地一老人去世，其女婿、外孙等四人分批以买菜为名出小区参加老人葬礼，于返程时被工作人员拦下并报警。信江公安分局认为其“拒不执行人民政府在紧急状态情况下依法发布的决定、命令”，对四人进行行政处罚，引发舆论关注。

**2. 中国女排戴口罩出赛引发争议，中国排协致歉**

据[自由时报](https://news.ltn.com.tw/news/world/breakingnews/4037832)报道，中国女排于当地时间 8 月 21 日至 29 日赴菲律宾出战亚洲杯女子排球赛。在 8 月 25 日下午 4 时对战伊朗的小组赛上，中国女排全员戴口罩上场，引发对科学防疫、运动员健康的讨论。26 日凌晨，[中国排协于微博作出回应](https://weibo.com/1806128454/M2N0F0sfI?refer_flag = 1001030103_)称，戴口罩上场是因为部分参赛队伍有感染状况，而排协因临场经验不足，未能及时提醒运动员摘掉口罩。

**3. 日本宣布将大幅度重启核电厂**

根据[卫报](https://www.theguardian.com/world/2022/aug/25/japan-eyes-return-to-nuclear-power-more-than-a-decade-after-fukushima-disaster)消息，日本首相岸田文雄于当地时间 8 月 24 日表示，日本将重启闲置的核电厂，指示兴建新一代核电厂并延长现有反应炉运作年限，以改善能源结构。这一举动被认为是 2011 年 3 月 11 日福岛第一核电站事故后，日本核能政策的大幅度转向。据[NikkeiAsia 统计数据](https://asia.nikkei.com/Politics/Japan-PM-Kishida-orders-new-nuclear-power-plant-construction)，日本全国有 33 座核反应炉，目前仅有 6 座在运作当中，而电力公司申请重启的有 25 座，目前有 17 座已经通过原子能规划委员会审查，10 座已经经过地方政府同意。

**4. 越南卫生部宣告 LGBT 不是疾病，被视为越南 LGBT 平权运动的一大胜利**

当地时间 8 月 22 日，据[半岛电视台报道](https://www.aljazeera.com/news/2022/8/22/vietnam-says-homosexuality-not-a-disease-in-major-win-for-lgbtq?utm_source = substack&utm_medium = email)，越南卫生部于本月发布官方公告，引用 WHO 将同性恋除病化的决定，称“LGBT 不是疾病”，并公开谴责曾在越南广泛用于扭转性向的“转换疗法”，被认为是越南同婚合法化运动进程的重大成果。目前，越南 LGBT 组织联合发起的同婚合法化“我同意”（T ô i Đồ ng Ý）运动已经收到超过 100 万人的联署请愿。

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
