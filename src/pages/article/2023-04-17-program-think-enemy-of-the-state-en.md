---
templateKey: article
title: 'From Ruan Xiaohuan to "Program Thinking": How Did a Regular Citizen and a "Hacker" Become "Enemy of the State"?'
slug: 'program-think-enemy-of-the-state-en'
date: 2023-04-17T03:21:57.265Z
description: 'Now age 45, in his wifes eyes, Ruan Xiaohuan is an idealist and a genius in technology who loves reading. He is an ordinary person who cares about social justice. How did he become an "enemy of the state" step-by-step for simply posting online, and recently received a heavy sentence of 7 years of imprisonment?'
featuredpost: true
featuredimage: /img/bc-9.png
trending: 10
isbrief: false
contributors: 
  - type: 'Translator'
    name: 'Cassandra (Whats up Beijing), ChatGPT 4.0'
tags:
  - "阮晓寰"
  - "编程随想"
  - "公民"
  - "极客"
  - "国家敌人"
  - "理想主义者"
---

Special thanks to [What's Up Beijing](https://www.instagram.com/whatsup_beijing/) for the translation and proofreading. [The Chinese version is here](https://ngocn2.org/article/2023-03-29-program-think-enemy-of-the-state/), please help us to share it.

***Now age 45, in his wife’s eyes, Ruan Xiaohuan is an idealist and a genius in technology who loves reading. He is an ordinary person who cares about social justice. How did he become an “enemy of the state” step-by-step for simply posting online, and recently received a heavy sentence of 7 years of imprisonment?***

4497 days, 712 blog posts. From its creation on January 15th, 2009, to the last post on May 9th, 2021—this is the remarkable journey of the Chinese blog "Program Thinking", and also the "evidence" of the recent 7-year imprisonment sentence against its operator, Ruan Xiaohuan.

Since the last post on the blog, "Program Thinking," almost 2 years have passed, and the name has disappeared from the public. Previously, this blog was well-known among internet users for providing guides on circumventing the Great Firewall (GFW) and commenting on current events. Recently, however, it has gained attention again due to a WeChat group chat screenshot circulating online. The record shows that Mrs.Bei, the wife of Ruan Xiaohuan, sought help in a neighborhood WeChat group, revealing that she was pressured by investigators when she appealed for a lawyer for her husband. She also disclosed her husband's identity as the operator of "Program Thinking" and requested her neighbors to be concerned about her safety.

This was the first time that "Program Thinking" became linked to the name "Ruan Xiaohuan." Before this, despite rumors of "Program Thinking" being arrested, nobody knew the real name of this "mysterious geek" who shared basic political knowledge with Chinese internet users.

Ruan Xiaohuan, who is 45 years old this year, was taken from his home in Shanghai by the police on May 10th, 2021. He was detained under criminal charges the following day and arrested on June 17th of the same year. In October, he was prosecuted at Shanghai No. 2 Intermediate People's Court, and the case was filed for trial on October 13th.

On February 10, 2023, Ruan Xiaohuan was sentenced to 7 years in prison for "inciting subversion of state power," deprived of political rights for 2 years, and fined 20,000 yuan (about 2900 USD). With the publication of the first instance verdict, this case of being convicted for words finally came to light and has attracted enormous attention. Many people overseas have joined protests against Ruan Xiaohuan's heavy sentence and expressed their support, praising him as a "hero who fights against totalitarianism with technology."

"I never believed that his actions constituted the crime of inciting subversion of state power," said Mrs. Bei, the wife of Ruan Xiaohuan, in an interview with NGOCN. According to her, after being arrested, Ruan Xiaohuan admitted that he had written the blog posts, and he intended to "make the country better." Previously, she had defended her husband in a WeChat group chat with neighbors: "He was the chief engineer of the information security system for the Beijing Olympics in 2008, and he has made contributions to the country." "He is patriotic," she said. 


![22](/Users/admin/Documents/GitHub/ipmp/static/img/bc-6.png)
<center><font color=gray>In defense of her husband, Mrs. Bei posted a certificate of award, which proves Mr. Ruan’s participation in the information security project for the 2008 Beijing Olympics, in a WeChat group chat with neighbors. Image from the Internet.</font></center>

## The Sudden Arrest

On May 10th, 2021, around noon, in Yangpu District, Shanghai, Mrs. Bei’s doorbell rang. 

She thought it was the water delivery worker they had called earlier in the morning, so she didn't think much of it and told her husband, Ruan Xiaohuan, who was in the study, to answer the door. After a while, she heard someone outside the door saying "Don't move" and some sounds of scuffling. She rushed to the door, only to find it open and her husband nowhere to be seen, while a man with a thin figure had broken into her home.

"Are you committing burglary?" Mrs. Bei questioned the uninvited guest with an unclear identity standing in front of her.

The leading man pulled out his ID, indicating that he was a police officer handling the case. Then, nearly ten people came in, some in police uniforms and some in plain clothes. After entering the house, they carefully searched the study (where Ruan Xiaohuan usually worked) for a long time. "[They searched so thoroughly that they] almost turned it upside down." Then, they searched the rooms one by one.

There were about ten people, and three or four of them were watching me. They blocked me out of the room when they were searching. Two or three were in the living room, three or four were in the study, and they took turns," Mrs. Bei said.

During the search, the officers frequently talked to Mrs. Bei for more information and said that the articles published by Ruan Xiaohuan on overseas platforms "incited subversion of state power"; that the case was severe; that Ruan Xiaohuan would be sentenced to more than five years. The search lasted until around 2 or 3 a.m. the next day. Ms. Bei was taken to the Yangpu District Public Security Bureau in Shanghai for interrogation and was told that her husband was also being interrogated in the adjacent room. It wasn't until around 7 a.m. that the police sent her back home in a police car.

Mrs. Bei described herself as " feeling blank" on the day her husband was arrested. Her phone was confiscated, and she was unable to seek help or search online to see if a home search like this was legal. She felt powerless. She recalled that when she questioned whether the police had a search warrant, they told her, "We have many people wearing police uniforms when handling the case. What are you worrying about?" The search warrant was not shown to Mrs. Bei until the third day. It was not until two weeks after Ruan Xiaohuan was arrested that she received the detention certificate.

Mrs. Bei said that the first month after her husband was arrested, her physical condition was extremely poor, and she often had trouble sleeping due to anxiety. At that time, she had no idea what her husband had specifically written to be charged with such a serious crime, nor did she understand why her husband, who had long been engaged in technical work and made huge contributions to the information security of the 2008 Beijing Olympics, would be charged with "inciting subversion of state power".

She recalled that a few months after her husband's arrest, a neighbor told her that the police had rented a room upstairs in their building months before the arrest to surveil them, and there was also personnel surveilling their residence from the opposite building. Although these were just words from the neighbor, they made her slowly recall some details. During that time [before the arrest], there was often a sound of leaking water coming from the upstairs toilet. But when she and her husband went upstairs to knock on the door and asked them to cooperate with the property inspection, they insisted on not opening the door. She also recalled that in the year or two before her husband's arrest, he often complained to her that the home network was too frequently disconnected. Since he often found network abnormalities, he became more and more cautious; sometimes when they went out for a walk together, he would say "Let's take a detour" and return to their residence to check for anything unusual. These "clues" were all connected after her husband's arrest.


![22](/Users/admin/Documents/GitHub/ipmp/static/img/bc-1.jpeg)
<center><font color=gray>On May 10, 2021, there was a conflict between Ruan Xiaohuan and the police who came to arrest him, and this is a picture of his broken glasses. Photo provided by Mrs. Bei.</font></center>

## Secret Trials

A pair of broken wire-rimmed glasses quietly lay at home. the glasses were damaged during the physical conflict between the police and Ruan Xiaohuan on May 10, 2021, the day he was arrested. "We will provide him with a new pair of glasses," the police said as they returned the broken glasses to her.

For nearly two years, from the day of his arrest until the public verdict on February 10, 2023, little was known about the situation of Ruan Xiaohuan, and his family has also kept it in the dark. Although the detention and arrest warrants stated that Ruan was suspected of "inciting subversion of state power," his wife, Mrs. Bei, didn't know for a long time that the blogger "Program Thinking", widely discussed on the internet, was her husband Ruan Xiaohuan. She had no idea what he had specifically written or done.

“We don't discuss politics much. I only remember him telling me about the 'June Fourth' incident when we were in college, but we didn't talk about it much afterward," she recalled.

In 1996, both of them went to the East China University of Science and Technology to study chemical engineering and became classmates. During their college days, they often walked along and had conversations at night together. In Mrs. Bei's view, Ren Xiaohuan knew almost everything from astronomy, geography, science and technology, and philosophy, to history. During their conversations, she was always amazed by his vast reading and the breadth of his knowledge.

After graduation, Ren Xiaohuan entered the cybersecurity industry, while Mrs. Bei joined a foreign company. They got married in 2004. After marriage, their increasingly busy work schedules gradually reduced the time they spent having deep conversations with each other. Mrs. Bei recalls that although Ren Xiaohuan would also bring up some widely-discussed cases or criticize some social issues, they did not discuss current affairs much. Moreover, accustomed to the relaxed environment of international companies, she had little experience with these issues. On top of this, she is not sensitive to politics herself, so she rarely talked about it with Ren Xiaohuan. She only vaguely remembers him saying "If you don't care about politics, politics will take care of you" when he mentioned the news.

According to the family's understanding of Ruan Xiaohuan, he did not express "extreme" views in his daily life, nor did he ever mention the "Program Thinking" blog. "We were all stunned when we heard the news (of his arrest)," said his mother. The family thought that he had only read online articles and expressed some opinions on sensitive topics and that he was arrested because the year 2021 coincided with the 100th anniversary of the founding of the CCP, and internet censorship had tightened. It was not until later that they realized the seriousness of the situation, which was why, in the beginning, they only hired a local lawyer in Shanghai to defend Ruan Xiaohuan.

From Mrs. Bei's recount, the involvement of lawyers did not go smoothly. The family hired three batches of lawyers, all of whom were local lawyers in Shanghai. However, the lawyers were requested by the relevant authorities to sign a "confidentiality agreement" and were not allowed to disclose the case to the family.

According to Mrs. Bei, during the investigation period, she asked the lawyers to meet with her husband, but the investigating authorities refused them, citing that such meetings would hinder the investigation. Despite the family's questioning of whether this violated relevant legislation and their repeated complaints to the petition office, it amounted to nothing. It was not until mid-August 2021, after repeated urging and pleading from the family, and the case was transferred to the prosecutor's office, that the lawyer finally met with Ruan on September 29. It was nearly four and a half months after his arrest. After the meeting, the lawyer informed Mrs. Bei that Ruan had pleaded guilty. When Mrs. Bei asked whether he had confessed to the facts or the charges, the lawyer was evasive. On October 1, 2021, Mrs. Bei wrote a letter to the prosecutor's office questioning the characterization of the case.

On October 13, 2021, the Shanghai Second Intermediate People's Court accepted the case for trial. In March 2022, due to COVID-19 control measures in Shanghai, the trial of the case was "suspended". By June and July 2022, as the lockdown in Shanghai gradually eased, the family was informed that the trial was still suspended. The old parents of Ren Xiaohuan (his father is 86 years old and his mother is 76 years old) had called the judge to urge a prompt resumption of the trial and to avoid being stuck in an endless suspension. The response they received was: "The pandemic has not passed yet... and there are many backlogged cases. It's not just your case, you need to be patient and wait."

“Does it mean that if the pandemic doesn't end, the case will always be in a suspended state?” Mrs. Bei felt puzzled and angry about this. Until December 2022, when the lockdown was lifted nationwide, the case had still not resumed trial.

On February 7, 2023, there was an unexpected turn in the case. The lawyer called Mrs. Bei and informed her that her husband's case would be publicly announced in three days and that family members were allowed to attend. However, absurdly, no one had informed the family that the case had resumed trial in the previous few months. Mrs. Bei vaguely knew that the trial had started around December 2021, but did not know how many times the trial had been held, nor did she know what procedures had caused the trial to be delayed and then "suspended" due to the pandemic, resulting in the first instance lasting for one year and four months.

Due to the sudden notice of the public trial and their age, Ruan Xiaohuan's parents in Fujian were unable to attend. On the day of the trial, only Mrs. Bei, Ren's wife, attended as a family member. There were also five or six police officers present in the audience. 

## "A computer enthusiast and book lover who is obsessed with technology", "a righteous child" in the eyes of his parents

Almost two years since the incident, the family of Ruan Xiaohuan has kept low [about the case]. According to Mrs. Bei, this was mainly because the family did not know about his identity as a blogger, nor did they know specifically what he had written, making it difficult to judge the situation of the case. At the same time, they also feared that speaking out publicly would offend the judicial department and be detrimental to the verdict.

Now, the vague language of the first instance judgment has deepened Mrs. Bei's doubts. In her opinion, the judgment concealed a lot of information. This includes the fact that the judge did not specify which articles inciting subversion of the state was written by Ruan Xiaohuan, and even his blog account was not mentioned.

This made her realize that she "must find out what they are trying to hide." She hopes to unravel the causes and consequences of the case. However, this wasn't easy for her. On the day her husband was arrested in 2021, her Huawei MateBook, iPhone, and other electronic devices were confiscated by the police, and she was asked to provide passwords for "inspection." Several days later, the police returned them to her and warned her: it is okay to watch videos on the MateBook, but it cannot be used to access "sensitive" websites. This made her suspect that her MateBook, phone, and other communication devices were under surveillance all the time; and there might even be listening devices in her home. To avoid surveillance, she had to go to internet cafés to access the internet.

Through the internet, Mrs. Bei finally confirmed that her husband was the "Program Thinking" blogger, who went missing in May 2021 and was reported by the media. She remembered that the police handling the case had told her that Ruan Xiaohuan had written more than 700 blog posts, "including more than 100 that incited subversion of the state power." She also recalled that her husband liked a movie called "V for Vendetta," in which a freedom fighter wearing mask rebels against a totalitarian government, and which "Program Thinking" admired and emulated. The coincidence of time, the number of posts, writing style, and values confirmed her husband's identity as the blogger. The process of searching for her husband's online identity also solved the mystery that had troubled Mrs. Bei for nearly two years, and she finally learned that her husband had this life besides his daily routine.

"I don't know which word to use (to describe what he has suffered), but I finally understand why he received such a long sentence," said Mrs. Bei. When she finally confirmed that her husband was "Program Thinking," she couldn't help but burst into tears in the Internet cafe.

In his wife's impression, Ruan Xiaohuan has always been a computer enthusiast who loves reading and “has no involvement with politics”.

From Mrs. Bei’s recollection, though he majored in chemical engineering in his undergraduate studies, Ruan Xiaohuan was passionate about computer technology and software development. When he was in college, he and his roommates pooled their money to buy a computer and took turns practicing on it. To have more time to study computers, he didn't hesitate to put aside his classes for his major and use the computer in the dorm, when his roommates were in class during the day. Because of his diligence and eagerness to learn, he became a legendary figure in his class with his excellent computer skills as early as his freshman year. Whenever his classmates had any questions on computer science they couldn't figure out, they would come to him for advice. He not only answered all of their questions but also organized a weekly Q&A session on Tuesday evenings to help them prepare for the computer proficiency test.

In the eyes of Mr. Ruan's parents, their son has been honest and willing to help since he was very young. "When he was in second or third grade, he would stand in the same spot where he found money on his way to school and wait for the owner to come and claim it. He only went to school because he was afraid of being late, and handed the money over to the teacher," his parents recall. Now, Mr. Ruan's father, who has retired as a university professor, is 85 years old already, and his mother is 76 years old. The elderly couple worries about their son's safety day and night.

In the eyes of his wife, it was precisely his honesty and eagerness to help, as well as the scholarly and studious traits Ruan Xiaohuan displayed during his college years, that attracted her and eventually made them a couple.

## He once dropped out of school and devoted himself to software development. After resigning, he believed that "volunteering and open source contribute more."

According to Mrs. Bei, during his university years, though self-taught in computer science, Ruan Xiaohuan's outstanding technical skills gained him the favor of professors in the computer science department. In his third year of college, he began developing programs for the school's software company. He was even recruited as a "specially appointed" member of a research and development team by a CS professor. The professor even called Ruan Xiaohuan's parents to ask how they had raised such an outstanding "young talent." In his senior year, he faced the prospect of not being able to graduate due to his involvement in a software project and not completing the graduation requirements of the chemical engineering department. Although the professor suggested that he can transfer to the computer science department and promised to award him a computer science diploma with just one additional year of study, he still chose to drop out and devote himself entirely to software development. He believed that for mastering rapidly changing technology, time was far more important than a diploma.

After dropping out of college, Ruan Xiaohuan quickly made a name for himself in various network security companies with his excellent technical skills, and soon rose from a programmer to a project manager, technical director, then CTO. In 2008, Beijing Venustech Group Inc undertook the project of the information security system for the Beijing Olympics. At that time, Ruan Xiaohuan was the company's director of research and development, and he became the chief engineer and project leader of the Olympic information security system. He was also praised by the company for "leading the research and development team to complete the Olympic information security task with high quality."

Around 2012, to have more time to study the cutting-edge technologies that he was interested in, Ruan Xiaohuan chose to resign from his job. After resigning, he moved his "workplace" from the company back to his study, where he started to work alone, developing open-source software for free. In his view, providing free and open-source software is a way to make a greater contribution to society. He spends almost all his time in the study, programming, reading books, and keeping up with the news. Besides eating and going for weekly bike rides, he was reluctant to leave his study. According to Mrs. Bei, his wife, he is even busier than when he was working [for a company]. "He even got angry when I asked him to come out of the study to have a meal," she said.


![23](/Users/admin/Documents/GitHub/ipmp/static/img/bc-3.jpeg)
<center><font color=gray>Photo of Ruan Xiaohuan while traveling in 2014. According to Mrs. Bei, he was described as "positive, healthy, and free" at that time. Photo provided by Mrs. Bei.</font></center>

This "workaholic" nature was also evident in the "Program Thinking" blog. In the fall of 2017, during an allergy-induced asthma attack, Ruan Xiaohuan developed a severe case of generalized erythroderma due to steroid medication injections. Although doctors suggested hospitalization, he insisted on going home to rest instead. Mrs. Bei didn't think much of it at the time, assuming he simply disliked the hospital environment and agreed to his request. After just over ten days of resting in bed, he had his mother move the computer to the bed in his bedroom. Now Mrs. Bei realizes that even when he was sick, he didn't forget to update his blog.

"I feel sad when I think back to that time. His health was really bad and I even worried that he might have a [medical] incident." During that same period, in his blog posts, Program Thinking always apologized for being "too busy lately" and for delays in posting. When she thinks back to her husband's perseverance during that time, she can't help but start to have a lump in her throat.


![22](/Users/admin/Documents/GitHub/ipmp/static/img/bc-4.jpeg)
<center><font color=gray>The desk where Ruan Xiaohuan worked in his study. After his arrest, the position was adjusted, but the items remained untouched. The green small table on the desk was used by him while working in bed during his illness from late 2017 to early 2018. Photo provided by Mrs. Bei.</font></center>

In Mrs. Bei's view, Ruan Xiaohuan was very idealistic. According to her, he was extremely persistent in things he believed in. His long-term work in cybersecurity had him develop a rigorous personality, paying attention to details and striving for perfection. In addition, he was knowledgeable and enjoyed reading books. He could finish a book in just two or three days, and he read a lot. Sometimes, he would even be a bit arrogant because of his vast knowledge, giving people reading lists, and even criticizing Mrs. Bei for not reading the books he recommended. "When he met someone who clicked, he would talk endlessly and follow things through; but if he found that the other person was not on the same level [as he was], he would gradually stop talking."

Why does a "technology enthusiast" care about politics? Looking back after her husband was arrested, Mrs. Bei found it not strange at all, given Ruan Xiaohuan’s values and personality. 

She said that throughout their many years living together, she could always feel that he had a sense of patriotism and concern for the welfare of the country, and he fervently hoped that the injustice in a society would change. "As a knowledgeable, passionate, and patriotic person, it's not difficult to understand why he would be this way," she said.

According to her, Ruan Xiaohuan has always kept an eye on current affairs in society. She remembers that he once criticized the high cost of cancer treatment in China and that many medical treatment plans only consider economic benefits and are not humane. His mother also recalled that when he was in elementary school, he expressed concerns about environmental pollution affecting human welfare.

In fact, from his blog, one can get a glimpse of Ruan Xiaohuan's journey. According to [the introduction of "Program Thinking"](https://program-think.blogspot.com/2009/01/test.html) when it was first established on January 15, 2009, he only wanted to share some experience and insights in programming and Research and Design (R&D). But his mentality changed a few months later. On June 11, 2009, Ruan Xiaohuan published a blog post titled "[It's Time to Write Something Beyond Technology](https://program-think.blogspot.com/2009/06/writing-something-with-polity.html)". In this article, he wrote about the government's censorship of several foreign websites in a row, such as the censorship of BlogSport in May and the censorship of Twitter and Bing in June, during May and June (the twentieth anniversary of the Tiananmen Massacre). Although the Great Firewall had been implemented since around 1998, the series of sudden censorship still made him feel that the freedom achieved by technology was rapidly shrinking.

He wrote: "From this (censorship) news, it seems that the 'Great Bright Correct” (referring to the current regime) feels that the GFW (Great Firewall of China) established at the international internet gateway is not enough, and they want to go further: establish a GFW on every computer to control all information tightly in the hands of the Party. If that day comes, what freedom do we have? What human rights do we have?"

Perhaps because he could not tolerate technology being used to suppress freedom and human rights further, and because of his honesty and insistence on truth, he decided to go against the tide and "write something other than technology". From then on, he began to use his professional knowledge to teach netizens how to bypass the Great Firewall and hide their identities, expose censored news and data, share various books, and write a lot of comments on politics--from "Liu Xiaobo's Nobel Prize" to the "June 4th" commemoration, from the "Jasmine Revolution" and the "709 crackdowns" to the "Anti-Extradition Law" movement in Hong Kong, and the Xinjiang cotton and the Wuhan pandemic, he left a comment on almost all major social events and movements of the past decade. Until May 9, 2021, the day before his arrest, he was still [sharing political books such as "On Democracy"](https://program-think.blogspot.com/2021/05/share-books.html) on his blog.

"When I saw these [online activities], I had a lot of regrets. I felt that I should have learned how to bypass the Great Firewall earlier so that I could have known his identity earlier, and we could have had a clearer direction when hiring a lawyer for his trial. He wouldn't have suffered for such a long time. Being able to see things and not being able to see things are completely different. Now I can understand the significance of my husband's actions," Mrs. Bei said.

## "The wife saw the look of plea in his eyes in the courtroom, while the parents are worried they "won't see him come back."

On the day of the verdict, February 10th, 2023, one year and nine months later, Mrs. Bei finally saw her husband, Ruan Xiaohuan, again.

Due to the court's requirement to wear masks, she could only see the part of his face above the bridge of his nose. Compared to the last time she saw him, he had become completely white-haired and shockingly thin. "Although he had some white hair before he was arrested, it was mixed with black hair. Now it's black hair mixed with white hair," she described, "I've been with him for so long, I've never seen him this thin, never. No matter how hard it was before, it was never like this." This is completely opposite to what the first trial lawyer described to her "He is doing fine." She was heartbroken about this.

After the trial ended, Ruan Xiaohuan was taken away by two court police officers. As he was leaving, he kept looking back at his wife and pleading for help with his eyes, revealing the pain of suffering injustice. Seeing this, Mrs. Bei couldn't help but softly say with her mask on, "Appeal."

“At that moment, I thought I was the only one who could save him,” she said.

The helpless look of her husband made Mrs. Bei unsettled. In addition, how the first instance verdict recognized her husband’s behaviors and some of the obvious discrepancies, made her feel strongly about the unfair judgment of the first instance. The verdict did not even list what speech Ruan Xiaohuan made was “inciting”. She told NGOCN, the confiscated iPhone mentioned in the verdict as the physical evidence was not Ruan Xiaohuan’s—it was hers; the Xiaomi phone mentioned had been broken and kept in the living room, and nobody used it. In addition, both phones have been returned a few days after they were taken away, so this was not a confiscation. Plus, Ruan Xiaohuan didn’t have a Huawei Matebook, only Huawei phones. His Dell Laptop and a ThinkPad laptop seized were, however, not listed in the evidence list. 

She said, "The careless handling of evidence in the case has led us to question how serious, truthful, and reliable the handling of evidence was by the authorities."


![23](/Users/admin/Documents/GitHub/ipmp/static/img/bc-10.jpg)
<center><font color=gray>Verdict of Ruan Xiaohuan. Image from the internet.</font></center>
 
In addition, the sentence in the verdict stating "the defense counsel does not raise objections to the facts and charges in the prosecution" also made her question whether the first-instance lawyers were fulfilling their duties. After her husband's arrest, she spent a lot of time self-studying criminal law and other legal knowledge, understanding that pleading guilty benefits reducing the sentence, but she couldn't understand why the lawyers "did not raise objections." "Lawyers should independently defend their clients based on legal facts and evidence. If there are no objections to the charges and facts in the prosecution, where is the defense reflected?" Previously, she suspected that Shanghai's judicial authorities had pressured local lawyers. She was determined to replace the second-instance lawyers with more experienced human rights lawyers from outside the region to fight for a fair judicial verdict for her husband.

On February 20th, Mrs. Bei signed a defense commission agreement with two human rights lawyers in Beijing, Shang Baojun and Mo Shaoping. However, her commission did not go smoothly. The day before she went to Beijing to sign the lawyer's commission, she was blocked and questioned by the police, warning her not to sign the defense commission with Mo Shaoping's law firm. Later, even after successfully signing the agreement, her defense work was heavily obstructed.

On March 9th, lawyer Shang Baojun went to the Yangpu Detention Center in Shanghai to request a meeting with Ruan Xiaohuan, as he had been unable to contact the judge in charge of the case to arrange for document review. However, the meeting was refused. Shang was told that the Shanghai Higher People's Court had appointed two legal aid lawyers, including lawyer Lu who had represented Ruan in his first trial, and another lawyer from the same firm. [Shang had previously told the media](https://www.rfa.org/cantonese/news/programthink-03222023054449.html) that this was a common tactic used by the authorities to prevent family-hired lawyers from intervening, as each defendant is allowed a maximum of two lawyers.

As a result, Ms. Bei has made multiple phone calls to Judge Xu Meihua of the Shanghai Higher People's Court, who is handling this case, requesting the revocation of the legal aid or providing written evidence that Ruan Xiaohuan, despite knowing that she has entrusted lawyers Shang and Mo, still chose legal aid lawyers. In addition, she has continuously communicated with 12368 to correct the issue of legal aid "occupying positions" and filed complaints with 12309 China Procuratorate Network and the Supervision Office of the Shanghai High Court. However, these demands have been evaded, transferred to relevant agencies, or informed that they are not within the scope of service, and so far, there have been no direct responses. On the afternoon of March 23rd, she went to the complaint reception office of the Shanghai Higher People’s Court to complain about Judge Xu Meihua’s infringement of the defendant's legal right to defense and requested the revocation of legal aid. Upon her insistence, the receiving judge made a record and reported the complaint to the Director of the Criminal Division. On the morning of March 28th, Ms. Bei called Judge Xu Meihua again, but her call was not answered.

"As family members, we have been cooperating with the judicial authorities all along, but now we have received such a result. We cannot accept this judgment. Is this judgment legal and reasonable?" Ms. Bei questioned. Currently, her most important effort is to have the court remove the "officially appointed" legal aid lawyers and allow the lawyers appointed by the family to defend in the second instance. She appeals for more people to pay attention to this case. She said that there are apparent problems with the first-instance verdict, and the family strongly questions it. Now it is the stage of appeal and second instance, and she will do her utmost to ensure judicial fairness in the second instance, and to have her husband restored to freedom as soon as possible."

Ruan Xiaohuan's elderly parents also made appeals for their son: "We don't understand why our son, who has made so many contributions to the country, is treated like this just because he expressed dissatisfaction with corruption and the reality. He only wants the country to be better and the people to be happier. We cannot accept such a judgment!" They are old and worried that they may not live long enough to see their only son, who has been sentenced heavily, coming back home.


![22](/Users/admin/Documents/GitHub/ipmp/static/img/bc-5.jpeg)
<center><font color=gray>On March 24, 2023, some netizens left comments on the "Program Think" blog. Image from the Internet.</font></center>

Now, nearly two years have passed since Ruan Xiaohuan was arrested and sentenced to 7 years in prison on charges of "inciting subversion of state power." After the news broke, many netizens flooded his "Program Think" blog, expressing their gratitude for what he had done and praying for his safety: "We don't ask you to write again, just hope you stay safe." "Hoping for the arrival of the ultimate dawn."

His wife, on the other hand, worries about his health. His thin figure and pleading eyes in the courtroom have been lingering in her mind. "I hope he can come back home as soon as possible and let his body recover slowly," she said.
