---
templateKey: article
title: '告读者：NGOCN推特账号已解封'
slug: 'NGOCN-Twitter-unsuspended'
date: 2023-03-14T03:21:57.265Z
description: 'NGOCN推特账号@ngocneng在两周前疑遭恶意举报，被系统误判为垃圾账号被“永久冻结”，现已申诉成功，恢复正常使用。感谢各位读者和师友支持！'
featuredpost: false
featuredimage: /img/hua-1.png
trending: 1
isbrief: false
columnist: 告读者
contributors:
  - type: '作者'
    name: 'NGOCN'
tags:
  - "推特"
  - "解封"
  - "垃圾信息程序"
  - "NGOCN"
  - "言论自由"
---

NGOCN[推特账号@ngocneng](https://twitter.com/ngocneng) 在两周前疑遭恶意举报，被系统误判为垃圾账号被“永久冻结”，现已申诉成功，恢复正常使用。感谢各位读者和师友支持！

除了NGOCN账号被误判外，我们也发现推特上中国行动者与人权捍卫者的账户容易受到限制或遭到封锁。据[纽约时报报道](https://cn.nytimes.com/technology/20230216/twitter-china-elon-musk/)，中国人权倡导者的账户常常遭到搜索可见度异常、推文限流、在无预警的情况下被冻结等问题，而这源于公司自动化系统中的错误。自伊隆·马斯克接管推特后，推特大幅裁员并引发离职潮，推特亚太地区负责人已于今年1月被解雇，使得监督中文帖子内容审核的资源大幅减少；中文的推特更是被兜售色情、赌博网站和应召服务的[中文垃圾信息程序](https://www.nytimes.com/zh-hant/interactive/2022/12/20/technology/china-protests-twitter-bots-elon-musk.html)淹没。

因此，我们准备继续致信推特客服，邀请他们提供更多相关问题的资料和解决方案，并公开呼吁推特改进监管非英语推文的措施，切实打击自动程序账号，保护中国行动者与人权捍卫者的言论自由！

![22](/Users/admin/Documents/GitHub/ipmp/static/img/twitter-unsuspend.jpeg)
<center><font color=gray>推特回复的邮件截图</font></center>