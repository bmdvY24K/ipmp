---
templateKey: article
title: 'NGOCN Express | Independent journalist Huang Xueqin and labor activist Wang Jianbing disappeared'
slug: 'Huang-Xueqin-and-Wang-Jianbing-disappeared'
date: 2021-09-22T03:21:57.265Z
description: 'Huang Xueqin, Chinese independent journalist and #MeToo activist, and Wang Jianbing,  a labor activist focusing on occupational safety and health , lost contact with families and friends on the afternoon of September 19, and their whereabouts are still unknown now.'
featuredpost: true
featuredimage: /img/0922.jpeg
trending: 10
isbrief: false
contributors:
  - type: 'Author and Translator'
    name: 'Lucretia, 小鸭, Ming'

---

**Huang Xueqin, Chinese independent journalist and #MeToo activist, and Wang Jianbing, a labor activist focusing on occupational safety and health , lost contact with families and friends on the afternoon of September 19, and their whereabouts are still unknown now.** Wang Jianbing may have been detained on charges of "inciting subversion of state power"(煽动颠覆国家政权罪), mainly because of his party with friends at home.

Huang is an independent journalist and one of the initiators of #MeToo in China, formerly serving as an investigative reporter for several newspapers in Guangzhou, Guangdong Province. In 2019, Huang was detained by Guangzhou police on charges of "picking quarrels and provoking trouble"(寻衅滋事罪) and later released on bail after three months of residential surveillance. Wang Jianbing is a Guangzhou-based NGO worker focusing on adolescent civil education since 2014 and a labor advocate for occupational disease victims, like pneumoconiosis since 2018.

NGOCN interviewed Li, a mutual friend of Huang and Wang. He said Huang planned to fly from Hong Kong to Frankfurt, and then arrive at University of Sussex, where she was going to start her Master’s program in Development Studies. Wang was accompanying her to the airport from Guangzhou. However, from the afternoon of September 19th, their friends couldn’t get in touch with them both, despite that they tried various ways, online and onsite.

Li said that they may be under the charge of police of Haizhu District, Guangzhou. Also, their homes were broken into by the police, and some of their personal belongings were taken away. On the day when they disappeared, a friend of Li was interrogated by the police for a whole day, and was asked about Wang’s party held in his home.

![](/Users/LIQI/ipmp/static/img/wang.jpeg)

Pictures resource: [Facebook Page](https://www.facebook.com/Freexueqinjianbing/) **Freexueqin黄雪琴&Jianbing王建兵**



After 5 years serving as the director of the rural educational program Western Sunlight Foundation, Wang moved to Guangzhou, working on adolescent civil education and the disabled community empowerment, and later shifted to advocate for the rights of workers suffering from occupational diseases. His friends say that Wang Jianbing, with his goatee, is a kind, warm person who always takes good care of people. They call him "Pancake"(煎饼), which sounds the same as his name in Chinese.

Haizi shares a long-lasting friendship with Wang, ever since they met each other at an NGO event in 2012. He said Wang's passion in education inequality grows from his own educational experience in underdeveloped western China. Later, Wang came to Guangzhou and still focused on marginalized and unprivileged communities, such as the disabled and workers with occupational disease.

Guangzhou was a city with the most flourishing civil society in China, attracting many idealistic young people to devote themselves to NGO careers, including Wang. However, around 2014, civil society and NGO development in China confronted a large crackdown, represented by [the arrest wave in mainland China in the fall of 2014](https://theinitium.com/article/20150917-mainland-NGO3/). Guangzhou, a city that used to be reckoned as "the closest place to a mature civil society" in mainland China, was also mired in politics. [The Law of the People’s Republic of China on Administration of Activities of Overseas Nongovernmental Organizations in the Mainland of China](http://gkml.samr.gov.cn/nsjg/bgt/202106/t20210610_330541.html#:~:text=中华人民共和国境外非政府组织境内活动管理法&text=第一条为了规范,非政府的社会组织。), which officially came into effect on January 1, 2017, was deemed as [a symbol](https://www.hrw.org/zh-hans/news/2016/04/28/289284) of the Chinese authorities' crackdown on dissent and further tightening of Chinese civil society.

In recent years, labor issues have become increasingly sensitive in mainland China, and the Guangdong provincial authorities have repeatedly taken action against labor rights organizations. 2018 was the most intense year, in which [numerous workers were arrested by local police during the Jiashi workers' rights movement](https://zh.amnesty.org/content-type/more-resources/news/one-year-after-jasic-protest/), and Yue Xin, a left-wing undergraduate student of Peking University, [disappeared because of this movement](https://www.voachinese.com/a/china-labor-activist-yuexin-missing-20181020/4621735.html). In 2018, under the harsh circumstance, Wang Jianbing still began to focus on the group of workers with occupational diseases, providing them with community services and legal support, marking his courage and sense of social responsibility.

Haizi said that Wang will not forget a single friend he has helped. Once he wanted to visit his friends with pneumoconiosis that he had helped at the Vocational Defense Institute, but because of the epidemic, the patients couldn't get out and it was hard for people outside to get in, he was worried that this would have a big impact on the patients' lives, and he said he would try to see how he could help them.

**"He is enthusiastic for public interest, so I definitely do not believe he will do anything related to inciting subversion of state power, definitely not."** Haizi said.

![](/Users/LIQI/ipmp/static/img/huang.jpeg)

Pictures resource: [Facebook Page](https://www.facebook.com/Freexueqinjianbing/) **Freexueqin黄雪琴&Jianbing王建兵**



In 2018, Huang Xueqin [reported Chen Xiaowu](https://www.reuters.com/article/us-china-harassment-idUSKBN1F10J9), a professor who worked for a top university of China, engaging in sexual harassment against many of his former students, making her a de facto intiator of Chinese #MeToo movement. She offered incredible encouragement and help for many sexual harassment victims to break the silence and speak out about their #MeToo stories in the unprecedented discussion of anti-sexual harassment during 2018-2019.

In October 2019, she was detained by the Guangzhou police for "picking quarrels and provoking trouble" (寻衅滋事罪) for releasing an [article](https://matters.news/@sophia/记录我的-反送中-大游行-zdpuAysW5ZoQVpbPWDWDK22Dg6jF3GSGYBfx4uk3buXQSkLsA) on the protest in Hong Kong, which was later changed to residential surveillance (指定居所监视居住). Three month later she was released on bail. 

[“Picking quarrels and provoking trouble”](http://www.calaw.cn/article/default.asp?id=13915) is known as “pocket crime” due to its vague definition. The seemingly tender name of [“residential surveillance”](https://www.voachinese.com/a/voa-news-residential-surveillance-20150901/2940735.html) may create an illusion that it is better than being arrested. However, in practice, families and lawyers can’t meet the surveilled one, and don’t even know where they are. Free from the law and rules of the detention house, residential surveillance often turns into abuse of cruel torture. And these two things are often used against people who criticize the government.

Huang is not the only [investigative journalist](https://cn.nytimes.com/china/20190715/china-journalists-crackdown/) who suffers pressure from the government. Many other people who speak for the unprivileged are also harassed and detained. [Fang Ran](https://ngocn2.org/article/2021-09-01-fang-ran-arrested/), a PhD student of the University of Hong Kong researching on labor empowerment in Mainland China, disappeared and was under residential surveillance ever since August, and his situation is still unknown.

Haizi is also a friend of Huang. He said he had contacted Huang on September 18 to talk about her study in the UK, and had not heard from her since.

"She went through a lot of difficulties but never gave up." said Haizi, "She was planning to pursue an LLM degree at the University of Hong Kong in 2019, but had to give up such a good opportunity because the police detained her. It broke my heart that she got a scholarship this year to study in the UK, and then encountered such a thing."

"She is a female journalist with a strong sense of social responsibility and courage to speak out for justice, and it is not fair for her to suffer this again and again." Haizi said.

Qingfeng and Huang met in an online summer session, where Huang was an instructor and guided Qingfeng's news writing. After the session, they kept in touch, but last week, Huang's replies gradually became less frequent.On September 19, Qingfeng contacted her again: "Are you okay?" The message showed that it had been read and not replied.

Several interviewees who have attended Huang Xueqin's news writing sessions said that Huang Xueqin is warm, genuine and "like an elder sister", always trying to comfort young people's uncertainty and confusion, giving encouragement and advice.Qingfeng said Huang is the "embodiment of justice". **"I don't know where she got the courage to do something that very few people dare to do. She replied that she likes this job."**

After regaining her freedom last year, Huang Xueqin planned to pursue a degree in the UK this year, but was taken away by police the day before she was due to embark on an international flight. At 11 p.m. on September 20, when NGOCN was editing this piece, Huang's planned flight, LH797, just took off.

(To protect the security of interviewees, all characters in the text are pseudonyms)

Original Chinese Version: [N记快讯 | 独立记者黄雪琴、职业病权益倡导者王建兵失联 | NGOCN (ngocn2.org)](https://ngocn2.org/article/2021-09-21-sophie-huang-and-jian-bing-wang-are-missing/)

**Reference**

1. Zuowang

   **Guangzhou-based labor and women’s rights activists go missing**

   Chinese investigative journalist and #MeToo activist Sophia Huang Xueqin and workers’ rights advocate Wang Jianbing went missing in Guangzhou on Saturday, Sept. 19, with their friends fearing detention.

   Wang was accompanying Huang to Shenzhen, from where Huang was supposed to travel on to Hong Kong and then fly to Britain for the start of her master’s program at the University of Sussex. Their friends lost contact with them on the afternoon of Sept. 19.

   Wang is assumed to be under investigation related to charges of subverting state power, according to people familiar with the situation. The charges are related to Wang having regular meetings with his friends at his apartment.

   Born in 1983, Wang is an independent labor rights advocate. After graduating from university in 2005, he joined the Beijing Western Sunshine Rural Development Foundation, where he worked as a project manager for five years. In 2014, he joined the Guangzhou Gongming Social Work Development Center and developed projects focused on youth with disabilities.

   Huang, born in 1988, is an independent journalist and women’s rights activist. She was previously an investigative reporter for the Guangdong-based Southern Weekly and New Express newspapers. She was one of the initiators of the #MeToo movement in China.

   Huang was detained by Guangzhou police in October 2019 on charges of “picking quarrels and provoking trouble” after posting an essay on Chinese social media about peaceful protests in Hong Kong. She was released on bail in January 2020.

   This year, she was a recipient of the Chevening Scholarship for graduate studies, funded by the British government. She was scheduled to fly out to Britain on Sept. 20 to start her master’s program at the University of Sussex.