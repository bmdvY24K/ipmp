---
templateKey: article
title: 'Love and Revolution: The Life Themes of Activist Li Qiaochu'
slug: the-life-themes-of-activist-li-qiaochu
date: 2021-05-04T21:21:28.132Z
description: When human rights lawyer Chang Weiping was detained again during his bail period, Li Qiaochu clearly understood the nature of what was happening. 
featuredpost: true
featuredimage: /img/2A3ijSq.jpg
trending: 10
isbrief: false
typora-copy-images-to: ../../../static/img
tags:
  - LiQiaochu
  - freedom
contributors:
  - type: Author
    name: 費頓
  - type: Translator
    name: Isabella Tang
  - type: Proofreader
    name: Selin
typora-root-url: ../../../static


---

#  **Love and Revolution: The Life Themes of Activist Li Qiaochu**

This is a retaliatory arrest. 

When human rights lawyer Chang Weiping was detained again during his bail period, Li Qiaochu clearly understood the nature of what was happening. 

A group of young activists launched solidarity actions two days later. The [FreeChangwei Facebook page](https://www.facebook.com/pages/category/Community-Service/Free-Chang-Weiping-%E5%B8%B8%E7%8E%AE%E5%B9%B3-107416584492421/) was established. Chang Weiping’s recount of his situation was quickly translated to English so it can be used by international media. Li circulated the hashtag #WalkAnotherKilometreToHelpFreeChangWeiping and also sent postcards as a form of performance art. 

She had devoted herself to public issues for a number of years: she’d participated in the 2017 Beijing Evictions, #MeToo movement, advocated for labor and human rights lawyers, and has participated in the solidarity action of the 12.26 Citizen Case in 2020. Li had already become comfortable and methodical with rights defense actions. 

“The objective of the retaliatory arrest is to scare and threaten the family members in the 12.26 Citizen Case and to suppress their voices. If we become afraid and retreat, they will continue to persecute us.” Li was extremely determined, and her actions would not stop no matter how many obstacles lay ahead. 

Although human rights defenders today in China cannot win against these rogues, at least they can take off the masks of authority.  Writing, recording and exposing the false mask of totalitarianism have become the focus of Li’s life since she was released on bail. 

As she continued solidarity actions for political prisoners such as Chang Weiping, Li also posted her own prison account – “[120 Days of Residential Surveillance, Accused of Incitement by Association”](https://seriousli.home.blog/2020/11/08/118/), a series of articles exposing the details of her residential surveillance and accusing the totalitarian regime of utilizing their absolute power to exploit an individual’s life in the name of “national security”. Her most courageous action was when she released a three-page complaint targeting the illegal behavior of Beijing Public Security Bureau, and later sent this to Beijing People’s Procuratorate, requesting for an inspection and rectification of the illegal actions of the police; for example, illegally questioning her for 12 hours, forcing her parents to sign a pledge letter, and also detaining her for 20 days around International Human Rights Day. 

In prison, “To live means that you are interrogated in order to serve the police’s goal of extracting information from you.” 

Now, for Li, regaining her freedom and living is for “revolution and love.” 

“Revolution and love are my life; they are the subjects of my life”. Li has repeatedly emphasized to her friends that she is a committed activist. 

Lin Yue, a close friend of Li, felt shocked hearing this and didn’t react for a few seconds. They knew each other at university and went on exchange together in the UK. They had fantasized about love, talked about labor rights, religion, freedom and democracy. But they never planned to become a revolutionary or an activist in the future. 

“She is the most pure-hearted person I know,” Lin said. Li’s life was very bright and happy, and it was admirable. 

Li grew up in a middle-class family in Beijing, and her family atmosphere was democratic, liberal and respectful of individual choices. She was always the “perfect child” – she is kind; she was a high achiever; and she was well-behaved and obedient. Her parents were middle school teachers. Her father taught physics, and her mother taught mental health. They never took a utilitarian approach to educating Li. Unlike many parents who hoped their kids would be highly successful, Li’s parents instead taught her to be “kind, honest and a good person.” 

During the final year of high school, the majority of schools would secretly have extra classes for students and request parents to sign a notice for voluntary extra classes. Li’s parents did not agree to this and refused to sign. As such, on the teacher’s desk, there was one side with a thick stack of signed notices for extra classes, while the other side was just Li’s form. 

On the weekends during her final year of high school, Li’s parents took her to watch stage plays, attend concerts and have picnics. The school reached out to her parents to talk about Li’s future and hoped they would cooperate. Her parents’ answer was: “There’s no need to worry; as long as my child goes to a third-tier school, that’s fine.” 

When thinking back to her childhood, Li would always laugh, and says her parents are “too relaxed”. 

This was also the paradise that Lin admired the most: having parents that are loving and harmonious and don’t give pressure to their children; having a lot of friends who are sincere and friendly. Li grew up in this sort of environment, “very kind, sincere and with a pure heart.” 

In 2009, Li without much effort got into Renmin University. Having been brought up in such a relaxed environment, she studied labor relations and started paying attention to workers’ rights. At the time, the concept of a new generation of migrant workers was just introduced, and news regarding the Foxconn suicides and Hunan pneumoconiosis workers broke out. Li participated in a two-year research project named “New Generation Migrant Workers”. She went to the factories constantly to take surveys and came into contact with workers, and this is how she began to understand society’s reality and cruelty. 

Li became attached to the working class after seeing these individual struggles, and she was unhappy that they suffered from discrimination. She realized she was a beneficiary of the social system and hoped that the middle class could take on more responsibilities in promoting social justice and equality. 

She learnt how to use VPN and frequently felt unsettled by the injustices in China: Tan Zuoren compiled a list of students who were affected by the Wenchuan earthquake and was arrested after questioning the quality of the school buildings; Liu Xiaobo who received the Nobel Peace Prize was sentenced to 11 years for “inciting subversion of state power”. She was outside the court at the time as a supporter, and she remembered the cold winter day. Xu Zhiyong and other legal scholars had started the New Citizens’ Movement to promote equal rights in education and came across many impediments by several parties. Citizens who were commemorating June 4th were arrested and criminalized. Migrant workers were collectively beaten to maintain social stability. 

This uneasiness was a catalyst for action. From 2009, Li began paying attention to civil society; she started an independent record of major public events every year and she also started some excerpts of relevant poetry. In doing so, she wants to fight the short memory of the internet and the unified narrative. This persisted until 2019. 

“She felt deeply for the hardships of others, and from the heart, she hoped for improvements in society.” Lin believed that Li’s participation in civil society and her protest actions was from a young individual who had conscience, who was just doing what she believed was right. However, in the current context of China, if you carry out actions and protest against injustices just because you are following your conscience, you have entered the path of an activist.” 

“Xu Zhiyong’s girlfriend”. After January 9, 2020, this was how people began identifying Li. 

That day, she [penned a piece](https://twitter.com/liqiaochu01/status/1215279449677148166) revealing that she was questioned for 24 hours and wore handcuffs to pass the New Year. It was also the first time she publicly revealed her relationship with Xu. Xu established the Open Constitution Initiative to promote the rule of law, launched the New Citizens’ Movement and is a leading figure in defending the rights of Chinese citizens. The national security police couldn’t find Xu, so they questioned his girlfriend Li for information. 

Very quickly in related media reports, Li’s name would always be preceded by “Xu Zhiyong’s girlfriend”, admiring their “beautiful romance” and saying she was “the woman behind a great man”. Li’s continuous public participation and actions were frequently ignored. 

Li criticized these representations that ignore individualism and treat women as subordinate. She doesn’t take issue with her identity as “Xu Zhiyong’s girlfriend” because this is the truth. Even if domestic security tried to take out evidence to divide them during interrogation, she believes in their revolutionary companionship and love. However, she hopes that people could see her many other sides – a labor activist, a feminist, a Christian, a fighter who has made “revolution and love” a theme of her life. 

Li’s frequent participation in activism started in 2017. That Winter, a fire broke out in an illegally constructed apartment block, which was used by authorities as a reason to launch a campaign to “evict the low-end population” and “light up the skyline”. This “clean up” displaced tens of thousands of migrant workers overnight. 

Li quickly rushed to the villages that were to be torn down. She remembers seeing the cruel scene which seemed like it came from a movie: On one side there were tenants who were resisting as their water and electricity had been cut, on the other side there were riot police with shields to drive the migrants away. The tenants who didn’t dare to resist were crying and yelling, as they moved away in the dark. For the houses that were emptied, the half-burned candles were especially blinding. 

She was angry of course. At night, she joined the volunteers and journalists and lived with the tenants, keeping tenants from being violently evicted. During the day, she went to more urban villages to collect the needs of tenants and to also put together a list of organizations and contacts who could provide free or cheap accommodation, helping those who were evicted find a place to store luggage or to stay for a couple of days. 

Li felt alive as she became busy with this. Participating in this public relief work was a form of self-help for herself. It helps her to regain her self-worth which was shattered in her previous marriage. 

Now, Li comes to realize that the citizen participation at the time was more of a humanitarian rescue with the mentality of an NGO-style service provider. It lacked actions in holding the government accountable. “It seemed like the government ordered evictions, we came and helped tenants to leave, without responding seriously to their needs and their plight– which was that they wanted to stay.” In 2018, when the Chinese #MeToo movement started, she tried to respond to the needs and the plight of victims of sexual assault. Li was very active in participating in this. She and several others helped Tao Chongyuan, a student at Wuhan University of Technology who jumped to his death, raise lawyer fees, and they also went to Wuhan to comfort Tao’s sister; she was also concerned with the sexual harassment case by Shen Yang, a former Peking University professor, supporting students who were questioned and calling on the university to disclose information. 

China’s social environment has never been tolerant of women, and there is no exception in the civil society community. When Li accused labor expert Wang Jiangsong of verbal sexual harassment, the response she received from the community was overwhelmingly to “keep quiet” and to “think about the bigger picture”. The logic is, the space for civil society was shrinking, and it was best not to give the government “ammunition”. Having experienced oppression from both the government and patriarchy, Li could see clearly how many of the older generation pro-democracy and freedom activists actually still had patriarchal attitudes, promoting female virtues but lacking the ability to reflect. 

“In a realistic situation where there is a lack of political opposition, there is a greater need to return to the plight of the individual and their authentic voices.” Li admits that she does not want to knock these people to the ground, but she opposes their indifference in order to protect the so-called “bigger picture" and their neglect of individual harms. 

Despite being injured mentally, Li never stopped her activism. In 2019, when the coronavirus epidemic swept across the city, Li was worried about her boyfriend Xu’s “escape” and also put her heart into volunteer work. She bought face masks and handed them out for free to the sanitation workers in the community. After she heard that a friend had an online clinic, she designed a virus prevention poster and assisted in linking up patients with ventilators. After she discovered that the mobile cabin hospitals did not have separate men and women toilets, she organized volunteers to collect and collate advice on preventing sexual violence. 

These private self-help efforts which were full of vitality gave people an illusion that China’s civil society was developing and flourishing again. Li has analyzed this and realized that these citizen rescue efforts like the “Beijing Evictions” were more to do with humanitarian aid. They weren’t able to hold the government accountable nor did they create a chance for civil society to survive.

The reality is even more absurd. Some activists who enthusiastically participated in these relief efforts were detained; the volunteers Chen Mei and Cai Wei who collected and saved coronavirus resources, [were accused of “picking quarrels and provoking trouble”](https://www.facebook.com/%E7%AB%AF%E7%82%B9%E6%98%9F404%E4%B9%8B%E5%A3%B0-100806644983621/?ref=py_c); writers who donated supplies to Wuhan were threatened with charges of “spreading rumours”; and Zhang Zhan and Chen Qiushi who went to Wuhan to do independent reporting were disappeared. Thus, Li worked on her own by providing mutual aid while also showing solidarity with arrested activists on social media platforms. 

The maximum penalty for inciting subversion of state power is being sentenced to death. 

On February 16, 2020, domestic security officers informed Li that she was being placed under residential surveillance in a designated location (RSDL). She never thought that as Xu Zhiyong’s girlfriend, she’d also be punished and could be facing charges for inciting subversion of state power. 

Returning to December 2019, Xu went to Xiamen to participate in a gathering of lawyers, scholars and citizens to discuss China’s current and future politics and their experience of building civil society. On December 26, lawyer Ding Jiaxi, citizen Dai Zhenya, Zhang Zhongshun and Li Yingjun who were at the gathering were detained by police, and this event was called the “12.26 Citizen Case”. 

Xu went into hiding. And during this time, he wrote the [famous letter](https://cmcn.org/archives/46079) urging President Xi Jinping to resign. He questioned Xi Jinping’s ability to handle the US-China trade war, the Hong Kong protest movement and the coronavirus pandemic, requesting him to step down. On December 31, Beijing national security questioned Li for 24 hours to inquire into Xu’s whereabouts. 

On February 5, 2020, Xu was arrested in Guangzhou. In the early hours of the next morning, Li was also detained as collective punishment. 

This was an unexpected disaster. Her family and friends were outraged. 

“This is so wrong; this is just so wrong.” Ding Jiaxi’s wife, Luo Shengchun, sighed repeatedly after hearing the news of Li’s detention. “Liu Xia, who was Liu Xiaobo’s wife, was put under house arrest, and now Li, who is Xu’s girlfriend, is being put in a black prison. This is terrible.” Liu Xiaobo is a prominent Chinese dissident. His wife Liu Xia was placed under house arrest for eight years after he won the Nobel Peace Prize in 2010. 

After being detained for four months, Li analyzed that detaining the family members and partners of activists as collective punishment was a tactic used by the government as a stabilization strategy and also a means of repression. Their aim was to “deprive family members and partners the space for their voices and to prevent family members from creating a rights defense alliance”. 

In 2015, around 200 were detained during the 709 crackdown on human rights lawyers. The family members of the human rights lawyers established a rights defense alliance – 709 Family Members. Among them, the wives of the 709 lawyers were very prominent. They had an attitude of uncooperative resistance and launched several innovative actions as resistance, such as the “1000 miles in search of my husband”, “Red bucket resistance” and “We can be hairless, but you can’t be lawless” (the word for hair and law have the same pinyin in Chinese). These actions were within the law and received international attention and solidarity and helped open a new space for rights defense.

The authorities quickly adjusted their strategy: firstly, there would no longer be mass arrests, and instead they’d be detaining a few individuals at a time so that family members would not be able to form an alliance; secondly, is to punish or threaten family members or partners as collective punishment. Even if they will be released on bail pending a trial, on top of legal restrictions, this will eliminate the space for solidarity actions. In the recent two years, there have been several cases which follow these patterns: The wife of NGO staff Cheng Yuan, Shi Minglei, [was threatened with punishment for “inciting subversion of state power”](https://www.facebook.com/1466849490028320/posts/2362239370489323/) and was placed under residential surveillance for half a year; poet Wang Zang and his wife Wang Li [were in detention for “inciting subversion of state power”](https://twitter.com/0530wlq); in the [Terminus 2049 case](https://www.facebook.com/%E7%AB%AF%E7%82%B9%E6%98%9F404%E4%B9%8B%E5%A3%B0-100806644983621/?ref=py_c), Cai Wei and his girlfriend Tang Hongbo were arrested for “picking quarrels and provoking trouble”; cultural entrepreneur Geng Xiaonan and her husband [were criminally detained for “illegal operations”](https://www.rfi.fr/tw/%E4%B8%AD%E5%9C%8B/20201121-%E8%80%BF%E7%80%9F%E7%94%B7%E6%A1%88-%E8%A6%8B%E5%BE%8B%E5%B8%AB%E9%81%AD%E6%8B%92-%E8%A8%B1%E7%AB%A0%E6%BD%A4%E9%80%A3%E5%B8%B6%E8%A2%AB%E8%AD%A6%E5%91%8A). 

This means that family members and partners must have a new strategy. From the 709 wives resistance, the strategy Li learnt was: “The more public you are, the safer you are”. 

She took administrative measures and applied for information disclosure, requesting to make public the location of residential surveillance, the medicine she took during that time and the doctor’s qualifications; at the same time, she made public her experience being in residential surveillance, continuous harassment from national security and the details of their threats. She even publicized the names of the national security officers and phone numbers. 

There were some results from this resistance. National security began retreating from their actions, and the number of times and the number of people they harassed decreased. “We cannot win against rogues, but we can still put up a fight” was Li’s conclusion. Li discovered that the more public she was, the more her fears and feelings of helplessness also subsided. In the recent month, national security did not come to question her, and she felt it was a little hard to adapt: “If they don’t come, I have nothing to write. I’ve taken the reins, and now the mouse wants to catch the cat.” Li regularly shares her feelings with friends but she rarely mentions the stress and weight of fighting alone. 

Luo Shengchun witnesses Li’s progress with admiration. She saw Li setting up the webpage, writing articles, designing postcards, making posters and promoting information disclosure. Li had transformed from the cowering individual after being released on bail, to someone who was bravely resisting. Luo is the wife of Ding Jiaxi, who was part of the “12.26 Citizen Case”, but she is currently in the U.S. and is unable to be present with Li and resist in person. 

Space and freedom come from resistance. Luo knows this, and she regrets that the “12.26 Citizen Case” did not form a rights defense alliance like the 709 wives. “Li is the only voice in the Mainland, and she is taking on all the pressure from this solidarity work. She also has depression and trauma too.”

Trauma can be triggered at any time. 

Hallucinations, insomnia and the nightmares that don’t stop. Even when she practices positive thinking, the phrase “sit upright on one half of the chair” can also shock her like electricity, which brings back memories of the guards saying: “Target sit down! Target keep your hands down! Target stop moving around!” 

At the time, she was not seen as Li. She was not treated as a human. She was only their “target”. 

Some of the scenes kept haunting her and she had layers of guilt and shame: When she asked national security to bring her anti-depressants, when she thanked national security for bringing fruits once in a while, when she begged for forgiveness, when she threw up after taking some medicine, when she was threatened with having more female police, when she had to write a repentance statement over and over again, when she was asked to criticize Xu Zhiyong’s ideology. That humiliation was “like trying to commit suicide by biting her tongue, but not dying, and instead leaving behind her tongue and being forced to sing songs of praises.” 

Li lay in bed for an entire week, and she thought about committing suicide. It seemed like she had returned to 2017 when she was hurt by her former marriage, and the environment for civil society was also worsening. She felt she was permanently damaged. She tried several means to commit suicide, using sweets, junk food and alcohol to ease the pain. She was afraid that her parents would be disappointed, so she didn’t dare go home. She resigned from her job, divorced, and she could not think straight, locking herself up in her apartment. Alone and helpless, she clung onto civil society which was waning and the people who still continued with their work. They were her last bit of hope. She saw tens of thousands of people displaced overnight. She also remembers the people who were facing arrests just outside their door, but calmly recorded their situation on camera and confided in their families. She suffered from sexual harassment and connected with survivors, empathising with each other and giving each other warmth. Ultimately, with love, respect and curiosity for the world, on top of her reluctance to let go of life, she perseveres and lives with the trauma.

Li again thought of recording this and putting her trauma within the context of state violence, recalling and writing about it every day, “Trauma and feelings are also a kind of historical testimony, a witness to state violence.” 

In the process, she began to understand that humans are soft. They are not solid machine parts and will suffer; they may be strong-willed but will have moments of weakness. She feared, she compromised, and although her resistance wasn’t perfect, that was her authentic self. She slowly accepted that she would be “walking with trembling legs and wiping tears from her eyes as she moved forward.”

After accepting an imperfect but a complete and authentic self, she suddenly had the strength to fight. She shifted her feelings of humiliation into motivation and picked herself up again. She began to show different sides of herself on different platforms. “The tough and gentle, and sometimes weak” activist who resists is the image she presents herself on Twitter. In many of her posts, she presents herself as “afraid of being disappeared again” and “needs to be cautious”, and thus she has gained more attention and sympathy. 

The funny thing is, after national security found Li, they said to her: “You paint yourself as someone who is timid, yet there is nothing that stops you.” What they were referring to was that Li shows herself as fearful and timid, but she also perseveres with actions such as information disclosure and revealing the contents of national security’s questioning and also their identity.

On Facebook, she shows her more incisive side. She criticizes the so called “democracy veterans”, who hold up the banner of democracy and freedom but also deny women their rights and equality. She admires how bold feminist activist Zheng Churan was when she showed her armpit hair in photos. When the publisher Geng Xiaonan was called “The woman of the Decembrists”, Li was critical of Geng being seen as subordinate and pointedly asked “In China, who can be called a Decembrist?” 

A close friend of Li, Han Ji, who was also a labor activist and researcher, was surprised when Li was released on bail. She was still weak but she was still resolute, “She has trauma and will show her weakness. She’ll cry, complain and be sad, but this never interfered with her actions. It was as if there was strength that was building inside her and was pushing her forward.”

Han and Li met in July 2018, and together they had assisted the students who were part of the [Jasic Solidarity Group](https://zh.wikipedia.org/wiki/佳士事件). Han was most impressed by Li’s ethics as an activist. At the time, there was someone who encouraged those who participated in solidarity actions to hide the details of the case and to purposely play down the risks associated with being involved in the solidarity efforts. Li expressed her disagreement, “She can take risks herself, but she can’t request others to follow suit. She hopes those who participate fully know the risks and are able to make their own decisions.” 

In the 12.26 Citizen Case, Li also knew the plight that other family members were in. She never requested family members to engage in solidarity efforts and resistance even though this meant she would have to fight alone. 

Han has seen Li during her weak moments. That was at the end of 2019 when Xu was in hiding and Li was followed by national security. She was so weak that she once fell and sat on the steps of the metro, unable to get up, and her eyes were filled with tears. “She is much better now, and she is focusing more on the safety of others rather than herself.” 

“Living for the movement.” From Han’s point of view, Li has made resisting a habit and instinct, “She doesn’t need to have mental struggles anymore; resisting is a form of self-help for her, and it is part of her life. 

Revolution and love, these are the themes of my life. Li has said this many times. 

What does revolution refer to? What is the meaning? I asked her. 

“Being awake, conscious and living is revolutionary. Seeing the issues and seeing the reality, this is the meaning. “

She has reflected that because of Xu’s fame, she has become a beneficiary of the limited support efforts. Compared to other political prisoners, she has received more international attention. She feels she has a lot more responsibility and moral obligation that she cannot abandon the movement. She has to be vocal for herself, for Xu, for Chang Weiping and even more political prisoners. 

Even if she wants to resist, she’ll have to be like a fishbone. Li said, “Even if we are swallowed up, we still need to make sure it is difficult for them to bear. Even if we are broken, we still want to be laying there and be a stumbling block. Even if we are ruined, we want to expose the process of this.” This is similar to the “burnism” theory of Hong Kong protestors. 

In reality, she knows the experiences of many activists; cooperation with authorities and concessions won’t lessen the suffering or punishment. Totalitarianism will not stop repressing just because someone has compromised or stayed silent, “In fact, publicly resisting isn’t a choice; it is the only road.” 

“Her resistance is very brave. In fact, she is very determined and has a non-cooperative attitude,” said activist Shan Zhu who had also been placed under long-term residential surveillance. Originally, she admired Li’s attitude to resistance, “After being questioned, she took photos wearing a shirt with the word ‘citizen’ to take artistic photos, and afterwards she went to take photos in a wedding dress appearing as a very beautiful activist.” 

However, Shan gradually discovered, “Li is very tolerant and warm-hearted with others, but she is very harsh towards herself. She pushes herself a lot, and she is becoming more and more determined with her resistance.” She once saw Li couldn’t help crying while walking on the side of the road. 

Shan and Li previously went to visit [Li Ning who has been advocating for her mother for 10 years](https://www.sohu.com/a/191937601_616821). Li Ning said to them: “Life is very short. After persevering with rights defense and resistance, you aren’t able to pursue anything else.” In order to get justice for her mother who died in a black jail after petitioning in Beijing, Li Ning has not been able to study or work normally for the past 10 years, and she is physically unwell. She needs to continue to take medicine, and her family situation is not ideal. Li Ning’s determination and temperament made Shan feels that “their (Li and Li Ning) temperament is becoming more and more similar as they are fighting like soldiers, and they’re hiding all their weakness and fears.” 

“The ability and courage to use their lack of freedom to fight for freedom,” Shan remembers the words of scholar Deng Xiaomang. She recently has a clearer understanding of herself and gradually has put herself back together with courage and confidence. 

Comrades have begun to understand that the sincere and righteous Li cannot forgive herself for being insincere and unrighteous under the machinery of power. She believes she needs to be even more determined in resisting so that she can pick herself up again. She pursues justice without second thinking, not only to resist totalitarianism and to transform the shame she has, but to live. 

Originally someone who was bright and relaxed, after fighting under totalitarianism and patriarchy, becomes critical, stronger and determined. But Li rejects this sadness, and sometimes she would cook meals and brag about her cooking skills. Sometimes, she’d complain about her boyfriend’s male chauvinism and makes fun of the fact that each time national security officers question her, it boosts her morale. Writing about the details of her residential surveillance wasn’t to exaggerate the tragedy, but was to confront trauma and weakness and to fight back against totalitarianism by telling the truth. 

“Even though she has analyzed the risk many times, Li still continues to pursue justice without thinking. In China’s history of human rights struggle, female activists are a beautiful sight; sometimes they are braver and more moving than men.” Teng Biao, a human rights lawyer living in the U.S., admires Li’s resilient attitude even after she was released on bail. 

Xu Zhiyong’s good friend and human rights activist Hua Ze was previously very concerned about Li’s mental health. She knows Li has depression and felt sad that she had encountered so much pain and injustice. But Hua discovered that Li was “not a pushover; she was like playdough, which is tough and flexible. No matter what comes after her, she can still fight back.” 

Hua saw Li grow very quickly, not only was she determined, but she had strategies. For the article Li wrote exposing her experience in residential surveillance, Hua had advised Li to finish writing the entire piece and publish it all at once. But Li’s response was, “I need to extend the timeline by releasing one section at a time, so that people would pay attention to the case longer.” “She is able to learn, has theoretical knowledge and has a wealth of experience in resisting.” Hua could foresee Li becoming a strategist. 

Recently, the movie “Mulan” was released. Li expressed her feelings on Facebook: Mulan isn’t a story about a daughter replacing her father in the army, nor is it a story of loyalty and patriotism. It is a story about a woman who broke through chains to fight a war. These past few years, Li saw a lot of Mulans coming out to fight. She has now become one of them. When Mulan is on the battlefield, she has to overcome her physiological and psychological limitations. Within the circle of activists, for female activists, they need to maintain subjectivity, express their personal views and learn to ignore the negative evaluations from others. This is no less difficult than Mulan going to the battlefield. 

“There is a need to regrow and to fight against totalitarianism and patriarchy.” Li no longer hides her partner’s issues: They pursue democracy, but they lack gender awareness; they don’t face up to the investment of their companions and value female activists. They want themselves to play a leading role in improving society. 

The male leaders were detained one after another. After 2013, the space for China’s civil society began to shrink; the price for activism began to increase; and there were increased efforts to detain activists. In the 12.26 Citizen Case, many of those who were involved in citizen’s rights have either been released on bail or have fled China, but they have kept silent. In the 12.26 Citizen Case, Li is the only vocal one in China. 

Lawyer Chang Weiping, who is from the same case, previously had reached out to Li. His first sentence was: “I’m sorry that you had to suffer by yourself for so long.” These moments where female activist’s values are acknowledged is very precious. But not long after, Chang Weiping was arrested again. It was impossible for Li to accept this, “Before his family members come out in support, I am his family.”

Previously, Li was clear-cut with Xu: She wants a partner who sees her as a revolutionary companion, not a perfect wife for home. 

Female activists are seen as partners, but their value as activists has been frequently overlooked. Li is well aware of this: “Male activists are unwilling to acknowledge the contribution of women in the public sphere and frequently reinforce their subordination and dependency. Women are suppressed, devalued and disregarded everywhere. When they are resisting, they sometimes lose their own voices or their own subjectivity.” 

Li has also reflected whether or not she would lose her subjectivity in the midst of resistance. When she was in a relationship with Xu, she published an article, and she’d consult him for his opinion and receive his approval before publishing. When she was showing support for “iLabour Three” editor Wei Zhili, she took some photos at the Great Wall and posted them on social media platforms. Xu did not repost them, and after asking about it, he said that “the photo makes you look too tall; it doesn’t reflect your gentle temperament.” 

Li and her comrades are able to find happiness out of being bitter: now it seems that they can fight in their own way. She can freely choose the pictures and words to express solidarity with political prisoners. She can share her paintings without being fearful of being ridiculed for not painting well enough. She can decide her own resistance strategy. She can be vulnerable and timid while also be strong and determined, “Xu used to laugh at me for being timid, vulnerable and who loves to cry. He couldn’t imagine me being strong.” 

“Although Li does not have a glittering resume like Xu, the more I was in contact with her, the more I was impressed by her personality.” Han Ji once confessed that it was the presence of pure, genuine people like Li that kept him from giving up hope on activism. 

Hua Ze also praises Li’s morality and integrity, “Some of the activists have sacrificed a lot but then receive little attention. Because she is comparatively well-known, Li always says when she faces suppression from the government, she can use the attention received to be a voice for others. Li’s resilience and moral righteousness is far above that of many well-known activists.” As a female activist, in Hua’s own experience, she has seen a lot of famous activists who sexually harass and suppress their partner or other female activists. She discovered the older generation of male activists lacked the ability to constantly learn, reflect and self-criticize while the new generation of female activists are more capable of learning, are more empathetic and reflective and perhaps have more of the potential and ability to change China. 

“You have grown from a woman who was innocently in love, to a wife who has to face the harsh reality of visiting her husband in prison.” This is Liu Xiaobo’s poignant love letter to Liu Xia and he saw the fate of a Chinese dissident’s wife. 

Lin Yue, a friend of Li, believes that Li has hero complex and sees the democracy circle through “rose-coloured glasses”. Other friends believe that Li and Xu’s love for each other is like a romantic song of revolutionary companionship. 

Li wanted to take this romantic song forward into marriage. She plans to get married with Xu, who is in prison, in the future. 

After knowing this, her friends felt helpless and were in a difficult position: On the one hand, they don’t want to see Li become another Liu Xia, as the wife of dissident Liu Xiaobo, she had lived under house arrest long term and became very depressed; on the other hand, they wanted to respect and support Li’s own choice and decisions. 

“Even though we understand and are moved by the romance and true love, there is a harsh reality. Are you going to wait for him for decades?” Luo Shengchun kept telling Li to think twice. 

Luo met Li on February 14, 2020, Valentine’s Day. At the time, her husband Ding Jiaxi was already arrested, and Xu was still in hiding. Li made a Valentine’s card for Xu and used Luo Shengchun and Ding Jiaxi’s headshot to design postcards. Luo was impressed by this and was moved by Li’s attentiveness, affection and authenticity. 

But the more she spoke to Li, the more she felt helpless. From Li, she saw an image of herself. She was also someone who had tried her hardest to fight: if she could have let go of Ding Jiaxi, she would have done this a long time ago and not waited so long.

“Li said, revolution and love are the themes of her life. It sounds old school, but she tried her best to constantly push for social change and to love deeply.” Luo said, she still understands Li. Even if she has to wait for Xu, she can still choose to wait quietly and live and work normally. But Hua Ze pointed out, “Li probably would choose the harder way.” 

In June 2019, Li and Xu became a couple. She could see that he had strong political beliefs, was tough and persistent in his resistance; she also discovered his neglect of the underclass and women activists. She criticizes his lack of awareness on the extent of patriarchy and gender. From him, she also gained some political awareness. He also understands her pain. When she was experiencing an episode of depression and hid in the wardrobe, he sat quietly and patiently outside with her. 

The two were in love and also quarrelling. But only after six months of being together, Xu was arrested again. As his companion, Li was worried about his situation. As his revolutionary comrade, she felt proud of him and knew that he’d have to bear the punishment for his actions, “Actually, when he was hiding, he did not think about fleeing the country. He has a moral responsibility and needed to buy more time to speak out and expand his influence of the 12.26 Citizen Case.” 

I asked Li if her desire to marry Xu was the result of her hero complex. 

“Yes, I have hero complex. But my image of a hero is myself.” Li explained that she loved Xu, but she never saw him as a hero. National security agents also thought Li had worshipped Xu as a hero and tried to undermine his image. In reality, Li had long recognized Xu’s shortcomings and flaws. 

The two had discussed marriage a long time ago, considering perhaps one day they would need to sign a power of attorney and appeal for solidarity for each other. 

“For someone who has a conscience, helping him out is merely the role of his family.” This was her original reason for wanting to apply for marriage, not because she is haunted by a rose-colored imagination. 

In the middle of September, Li went to take a series of wedding photos and sent these to Xu who was in prison; on one of the couple shirts Li designed, Xu’s one reads: 

My other half – the gentle warrior. 

 

*Han Ji, Lin Yue and Shan Zhu are pseudonyms.* 

Original article in Chinese: https://ngocn2.org/article/2020-12-22-qi-qiao-chu-revolution-love/
