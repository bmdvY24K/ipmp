---
templateKey: article
title: '告读者：NGOCN推特账号二次被封经过暨行动倡导'
slug: 'NGOCN-twitter-suspended-second-time-and-action-advocacy'
date: 2023-03-21T03:21:57.265Z
description: '在短短不到二十天内，我们的推特账号已经历了两次被封，而且与上次被解封时间仅隔了一周。我们深知言论自由来之不易，愿捍卫言论自由的每一寸领土。我们希望找到更多正在或潜在受影响的推特用户，与我们一起行动，敦促推特采取实际措施改进非英语语言的审核机制，切实保障中国行动者和人权捍卫者的言论自由。'
featuredpost: true
featuredimage: /img/hua-1.png
trending: 10
isbrief: false
columnist: 告读者
contributors:
  - type: '作者'
    name: 'NGOCN'
tags:
  - "推特"
  - "永久冻结"
  - "言论自由"
  - "行动倡导"
---

3月18日，NGOCN推特账号再一次被“永久冻结”。我们迅速向推特客服团队进行申诉，3月20日，我们收到了邮件，账号解封。虽然此次账号很快解封，但在短短不到二十天内，我们的推特账号已经历了两次被封，而且与上次被解封时间仅隔了一周。

自伊隆·马斯克接管推特以来，对公司进行大量裁员，账号内容审查严重依赖机器系统，导致中国独立媒体和人权行动者的账号[无故被冻结](https://cn.nytimes.com/technology/20230216/twitter-china-elon-musk/)。这正严重危害本已脆弱的中国行动者和人权捍卫者等的言论自由空间。

NGOCN是从中国互联网空间发展起来的公益和独立媒体平台，多年来我们经历了众多审查上的挑战，也报道了很多中国言论空间受到限制的事件。我们深知言论自由来之不易，愿捍卫言论自由的每一寸领土，同时，也为受到同类问题影响默默付出的无声行动者发声。

因此，我们希望找到更多正在或潜在受影响的推特用户，与我们一起行动，敦促推特采取实际措施改进非英语语言的审核机制，切实保障中国行动者和人权捍卫者的言论自由。

我们也号召更多推特用户与我们共同发声，呼吁更多人关注公共社交平台的言论自由。我们不要一封解封邮件，我们要一个系统的解决方案。

————————————

下面附上向推特提交的申诉信以及推特的解封邮件。

Hi Twitter Team,
 
This is Twitter account @ngocneng, which was suspended for the second time in March. NGOCN is a China-based independent media, coving topics from labor and gender to environment and human rights. We don't violate any rules of Twitter. Our twitter account was suspended at the beginning of this month due to being flagged as a spam account by mistake and thus unsuspended on 14 March.
 
We’re writing to request for immediate unsuspension of @ngocneng and address the issue that Chinese activists’ Twitter accounts are at risks of being maliciously reported and then misclassified as spam accounts by your automated systems. Here’re links to related news covered by the New York Times:
 
[“Twitter’s Glitches Are Taking a Toll on Chinese Activists”](https://www.nytimes.com/2023/02/14/technology/twitter-china-elon-musk.html) ;
 
[“How Twitter Bots Drowned Out Posts About Protests In China” ](https://www.nytimes.com/interactive/2022/12/19/technology/twitter-bots-china-protests-elon-musk.html)
 
We’d like to urge that more information, such as reasons for suspension, and solutions on this issue should be provided in order to stop the iteration of ‘flagged as a spam account - appeal - unsuspended’. Moreover, it is necessary for you to improve non-English language moderation and protect the freedom of speech of Chinese human rights activists, as their posts has always been censored on Chinese social media platforms.
 
Thanks for your consideration.

Best,
NGOCN

![22](/Users/admin/Documents/GitHub/ipmp/static/img/twitter-2.png)
<center><font color=gray>3月20号，推特回复的邮件截图</font></center>