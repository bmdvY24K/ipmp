---
templateKey: article
title: 12.18 N记早报
slug: zao-bao
date: 2020-12-18T02:54:47Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: hekuraaa

---
**1. 浙江、湖南、江西等省份“限电”，浙江义乌关闭路灯**

中国浙江、湖南、江西今日实施“限电”措施，浙江义乌关闭路灯、写字楼灯光，对居民生活造成影响。[中国官媒新华社发文引述国家发改委解释](http://www.xinhuanet.com/2020-12/17/c_1126874945.htm)“限电”原因为工业生产快速恢复而拉动用电增长、极寒天气增加用电负荷、外受电力限制和机组故障增加电力供应困难。[财新网则引用浙江省发改委能源局电力处工作人员的说法](http://www.caixin.com/2020-12-16/101639784.html)，称浙江省最大的压力来自能源“双控”和“减煤”的国家考核。[世界新闻网报道浙江省机关管理事务局通告中也提到限电依据是](https://www.worldjournal.com/wj/story/121344/5094156)“全省能源‘双控’和‘减煤’攻坚电视电话会议”精神。《[环球时报》总编辑胡锡进否认](https://weibo.com/huxijin?ssl_rnd = 1608254806.8468&is_all = 1%22%20%5Cl%20%22_loginLayer_1608254864746)“限电”与中国拟打击澳洲对华煤炭出口有关，称此种说法“纯属瞎扯”。

**2. 法国总统马克龙确诊“2019 冠状病毒”，欧洲多名政要宣布自我隔离**

当地时间 12 月 16 日，[法国总统马克龙（Emmanuel Macron）确诊](https://www.reuters.com/article/health-coronavirus-macron/frances-macron-has-tested-positive-for-covid-19-presidency-idUKKBN28R13S) “[2019 冠状病毒](https://www.reuters.com/article/health-coronavirus-macron/frances-macron-has-tested-positive-for-covid-19-presidency-idUKKBN28R13S)”，将接受隔离治疗，期间远程履行总统职务，并取消原定于 22 日展开的黎巴嫩访问之旅。欧洲多国曾接触过马克龙的政要宣布开始自我隔离，包括西班牙首相桑切斯（Pedro S á nchez）、葡萄牙首相科斯塔（Ant ó nio Costa）等。马克龙上周出席了与其他欧洲领导人在布鲁塞尔参加欧洲峰会，本周接待过经合组织成员及法国国会领导人等。

**3. 新研究揭露数十万维吾尔族和其他少数民族被强迫在新疆棉田劳动**

[英国广播公司（BBC](https://www.bbc.com/zhongwen/simp/chinese-news-55344353))报道，[根据一项研究显示](https://cgpolicy.org/briefs/coercive-labor-in-xinjiang-labor-transfer-and-the-mobilization-of-ethnic-minorities-to-pick-cotton/)，中国正强迫数十万维吾尔族和其他少数民族人群在西部新疆地区广阔的棉田中从事艰苦的体力劳动。该研究根据网上发掘的文件，首次清晰地展示了采棉产业背后潜在的强迫劳动规模。研究称，每年有超过 50 万少数民族工人也被调往参与季节性采棉工作中，其工作环境可能具有高强迫性。这些文件包含了网上的政府政策文件和官方新闻报道。文件显示，2018 年，阿克苏及和田地区“通过劳动力转移”派出 21 万名工人，跨地区为中国准军事组织新疆生产建设兵团采摘棉花。文件中称要“引导”拾花工“自觉抵制非法宗教活动”，表明这些政策主要是为新疆的维吾尔人和其他传统穆斯林人群制定的。文件中亦有暗示可能存在的强迫问题：有报告指一些村庄的人“不愿意从事农业工作”，官员需要再次“上门做思想工作”。

**4. 《查理周刊》恐袭案 14 名嫌疑人被判罪名全部成立，判囚四年至终身**

法国最高法院于当地时间 12 月 16 日宣布《查理周刊》 14 名 2015 [年恐怖袭击案嫌犯的判决结果](https://www.aljazeera.com/news/2020/12/16/france-braces-for-verdicts-in-charlie-hebdo-attack-trial)，判决 14 人罪名全部成立。 14 名被告当中，三人仍然在逃。当局相信，三名在逃被告早在案发前后已经返回叙利亚，当中两人后来死亡，其中一人被指生前担任枪手的宗教导师，他在缺席审判下被判终身监禁。另一名在逃被告为女性，她因“资助恐怖主义活动”等罪名，缺席审判下下被判监禁 30 年。出席审判的 11 名被告，分别判刑 4 年至 30 年不等。 2015 年 1 月 7 日，三名枪手因不满《查理周刊》刊登讽刺伊斯兰先知穆罕默德的漫画，闯入其巴黎的办公室施袭，导致周刊发行人、总编辑、漫画家、专栏作家，以至到场警员等共 12 人死亡。

**5. 美国食药监局专家组支持使用莫德纳（Moderna）疫苗应对“2019 冠状病毒”**

美国食品和药品监督局（FDA）[的专家顾问组周四经过讨论](https://www.washingtonpost.com/health/2020/12/17/covid-fda-moderna-vaccine/%22%20%5Ct%20%22_blank)，投票推荐使用药厂莫德纳（Moderna）研发的疫苗应对 2019 冠状病毒。专家组以 20 票赞成， 1 票弃权的几乎一致结果投票推荐该疫苗。食药监局计划当地时间本周五授权使用该疫苗。

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno
