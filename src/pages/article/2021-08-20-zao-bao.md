---
templateKey: article
title: 8.20 N记早报
slug: zao-bao
date: 2021-08-20T01:53:47Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: 易拉宝

---
**1. 阿富汗伊斯兰酋长国建国，华春莹称“要用全面、联系的、发展的辩证思维来认识、看待和处理问题”**

8 月 19 日，塔利班官方发言人扎比乌拉·穆贾希德在推特上[宣布](https://www.zaobao.com.sg/realtime/world/story20210819-1184336)，塔利班成立阿富汗伊斯兰酋长国（Islamic Emirate of Afghanistan）。学者王明远[评论](https://mp.weixin.qq.com/s/_tXfBmbJhhMvboUAKXRSZg)称：“Emirate 是‘埃米尔国’的意思，‘埃米尔’则是伊斯兰世界政教合一领袖的一种，类似的称谓还有哈里发、苏丹等…… ‘酋长国’这种翻译法，未能反映政体的实质内涵，容易让人……以为是个部族国家。”他还表示：“塔利班坚持‘埃米尔国’的称谓，而不愿意接受全球穆斯林国家普遍采用的‘伊斯兰共和国’的称谓，这表明……这个国家的最高领袖不是世俗主义的总统，而是政教合一的埃米尔。”

阿塔高层哈希米于 19 日接受路透社[采访](https://c.m.163.com/news/a/GHOTV7K705508U92.html)时表示，塔利班最高领导人是宗教领导人，政治是宗教的一部分，而民主在阿富汗没有任何基础。他还[表示](https://cn.reuters.com/article/taliban-comments-women-role-0819-thur-idCNKBS2FK08V)，乌理玛（学者）将决定女性的工作与受教育权、穿着选择。

中国外交部发言人华春莹 19 日[回应](https://international.caixin.com/2021-08-18/101756952.html) “中方在何种条件下会承认阿塔政府为阿富汗合法政府”时称：“世界上没有任何事物是一成不变的。我们主张要用全面、联系的、发展的辩证思维来认识、看待和处理问题。”  #阿富汗

**2. 拜登称美国对台承诺与对北约盟友、日本、韩国的承诺相同，随后澄清**

当地时间 19 日，拜登在 ABC News 的[专访](https://abcnews.go.com/Politics/full-transcript-abc-news-george-stephanopoulos-interview-president/story?id = 79535643)中表示，对台承诺与对阿富汗完全不可比（not even comparable），对日本、韩国、台湾的承诺等同于对北约盟友的神圣承诺（sacred commitment）。随后，拜登办公室高级官员[澄清](https://www.reuters.com/world/asia-pacific/us-position-taiwan-unchanged-despite-biden-comment-official-2021-08-19/)称，美国对台政策未发生变化。

此前，参议院共和党资深参议员约翰·科宁（John Cornyn）17 日的推文[误称](https://www.voachinese.com/a/Chinese-media-threatens-immediate-war-on-US-troops-in-Taiwan-if-senator-figures-are-correct-20210817/6005967.html)美国在台湾有 3 万驻军。《环球时报》回应道：“如果这是真的……我们相信国家会立刻……武力收复台湾。”当日，东部战区在台湾附近举行军事演习。蔡英文在 18 日的致辞中[表示](https://cn.reuters.com/article/idCNL4S2PP1T2)：“有鉴于阿富汗情势的变化，台湾唯一的选项就是让自己保卫自己。” #台湾

**3. 中央财经委员会谈“调节过高收入”、“三次分配”**

8 月 17 日，习近平主持召开[中央财经委员会第十次会议](http://www.xinhuanet.com/2021-08/17/c_1127770343.htm)，强调“在高质量发展中促进共同富裕，统筹做好重大金融风险防范化解工作”。会议提出，要“构建初次分配、再分配、三次分配协调配套的基础性制度安排”，“合理调节过高收入”。据[《学习时报》](https://m.yicai.com/news/101144411.html)，三次分配指促进社会力量参与公益慈善。

19 日，中国央行、银保监会[约谈](https://cn.reuters.com/article/china-cen-cbirc-evergrande-debt-risk-081-idCNKBS2FK133?il = 0)恒大集团，要求其“积极化解债务风险，维护房地产市场和金融稳定”。恒大集团长期面临严重高负债，恒大地产集团在 17 日发生董事长、法人、总经理变更。 #政府监管

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
