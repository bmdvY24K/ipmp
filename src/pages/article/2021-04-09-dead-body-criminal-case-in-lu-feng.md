---
templateKey: article
title: "#N记快讯｜《寻找尸体的人》报道被删，第二作者被陆丰警方电召返乡"
slug: dead-body-criminal-case-in-lu-feng
date: 2021-04-09T07:29:12.668Z
description: null
featuredpost: false
trending: 0
isbrief: false
---

#N记快讯

**《寻找尸体的人》报道被删，第二作者被陆丰警方电召返乡**

通讯员：轰轰

4月7日，搜狐极昼工作室刊发《寻找尸体的人》一文，对推行火葬[一刀切](https://www.google.com/url?q=https://www.google.com/url?q%3Dhttps://www.swrtv.com/shanwei/bzggxczl/201207/a83174146a6d4e6cb5ae48cb8878058e.shtml%26amp;sa%3DD%26amp;source%3Deditors%26amp;ust%3D1617972677359000%26amp;usg%3DAOvVaw3G0bgfSqk2aJ_hJQngYj7y&sa=D&source=editors&ust=1617972677369000&usg=AOvVaw18fbkFHORzmYf4i7x2HrN6)政策的广东汕尾市“调包火化尸体”一案进行报道。次日晚9时许，有消息称文章第二作者、负责翻译方言的广东某大学陈同学家中父母被地方警方骚扰，陈本人被要求“今晚返回汕尾报到”并做笔录。当晚10时许，极昼工作室删除了原文报道。

4月9日上午9时许，陈同学的老师称陈同学接到陆丰警方电话，警方称“报道影响了陆丰形象，要帮助消除影响”，仍“要求他回去接受调查”，并威胁“如不回去，警方要找到学校来”。有群众致电案件事发地碣石镇的派出所询问此事，得到回复称“没有所反映的情况”。

《寻找尸体的人》的报道来源是中国裁判文书网上的[刑事裁定书](https://www.google.com/url?q=https://www.google.com/url?q%3Dhttps://wenshu.court.gov.cn/website/wenshu/181107ANFZ0BXSK4/index.html?docId%3D7be05a90df304120bd13aca700a8ff62%26amp;sa%3DD%26amp;source%3Deditors%26amp;ust%3D1617972677360000%26amp;usg%3DAOvVaw0lfmJ9qlfqSHvSgnpHJQdW&sa=D&source=editors&ust=1617972677370000&usg=AOvVaw3qmc-jCOU7P1dixqAdczwd)。判决书上显示，2017年3月1日，黄某斌向逃避火化的黄某青收取人民币10.7万元，承诺提供一则尸体代替死者火化。黄某斌后诱骗弱智人员林某仁并将其杀害，作为交易的替身尸体。3日中午，死者黄某坚出殡时，其棺木与林某的棺木调包后被秘密土葬，林某仁的尸体则被送至殡仪馆代替火化。2020年12月，广东省高级人民法院裁定黄某斌构成故意杀人罪，判处死刑，缓期二年执行。刑事裁定书显示黄某青“已被不诉”。

目前，《寻找尸体的人》原文在[澎湃新闻·澎湃号平台中](https://www.google.com/url?q=https://www.google.com/url?q%3Dhttps://www.thepaper.cn/newsDetail_forward_12083020%26amp;sa%3DD%26amp;source%3Deditors%26amp;ust%3D1617972677361000%26amp;usg%3DAOvVaw1_M0yiW-XVb156RKIw8tIP&sa=D&source=editors&ust=1617972677370000&usg=AOvVaw39duKsSvHUKikbWiUPLLm7)仍可阅读。