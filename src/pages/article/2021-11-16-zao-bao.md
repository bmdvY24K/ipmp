---
templateKey: article
title: 11.16 N记早报
slug: zao-bao
date: 2021-11-16T04:21:53Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: NL

---
**1. 张展在狱中垂危，家属提交保外就医申请**

因在 2020 年 2 月前往武汉追踪报道新冠肺炎疫情而被以“寻衅滋事罪”判刑 4 年的张展，在上海女子监狱服刑期间绝食抗议，体重下降至不到 40 公斤。今年 8 月初，监狱告知家属张展有生命危险。10 月 29 日，张展母亲与张展[视频会见](https://twitter.com/changchengwai/status/1458767696368214019)，得知张展已无法独自走路，需人搀扶。张展哥哥张举曾向上海市监狱管理局留言[请求保外就医](http://jyj.odb.sh.cn/jyapp/InquiryFocusDetail.aspx?no = Z20219111)；[张展母亲](https://twitter.com/changchengwai/status/1460275041862668289)于 11 月 15 日到上海女子监狱再次提交保外就医书面申请。

11 月 8 日，美国国务院要求中国政府[立即释放](https://www.nytimes.com/2021/11/09/world/asia/us-release-china-zhang-zhan.html)张展。数十个[国际组织](https://www.cna.com.tw/news/firstnews/202109180062.aspx)也曾发声敦促中国当局释放张展。 #张展

**2. 李翘楚在狱中幻听严重，家属申请保释被拒**

因声援 2019 年 12 月厦门聚会案被捕的公民和律师，李翘楚于今年 3 月 15 日被批准逮捕，羁押于山东临沂市人民医院东区（监管医院）。在狱中，本患有抑郁症的李翘楚出现严重幻听，因长期服药出现闭经等副作用。[据 rfa 报道](https://www.rfa.org/mandarin/Xinwen/wul1115d-11152021053029.html?fbclid = IwAR1x3JxFwgSgQf-Bu8Ib_0EGimKh-gdPhYbd5uwfElcxr2LTSo7V4zXxdPg)，李翘楚父母上月底以其身心健康状况及看守所就医条件差为由，向山东临沂检察院申请取保候审，本月 11 日收到检察院的驳回通知书。据丁家喜律师妻子[罗胜春推特](https://twitter.com/luoshch/status/1460457766741594122?s=21)称，自李翘楚被羁押九个多月，律师和家属一共为其申请了8次保释和取保候审，均被拒绝。 #李翘楚

**3. 朝鲜脱北者于吉林监狱越狱，警方悬赏金升至 50 万元**

10 月 18 日，吉林监狱发生一起朝鲜服刑人员越狱事件，越狱者朱贤健在收工时间强行脱逃，已下落不明近一个月。[长春市公安局](https://www.thepaper.cn/newsDetail_forward_15302673)发出悬赏令，14 日，警方将悬赏金额提升至[50 万元](https://www.thepaper.cn/newsDetail_forward_15385102)。

朱贤健曾因犯偷越国境罪、盗窃、抢劫罪，被判处有期徒刑十一年三个月，后获得两次减刑，本应于 2023 年 8 月 21 日刑期结束后被遣送回朝鲜。[网络流传吉林监狱 AB 门监控视频显示](https://www.youtube.com/watch?v = Xj3gW4v6MDk&t = 1s)，朱贤健先破坏监狱电网，随后从 6 米高的围墙翻出，身手矫健，相信曾受过专业训练。 #朝鲜

**4. 两名印度女记者因报道反穆斯林暴力活动而被捕**

据[半岛电视台报道](https://www.aljazeera.com/news/2021/11/15/india-journalists-arrested-for-reporting-on-anti-muslim-violence)，两名印度女记者 Samriddi Sakunia 和 Swarna Jha 因报道印度东北部特里普拉邦的大规模反穆斯林暴力骚乱，被当地警方以散播假消息为由逮捕，目前已获保释。据悉，当地警方收到来自世界印度教理事会 (the Vishwa Hindu Parishad)的书面投诉，因而对 102 人提起诉讼，其中包括记者。印度新闻界、人权团体及反对党谴责警方对新闻自由的侵犯。 #印度

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
