---
templateKey: article
title: '“The courage of one person should not be without echo”: A Mandarin demonstration in London'
slug: 'si-tong-bridge-london-protest-en'
date: 2022-12-17T03:21:57.265Z
description: '"As soon as I come to protest, and people around me do the same, then we will have a new collective memory, and a new imagination of the future. It will prompt us to no longer be complacent about the status quo and make some changes."'
featuredpost: true
featuredimage: /img/london-protest-1.jpg
trending: 10
isbrief: false
contributors:
  - type: 'Author'
    name: 'Shanshan'
  - type: 'Editor'  
    name: 'Golda'
  - type: 'Photo'
    name: 'Chestnut、tamyuca'
tags:
  - "四通桥"
  - "伦敦"
  - "海外留学生"
  - "普通话"
  - "政治表达"
  - "抗议"
  - "English"
---

**This article was co-published by WHYNOT and the NGOCN Voice Program. [The Chinese version](https://ngocn2.org/article/2022-11-08-si-tong-bridge-london-protest/) first appeared on WHYNOT, and the English version first appeared on NGOCN.**


*"I'm so jealous of the Iranian assemblies where people can dance and sing together and not be afraid to reveal their identity." Lulu said.
"As soon as I come to protest, and people around me do the same, then we will have a new collective memory, and a new imagination of the future. It will prompt us to no longer be complacent about the status quo and make some changes." Nana says.*


On Saturday 29 October, at 4pm UK time, over one hundred Mandarin speakers gathered in Trafalgar Square in central London to protest against China's current "zero-covid" policy, and to show solidarity in support of the "Beijing Sithong Bridge Protest".

The protest lasted for about two hours. People chanted in Mandarin, with demands that included opposition to excessive pandemic prevention policy, demands to restore economic livelihoods, freedom for overseas Chinese to return to China, opposition to Xi Jinping's third term in power, demands for political reform, and freedom of speech. On and around the scene, a number of participants and bystanders said that this was the first time in recent years that they had seen a political rally in Mandarin Chinese in London.

The protests that took place on October 13rd on Beijing's Sithong Bridge did not fall into silence with the CCP's extremely strict " removal of accounts" and "deletion of posts", but instead, sparked various forms of protests both inside and outside China. Outside of China, the ["poster campaign"](https://ngocn2.org/article/2022-10-26-si-tong-bridge-chinese-student/) continues to spread, and the phrase "the courage of one person should not be without echo" becomes a key slogan in many of the protests.

![22](/Users/admin/Documents/GitHub/ipmp/static/img/london-protest-2.jpeg)
<center><font color=gray> 图：instagram @tamyuca</font></center>

### A Chinese student who come alone: from standing at a distance to "going up to the front"

China-related posters and banners have been running in London for half a month, but this rally on October 29th still stood out.

It was a "decentralised" protest, initiated by the members from the Telegram chat group titled “‘My Duty’ Democracy Wall in London”. Different from previous protests, the majority of participants were from mainland China, varying in age and background, but mainly young Chinese. Most of them wore masks and hats to protect themselves. Many of the participants said that they were hiding their identities due to the worries that their activism would cause "trouble" for their families back home.

At around 4pm, on the corner of Trafalgar Square, people came to the rally and started putting up "Sitong Bridge Protest" banners in black on a white cloth. The rally started at 4.30pm. There were people who wrote "FREE CHINA" in light yellow chalk on the ground. Some people held up printed posters with slogans such as "the courage of one person should not be without echo", "Anti-dictatorship to save China", "We want to eat" and "We want votes". Some had white cloth bands tied to their foreheads with the words "The Last Generation" and "No PCR test". Others wore white protective suits with the words "IT'S MY DUTY", "NOT FOUND", and an exclamation mark in red on the back that stood for "Articles not visible", or with blood handprints all over the suit.

![22](/Users/admin/Documents/GitHub/ipmp/static/img/london-protest-3.jpeg)
<center><font color=gray> 图：instagram @tamyuca</font></center>

One Chinese student, who came alone under the pseudonym "Ning Zhitou" for safety concerns, told NGOCN that the name was referred to a Chinese poem "I'd rather die holding incense on a branch". At the beginning of the protest, she just stood and watched, but when she saw a huge black banner on a white background unfolding in front of her, she felt something inside her that she "couldn't hold back any longer", so she went forward. For the next two hours, Ning held up a poster that someone else handed out, and standed with the other participants.

### Please be brave, and write "zy" as "freedom"(“Zi You” in Chinese)

Unlike conventional rallies and protests, there was no central speaker at this event. The protest started with the voice of a woman, and the crowd could walk to the centre and take the loudspeaker to make a speech or appeal.

A man came forward wearing masks and hats and read Bei Dao's poem “An End or a Beginning: to Yu Luoke”, another man read the anonymous poem[ “Try to be brave and write 'zy' as 'freedom'”](https://www.chinesepen.org/blog/archives/176751), and a female called for people to sing "Do You Hear the People Sing?”

Elena Huang was one of the protesters. She said, "I am one of those who chant with everyone else under the dictatorship."

Elena says that when she first arrived at the rally, there weren't many Chinese people there yet and she felt scared. "I was scared to be the one to stand up and concerned whether I would be able to actually do a good job".
There were not only supporters of the "Beijing Sithong Bridge Incident", but also someone who raised the banner of "Liberate Hong Kong, revolution of our times", as well as those who came to support the human rights issues in "Tibet'' and "Xinjiang". A short distance away, Iranians were demonstrating in a huge, weeks-long "hijab revolution" against the arrest and death of Iranian girl Masha Amini for "dress code violations''.

Elena was concerned that there would be conflicts over political differences at the rally, but the situation there eventually cheered her up. "When I saw everyone work together to put up the big banner from Tibetan friends, I was not so scared. I felt an infinite amount of warmth and courage to do what came next." She said,

Standing in the crowd, and hearing the people respond to each slogan shout, Elena felt a sense of power from her heart. "That long-suppressed cry was as captivating as a firework suddenly bursting into the night sky." She said,
It is very rare for Chinese people to organise protests overseas, so it was inevitable that they would be inexperienced. But what Elena didn't expect was how well organised everyone was. There was someone who helped set up the venue, distribute leaflets, use professional devices to film the protest, mediate disputes, and wait for others to leave. Everything went smoothly.

What touched her most was that, in the absence of any speakers, everyone spontaneously picked up the loudspeaker to make their voices heard. "It was very picturesque to watch people being willing to come together for the same goal." She said, "I was moved by the Chinese people's will to stand up, I was moved by the Hong Kong people's will to come and support, I was moved by the big banner that the Tibetans had worked so hard to deliver, and I was moved by the Iranians who came to hear our voices."

But Francis, who was also in the front row, held a different view. He joined the crowd at the beginning of the protest and was one of the few Chinese present who did not wear a mask to cover his face. "I don't wear a mask to be a dignified Chinese person," Francis said.

Francis said that during the Shanghai lockdown this year, his WeChat account was blocked for reposting a poem. But when he stood on the spot and witnessed people who gathered with various demands, he showed some discomfort. He argued that he hoped to build a democratic China, but he opposed Hong Kong's independence. "I feel that our hard-won light of democracy is being robbed by them," Francis said.

![22](/Users/admin/Documents/GitHub/ipmp/static/img/london-protest-5.jpg)
<center><font color=gray> 图：@Chestnut</font></center>

### “Taking actions does take away the fear they thought they had."

For Elena, this rally meant a lot. "This is a moment that symbolises that the Chinese do not want to be oppressed anymore. This moment has an extraordinary impact both for the world and for China." Elena said, "For the world, people would see that the Chinese people are not apolitical anymore. Even in a ‘vacuum’ like China, people are still reaching out to the world with one voice after another.”

"For China, while the impact may not be great in the short term, at least the CCP will be informed that we are not afraid to stand up! I don't really dare to comment on what changes such a rally will achieve, but I believe that demonstrations like this will give the Chinese people more courage to make a difference."

Elena hopes that this demonstration will convey a message to everyone that "despite the difficulties, we still managed to make our voices heard. If that message gets through, then similar protests will continue to happen in the future.”

Meanwhile, in the middle of the crowd, Nana, a Chinese student in the UK, shared similar feelings. “This was really one of the largest demonstrations initiated by mainland Chinese and it was mainly centred on the political demands of mainland Chinese." Nana said.

“What's significant about being involved in this kind of activity is that every action you take is making an impact on others, and it's very broad and deep", said Nana, "like if you have the courage to chant the first line, someone will follow your next line. You go out on the street or you take actions on other things, then your friends will know, too, that it doesn't appear to be as scary as you might think. I also hope that my friends around me will be able to see that taking actions does take away the fear they thought they had."

"As soon as I come to protest, and people around me do the same, then we will have a new collective memory, and a new imagination of the future. It will prompt us to no longer be complacent about the status quo and make some changes." Nana says.

Xiaodong was a bystander in the crowd. He thought that the Chinese protest in the square looked inconspicuous compared to the large scale of Iranian protest next to him. "But it still feels like a step forward to see so many Chinese willing to stand up and speak out." Xiaodong said. Xiaodong believes that before such events, many Chinese may have had political depression, but such events can "let like-minded people know that they are not alone."
“That’s the change, I guess.” Xiaodong said.

Coco, a Chinese student in the UK, told NGOCN she was rather pessimistic about the impact the protest would have. "It feels like people are still mostly constrained too, though this is the first time after all", she said, "Something is better than nothing, and symbolism also still has a tangible meaning. Maybe more events like this will have a cohesive and centripetal effect, and will bring about some change a little deeper down."


### "I had a great time subverting state power with my sisters."

![22](/Users/admin/Documents/GitHub/ipmp/static/img/london-protest-7.jpg)
<center><font color=gray> 图：@Chestnut</font></center>

After 5pm UK time, Nana and two other female friends walked from the crowd to the middle of the venue, where they bought three chains at their own expense and chained themselves to each other, to emulate the “chained women”. This ironic move represents the protest against the [“Xuzhou chained woman incident”](https://zh.wikipedia.org/wiki/%2525E4%2525B8%2525B0%2525E5%25258E%2525BF%2525E7%252594%25259F%2525E8%252582%2525B2%2525E5%252585%2525AB%2525E5%2525AD%2525A9%2525E5%2525A5%2525B3%2525E5%2525AD%252590%2525E4%2525BA%25258B%2525E4%2525BB%2525B6) that came to light on social media in January this year.

In the field, they wished to embody a wider range of elements through posters and actions, to reflect broader and more universal issues in Chinese society. 

![22](/Users/admin/Documents/GitHub/ipmp/static/img/london-protest-8.jpg)
<center><font color=gray> 图：@Chestnut</font></center>

Nana said she had observed divisive and contradictory voices in the Telegram chat group before she attended the rally. "For example, I saw people who didn't support Hong Kong, who didn't support Taiwan, or who attacked feminism directly. Some would insult political leaders by feminising the target of their insults. This kind of behaviour is very misogynistic and makes me feel very uncomfortable. Because of these things, I don't have as much trust in the people who were there with me."

Nana decided to create a space with her feminist friends that would make women more comfortable. “To attend such demonstration, all we wanted to do was to prepare materials with our feminist sisters and develop more creative and memorable movement together.”

"Many democratic movements may not focus as much on women's issues", said Nana, "I particularly hope that, at a social movement like this, people will see that there are many women speaking out, while women are facing a unique gender dilemma in Chinese society. I speak out for feminist issues, and that's something which I'm committed to. As long as I am there, feminist issues have to be there."

As a result, Nana and her feminist sisters spent a lot of time designing such social movement. Nana said, "I felt a lot of support from my sisters and it was a very safe space. Everyone was supportive and had mutual understanding. Many of my ideas were based on their actions, which were very well done."

Lulu is one of the feminists in the group. She said that before the protest started, she had heard about the misogynistic comments in the chat group and cutting of ties with Hongkonger. Therefore, at the beginning of the protest, she feared that she would encounter not only those who opposed the protest, but also those in the group who had made misogynistic comments. But then, as more and more feminist supporters stood out, she gained more courage. She said, "I felt not so scared when I was making blood handprints with my friends who also support the feminist movement, shouting the slogans on Siting Bridge with protesters, and chanting 'stand with the oppressed everywhere'.”

Coco, a woman and feminist, also expressed her views on female participation in the protest. She said it was the first time she had participated in a protest “related to home country”, and Coco felt that there was a large amount of female participation and ownership in the protest, “Before, (the issue of) women's oppression was an argument against dictatorship, but it was never the mainstream. And (women) were represented”, but now, “I’m happy to see us standing up to express ourselves, and taking a very dominant stance.”


![22](/Users/admin/Documents/GitHub/ipmp/static/img/london-protest-10.jpeg)
<center><font color=gray> 图：instagram @tamyuca</font></center>

### "When can we have a social movement on the similar scale as the Iranians’?"

The protest ended with a final round of chants at around 6pm. By this time, the Iranian demonstrations on the side were still on fire. Several participants at the scene said that they were “envious of the Iranians' resistance”.

"I'm so jealous of the Iranian assemblies where people can dance and sing together and not be afraid to reveal their identity." Lulu said.
Nana says: "My hope for the future is that, at least in the UK, we can have social movements on the scale of the Iranian demonstrations. I really hope we have such large demonstrations too, and scenes where a number of men are speaking out for women.”

“In the UK, many Chinese people are still apolitical, and either don't care or are ‘pinky’. I'm particularly keen to influence this group of people, to connect with more potential activists, to show them that it's not so scary to be an activist. Because if you don't care about politics, at some point in the future, you're bound to get caught up in political issues as well.”
At around 7pm, the crowd largely dispersed, but a few people remained to discuss the demonstration. As the final group of participants were about to leave, someone came up to warn that there was a Asian male with sunglasses nearby who had been standing around looking "suspicious", and advised everyone there to leave.

After the protest, Ning Zhitou returned home. In response to a question about why she had courage to come to the rally alone, she said it was the lone man who protested on Beijing's Sithong Bridge that motivated her. "He dared to show up in Beijing alone, and I was thousands of miles away from home, so why couldn't I do it?"

At the end of the night, several participants on-site expressed their regret at not having their own protest song. "When are we going to have our own song? Whilst censorship makes it hard for such pieces to come out, even sometimes the lyrics of China's national anthem are censored."

"And who are we really? China's history, geography and culture span so much that can there really be one song or one slogan that speaks for everyone? And who are we to decide for anyone else?" Lulu said.


(At the request of the interviewees, the names in the article are all pseudonyms)

Please help us to share with this sroty.