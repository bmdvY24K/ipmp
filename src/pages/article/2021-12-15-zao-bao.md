---
templateKey: article
title: 12.15 N记早报
slug: zao-bao
date: 2021-12-15T07:38:25Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: 小咚

---
**1. 天美工作室游戏开发专家毛星云离世，腾讯确认死讯**

据[红星新闻](https://www.163.com/dy/article/GR6PQ86C051492T3.html)报道，12 月 14 日腾讯确认旗下天美 F1 工作室游戏专家、引擎组组长毛星云于 12 月 11 日上午意外身故。腾讯方面称，已于当天第一时间成立了应急小组，协助家属处理后续事宜。腾讯也于 12 月 13 日向毛星云所在部门其他同事[通发邮件](https://www.yinsfinance.com/article/137433)，其中提到今年 8 至 9 月，毛星云曾住院休养，复工后积极投入工作，“没想到最终发生如此不幸的事件”，疑似曾患抑郁。多名网友至[毛星云知乎专页](https://www.zhihu.com/people/mao-xing-yun)留言悼念。

关于毛星云意外身故原因，腾讯未加以解释。[澎湃新闻引述一名前腾讯员工说法](https://finance.sina.com.cn/tech/2021-12-14/doc-ikyakumx4139941.shtml)，猜测毛星云项目组可能因未达年终绩效目标而自杀，也有[传言称](https://www.ettoday.net/news/20211215/2146645.htm)其与高层协调无果，最终跳楼身亡。包括[触乐网创始人](https://wallstreetcn.com/articles/3647275)在内的多名游戏行业从事者否认其离世原因与绩效有关。

**2. 藏族学生于奥委会前抗议 2022 北京冬奥会**

据[路透社报道](https://www.reuters.com/lifestyle/sports/tibetan-students-lock-themselves-olympic-rings-protest-beijing-games-2021-12-11/)，当地时间 12 月 11 日，两名藏族学生于国际奥委会总部门前抗议中国侵犯少数民族人权。此次抗议针对即将举行的 2022 年北京冬奥会，两名藏族学生将自己锁在五环标志上，不满奥委会听任中国政府借由奥运掩饰“文化种族灭绝暴行”。据悉，美国已于本月宣布拒绝派政府官员参加北京冬奥会，早前欧洲西藏青年协会（TYAE）和自由西藏学生组织成员也曾在国际奥委会大楼静坐抗议。#西藏

**3. 香港圆玄小学上课播放南京大屠杀纪录片引发部分小学生恐惧**

[据环球网引述星岛日报](https://china.huanqiu.com/article/45yHd9ezqUx)，保良局香港道教联合会圆玄小学 9 日在“多元智能和德育课程”课堂上，向学生播放长约 5 分钟有关南京大屠杀的纪录片。[香港 01](https://www.hk01.com/%E7%A4%BE%E6%9C%83%E6%96%B0%E8%81%9E/711597/%E5%9C%93%E7%8E%84%E5%B0%8F%E5%AD%B8%E6%92%AD%E5%8D%97%E4%BA%AC%E5%A4%A7%E5%B1%A0%E6%AE%BA%E7%89%87%E6%83%B9%E8%AD%B0%E4%BB%8A%E5%AE%89%E6%8E%92%E8%BC%94%E5%B0%8E-%E5%AE%B6%E9%95%B7-%E5%BF%98%E5%8F%B2%E7%AD%89%E5%A6%82%E8%83%8C%E5%8F%9B%E9%81%8E%E5%8E%BB)引述学生回忆，称影片中有“枪毙”等情节。低年级学生产生较大情绪反应，如哭泣、惊慌不安、大喊等，引发家长投诉。校方回应此举响应香港教育局指引，并在接收家长反馈后表示将向学生提供情绪辅导。香港教育局称，“历史就是历史，不能回避”，学校应选择合适片段作为教材。#南京大屠杀 #教育 #香港

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
