---
templateKey: article
title: 'N记快讯｜联合国为张展发声，促中国政府立即无条件释放'
slug: 'un-urges-china-to-free-zhang-zhan'
date: 2021-11-22T03:21:57.265Z
description: '自张展被捕开始，对她的声援从未停止。在她的健康危殆之际和外界强烈声援下，家属和律师仍无法进行会见和获得保外就医的回应，这让人更为担心她目前情况。'
featuredpost: true
featuredimage: /img/zhang-zhan.png
trending: 10
isbrief: false
contributors:
  - type: '报道'
    name: 'Mike'
---

11月19号，[联合国人权事务高级专员办事处](https://news.un.org/zh/story/2021/11/1094702)公开发表谈话，对中国公民记者张展急剧恶化的健康状况表示担心，并呼吁中国政府出于人道主义考虑，立即无条件释放张展，在尊重她的意愿和尊严的情况下提供紧急救生医疗服务。 

但据[法广网](https://www.rfi.fr/tw/%E4%B8%AD%E5%9C%8B/20211120-%E8%81%AF%E5%90%88%E5%9C%8B%E6%95%A6%E4%BF%83%E4%B8%AD%E5%9C%8B%E9%87%8B%E6%94%BE%E5%BC%B5%E5%B1%95-%E5%8C%97%E4%BA%AC%E8%AA%AA%E5%B0%8D%E6%AD%A4-%E9%8C%AF%E8%AA%A4-%E8%88%87-%E4%B8%8D%E8%B2%A0%E8%B2%AC%E4%BB%BB-%E6%86%A4%E6%85%A8)报道，在这个谈话公开发布的第二天，中国驻日内瓦代表团就批评联合国这个谈话“不负责任”和“错误”，并“表示愤慨”。

38岁的张展曾在新冠疫情爆发之初，即2020年2月前往武汉市进行相关报道。在当局持续威胁下，坚持通过[社交媒体直播](https://www.youtube.com/channel/UCsNKkvZGMURFmYkfhYa2HOQ/featured)当地疫情情况。但同年5月随即被捕，并在同年12月，因“寻衅滋事罪”被判处有期徒刑四年。在狱中张展曾多次进行绝食，以抗议自身遭受的不公判刑和不当待遇，并因此被狱方捆绑和强制喂食。

持续的绝食让她的身体状况急剧恶化。今年8月初，狱方已向家属下达病危通知。10月29日，张展母亲与张展[视频会面](https://twitter.com/changchengwai/status/1458767696368214019)，得知张展已无法独自走路，需人搀扶。张展哥哥[张举10月30日在推特](https://twitter.com/Jeffreychang81/status/1454379245048795140)上说：张展身高177cm，目前体重不足40kg。她那么倔强。我觉得她可能活不了太久了。 同时，张展的家人一再向狱方申请保外就医，本月15号狱方接收了张展家人的保外就医书面申请，但却一直未获回应。张展律师曾在10月13日提交会见申请，同样一直没有回应。

张展的病危消息始终牵动着国内外社会的关注。除了联合国近日的公开发声外，[美国国务院](https://cn.nytimes.com/usa/20211110/covid-china-zhang-zhan/)和[德国外交部](https://www.dw.com/zh/%E5%BE%B7%E5%9B%BD%E6%94%BF%E5%BA%9C%E5%91%BC%E5%90%81%E5%8C%97%E4%BA%AC%E9%87%8A%E6%94%BE%E5%BC%A0%E5%B1%95/a-59836487)也在11月相继发声，要求中国政府立即无条件释放张展。[无国界记者（RSF）](https://rsf.org/en/news/chinese-journalist-palestinian-journalist-and-pegasus-project-receive-2021-rsf-press-freedom-awards)在11月17日宣布张展成为今年新闻自由奖勇气奖得主，并促请中国立即释放张展。女性新闻工作者联盟(CFWIJ) 近日发起释放张展的[紧急呼吁联署](https://twitter.com/CFWIJ/status/1461700223516954626)，目前已有超过50个机构、个人签名。而人权机构[国际特赦](https://www.amnesty.tw/civicrm/event/register?reset=1&id=28&nid=10215)鼓励公众通过线上或亲笔等方式为张展写信，[人权观察更呼吁](https://www.hrw.org/news/2021/11/04/china-release-gravely-ill-activist)外国驻华使馆人员要到监狱探视张展情况……

而在国内，有公民发起《给张展全面身体检查和紧急救治的呼吁书》联署活动，至今已有超过[500人联署](https://twitter.com/changchengwai/status/1462120036747911176)。也有人号召公众可以把呼吁书和联署名单打印出来，寄给上海市司法局局长和上海市女子监狱监狱长，以改善张展目前在狱中的处境。

自张展被捕后，对她的声援从未停止。在她的健康危殆之际，外界的呼吁更为强烈，但当局对张展家属和律师的各种合理申请采取已读不回的态度，让人更为担心她目前状况。除了张展以外，今年9月原计划到英国读书的独立记者黄雪琴在出发前[被迫失踪](https://ngocn2.org/article/2021-10-26-huang-xue-qin/)，新京报原社长[戴自更](https://www.rfa.org/cantonese/news/prison-11182021054708.html)也在最近被传出判刑八年，一系列事件折射出中国目前的新闻环境更为严峻。