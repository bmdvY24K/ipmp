---
templateKey: article
title: '律师常玮平，一个不见容于国家的人Intolerable to the State: Lawyer Chang Weiping'
slug: 'lawyer-chang-weiping'
date: 2021-09-07T03:21:57.265Z
description: 'When Chinese lawyer Chang Weiping was released on bail for 10 months，he published 216 videos documenting his daily life. He says he treasures every day that he can be seen and heard, and hopes that he can find a way to live his life so that it still has value.'
featuredpost: true
featuredimage: /img/chang-wei-ping.jpeg
trending: 10
isbrief: false
contributors:
  - type: 'Author'
    name: '夜火'
tags:
  - "human rights lawyer"
  - "Chang Weiping"
  - "Chen Zijuan"
  - "Xiamen Case"

---

编者按：本文由端传媒与NGOCN声音计划联合出品，首发于端传媒。
Editor’s Note: Initium Media co-published the article with NGOCN. The article first appeared in Initium Media.



216个视频，是中国律师常玮平在取保候审的10个月里录制的生活日记。他说他很珍惜每一个还能被看到和被听到的日子，希望探索一种还有价值的存在的方式。

今年37岁的常玮平，在2020年1月12日因危害国家安全被指定居所监视居住10天。之后以煽动颠覆国家政权罪被取保候审，软禁在他陕西凤翔老家近10个月。

When Chinese lawyer Chang Weiping was released on bail for 10 months，he published 216 videos documenting his daily life. He says he treasures every day that he can be seen and heard, and hopes that he can find a way to live his life so that it still has value.

Chang is 37 years old this year. On January 12, 2020, he was placed under residential surveillance at a designated location for 10 days for endangering national security. Afterwards, he was released on bail pending a trial for inciting subversion of state power, and was under house arrest for 10 months in his hometown of Fengxiang County, Shaanxi Province.

“我当然无罪。记录，就是我的态度。”视频里和生活中，他都多次强调。他认为自己有义务为研究这一阶段中国律师生存状态、法治及人权状况的调查提供真实资料。2020年10月16日的视频里，他罕见地发表了总结和声明，揭露自己在指定地点监视居住期间遭遇的酷刑：连续10天被铐在老虎凳上，不能睡觉，导致他右手拇指和食指至今麻木无知觉，每天只能吃一碗面条汤和一个鸡蛋大小的馒头，疲劳审讯，共做笔录16份。

视频日记在2020年10月21日戛然而止。第二天，他再次被以“涉嫌煽动颠覆国家政权”罪名指定居所监视居住，后于2021年4月7日被逮捕，关押在宝鸡凤县看守所。

“Of course, I am innocent. Recording everything, this is my approach.” In his videos and during his life, he has stressed this many times. He believes he has the duty to provide real information to investigate the state of survival of lawyers, the rule of law and the human rights situation in China. In the October 16, 2020 video, in a rare move, he made public a summary and statement revealing the torture he experienced during his time under residential surveillance: he was strapped to a tiger chair for 10 days, and prohibited from sleeping which led to his right thumb and index finger being numb even to this day. Every day, he was only allowed to eat a bowl of noodles and a steamed bun the size of an egg. He was interrogated even when he felt fatigue and did around 16 transcripts.

The video diaries came to an abrupt end on October 21, 2020. On the second day, he was placed under residential surveillance once again for “suspicion of inciting subversion of state power”. On April 7, he was arrested and detained at the Feng County Detention Center in Baoji City.

“我做的事情，是一个律师的职责，也是一个公民的社会义务，即便不被奖励，也不应该被如此对待。”常玮平在总结视频里说，他唯一感到遗憾的是，自己做的还不够多，对波及的家人和朋友尤其愧疚。

“What I’ve done is a lawyer’s duty and a citizen’s social obligation. Even if I don’t receive a prize, I still should not be treated in such a way.” Chang said in his summary video. He felt most guilty that he hadn’t done enough and that he had affected his family and friends.

**支持#MeToo的男律师**

**A male lawyer supporting #MeToo**

无力感，愤怒感，羞耻感。陈紫娟一口气说出半年来她与陕西省司法部门打交道的感受。

陈紫娟是常玮平的妻子。丈夫2020年10月22日再次被关押后，陈紫娟在推特、微博等平台注册了以“常玮平律师的妻子”为名的账号，声援丈夫，记录自己的维权遭遇。

Helpless, angry, ashamed. Chen Zijuan in one breath spoke about her feelings regarding her dealings with the Shaanxi Province Justice Bureau in the past six months.

Chen is Chang’s wife. On October 22, 2020, after her husband was detained again, she registered an account on Twitter and Weibo under the name “Lawyer Chang Weiping’s wife”, calling for the release of her husband and also recording the obstacles she faced with rights defense.

“这些写着『人民』的机构部门，都无比宏伟壮观，可人民却连门都进不去。”陈紫娟和律师多次去陕西的公安局、检察院、监察委等部门依法控诉常玮平在此前的监视居住中遭遇的酷刑，被无视和驱赶。

过程中，陈紫娟加深了对丈夫的了解。在维权律师群体中，常玮平即便不是最温和的，也是十分温和的。他只是相信并且尝试通过个案维权，逐步有序地推进中国法治。

“Departments that have ‘people’ written in their names are incredibly grand and magnificent, yet ordinary people can’t even get through the door.” Chen and her lawyers have repeatedly gone to Shaanxi’s Public Security Bureau, the Procuratorate, the Supervisory Commission and other related departments to legally complain about the torture that Chang suffered when he was under residential surveillance previously. Yet they were ignored and driven away.

In the process, Chen began to understand her husband more. In the rights defense lawyers’ group even though Chang is not the most moderate one of them in terms of his views, he is still quite moderate. He believes he can try to advance the rule of law in a gradual and orderly manner through rights defense on a case-by-case basis.

不是一个反抗者。常玮平多次明确自己的定位。虽然以维权律师自居，但他不仅关注政治敏感性的案子，还把更多精力放在维权圈里不太在意的性别歧视、就业歧视等公益案上。

2017年，青年刘元因体验出艾滋病被贵州茅台公司拒绝录用。常玮平作为代理律师，通过媒体动员获得公众支持。同年4月，又推动51名律师联署致信中国人社部和衞计委，建议删除《公务员录用体检通用标准》里与“艾滋就业歧视”有关的规定，保障艾滋病感染者和乙肝病毒携带者平等的就业权利。

He is not a resister. Chang has made his position clear many times. Even though he presents himself as a human rights lawyer, he not only focuses on politically sensitive cases. He also puts a lot of effort into public interest cases which have received less attention in the human rights space such as gender discrimination and employment discrimination.

In 2017, Liu Yuan was refused employment at Kweichou Moutai Co. Ltd. because he was diagnosed with HIV/AIDS. Chang, as his lawyer, was able to mobilize support through the media. In April of the same year, he sent a letter with signatures from 51 lawyers, to China’s Ministry of Human Resources and Social Security and the National Health Commission, suggesting to delete regulations regarding HIV/AIDS employment discrimination in the “General Standards for Civil Service Recruitment Examination”. He also urged them to ensure equal employment rights for those with HIV/AIDS and Hepatitis B.

常玮平为刘元赢回了工作和精神抚慰金5万元。这个案子也成为2018年《民事案件案由规定》中增加“平等就业权纠纷”案由以来的中国平等就业第一案。

反歧视的案子既可以维护个人权益，又可以激活政府功能，逐步改善政策。常玮平发现这些反歧视案子似乎更能推动人们的权益意识觉醒，更加积极投身艾滋歧视、乙肝歧视、性别歧视、性骚扰等案件中。

Chang was able to win Liu Yuan his job and also 50,000 RMB in emotional compensation. The case also became the first equal employment case in China since the addition of the “equal employment rights disputes” to the “Cause of Action of Civil Cases” in 2018.

Anti-discrimination cases can defend an individual’s rights and can also push the government to gradually improve its policies. Chang discovered that these anti-discrimination cases can also promote people’s rights awareness. He then became more active in cases related to AIDS discrimination, hepatitis B discrimination, gender discrimination, sexual harassment among others.

2018年，发轫于美国的#MeToo运动蔓延到中国。据端传媒记录，2018年1月1日到2018年10月媒体曝光的#Metoo案子就有36例。常玮平和几位女权主义者组成一个反性暴力援助小组，为幸存者提供法律援助。他成了维权律师中少有的公开站出来支持#MeToo运动的男律师。

后来，这个小组里多人不同程度上被失去人身自由。常玮平取保候审后重新加入，在小组打趣，“这个小组明明在帮人，罪犯成分却有点高，犯罪率都三分之二了。”

In 2018, the #MeToo movement that began in the US also took off in China. According to Initium Media, from January 1, 2018 until October 2018, the media exposed 36 cases related to #MeToo. Chang and several feminists established an anti-sexual violence assistance group to provide legal aid to victims. He became one of the few male lawyers among rights defense lawyers that publicly came out in support of #MeToo.

Afterwards, several individuals in this small group lost their freedom to varying degrees. When Chang was released, he rejoined the group and joked that “This group is just helping people, yet the crime rate is so high. Around 2/3rds of us have committed crimes.”

在以男性为主体的维权律师中，常玮平性别意识比较好，也算个另类。律师朋友小茗记得，他们同在的律师群里，每次有关性别的话题，总会有人去@常玮平，“这个问题，常律师怎么看？”有同行直接警告同行，“常玮平是搞#MeToo的，我们离他远一点。”

常玮平不在意，自嘲只是“底气不足的伪女权主义者”。他做过不少职场歧视的案子，了解性别歧视、性骚扰等给女性带来的不公和伤害。

Among the human rights lawyers who were predominantly male, Chang’s gender awareness was particularly good which is considered rare. His lawyer friend Xiao Ming remembers that they were in the same lawyer group and whenever there were topics related to gender, there would be someone who would always tag Chang and say: “What does Lawyer Chang think about this question?” There were some that directly warned others that: “Chang is engaged in #MeToo, let’s stay away from him.”

Chang didn’t care, and laughs at himself as a “pseudo-feminist that lacks confidence”. He has taken up many cases related to employment discrimination and understands the injustices and harms brought to females by gender discrimination and sexual harassment.

2017年，厨师高晓应聘广州一酒楼时被以“我们只招男的，不招女的”为由拒绝。常玮平指出，中国家庭里大多是女性掌厨，怎么到了酒楼，就说女性不行了？这是职业歧视。刚好，高晓是爱做饭、能吃苦、还能回怼性骚扰的女权主义者，两人一拍即合，先是告了酒楼，后再告广州市人社保的不作为。过程中，常玮平认识了一群穿着“女权主义者长这样”文化衫的女权主义者，更加了解了她们行动主义理念。

2018年3月7日，《女权之声》遭遇封号。编辑熊婧找到常玮平，起诉新浪微博和腾讯微信，之后经常跟他谘询和讨论性骚扰案子。她看过常玮平对待幸存者的耐心，也读过常玮平写的“一个女人系列”文章，发现常玮平“是维权律师中难得能用心倾听女性声音的人”。

In 2017, chef Gao Xiao was rejected during recruitment at a restaurant in Guangzhou for the reason that they only recruit men and not women. Chang pointed out that most Chinese families have women who do most of the cooking, so why is it that in restaurants they say women aren’t able to? This is employment discrimination. It also happened that Gao Xiao is a feminist that enjoys cooking, can take hardships and also hates sexual harassment. The two of them hit it off immediately, and they firstly sued the restaurant and later sued the Guangzhou Human Resources and Social Security for their inaction. During the process, Chang got to know a group of feminists wearing “Feminists look like this” cultural shirts and began to understand more about their activist philosophy.

On March 7, 2018, “Feminist Voices” was censored. The editor, Xiong Jing reached out to Chang who then sued Sina Weibo and Tencent, and afterwards regularly consulted and discussed sexual harassment cases with Chang. She saw how patient Chang was with victims, and through Chang’s “One Woman Series” articles, she discovered that Chang is “one of the rare human rights lawyers who actually listens to the voices of women with care”.

2018年7月独立记者黄雪琴因报导中山大学人类学教授张鹏性骚扰事件，被张鹏要挟控告名誉侵权。常玮平第一时间给黄雪琴发去信息，“要是你被告了，我免费帮你打。”

常玮平再次被抓后，2021年3月8日国际妇女节，女权主义者把常玮平评为“#MeToo里的男律师”。办案国保（注：全称“国内安全保衞”，是中国人民警察的一种警种）多次提示陈紫娟，“常玮平时常住在别人家”，“常玮平和女权关系亲密”。后知后觉的陈紫娟说起来就笑，“他们想挑拨我们夫妻关系，但我从高中就认识常玮平，太了解他了，他志不在此。”

In July 2018, independent journalist Huang Xueqin was blackmailed by Zhang Peng who had sexually harassed her. Zhang was an anthropology professor at Sun Yat-Sen University and he had threatened to sue her for defamation. Chang was the first to send Huang Xueqin a message saying: “If he sues you, I’ll help you fight this case for free.”

When Chang was detained again, on International Women’s Day, March 8, 2021, feminists named Chang a “male lawyer in #MeToo”. China’s national security who were handling the case told Chen many times: “Chang is regularly at other people’s homes”, “Chang has a close relationship with feminism.” In hindsight, Chen laughed and said, “They want to try and divide us as a couple, but I have known Chang since high school and know him too well; he doesn’t aspire to be such a person.”

常玮平坦言，从维权律师转向更为温和的公益律师，一是法律空间被严重压缩，维权律师备受打压，曾经的维权方式不再可行，二是他看到维权圈某些言行举止并不符合人权捍衞者的要求，圈里的性骚扰、歧视、家暴等现象也让他反思圈子文化；三是，他承认自己怂了，并没有做好“将牢底坐穿”的思想准备，他希望守住底线的同时，留得青山在不愁没柴烧。

Chang acknowledged that he changed from a human rights lawyer to a moderate public interest lawyer for several reasons. Firstly, the legal space has been shrinking, and human rights lawyers have been experiencing crackdowns and it was no longer feasible to use the same ways they did to defend human rights. Secondly, he has seen that certain words and actions within the human rights circles did not meet the requirements of human rights defenders, and the phenomenon of sexual harassment, discrimination and domestic violence within the circle made him reflect on the culture. Thirdly, he admitted that he was terrified and was not ready to be put in jail. He hopes that by maintaining his principles at the same time, he can endure the setbacks and that as long as he lives, he has hope.

**维权律师的最高峰和最低谷**

**The Peaks and Valleys of Human Rights Lawyers**

曾经的常玮平没有“怂”和“怕”的概念。在一分享会上，他坦言，即便2015年709大抓捕时近百名维权律师被关押或传讯，他也没怕过。当时，发现自己不在抓捕的名单里，常玮平还“感到有些失落，似乎一向以来的工作不被党国认可。”

那是初生牛犊不怕虎。同行好友冷林说常玮平是少有的“一踏进律师圈，就做维权律师”。

2011年，常玮平通过国家司法考试。2013年，他取得律师执照。同年，法学家、宪政学者许志永和商人王功权等人提出主旨为“自由，公义，爱”的新公民运动，发起教育平权、要求官员财产公示等系列行动。运动刚发起就被中国官方严格监控和压制。当年，许志永和王功权都被刑拘、审判。

Previously, Chang had no concept of “weakness” or “fear”. In a sharing session, he confessed that he was not afraid even when several hundred human rights lawyers were questioned and detained during the 709 Crackdown in 2015. At that time, he realized he was not on the list to be arrested. Chang felt at a loss, as it seems as though his work all along had not been acknowledged by the Party or the state.”

The young are fearless. His fellow friend Leng Lin said that Chang was one of the few who became human rights lawyers as soon as they entered the legal field.

In 2011, Chang passed the national judicial examination. In 2013, he received his lawyer license. In the same year, jurist and constitutional scholar Xu Zhiyong and businessman Wang Gonquan launched the New Citizens’ Movement with the main themes being “freedom, justice and love”. They initiated a series of actions such as equal rights in education and the disclosure of the assets of officials. The movement was strictly monitored and suppressed by Chinese officials as soon as it was launched. In that year, Xu Zhiyong and Wang Gongquan were both detained and tried.

为了支持新公民运动，为被抓的公民提供开放的、协作式的法律服务，2013年9月，知名律师唐吉田、江天勇、王成提议成立中国人权律师服务团，开始风风火火地参与个案维权。冷林和常玮平同时加入，成为第一批的维权律师。人权律师服务团备受关注，一度成为人权、勇气、死磕的象征，最高峰时团里有三百多位律师。

冷林记得，2014年初他和常玮平参加律师陈建刚的孩子的喜酒会, 维权律师就有六桌，一桌十人，集中了国内最活跃、最知名的维权律师。大家举杯庆祝律师团的壮大，要推动社会进步，“当时的我们都意气风发，根本不会怕，也没有怕的概念。我们热热闹闹地成立，光明正大地做事，多方协作，代理敏感案子，发表声明，开研讨会，签署联名信，一切看起来都充满希望。”冷林说。

In order to support the New Citizens’ Movement and to provide open and coordinated legal services to arrested citizens, in September 2013, prominent lawyers Tang Jitian, Jiang Tianyong and Wang Cheng suggested to establish the Chinese Human Rights Lawyers Service Group. They started to energetically participate in rights defense cases. Leng Lin and Chang also participated and were the first group of rights defense lawyers. The group received a lot of attention, and became a symbol of human rights, courage, and struggle. At its peak, there were more than 300 lawyers in the group.

Leng Lin remembered that at the start of 2014, he and Chang participated in lawyer Chen Jiangang’s child’s celebration. There were six tables of human rights lawyers, 10 to a table, with the most active and well-known rights lawyers at the time attending. Everyone raised a glass to celebrate the growth of the lawyers’ group and to the promotion of social progress. “At that time, we were high-spirited; we weren’t afraid at all, and didn’t have the concept of fear. We established the group with a lot of enthusiasm and did things openly, collaborated with many parties and represented sensitive cases, issued statements, held discussion sessions, signed joint letters and everything looked full of hope.” Leng Lin said.

作为为数不多的八零后青年律师，冷林和常玮平看到那样的景象十分感动。性格里带着乐观因子，从未被残酷现实碾压过的常玮平更是如此。2013年初，北京公民李蔚因与律师丁家喜共同参与 “官员财产公示”活动，并积极组织公民聚餐、营救黑监狱里的访民、接受外媒采访以及组织申请游行示威等活动，被以涉嫌“非法集会罪”刑事拘留，常玮平成为李蔚的代理律师。之后，常玮平介入了近十起公民和访民的寻衅滋事案。

“那时候公民还可以申请游行，可以要求官员财产公开，现在呢？”冷林感慨，“常玮平一踏入，就是维权律师的最高峰和最低谷。”

As one of several young lawyers born in the post-80’s, Leng Lin and Chang were moved by what they saw, particularly for Chang who was optimistic and had never been crushed by harsh realities. At the start of 2013, Beijing resident Li Wei was detained for “unlawful assembly” after he and lawyer Ding Jiaxi had participated in the campaign regarding “disclosure of officials’ assets”. They had actively organized citizen dinners, rescued petitioners from black jails, gave interviews to foreign media and organized the application for demonstrations. Chang represented Li Wei in the case. Since then, Chang has been involved in a dozen cases related to residents and petitioners where they were accused of “picking quarrels and provoking trouble”.

“At that time, citizens could still apply for demonstrations and request officials to disclose their assets. But now?” Leng Lin sighed. “Chang experienced a period of openness but also a period where the space for human rights lawyers to work in was shrinking.”

除了代理公民案，常玮平曾在2016年7次诉陕西省西安市人民政府不履行法定职责，还把陕西的农业厅、公安局、开发区、看守所、人社局、物价局等部门告了个遍，他自嘲“因告政府而恶名昭彰”。

最开始的几年，常玮平很享受做律师。庭下阅卷找证据，庭上据理力争。线下与同行激烈探讨，线上激情发文，利用自媒体获取公众关注。有些案子，代理费不过2000元，还覆盖不了他多次来回的交通费，他也不在意。2014年代理金哲宏的冤案，发现当时监狱要求两个律师才能会见在押者，他就自掏腰包，帮金哲宏再请了一名律师一起会见。

In addition to representing citizens, in 2016, Chang sued the Xi’An Municipal Government in Shaanxi Province seven times for not fulfilling its statutory duty. He also sued Shaanxi’s Department of Agriculture, Public Security Bureau, Development Zone, Detention Center, Human Resources and Social Security Bureau, Price Bureau and other departments. He mocked himself for being “notorious for suing the government”.

In the first couple of years, Chang enjoyed being a lawyer. He read records and files to find evidence and argued strongly in court. He also discussed with his peers offline and posted articles online. He also used the media for public attention. For some of the cases, the fees are only 2000 RMB, which doesn’t even cover his return trips, but he didn’t mind. In 2014, when he represented Jin Zhehong’s case regarding miscarriage of justice in 2014, the prison required two lawyers to meet with Jin at the time. Chang paid out of his own pocket to hire another lawyer to meet with Jin together.

他还起诉过世界500强公司丰益国际旗下的粮油品牌金龙鱼和中央企业华润万家，因为在其宣传海报上“转基因大豆”几个字不够大，不利于消费者发现，侵犯消费者的知情权和选择权。他起诉民航陕西机场公安局，原因是机场开临时身份证明要收40元，而同样的服务在火车站是免费的。这一告，虽然一审被驳回，但后来陕西机场取消了这笔收费。

运用法律是常玮平的谋生之道，更是他的人生价值所在。香港资深记者江琼珠在《中国维权律师及其一伙》书中写过这样一个片段，2010年常玮平在北京准备司法考试，同乡到访，问他考律师证后做什么？常玮平说，给天安门的访民当律师。

He has also sued Jin Longyu, a grain and oil brand owned by Fortune 500 company Wilmar International and China Resources Vanguard. He sued the company because on their posters, the words “genetically modified soybeans” were not large enough for consumers to see. This infringed upon consumers’ right to information and their choice. He also sued Civil Aviation Shaanxi Airport Public Security Bureau because the airport charged 40 RMB for a temporary identification certificate when this service was free at the train station. Even though the case was dismissed, Shaanxi Airport later cancelled this fee.

Utilizing the law is Chang’s way of making a living, but it is also his value in life. In Hong Kong veteran journalist Jiang Qiongzhu’s book *China’s Rights Lawyers and Their Partners*, there was a segment which mentions that in 2010, when Chang was preparing for the judicial exam in Beijing, a resident from the same village as him visited and asked him what he would do after obtaining his lawyer’s certificate. Chang said he would be a lawyer for the petitioners at Tiananmen Square.

风云激荡，一切如此热烈，又如此刺痛。2014年，常玮平参加“小河案两周年暨冤案申诉研讨会”，70多名律师与警察现场冲突，打了起来。他代理“访民张小玉被指袭警致死案”，因拒绝给警察提供他与张小玉的通话记录，先被警方留置询问，后又被以“涉嫌故意杀人”传唤23小时。1994年被认定为一强奸案杀人凶手的金哲宏坐牢20多年仍在申诉喊冤，常玮平接到案子后尽心尽力找证据，却被强行解除代理，之后仍一直推动着案子发展……他深切地感受着中国刑辩律师的困境。

The weather rages, everything seems so enthusiastic, but also so painful. In 2014, Chang participated in “Xiao He’s second anniversary of miscarriage of justice” seminar, where 70 or so lawyers clashed with police at the scene. He represented the petitioner Zhang Xiaoyu who allegedly killed a police officer. Because he refused to provide the police with his phone records with Zhang Xiaoyu, Chang was first detained by police for questioning and then summoned for 23 hours on “suspicion of intentional homicide”. Jin Zhehong was found guilty of rape and murder in 1994, and jailed for more than 20 years and had been appealing his case. Chang took up the case and had tried his best to find evidence, yet he was forcibly discharged, and afterwards continued to promote the development of the case. He understands and feels deeply about the plight of Chinese criminal defense lawyers.

更刺痛常玮平的是，他发现法律时常失效，司法机构无法为人民提供司法救济反而制造了大量冤案和访民。冤案背后大多是刑讯逼供，访民背后更是民间无处不在的不公和无处可放的愤怒。而应该扮演法治守护者角色的律师——却时常被当作敌人对待。

What was even more painful for Chang was that he found that the law often failed and the judicial system was unable to provide remedies for citizens and instead, had created a large number of cases where there was a miscarriage of justice. There were also many petitioners. Many of the cases had confessions that had been extracted by torture and what’s behind these petitioners is widespread anger and injustice. Lawyers, who are supposed to play the role of guardians of the rule of law, are often treated as enemies.

**他内心比我们都乐观**

**Deep down, he is more optimistic than we are**

2018年10月，常玮平被宝鸡市司法局停业3个月。同年11月22日，常玮平所执业的陕西立刚律师事务所被注销。按照中国《律师执业管理办法》，若所在的律师事务所被注销，需要在六个月内找到其他律师事务接收，否则律师证将被注销。

半年里，超过十家律所向常玮平伸出了橄榄枝。然而，每到去司法厅申请转所手续这一步，就遭遇挫折，要么是律所打来电话称“受到压力，不能聘请”，要么是开不出转所手续。

In October 2018, Baoji City’s Justice Bureau suspended his lawyer license for three months. On November 22 of the same year, Shaanxi Ligang Law Firm, where Chang was employed, was closed. According to China’s “Administrative Measures for the Practice of Law”, if the law firm you work for is closed, then you must find another law firm within six months, otherwise your law license will be cancelled.

During those 6 months, over 10 law firms extended an olive branch to Chang. However, each time he went to the Ministry of Justice to apply for a transfer, he encountered setbacks. Either the firm called and said it was under pressure not to hire him, or there were issues with the transfer procedure.

两位不愿透露姓名的、在陕西执业的律师表示，他们在饭局或司法局的小型会议上听过陕西省司法厅某工作人员说，就是他盯着宝鸡市司法局，处罚常玮平，注销立刚所。

常玮平当然不会坐以待毙，他不断地对接新律所，同时向陕西律师协会投诉，也收集证据起诉陕西司法厅滥用职权践踏律师执业权。最强硬的对抗发生在2019年6月4日，常玮平在陕西司法厅门口绝食、静坐了一晚上，一边抗议转律所被刁难，一边纪念六四三十周年。

律师小茗至今都觉得荒谬，常玮平代理的案子都不是政治最敏感的，更多是反性别歧视、就业歧视的公益案，不属于“非要被处理的人”，怎么会成为司法局的眼中钉？最后还被扣上了“涉嫌颠覆国家政权”的罪名？她感叹，“只能说，一阵妖风刮来，常律师刚好在风口，根本没得选择。”

Two lawyers practicing in Shaanxi who wish to remain anonymous, said they had heard a staff member at the Shaanxi Provincial Justice Bureau said at a dinner or a small meeting of the Justice Bureau, that he was keeping an eye on Baoji city’s Justice Bureau, punishing Chang and closing the Ligang law firm.

Chang of course would not sit by idly, and he continued to reach out to new law firms while at the same time, complaining to the Shaanxi Bar Association. He also collected evidence to sue the Shaanxi Justice Bureau for abuse of power and trampling on a lawyers’ right to practice. His biggest act of resistance was on June 4, 2019. Chang went on a hunger strike and sit-in in front of the Shaanxi Justice Bureau for one night, protesting that they had deliberately made it difficult for him to transfer law firms. He was also commemorating the 30th anniversary of the June 4th Tiananmen Massacre.

Lawyer Xiao Ming finds it absurd since Chang didn’t take on politically sensitive cases. Most of the cases are public interest cases such as gender discrimination and employment discrimination. He doesn’t belong to the category of “people who must be dealt with”. Why did Chang become a target of the Justice Bureau? In the end, he was even given the charge of being suspected of “subversion of state power”. She sighed, “I can only say that an evil wind has come, and Chang has been swept up and has no choice.”

与常玮平一起代理过两个案子的冷林指出，常玮平十分较真，坚持程序正义，“他做案子是一定要把每一个法律程序都走完走尽的，有着太多明知不可为而为之的执着。估计得罪了不少人。”

2017年，冷林和常玮平代理一起香港商人郭桦昌涉嫌掩饰、隐瞒犯罪所得经济案。恰逢后雨伞运动的政治敏感期，这个案子受到诸多刁难。

冷林记得，他们要求会见经办警察被拒绝，开庭时又被逐出法庭，还差点被警察打。温和的常玮平展示了刚烈的一面，他把眼镜和文件夹往地上一扔，指着警察大喊，“来啊，你打我啊，没打死我啊。”

very serious and adhered to procedural justice, “When he handles cases, he is sure to go through every legal process to the end and insists on doing the impossible. I’d guess he has offended many people.”

Leng Lin remembers that he requested to see the police handling the cases and was denied. They were also driven out of the court room and were almost beaten by police. The mild-mannered Chang showed his fierce side, threw his glasses and folder to the ground, pointed to the police officer and shouted, “Come on, come and hit me, you haven’t beaten me to death.”

在另一个合作的案子中，合议庭上，常玮平有理有据，娓娓道来。审判长突然离庭，毫无征兆。法庭上的人都懵了，常玮平很镇定，停下来说“等他回来再继续。”半个小时后，审判长回来了，常玮平又开始据理力争地辩。审判长看不下去，直接问他，“都什么时代了，还这么不听话，还这样抗辩？”冷林的解读是，709大抓捕之后，法庭氛围变了，辩护律师时常遭遇审判长的粗暴打断、无理指责，义正严辞的抗辩也已经很少了，庭审变成各方心知肚明的走过场。但常玮平依旧努力抗辩，让法官都觉得他傻。

冷林说，中国的刑事、民事是两条腿。前腿被打断了，后腿拖着身子艰难地走。案子只分是否触及统治阶层的利益，不触及，按照正常法律逻辑走；若触及，“门都没。”

In another case they collaborated on, Chang was rational and speaking tirelessly in the courtroom. The judge suddenly left the courtroom without any warning. The people in the courtroom were all surprised and Chang was calm and he stopped and said: “Let’s continue after he gets back.” After half an hour, the judge returned and Chang started to argue and debate on strong grounds. The judge could no longer look on and directly said to him: “What era are we in, you are so disobedient and what kind of defense is this?” Leng Lin’s interpretation was that after the 709 arrests, the atmosphere of the courtroom had changed and the defense lawyers often encounter interruptions and unreasonable accusations from judges. Strong defenses were rare, and all parties are generally aware that court hearings are just perfunctory. But Chang still tried his best to provide a strong defense, which led the judge to think he was stupid.

Leng Lin said that China’s criminal and civil cases are two legs. If the front leg is broken, then the back leg drags the body along with difficulty. Cases are separated based on whether they affect the interests of the ruling class. If they don’t, then cases will go by the logic of the law; if they do, then it is impossible to fight these cases.

这些年来，看着同行一个个被吊销执照，一个个入狱，又一一释放，再一次次入狱，冷林说，体制的维护者都被逼成了反对者，“或许，律师应该开始思考改变定位，现在
 没有体制内改进的空间了，要有准备，真的做个反对者。”

做维权律师越久，冷林越发绝望。常玮平也说自己越发绝望，“但他是虚假的绝望，他还是相信法治，把法律当真在践行，即便被监禁在老家，他还在尝试转所，以法律顾问身份援助案子。他内心还是比我们都乐观积极一点。”

In recent years, Leng Lin has seen many in the same field who have lost their law license, been arrested and released, and then rearrested. Defenders of the system have become opponents, “Perhaps lawyers need to start thinking about changing their position. At the moment, there is no space to improve the system, we need to be prepared and really become opponents.”

Leng Lin feels more despair, the longer he becomes a rights defense lawyer. Chang also says that he is feeling more despair, but it is more a false sense of despair. He still believes in the rule of law and that the law is really being put into practice. Even though he is being imprisoned in his own hometown, he is still trying to transfer to another law firm, and provide assistance as a legal consultant. Deep down, he is a bit more optimistic than us all.

**世界曾在他面前打开另一扇门**

**The world once opened another door in front of him**

常玮平的这些经历陈紫娟大多不清楚。她是青年科学家，醉心科研，最近在攻克某种肿瘤研究。常玮平支持女性追求事业。夫妻各自忙碌，尊重也享受独立的空间。

两人是青梅竹马。高一，常玮平就开始追求陈紫娟。但陈紫娟志不在此，只想着学业。常玮平不一样，他是大家眼中的“天才型同学”，是那种上课看金庸小说，下课就坐到喜欢的女生桌上聊天；上学打篮球，放学跟着“小混混”厮混，一考试却总能轻松进入班里前一前二的人。

Chen Zijuan did not know about these experiences that Chang had. She is a young scientist, devoted to research and recently has been carrying out some sort of tumor research. Chang supports women in their career pursuits. The couple are busy in their own way, and respect and enjoy their independent space.

The two are childhood sweethearts. In the first year of high school, Chang began chasing Chen. At that time, Chen had her mind elsewhere as she only wanted to study. But Chang was different. In everyone’s eyes, he was a “talented student”, and the type who read Jin Yong novels during class and after class would sit at the desk of the girl who he had a crush on to chat. He played basketball at school and after school would hang out and muck around with his friends. During exams though, his results easily placed him in first or second place in class.

常玮平一个月给她写一封信，信纸都是精心选的文艺范，从高一到高三，写了近百封。陈紫娟回忆，每封信都是洋洋洒洒好几页。高考填报志愿时，常玮平围在陈紫娟一家人身边出谋献策，说重庆西南大学怎么怎么好——原来他报了重庆大学。那时陈紫娟的父亲就说，“这小子有耐心，也有居心。”

后来陈紫娟报考了重庆的西南师范大学。开学前，常玮平提前去了重庆，刚把行李放到重庆大学，就先把西南师范大学逛了个透。等陈紫娟来报到，他成了向导。常玮平每周都去看陈紫娟，她周六有课，他就一早去到课室等。4年的坚持把她打动了，两人成了一对。陈紫娟笑：“常玮平一个突出的特点就是对喜欢的人和事情都能坚持不懈。”

Chang wrote Chen a letter once a month. The letterhead was meticulously chosen and artistic. From freshman to senior year, Chang wrote close to 100 letters. Chen remembers that each letter was lengthy and was several pages. When filing out the college aspiration form, Chang offered advice to Chen’s family, telling them how good Chongqing Southwest University was – it turned out he had applied for Chongqing University. At that time, Chen’s father said, “This kid has patience, but he also has his intentions.”

Afterwards, Chen enrolled in Chongqing’s Southwest Normal University. Before university started, Chang went to Chongqing earlier, and he dropped off his luggage at Chongqing University before going to walk around Southwest Normal University. After Chen arrived, he became her tour guide. Chang went to see Chen every week. She had class on Saturday, so he went early to the classroom and waited. Four years of persistence touched Chen, and the two became a couple. Chen laughed, “One of Chang’s most outstanding characteristics is his perseverance with people and things he likes.”

大学生活里，常玮平参加了校报的编辑工作。他爱看《南方周末》和《经济观察报》，也自己写文章，学摄影和编辑。见到陈紫娟，除了谈情说爱就是讲时事政治，那是青春飞扬的日子。

他还有一副左翼革命青年目空一切的模样，有老师上课姗姗来迟，他就质问了半小时。陈紫娟说，这样的常玮平会做律师，对社会公平正义有追求太正常不过。

During his college life, Chang joined the editorial team of his school’s newspaper. He enjoyed reading *Southern Weekly* and *Economic Observer*, and also wrote articles, learned photography and editing. When he met Chen, other than just being in love, he talked about current affairs and politics. Those were the days he was full of youthful exuberance.

He also had the style of a condescending, young left-wing revolutionary. When a teacher came late for class, he would question them for half an hour. Chen said that Chang with his character will definitely become a lawyer, the pursuit of social justice is second nature to him.

“但你若认识读大学时，或者当律师之前的他，会大吃一惊，他是那么积极向上，走路都哼着歌，带着风。”陈紫娟说，常玮平常在耳朵上挂着大耳塞，要么在听英文新闻，要么在听周杰伦，还时常旁若无人地哼出来，“他的快乐是肉眼可见的。”

好友冷林形容2012年刚进入律师行的常玮平是从没被生活碾压过的模样，才气横溢，意气风发，带着年轻人对美好未来的期待，“他有改良主义思想，相信通过自己的努力，可以推动社会的进步，这本该就是年轻人应该有的期待和未来。”

“But if you knew him during college or before he was a lawyer, you’d be amazed by the type of person he was. He was positive and was singing as he walked, with a spring in his step.” Cheng Zijuan said that Chang was usually wearing large headphones, and was either listening to English language news or Jay Chou songs and hummed aloud as if no one was watching.” “His happiness was clearly visible.”

Leng Lin, a good friend of Chang, describes Chang in 2012 when he’d just entered the legal field. He was a young man who had never been crushed by life. He was energetic and high-spirited, and had expectations for a better future like many young people. “He had reformist thinking and believed that through his efforts, he’d be able to push for social progress. These should be the expectations and future that young people have.”

大学毕业后陈紫娟北上读研，常玮平南下海南做工程师。工作两年后也辞职北上发展。那是2009年的北京，奥运会带来了短暂的开放、包容气息，公民社会蓬勃发展，学术界百花齐放。用常玮平的话来说，“自己像刘姥姥进了大观园”，开始追着各种名家转。

经济学家马光远开讲座，他去抢位；企业家任志强发表演讲，他去买票；法学家高铭暄开课，他去旁听；作家李承鹏发布新书，他去凑热闹；“公知”罗永浩开英语培训班，他报名学习；艺术家艾未未众筹罚款，他捐上几百，还拿到谢酬两颗雕刻的葵花籽和一张借款条；作家兼赛车手韩寒比赛，他跑去现场和韩寒拍照。一切那么热闹有趣，世界在常玮平面前打开另一扇门。

After graduating from university, Chen headed north for graduate school, and Chang went south to Hainan to work as an engineer. After two years of work, he resigned and went north for more opportunities. It was in 2009 around the time of the Beijing Olympics, that there was a degree of openness and tolerance, and civil society flourished and there was more freedom of expression within academia. In the words of Chang, “I was like Grandma Liu visiting the Grand View Gardens”, and he started to follow various famous artists and experts.

He went to grab a seat when the economist Ma Guangyuan gave a lecture; when the entrepreneur Ren Zhiqiang gave a speech, he went to buy tickets; when author Li Chengpeng released a new book, he went to join in the fun; he enrolled in an English course by intellect Luo Yonghao; when the artist Ai Weiwei started fundraising to pay off his fine, Chang donated a couple of hundred dollars and he even received a carving of two sunflower seeds as a thank you gift and a loan note; when Han Han, the writer and rally driver, had a race, he went to take a photo with him. Everything was so lively and interesting that the world opened another door in front of Chang.

他白天做证券经理工作，晚上去清华、北大旁听法律课或者自习，雷打不动。陈紫娟说，“他这样一边上班，一边追“星”，一边自学法律。没想到，真的考过了司法考试。”

陈紫娟至今记得常玮平拿到司法考试成绩的那个晚上，他们一起去北师大旁边的小饭馆庆祝，常玮平告诉陈紫娟，他要做律师了。

He was a fund manager by day, and at night he went to Tsinghua University and Peking University for law classes or even studied on his own. He was extremely committed. Chen said that, “He worked while chasing ‘stars’ and learning law himself. I never thought that he would be able to pass the judicial examination.”

Chen still remembers the night that Chang received the results of his bar examination. They went to celebrate at a small restaurant next to Beijing Normal University. Chang told Chen that he was going to be a lawyer.

**不完美的丈夫和父亲**

**The imperfect husband and father**

做律师是常玮平人生的分水岭。陈紫娟说，做律师之前的常玮平十分舒展，走路带着风，脸上总是挂着几分狡黠的笑，“做律师后，他明显地忧愁了很多，歌也不听了，周杰伦的演唱会也不追了。”

起初，常玮平也和她诉说见到的不公，陈紫娟看着他以肉眼可见的速度苍老着，偶尔也劝他悠着点。如今想来，常玮平有很强的同理心，“看的苦难多了，他也跟着苦难起来。”

Being a lawyer was a watershed moment in Chang’s life. Chen said that before Chang was a lawyer, he was more relaxed and there was a spring in his step. He always had a wry smile in his face. “After becoming a lawyer, he had clearly become more worried. He stopped listening to music and didn’t follow Jay Chou’s concerts anymore.”

At first, Chang told her about the injustices he was seeing. Chen watched him age rapidly, and occasionally told him to take it easy. Now she thinks that Chang is very empathetic, “The more he sees suffering, the more he feels the pain himself.”

常玮平和她说过，没有司法独立，没有监督机制，侵害人权的事几乎每天都在发生。随着政权越发大胆介入司法，操纵法律，打击律师，他越来越少在家讨论案子，709大追捕之后，律师家属也备受波及，他刻意不再与妻子探讨任何案子。

出于对家人的保护，他有意无意对家人表现一种疏离。他曾经在网上写博客，抒发对孩子的热爱，知道国保会收集一切私人资料对付律师后，不再在任何平台谈论家人。作为丈夫，他不爱做家务。作为父亲，他长期出差，较少承担教育孩子的责任。然而，孩子却越发喜欢他，得知爸爸再次被抓，虽然不知道具体的意思，但7岁的孩子写了一张“shi放常玮平”的大字报。释字还不会写，就写成拼音shi。

Chang said to her that there was no judicial independence and no monitoring mechanisms, and rights violations were happening almost every day. As the regime become more and more bold in intervening in the judiciary, manipulating the law and cracking down on lawyers, he began discussing cases to his family less and less. After the 709 Crackdown, the families of the lawyers were also affected. He deliberately stopped discussing cases with his wife.

Out of concern and for the protection of his family, he intentionally showed detachment from them. He wrote a blog online and would write about his love for his child. But after he knew that national security was collecting personal information from lawyers to use against them, he stopped talking about his family on all platforms. As a husband, he did not enjoy doing chores. As a father, he was on work travel long term, and did not take on much responsibility in educating his child. However, his son loved him more and more. After his 7-year-old found Chang had been detained, even though he didn’t understand what it meant, he wrote a big character poster which reads “Release Chang”. He didn’t know how to write one of the characters, so just wrote it in pinyin (“shi”).

陈紫娟感到懊恼的是，在常玮平第二次被抓前几天，她还和他怄气，一气之下把他给拉黑了。当时她气常玮平人在凤翔老家，却很少给她发微信或打电话。后来为常玮平声援，微信和手机都被监控，她才懂得生活在监控之下容易导致人失去说话的欲望。

况且，经过牢狱之灾又处于软禁状态的常玮平，内心还有一些未曾厘清和面对的创伤。常玮平和同样遭遇国家暴力的朋友说，自己也有政治性抑郁和创伤后的应激障碍，有时会丧失与人正常沟通的能力。

What Chen feels most annoyed about was that the second time Chang was detained for a few days; she became frustrated at him and blocked him in a moment of anger. At that time, Chang was at his hometown in Fengxiang, yet he rarely sent her messages on WeChat or called her. Afterwards, when she was advocating for Chang’s release, her WeChat and phone were being monitored and that’s when she understood that individuals who are under surveillance can easily lose the desire to speak.

Moreover, after being imprisoned and placed under house arrest, Chang had some unresolved traumas that he still has to face. Chang has spoken to friends who have also experienced state violence, that he suffers from political depression and post-traumatic stress disorder, and sometimes loses the ability to communicate with others.

**国家暴力碾压过的痕迹**

**Traces of State Violence Crushed**

2020年，从被监视居住回来后，常玮平开始恐惧父母房间里的两口棺材，爷爷奶奶在60岁时给自己准备了棺材，两老离世后父母也跟着效仿，常玮平看着棺材长大，原本无畏无惧。如今，他无法独自一人在房间里久呆，两、三个小时就是极限。即便是大白天，进进出出，他也总也要把院子、客厅、卧室的门都关上。好些深夜，他开着车去县城溜圈，即便路上也无行人，但看看城里橘黄色的灯火，他都能生起一丝温暖，感觉不那么寂寞。

家人也随着成了惊弓之鸟。只要两通电话没接到，就心惊胆战的，四处打电话寻找他的下落。两个姐姐更是心疼他的暴瘦和憔悴，比赛着给他做饭，送面条和花卷。父亲从深圳回到凤翔老家时，晚上10点一到，就要打电话问他在哪里，什么时候回来？常玮平苦笑，“父亲回来了，就像国安法被强行通过，以后我和香港都没好日子过了。”

In 2020, after being released from residential surveillance, Chang began to be frightened of the two coffins in his parent’s house. His grandparents had prepared their own coffins when they were 60 years old, and after they passed away, his parents followed suit and prepared theirs as well. Chang saw the coffins growing up and originally didn’t think much about it. But now, he is unable to be alone in the room for too long, at most two or three hours. Even if it is day time, coming in and out, he always had to close the doors to the garden, the living room and bedroom. There were many late nights where he’d drive around town, even if there was no one on the streets, he’d watch the city lights and feel warm inside. He’d also feel less lonely.

His family became frightened easily as well. When he doesn’t pick up after two phone calls, they become afraid and call around to find out where he is. His two sisters were especially worried that he had become so thin, and they were basically competing when it came to cooking him noodles and steamed buns. When his father returned from Shenzhen to Fengxiang, their hometown, he would call at 10pm to ask where he was and what time he would be back. Chang laughed bitterly, “When my dad comes back, it is like the national security law being forcefully enacted in Hong Kong, in the future there won’t be any good days for me or Hong Kong.”

他偶尔也发泄愤怒。2020年5月中旬的一天，风雨欲来，狂风大作。在闪电雷鸣的时候，常玮平跑出家门，感受大自然的神力，对着风滚云涌的天空喊，“我想要战天斗地”。

牢狱之灾也让他对日常的生活多了一份珍惜。每一次朋友的邀约，聊天、爬山、看电影、喝酒、打球，他都积极地一一应约，总觉得“见一次少一次，吃一餐少一餐，打一场少一场。”说着说着，他语音哽咽，眼里含泪，却唱起了歌，“把每天当成是末日来相爱，一分一秒都美到泪水掉下来”。

He occasionally vented his anger. One day in mid-May in 2020, a storm was coming and it was very windy. When there was thunder and lightning, Chang ran outside to feel the power of nature and shouted to the sky, “I want to fight heaven and earth.”

After imprisonment, he began cherishing daily life more. When he was invited out by a friend to chat, go hiking, watch a movie, drink, play football, he’d agree to each and every one of them. He felt that, “Human life is finite, you don’t know when is the last time you’ll see, eat together and have fun with your friends.” He choked on his voice and tears welled in his eyes, yet he still began to sing a song, “Love each other as though it is your last day on this earth, every minute and every second is so beautiful that tears will begin to stream down your eyes.”

他努力链接身边的人。36岁生日，他和篮球场上认识的两个小女孩一起吃蛋糕，女孩们又邀请了自己的小男朋友，篮球场上一片欢乐。他和迎面而来的乡亲们问好，偶尔也一起参与农活。要知道，他从小讨厌农活，还曾对着自己的父亲大喊“我生下来不是为了干农活的”。与人建立起真实可触摸的关系，常玮平珍惜这种平常。

为了调节性情，打发无聊，他还开始摆弄花草。院子里种起竹子，修剪蔷薇，给葡萄树搭架子，给橘子树剪枝丫，给绣球花驱虫，抢救小多肉，兴致盎然。

He tries to connect with those around him. On his 36th birthday, he ate cake with two little girls he met on the basketball court, who then invited their two young boyfriends. It was a lot of fun for him. He greets the residents in his village, and sometimes also participates in farm work. He grew up hating farm work and once yelled to his father, “I wasn’t born to do farm work!” Chang cherished this normal lifestyle of creating real and genuine relationships.

To manage his temper and pass the boredom, he began to do some gardening. In the garden, he planted bamboo, pruned roses and built frames for grapevines, cut the branches of orange trees, dewormed hydrangeas and saved dying succulents. All with great interest.

同学周少波医生是他在凤翔苦闷生活中的一抹亮色。视频里，每次放声大笑的瞬间，都有周医生在旁。每次休假，周医生都找他，太频繁了，常玮平笑着给周太太保证，“我不是周医生的小三。”

两个好基友还一起拍电影，注册了宝鸡咪咪毛影业有限公司，申报备案，拿到了开发许可证。他们公开招聘女主角，也开了电影说明会，还开始拉广告和赞助商。

His classmate Dr. Zhou Shaobo was a bright spot in his life during the time he was depressed. In the video, Dr. Zhou was by his side every time there was laughter. Every holiday, Dr. Zhou would reach out to him, and it happened so often that Chang laughed and reassured Mrs. Zhou, “I’m not Dr Zhou’s mistress.”

The two best friends also made movies together. They registered a media company called Baoji Mimi Mao Film Co., filed records and received a development license. They publicly recruited female leads, held a film presentation and even began getting ads and sponsors.

“一个前律师，一个医生，三十多岁进入演艺界。但我们不自卑，我们带着深深的感情去拍，你们会看到我们的诚意。”新晋导演和编剧常玮平在电影说明会上侃侃而谈。合作人周医生再次在常玮平身上看到了活力和阳光。

周少波还记得，2020年1月23日，常玮平取保候审，他陪同常玮平父亲去接人。一见面常玮平就立刻给他们来了个拥抱，笑容满面，然后转身和送出门口的警察一一握手告别，“那精神状态好像比进洞房还要好，我就说，这家伙真行。”

“A lawyer and a doctor entering show business at around 30 years of age. But we don’t feel inferior, and had deep feelings when we shoot. You will see our sincerity.” Up and coming director and screenwriter Chang spoke frankly at the movie presentation. His partner Dr. Zhou saw energy and light in Chang again.

Zhou Shaobo also remembered that on January 23, 2020 when Chang was released on bail, he went with Chang’s father to pick him up. As soon as Chang saw them, he immediately went in for a hug, and was smiling from ear to ear. He then turned around and shook the hands of each police officer that was by the door, “His mental state seemed better than when he went into the bridal chamber. As I said, this guy is amazing.”

常玮平不是一个悲情的人。他看美国的律政剧《Boston Legal》，相比大多人喜欢的主角Alan Shore，他更喜欢Danny Crane 这个古怪、幽默又有趣的传奇人物。他用Danny Crane的名字在YouTube上发表的“趣宝日记”，其实指的是“取保日记。”

刚做律师时，作为家人和村子的骄傲，父亲常栓明曾想把常玮平和他姐姐一起写进村史，他当时劝父亲，“现在写得多好，以后可能肠子就悔得多青。”取保候审回到家，他对父亲说，“看，我当时多有先见之明。”

只有偶尔的几次，他对着朋友，问得悲哀：做律师，打几个案子，开个会，聚个餐，吃个饭，吹几句牛逼，怎么就颠覆国家政权了呢？

Chang is not someone who is sad. He watched the American legal drama *Boston Legal* and even though many people like the main character Alan Shore, he prefers Danny Crane who he thought was a quirky, humorous and interesting legend. He uses Danny Crane’s name for his YouTube account to publish his *Diary of an Interesting Jewel*, yet he was actually referring to *Diary after Being Released on Bail*. (The characters for “interesting jewel and “released on bail” sound similar in Chinese)

When Chang was a lawyer, as the pride of his family and village, his father Chang Shuanming wanted to write about Chang and his sister together in the village history. At that time, he advised his father, “You write about me like I’m so amazing, but in the future you will regret it a lot.” After being released on bail and returning home, he said to his father, “Look, I was able to foresee the future.”

It was only on a few occasions that he turned to his friends and asked sadly, “How is being a lawyer, fighting some cases, holding a meeting, gathering for a meal, eating food and just talking about random stuff, subverting state power?”

**低调到泥土里**

**Low profile like dirt**

持续不断的骚扰、恐吓和问话，是常玮平取保候审的生活主题。他幽默地称定期来访的国保们为“客服”。

取保候审本是一种程度较轻的强制措施，只是限制而不是剥夺犯罪嫌疑人、被告人的人身自由。然而，常玮平取保候审期间，每天早上要报备行程，三头两天被约谈，甚至连到市区看牙医，也必须由两个国保陪同。他不能离开凤翔老家，即便自己的妻子、孩子、父母都在深圳生活。

Chang’s life after being released on bail consisted of continued harassment, threats and questioning. He humorously refers to the regular visitors from national security as “customer service”.

Being released on bail to a certain degree is a coercive measure but to a lesser extent. It only places restrictions on suspects or defendants, but does not deprive them of their freedom. Moreover, after Chang was released, he had to report his movement every morning and was questioned almost every day. Even when he went to the city to see the dentist, two national security officers had to go with him. He wasn’t able to leave his hometown in Fengxiang, even though his wife, son and parents live in Shenzhen.

他不是没想过拿起最熟悉的法律手段来维权，但直至今日，他第一次被指定地点监视居住以及取保候审都没有任何法律文件，也就是说，即便他想起诉，也没有任何文书证据来证明自己曾被消失、酷刑的10天以及当下的非法软禁。

再次被抓捕前，常玮平和记者聊过那10天的监视居住。当时关他的理由是，他参加了2019年12月初在厦门的聚会。那次聚会有许志永、丁家喜等数十名律师和公民参加。讨论的话题是时事政治、中国未来、律师的执业困境以及公民社会的出路。12月26日，与会的丁家喜、张忠顺、戴振亚、李英俊等十多人纷纷被捕。随后，逃亡的常玮平、许志永以及许志永从未参会的女朋友李翘楚也被抓捕，这被称为“12.26公民案”。

It isn’t that he hadn’t thought about using legal means to defend his rights. But up until now, since he was placed under residential surveillance and released on bail, he has no legal documents. This means that even if he files a lawsuit, he doesn’t have any relevant documents proving he was disappeared and tortured for 10 days and that he is currently under illegal house arrest.

Before being arrested again, Chang spoke to a journalist about his 10 days under residential surveillance. The reason for his detention was that he had participated in the December 2019 gathering in Xiamen. At that time, Xu Zhiyong, Ding Jiaxi and over a dozen journalists and citizens had participated in the gathering. They discussed topics related to politics, China’s future, the plight of lawyers and the way forward for civil society. On December 26, more than a dozen of those who attended including Ding Jiaxi, Zhang Zhongshun, Dai Zhenya and Li Yingjun were arrested. Later, Chang, Xu Zhiyong and Li Qiaochu who was Xu Zhiyong’s girlfriend and hadn’t participated in the gathering, were all detained. This was known as the “12.26 Citizen Case”.

没有人想过，后来被取保候审的常玮平和李翘楚又都再次被抓捕。至今，12.26案里，许志永、丁家喜、常玮平被指控颠覆国家政权，李翘楚被指控煽动颠覆国家政权，其他人皆已释放。

监视居住的10天里，常玮平做了16次笔录。该说的话，该表达的思想，该理论的，都说了个遍。他甚至反思，站在当权者维稳的角度上看，2013年后以人权律师为首，以弱势群体、宗教人士、网络大V和异议人士构成这样一个维权网络，确实是对党国统治产生威胁。

No one could have thought that Chang and Li Qiaochu who were both released on bail would be re-arrested. In the 12.26 case, Xu Zhiyong, Ding Jiaxi and Chang are not accused of subverting state power. Li Qiaochu has been accused of inciting subversion of state power. The others have been released.

During the 10 days he was under residential surveillance, Chang did 16 transcripts. He said what he needed to say in terms of his thoughts and ideas. He even reflected that, from the perspective of the authorities who are maintaining social stability, the network of rights defenders that was led by human rights lawyers after 2013, and included vulnerable groups, religious figures, internet celebrities and dissidents were indeed a threat to the party state.

但妥协没换来自由，他仍然被软禁在老家。他和朋友调侃，自己在宝鸡市是个“大咖”，做什么事都必须跟国保报备，“他们只需要你活着，呼吸着，其他都不能做。”他说自己都“低调到泥土里了”，不再争辩，面对打压“就地卧倒”，留得青山在哪怕没柴烧。

国保要他每周写思想汇报。为了证明自己的思想无毒，他写中国抗疫可歌可泣，又称赞金山银山不如绿水青山，还表扬国保深入基层慰问。他把这一切都如实记录在视频里，笑自己，“绝对是个好演员的料。”

But this compromise did not bring freedom. He was still put under illegal house arrest. He ridiculed with friends that he was an influential person in Baoji City and that whatever he does he needs to report to national security. “They only want you to live, and breathe and not do anything else.” He said he had to be as low key and be nothing more than just dirt, and to stop disputing everything, and to stop everything immediately when facing suppression. Where there’s life, there’s hope.

National security wants him to write his own thoughts every week and report it. To prove that his thoughts were not toxic, he wrote that China’s fight against the pandemic was inspiring; he sung praises and also commended national security for consoling those at the grassroots level. He recorded all of this in a video and laughed at himself, “I am definitely a good actor.”

国保们说需要他有“重大改变”，要他“有所作为”。常玮平打趣道，“我也想大有作为，也想弃暗投明，可就是没有料啊。”

常玮平也不是没有挑战过。2020年6月的一天，他把手机关机，扔在一旁，和朋友开车到了与凤翔相邻的千阳县郊区。后来国保来电询问他去向，他也回答得理直气壮，“在郊区，没信号。”

朋友们觉得，荒诞又苦闷的生活，在常玮平的讲述下，总是多了一丝嬉笑讽刺，总能让人苦中作乐，笑中带泪。

National security said that he had to make “significant changes”, and to “make a difference”. Chang laughed and jokingly said, “I also want to make a huge difference and turn to a better way of life, but I don’t have talent.”

It isn’t that Chang hasn’t challenged them. In June 2020, he shut his cellphone and threw it to the side and drove with a friend to the suburbs of Qianyang country, which is next to Fengxiang. Later, national security called him to ask where he was, he answered with a straight face, “I’m in the suburbs, there’s no signal.”

Chang’s friends felt that his life was incredible and bitter, but when he narrates it, there is always a hint of sarcasm and humour. It allows others to find joy in the sorrows and to laugh with tears.

对更大的不公不义，他还是无法无视。视频里，他谈自己对香港沦落的悲哀，为维权律师王全璋出狱后仍被软禁感到无奈，担心独立记者江雪因为发表疫情纪念文章被约谈，害怕记录武汉疫情的公民记者被重判。每件不公之事都会影响他的心情，让他处于一种“不知道谁明天进去”的状态。

离开中国吧。不止一个朋友规劝他。妻子陈紫娟也提议过，作为青年科学家，她有不少出国深造和工作的机会。常玮平也不止一次地回答，出去多没意思啊。留在这里，说不准还能运用法律为国家为老百姓做点事情呢，虽然不知道能做多少。说完又如平常一样自嘲，“也许底层老百姓不觉得自己苦呢！”

他很清楚，受制于自己的理念和性格，他无法放下脚下这片土地，他也笑自己真的哪天领悟的话，“可能已经too late”。

He was unable to ignore the bigger injustices that he saw. In the video, he talks about his sadness as the situation in Hong Kong deteriorated. He felt helpless seeing Wang Quanzhang, the human rights lawyer, was placed under house arrest despite being released from jail. He was worried about independent journalist Jiang Xue being questioned after she released articles commemorating the pandemic. He was afraid citizen journalists who wrote about the pandemic in Wuhan would be severely punished. Every case of injustice affected his mood and put him in a state of unease as he “didn’t know who would be taken the next day.”

Many of his friends advised him to leave China. His wife, Chen also suggested that, and as a young scientist she had many opportunities to study and work abroad. More than once, Chang replied that it was meaningless to leave. If he stays here, at least he can use the law to do something for his country and the people. Even though he doesn’t know how much he can actually do. After saying this, he laughed at himself as usual, “Maybe the people in the lowest rungs of society don’t think they are suffering!”

He knows very well that because of his own personality and values, he is unable to leave this country. He even laughed at himself, that when there comes a day and he realizes this, “Perhaps it is too late.”

**后记：陈紫娟的“破罐破摔”**

**Epilogue: Chen’s despair**

太迟了，陈紫娟很清楚。她担心的是，再次被捕，常玮平会不会又被酷刑，被喂药，万一被虐待到精神失常，或者落下残疾，该怎么办。眼泪总是抢在话语前流出来。

她不再只醉心科研工作，丈夫的再次被捕让她正视这个新标签——为丈夫喊冤维权的女人。

她一边工作，学习考证，一边照顾小孩，一边去各部门控告。这样的日子让她想起2010年左右的常玮平，一边工作，一边自学法律，一边追各种名家大流，完成公民思想的启蒙。

Chen knows it is too late now. She is worried about Chang being arrested again, and whether he’d be tortured, force fed drugs and what to do if he was abused to the point where he was mentally unstable or became disabled. She fought back tears as she said this.

She is no longer just absorbed in her research. After her husband was detained again, she has had to take on this new role: a woman who fights for her husband’s rights.

She worked and studied for her certification, taking care of her child and suing various departments. Days likes these remind her of Chang around 2010. He was working, studying law on his own, chasing famous writers and being enlightened by citizen ideology.

她感觉自己也逐渐被启蒙了，帮常玮平上诉，写文章声援，要求信息公开，虽然处处受挫，但似乎也越战越勇，“都没时间来伤心绝望，有太多司法机关要去告。”

陈紫娟听过709妻子的故事，知道她们曾被逼迁、被跟踪、被软禁，甚至被打被抓，但她无法放下，“作为人，我无法忍受自己的亲人被如此不公对待，被如此折磨。”

还没开始声援常玮平的时候，陕西省和宝鸡市公安局的人就9次到深圳她的家中和工作单位约谈她，先是安抚让她“相信国家”，后是威胁“将失去工作”。陈紫娟一开始也惊慌失措，听着国保的规劝，却发现，他们的逻辑和作为无法说服她。她开始了“反骚扰”。

She is slowly becoming enlightened. After helping to appeal Chang’s case, writing solidarity articles and requesting information disclosure, she has met setbacks everywhere. But she has also become braver, “I don’t have time to be sad and in despair, there are too many judicial authorities to sue.”

Chen has heard the story of the 709 wives. She knows they have been forcibly evicted, followed and placed under house arrest. Some of them have even been beaten and detained. But she cannot let this go, “As a person, I can’t accept that my loved ones are being treated so unjustly and tortured in such a way.”

Before she had begun solidarity actions for Chang, Shaanxi Province and Baoji City public security officials went to her house and workplace in Shenzhen nine times to question her. At first, they tried to appease her and told her to trust the state, and afterwards they threatened her that she’d lose her job. Chen at first was losing her mind as she was extremely scared, but after she listened to national securities’ exhortations, she discovered that their logic and actions were unable to convince her. She began to fight against this harassment.

宝鸡市高新分区公安局的副局长向贤宏主办常玮平的案子，表示会依法办事，给过陈紫娟他的手机号码和办公室电话，希望建立良好沟通关系。陈紫娟一有空就给他发信息：常玮平的身体和精神怎么样？贵局对常玮平实施酷刑的办案人员查不查？他做的案子怎么危害国家了？为什么把常玮平爸爸和二姐夫都监视居住了？为什么不回应？

问题太多太频繁，向贤宏要么以“开会”为由躲避，要么不回答，还一度把陈紫娟拉入黑名单。陈紫娟电话打到公安局投诉：你们之前说要建立良好顺畅的沟通关系，但是你们不接电话不回信息怎么建立关系？向贤宏只好继续听她持续不断的质问。

“他们哄不了我，也吓不了我，如果我真的被失业了，那就专职给常玮平维权呗。”陈紫娟说，破罐破摔或许还可以摔出些声响。

Deputy Director of the Baoji City Gaoxin District, Xiang Xianhong is in charge of Chang’s case, and said they would act in accordance with the law. He gave Chen his cellphone and office number and said he hoped they can establish a good communication relationship. When Chen has time, she sends him messages: How is Chang’s health and mental state? Is your bureau investigating the officers who tortured Chang? What cases has he worked on that have endangered the state? Why have you placed Chang’s father and brother-in-law under residential surveillance? Why are you not responding?

She sent many questions regularly, and Xiang would use the excuse that he was in a meeting to avoid her or he would just ignore her and not answer. There was once he even blocked Chen. She then called the public security bureau to complain: Previously, you said that you wanted to establish a good communication relationship, but you’re not even picking up phone calls or replying my messages. How is this establishing a relationship? Xiang had to continue listening to her questions regularly.

“They can’t cajole me, or scare me. If I lose my job, then I’ll just switch to defending Chang’s rights full-time.” Chen said, in the face of adversity, perhaps she can still use her voice to speak out about these injustices.

她时常做梦，梦里的常玮平偶尔笑嘻嘻告诉她“回来了”，更多的时候是在那坐着，不说话，也不看她。等她走近，才发现他坐在老虎凳上，旁边站着几个警察。

陈紫娟开始一个个地看常玮平的视频日记，缓解思念之情外，她有更重要的目标，要找到参与酷刑的警察的名字，一一去起诉。

2021年3月底去陕西维权时，她特意到常玮平被酷刑的宝钛宾馆住了一晚，近距离感受恐惧。住进宾馆的时候，陈紫娟满眼泪水，“谁能想到，朗朗乾坤，人流不息的市区里，他就被关在这不见窗户、四周都是软包的地下室房间十天？”

She regularly has dreams where Chang would smile at her and says, “I’m back.” More often than not though, he is just sitting there and not talking to or looking at her. When she goes closer, she sees he is sitting in a tiger chair with several police officers standing next to him.

Chen began watching Chang’s video diaries one by one to ease her mind. She has an even more important goal; she needs to find the names of the police officers who tortured Chang and sue each and every one of them.

When she went to Shaanxi for rights defense at the end of March, 2021, she specifically went to the Baotian Hotel where Chang was tortured to stay for a night. She felt the fear up close. When she was living in the hotel, Chen was in tears, “Who could have thought that in this bright and bustling city, he would be locked up in a basement with no windows and with padded walls for 10 days.”

恐惧的情绪让人崩溃，她当晚不敢关灯，整夜半睡半醒，她不知道丈夫当时是如何熬过这不见天日、分分秒秒都存在的恐惧？也不知道他第二次被抓捕又可能承受怎样的酷刑。

但她越发相信丈夫常玮平。他所做的一切都是基于良知、热爱和自己的专业，连这样的人都不见容于这个国家，那肯定是国家错了。

The fear made her fall apart and that night she was too scared to shut the lights. She was half awake the entire night. She doesn’t know how her husband was able to survive without seeing daylight and having to live in fear every minute, every second. She also doesn’t know how he was able to endure the torture during the second time he was detained.

But she trusts Chang more and more. Everything he does is based on his conscience, love and his profession. If even such a person is not tolerated by the state, then it is the state that is in the wrong.

**按受访者要求，冷林、小茗为化名。**
**Leng Lin and Xiao Ming are pseudonyms.**