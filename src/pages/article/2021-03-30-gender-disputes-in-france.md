---
templateKey: article
title: 幻灭法兰西（一）：漫漫性平路
slug: gender-disputes-in-france
date: 2021-03-30T03:21:57.265Z
description: null
featuredpost: true
featuredimage: /img/3GFOcuh.png
trending: 9
isbrief: false
typora-copy-images-to: ../../../static/img
tags:
  - MeToo
  - 法国
  - 性别
  - 平权
contributors:
  - type: 作者
    name: SomeMilk
  - type: 编辑
    name: 焦糖鲑鱼 Laitek 小咚 章章
typora-root-url: ../../../static


---

<center><img src="https://i.imgur.com/3GFOcuh.png"/></center>
<center style="font-size:14px;color:#C0C0C0">（封面制图：进口散装牛奶）</center>

#读者来函
（全文共10578字，阅读可能需要20-30分钟）

作者：SomeMilk
编辑：焦糖鲑鱼、Laitek、小咚、章章

如果说2017年因揭露[哈维·韦恩斯坦性侵犯丑闻](https://zh.wikipedia.org/wiki/%E5%93%88%E7%BB%B4%C2%B7%E6%B8%A9%E6%96%AF%E5%9D%A6%E6%80%A7%E4%BE%B5%E4%BA%8B%E4%BB%B6)而蔓延全球的#MeToo浪潮掀起了新一波女权运动，这场在美国兴起的女性解放运动或许并不如许多中文读者想象中的那样，在所有西方国家都得到了大量的民众支持，或引起大规模的群体反思。

在法国，一个在传统想象中或浪漫、或激进、或艺术、或对性关系和性本身百无禁忌的国度，#MeToo运动在不同领域相继爆发。在这里，#MeToo有着一个比英文世界更加尖锐的名字：#BalanceTonPorc（“揭发你的色猪”）。第一次在Twitter上使用这个标签的人是桑德拉·穆勒（Sandra Muller）：2017年10月13日，作为记者的她在Twitter上号召女性揭发自己在工作中遭受的性骚扰或性侵。在[巴黎人报的一篇专访](https://www.leparisien.fr/laparisienne/actualites/sandra-muller-a-lance-balancetonporc-je-l-ai-fait-pour-toutes-les-victimes-15-10-2017-7333434.php)中，她表示这个色彩更加鲜明的标签有助于受害者跟犯罪者拉开距离，也跟韦恩斯坦在戛纳电影节[私底下被大家称为”猪“ (the Pig)](https://www.leparisien.fr/faits-divers/a-cannes-les-soirees-partouzes-et-cocaine-d-harvey-weinstein-12-10-2017-7325640.php)相呼应。

<center><img src="https://cache.marieclaire.fr/data/photo/w1000_ci/1bi/hashtag-balance-ton-porc-denonciation-harcelement-sexuel.jpg" style=" zoom:50%" /></center>

<center style="font-size:14px;color:#C0C0C0">Sandra Muller掀起法版#MeToo浪潮的推特（图源：Marie Claire）</center>

但#MeToo运动在英语世界掀起的浪潮开始鼓励性侵犯受害者打开心扉、去污名化的时候，它在法国似乎陷入了各界的声讨风波与停滞之中。

## 艺文界：女性解放，还是猎巫行动？

2018年1月9日，包括法国影后凯瑟琳·德纳芙（Catherine Deneuve）在内的100名女性在[法国世界报联名发表一篇评论](https://www.lemonde.fr/idees/article/2018/01/09/nous-defendons-une-liberte-d-importuner-indispensable-a-la-liberte-sexuelle_5239134_3232.html)，谴责“某种主张仇恨男性的女权主义”以及这场“声讨运动”所显现出来的“清教主义”。


“强奸属于犯罪行为，但委婉、不太恰当的调情并不是，对女性献殷勤也不是一种大男子主义的侵犯。”声明中认为韦恩斯坦事件后，公众对男性滥用权力性侵女性的现象开始反思是一件好事，但这种话语权的解放、主张揭露性侵犯的“大型声讨运动”，是对言论自由和“表达爱慕之情的自由”的一种“钳制”：“我们被要求说‘该说的话’，对自己的不满保持沉默，那些拒绝向这种‘指令’屈服的人却会被视为叛徒与共犯”。她们认为，这种“所谓以普遍利益之名、声称保护和解放女性”的观点，实质上将女性永远束缚在受害者的位置；在媒体和社交网络上被公开指控的男性尚没有机会为自己辩护，就被当作罪犯来对待，但他们可能“只是碰了一下膝盖，想偷亲一下，在晚宴席间说了一些‘亲密’的事情，或者向他们仰慕的女性释出含性意味的信号”；这种“自封法官、营造极权氛围的愤怒”不是为女性赋权，而是“为性自由的敌人、宗教极端分子、最恶劣的反动分子的利益服务”。德纳芙因此事遭强烈批评后在解放报上对可能感到被冒犯的受害者致歉，随后在电视节目中表示自己不想再卷入这场争论。

<center><img src="https://www.telegraph.co.uk/content/dam/films/2018/01/09/catherine-jp_trans_NvBQzQNjv4Bq3480UNUU8UfSxDSaY1n7MG8nGi5dnbzU1Uvp1fqu20E.jpg" style=" zoom:18%" /></center>

<center style="font-size:14px;color:#C0C0C0">凯瑟琳·德纳芙在柏林电影节（图源：Telegraph）</center>


除了这场“自由之争”，法国艺文界并没有如美国影坛那样出现一连串骨牌效应，仅有的声音只有法国独立调查媒体[Mediapart](https://www.mediapart.fr/journal/france/dossier/l-affaire-luc-besson)于18年5月开始公布和追踪的9名女性指控吕克·贝松性侵的证词，而媒体调查、法院审理[至今仍未结束](https://www.mediapart.fr/journal/france/170321/affaire-luc-besson-une-instruction-trous?xtor=CS3-5)。

### 阿黛尔·艾奈儿、罗曼·波兰斯基：风起云涌的2019

2019年11月，法国女星阿黛尔·艾奈儿（Adèle Haenel）接受Mediapart长达一个多小时的[专访](https://www.mediapart.fr/journal/france/041119/violences-sexuelles-adele-haenel-veut-que-les-bourreaux-se-regardent-en-face)，揭露导演克里斯托夫·鲁吉亚（Christophe Ruggia）于2001年至2004年执导Adèle银幕首作《恶魔的孩子》（*Les Diables*） 时对她进行性骚扰。当时她年仅12岁，当时已36岁的导演在邀请她去公寓看电影期间性侵了她（亲脖子、闻头发、抚摸大腿、将手伸向性器官、穿过T恤伸向胸部）。当时的她被恐惧支配而无法动弹，在05年与他断联后经历了严重创伤期，一度退出电影圈；直到21岁出演席安玛（Céline Sciamma）执导的《水仙花开》（*Naissance des Pieuvres*）才重新开始演员生涯。

<center><img src="https://fr.web.img5.acsta.net/r_1280_720/newsv7/20/06/03/09/36/26537850.jpg" style="zoom:60%" /></center>
<center style="font-size:14px;color:#C0C0C0">参与反性侵游行的阿黛尔·艾奈儿（图源：AlloCiné）</center>

这份长达5页的[调查报道](https://www.mediapart.fr/journal/france/031119/metoo-dans-le-cinema-l-actrice-adele-haenel-brise-un-nouveau-tabou?page_article=5)获得多方证实：该报访问了超过20位当年参与电影拍摄的相关人员，乃至Adèle的家人与导演前妻，以佐证Adèle的证词。但鲁吉亚透过律师否认指控，在回应Mediapart时表示，“如果对她的爱慕和寄予的希望给她带来了痛苦，我希望能得到她的原谅”。

调查报道登出几天后正值波兰斯基新作《我控诉》（*J'accuse*）在法国上映，摄影师Valentine Monnier受Adèle鼓舞，发声[指控](https://www.parismatch.com/Actu/Societe/Affaire-Polanski-qui-est-Valentine-Monnier-1665004)名导罗曼·波兰斯基在1975年时强奸并殴打她，因此引发法国大批女权主义者发起对电影的抵制运动，多场首映礼和宣传活动也因此被取消。
<center><img src="https://lvdneng.rosselcdn.net/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2019/11/13/node_664931/42927298/public/2019/11/13/B9721569869Z.1_20191113072940_000%2BGN8ET60KL.3-0.jpg?itok=ik26TW8a1573631952" style="zoom:40%" /></center>
<center style="font-size:14px;color:#C0C0C0">示威者在影院外抵制波兰斯基新作（图源：la Voix du Nord）</center>


尽管处在强奸指控风波引起的抵制声浪中，《我控诉》依然在法国取得了票房[胜利](https://www.huffingtonpost.fr/entry/jaccuse-de-polanski-arrive-en-tete-du-box-office-francais-malgre-la-polemique_fr_5dd597a3e4b010f3f1d1d3bc)：上映首周，该片就以超过50万观影人次登顶当周法国票房冠军，也是波兰斯基职业生涯中开画成绩最好的一部。此外，该片在被称为“法国奥斯卡”的45届凯撒奖获得12项提名，居所有入围电影首位。颁奖典礼当晚，当嘉宾宣布波兰斯基获得最佳导演时，Adèle连同电影《燃烧女子的肖像》（*Portrait de la jeune fille en feu*）剧组成员愤怒离场（见现场及后台[视频](https://www.youtube.com/watch?v=9Qy7EJLp3l4)），并大喊“可耻”（*La Honte !*）、“恋童癖干得好！”（*Bravo la pédophilie !*），有媒体称此为凯撒奖历史上最混乱的一幕。

相似的场景在今年重现：《燃烧女子的肖像》导演席安玛（Céline Sciamma）在出席28届“法国电影奖”（*Trophées du « Film français »*）时，因大会将“法国电影荣誉奖杯”颁发给因强奸教子未遂被警方调查的法国国家电影中心（*Centre national du cinéma et de l’image animée*，下称CNC）主席Dominique Boutonnat而选择[离场](https://www.nouvelobs.com/societe/20210308.OBS41107/celine-sciamma-quitte-une-ceremonie-apres-la-remise-d-un-prix-au-patron-du-cnc-accuse-de-tentative-de-viol.html)，以示不满。但奖项执行总监Laurent Cotillon表示，这项荣誉并不是颁给Boutonnat个人，而是授予作为法国公共机构的CNC，其作为CNC主席领奖并无问题。
<center><img src="https://resize-parismatch.lanmedia.fr/img/var/news/storage/images/paris-match/culture/cinema/adele-haenel-pourquoi-elle-a-quitte-la-ceremonie-des-cesar-1677276/27339028-1-fre-FR/Adele-Haenel-pourquoi-elle-a-quitte-la-ceremonie-des-Cesar.jpg" style="zoom:26%" /></center>
<center style="font-size:14px;color:#C0C0C0">凯撒奖颁奖典礼，阿黛尔·艾奈儿愤怒离场（图源：Paris Match）</center>


面对业界人士要求撤回Dominique Boutonnat的奖项要求，法国文化部长Roselyne Bachelot表示应由Boutonnat本人来判断自己是否能继续行使职责，而不是由她来制裁“被视为无辜的人”。[巴黎人报引述消息指](https://www.leparisien.fr/culture-loisirs/cinema/mis-en-examen-et-sous-pression-dominique-boutonnat-patron-du-cnc-reste-soutenu-par-l-elysee-09-03-2021-8427757.php)，文化部长坚持自己的立场，对受害者的证词和无罪推定原则施予同等的尊重，故政府暂时不会考虑收回CNC主席的职位。

票房胜利、艺术殿堂的加冕、2年后的重现，无论民间还是业内，都如Adèle当时接受采访所说的那样，“往所有性暴力受害者的脸上吐口水”。

事发一年后，还发生了什么变化呢？Adèle[拒绝](https://www.nouvelobs.com/cinema/20210312.OBS41294/cesar-2020-une-46e-edition-sans-adele-haenel.html)了今年凯撒奖邀请她回去当颁奖嘉宾的请求；在这一年内她没有任何新片上映，也未传出新片拍摄计划。有公众怀疑她被业界杯葛，因当时事发后，有选角经理扬言要[封杀](https://www.lavoixdunord.fr/720249/article/2020-03-05/un-directeur-de-casting-menace-l-actrice-adele-haenel)她，声称“她的职业生涯已死”。而媒体追踪Adèle动向时[表明](https://www.leparisien.fr/culture-loisirs/cinema/cinema-un-an-apres-sa-sortie-fracassante-aux-cesars-ou-est-passee-adele-haenel-07-03-2021-8427507.php)自己目前将重心转移到戏剧上，也有业界人士表明在公共议题上持自己意见的公众人物很多，相信业内不会因为Adèle在事件上的表态而被行业封杀。

<center><img src="https://i.imgur.com/W6gigh7.jpg" style="zoom:25%" /></center>
<center style="font-size:14px;color:#C0C0C0">费加罗报对两个相似事件截然不同的反应（图源：Le Figaro）</center>

而在今年凯撒奖上，女演员[科琳娜·马西埃罗](https://www.bfmtv.com/people/rends-nous-l-art-jean-l-apparition-choc-de-corinne-masiero-aux-cesar-2021_AN-202103120512.html)（Corinne Masiero）身穿印有 “没有文化就没有未来”字样的黄马甲抵达红毯；而颁发最佳服装奖时，她身着驴皮公主（*Peau d’Âne*，法国童话）服装上台，全身涂满假血，将用过的卫生棉条做耳环，当着现场电视观众的面脱下服装，以抗议法国政府无视文化届诉求。在同场凯撒奖典礼上，女演员及导演让娜·巴利巴尔（Jeanne Balibar）在台上也[谴责](https://www.youtube.com/watch?v=E-AlK4PnlK4)了法国电影的性别和年龄歧视：“在法国，40岁以上的女性占人口的51%，却只占银幕角色的8%；更不用说银幕时间，可能只有2%左右。”

社交网络舆论迅速发酵，有人赞美马西埃罗的勇气，也有许多充满性别、年龄和阶级歧视的批评。2015年，相似的场景在法国莫里哀戏剧奖典礼上演：男演员塞巴斯蒂安·蒂埃里（Sébastien Thiéry）在台上全裸发表了长达4分多钟的[发言](https://www.lefigaro.fr/theatre/2015/04/28/03003-20150428ARTFIG00055-molieres-2015-sebastien-thiery-nu-apostrophe-fleur-pellerin.php)，呼吁公众关注戏剧界的弱势与困境，而发言后主持人则请大家再次为其鼓掌。法国著名右派报纸费加罗报对两件事做出了截然不同的反应：赞叹蒂埃里的大胆、勇敢，而马西埃罗的行为却让法国电影界“蒙羞”。


## 政界：无视抗议，丑闻不断

正如法国文化部长对CNC主席争议的回应，法国政府对政客相关的性暴力指控一个秉持“中立”、“拒绝未审先判”的态度，冷处理或拒绝接受社会上要求被指控政客下台的诉求。2020年6月，马克龙政府因上任总理爱德华·菲利普（Edouard Phillip）胜出地方选举后辞职而宣布重组内阁，其中内政部长由达马南（Gérald Darmanin）接任。他曾被[指控](https://www.liberation.fr/france/2021/01/27/plainte-pour-viol-de-nouveaux-elements-troublants-dans-la-defense-de-darmanin_1818555/)在2009年利用职权之便以法律扶助、帮忙申请住房为名性侵女子苏菲·帕特森（Sophie Patterson），但达马南否认指控、声称两人是在双方知情且同意的情况下发生性关系；2018年时检察官以证据不足为由终止调查。

![](/img/po1_1.png)

<center style="font-size:14px;color:#C0C0C0">20年7月10日，巴黎街头反对马克龙任命决定的示威人群（图源：Radio France/Nathanael Charbonnier）</center>

这项任命决定在法国各地引发了大量民众上街游行，要求马克龙撤回任命决定、让达马南下台。2020年6月9日，巴黎上诉法院最终下令重启调查。但法新社援引马克龙身边消息人士指出，重启调查并不会影响任命决定，法国政府不会对正在调查的案件发表评论。马克龙随后也在BFMTV接受采访时被问及如何回应相关争议时[表示](https://www.leparisien.fr/politique/interview-du-14-juillet-macron-defend-la-nomination-de-darmanin-14-07-2020-8352639.php)，他支持女权事业，但是如果这种正义的事业是建基于对民主基本原则的“蔑视”之上时，就已经“不再正义”；如果被指控者被未审先判，其也成了社交网络和街头抗议的“受害者”。他认为“我们的民主在本质上变成了一种'意见民主'”，重申自己珍视寻求公义和性别平等的抗争，但同时也珍视“让其本身更加强大、不向情绪屈服的民主”。

**但诉诸“理性”与“中立”，将抗议声浪矮化成情绪化的无理宣泄，甚至污名化为对民主价值与体系的攻击，是回应公众诉求的最佳出口吗？将一切交给法庭看似“公正”与“理性”，但现有政治和法律框架能保护受害者的合法权益、推动性别平等吗？**


性侵丑闻在法国政圈已非新鲜事。除了震撼法国政坛的DSK事件外（Dominique Strauss-Kahn，被指在纽约一家酒店强奸一名女性的法国政客，曾被认为是法国大选热门人选），还有在16年，被含4名议员在内的8名女性指控在1998至2014年间对她们实施性骚扰或强奸的前法国国民议会副议长德尼·博潘（Denis Baupin），包括袭胸、强吻绿党EELV发言人之一[桑德琳•鲁索](https://www.dw.com/zh/17名法国前女部长我们不再沉默/a-19260907)（Sandrine Rousseau），短信轰炸性骚扰议员伊莎贝拉•阿塔尔（Isabelle Attard），性骚扰绿党前成员[安妮·拉莫](https://www.lci.fr/societe/affaire-denis-baupin-agressions-harcelements-sexuels-classee-sans-suite-une-victoire-quand-meme-pour-annie-lahmer-une-des-victimes-2028235.html)（Annie Lahmer）被拒绝后威胁毁其政治前途。19年，巴黎检察院立案调查后认为案件时效已过，宣布将该案归档。

2011年，曾任国会议员及巴黎南部郊区德拉韦伊市长的乔治·特隆（Georges Tron）被两名女性下属维吉妮·埃特尔（Virginie Ettel）和伊娃·洛布里尤（Eva Loubrieu）控告，指控其2007至2010年间以“练习推拿”为由，在副手的协助下在办公室进行“足部按摩课程”，对她们实施性侵和强奸。特隆坚称自己“无罪”，而当时为其辩护的律师是法国现任司法部长埃里克·杜邦-莫雷蒂（Eric Dupond-Moretti）。庭上，埃特尔[形容](https://twitter.com/Mar_Barbier/status/941352589190483973)自己当时出于恐惧，不得已闭上双眼，而莫雷蒂则质疑她无法说明真实发生的情况，要求控方阐明难以举证的事件，如要求原告举证被告是否在内裤中射精。17年，法院因“没有足够证据显示上述性关系违反双方意愿”而宣布特隆无罪释放，事后他接受媒体[采访](https://www.lepoint.fr/justice/eric-dupond-moretti-plaider-c-est-bander-convaincre-c-est-jouir-19-04-2018-2212050_2386.php)表示“辩护即勃起，胜诉即高潮”（*« Plaider, c'est bander ; convaincre, c'est jouir »*）。不过，检方在2018年对法院判决提出上诉，陪审团维持其中一名原告埃特尔对特隆的强奸指控，他最终被判处5年监禁。

![](/img/po2_1.png)


<center style="font-size:14px;color:#C0C0C0">法国现任司法部长埃里克·杜邦-莫雷蒂（图源：AFP / Ludovic Marin）</center>

当年为其辩护的现任司法部长莫雷蒂也是法国反MeToo运动的[标志性人物](https://www.franceinter.fr/politique/eric-dupond-moretti-l-anti-metoo)。在被问及对#BalanceTonPorc运动的看法时，他谴责那些滥用权力实施性暴力的男性，但问题在于社交网络只会损害无罪推定原则，不应该是发出这些指控的场所，还指现实中依然存在许多女性心甘情愿用性换取利益；被问及对特隆案重审的看法时，他指责原告获极右翼政党支持，想从特隆身上“榨取”30万欧元，并指责受害者“如果第一次发生关系时觉得自己被强奸，就不会再有后面的24次”。

在马克龙宣布任命后引发的示威浪潮中，莫雷蒂与达马南都是示威者的谴责对象，但马克龙政府面对大规模的抗议声音无动于衷。


## 学界：被揭露的性侵文化，被攻击的性别研究

暂且不讨论法国文化中对诱惑与调情的迷恋与执着，政治圈常年不断曝光的性丑闻，其背后暗藏的性暴力传统早已在培养法国政坛精英的各地政治学院中存在已久。

今年3月8日妇女节，法国著名左派报纸解放报头版刊登了一封强奸犯写给受害者的[“忏悔信”](https://www.liberation.fr/societe/droits-des-femmes/le-mot-viol-etait-ecrit-noir-sur-blanc-20210307_OJPRR6K5RVCIZIFLOBMLOTD3W4/)，引起舆论哗然。

<center><img src="https://i.imgur.com/pqJStgU.jpg" style="zoom:50%" /></center>
<center style="font-size:14px;color:#C0C0C0">解放报3月8日头版左侧文章，“我强奸了你，Alma”（图源：Libération）</center>

文章主角是一名就读于波尔多政治学院（*Sciences Po Bordeaux*）、名叫Alma的女生。19年4月，她的前男友强奸了仅18岁的她后当即与她分手；第二年12月，她感觉自己被悲伤、不解、恐惧和愤怒吞噬，因抑郁症入院治疗。直至今年1月份，她才第一次与友人分享自己这份恐惧的来源。几天后，她决定在波尔多政治学院的学生Facebook专页打破沉默，揭露前男友对她的性暴力。住院期间，她收到了来自前男友的信。

信中，他承认被极端和罕见的情绪吞噬，用自己原始、兽性的目光，如审视死物般看待受害者。他开始质问性别、性取向、阳刚气质和父权压迫的问题，希望自己忏悔的证词能帮助Alma重建生活。他声称自己儿时是恋童癖的受害者，也希望看到个人和集体的反思。Alma认为，看到侵犯自己的人承认自己的罪行，是一种解放；她不希望看到对方被关在监狱里，而希望他得到治疗。**她理解和尊重有些受害者选择不听施暴者的说辞，但只要施暴者和受害者的话语权不被倒置，能让施暴者承认自己的暴行便是一种胜利。**

<center><img src="https://www.liberation.fr/resizer/pWA7cL9KaFdSZa-3i05zwUBSKos=/800x0/filters:format(jpg):quality(70):focal(2995x965:3005x975)/cloudfront-eu-central-1.images.arcpublishing.com/liberation/Z5536PNMK5BYFKWFSK62ONCB4Y.jpg" style="zoom:60%" /></center>
<center style="font-size:14px;color:#C0C0C0">妇女节游行中声援Alma的示威者（图源：Marie Rouge/Libération）</center>

许多愤怒的读者指责解放报在本应倾听女性声音的日子里，将话语权交给了性侵女性的男性；即使文中施暴者表明愿意公开和承担法律责任，但文章花费大量笔墨书写施暴者内心历程演变，回溯暴力行为的根源时却只用“恋童癖受害者”、父权与阳刚气概的负面效应匆匆带过；相反，受害者的说辞与经历被放在边缘的位置，有人认为文章编排、遣词造句隐隐将Alma的回应刻画成对施暴者忏悔的嘉奖，质疑报刊的编辑选择是否在引导读者对施暴者产生同情。但也有人质问，仅仅倾听受害者的论述、“一面之词”，难道就已经足够了吗？需不需要倾听肇事者的说法，或者创建双方对话的空间？

**在权力关系本身就已极不平衡的状态下制作对话的空间，是不是一种建基于不平等关系之上的对受害者的二次伤害？在“性”和“被性侵”仍旧被污名化、受害者往往成为众矢之的而施暴者却更容易得到同情的当代社会，这种建立对话的想象是不是天方夜谭？** 寻求这些问题的答案或许是永恒的课题，而Alma所经历的性暴力并不止步于波尔多，在#SciencesPorc的标签下，声讨性侵的星星之火开始在法国各地政治学院内蔓延。

### #SciencesPorc：高校版MeToo

今年二月，社交网络上出现了新一轮谴责性暴力的浪潮，而这次风波的中心是分布在十个法国城市、简称为“Sciences Po”的政治学院。作为法国MeToo运动#BalanceTonPorc的分支，发起人借用Porc（猪）的意象，结合“Sciences Po”组成了法国高校版的新一轮MeToo运动。

风波开启于几周前爆发的乱伦性侵丑闻：[今年年初](https://www.rfi.fr/cn/法国/20210210-不堪校内丑闻压力，巴黎政治大学校长辞职)，法国著名宪政学者杜哈梅尔（Olivier Duhamel）的继女卡米耶·库什内（Camille Kouchner）出版名为《大家庭》（*La Grande Familia*）的回忆录，其中揭露其继父性侵未成年继子和乱伦。杜哈梅尔在事发前曾担任巴黎政治大学（*Sciences Po Paris*）教授、巴黎政治学院重要资助方法国政治学全国基金会主席等职位。事件发生一个多月后，被指事先对此事件知情而无作为的巴黎政治大学校长弗雷德里克·弥雍（Frédéric Mion）2月9日致信全校师生宣布辞职。

<center><img src="https://images.midilibre.fr/api/v1/images/view/602961c13e45463144629886/large/image.jpg?v=1" style="zoom:60%" /></center>
<center style="font-size:14px;color:#C0C0C0">斯特拉斯堡政治学院门前声援受害者的学生示威者（图源：MAXPPP - CORINNE FUGLER）</center>


而#SciencesPorc的谴责对象则是施暴者以及政治学院管理层，斥责他们面对性侵举报保持沉默、不作为；反观肇事者，无论是学生还是教授，都因此得以逃避责罚。运动的[导火索](https://france3-regions.francetvinfo.fr/occitanie/haute-garonne/toulouse/sciences-po-toulouse-une-jeune-femme-denonce-sur-les-reseaux-sociaux-des-viols-dont-elle-aurait-ete-victime-1947082.html)是一名叫朱丽叶（Julliette）的图卢兹政治学院（*Sciences Po Toulouse*）学生在社交网络上发表的一封公开信，她在信中控诉自己被一名男学生强奸，而学校的无作为纵容了这些性暴力在政治学院里面横行。当地检察院在她提诉后立案调查，另外共有三个政治学院内部性侵相关案例在[格勒诺布尔](https://www.ledauphine.com/faits-divers-justice/2021/02/10/isere-grenoble-violences-sexuelles-mouvement-sciencesporcs-deux-signalements-a-l-iep-de-grenoble)和[斯特拉斯堡](https://etudiant.lefigaro.fr/article/sciencesporcs-les-etudiants-de-l-iep-de-strasbourg-se-mobilisent-pour-denoncer-les-violences-sexuelles_50bf0f16-6d48-11eb-847c-d66948eeb09e/)开展调查。1月以来，在波尔多政治学院，有超过100多份谴责性别歧视、性暴力的证词在社交网络上流传，促使学院管理层成立专项工作组，以更有效地避免性侵行为发生。3月，运动热度不减，普罗旺斯艾克斯政治学院（*Sciences Po Aix*）又相继[爆出](https://france3-regions.francetvinfo.fr/provence-alpes-cote-d-azur/bouches-du-rhone/aix-en-provence/sciences-porcs-a-sciences-po-aix-les-temoignages-se-multiplient-1953913.html)多项性侵指控；这一连串事件的后续发展、调查结果还有待观察。

如果"社交网络不应是指控性侵者的场所、性别平等的载体"，诉诸司法成为受害者寻求公义的"合理方式"。那鼓起勇气分享自己伤痛经历的受害者，在现有司法体制内能得到尽可能公平的对待吗？

![](/img/po3_1.png)


<center style="font-size:14px;color:#C0C0C0">法国国家警察官方推特中关于情欲短信风险的文宣（图源：Police Nationale）</center>

3月6日，法国国家警察官方推特发布了一则关于“情欲短信”的宣传推文：“发出一张裸照，就等于接受这张图片被广传的风险。”配图中的文字则表明，“他收到了你的裸照，你的朋友、父母、同学、表亲、老师、邻居...也都收到了”。推文发出后立刻引起广大网民批评，指责身为司法机关却将裸照泄露的责任归在受害人头上，而肇事者在此则宣传中似乎毫无责任。也有评论指出，正是这种“受害者有罪论”才让因为裸照泄漏被勒索和威胁的受害者不敢报案、伸张正义，批评再三自称“维护女性权益、推动性别平等”的政府机构带头污名化受害者。引发争议后，法国国家警察删除该则推文，并承认“措辞不当”。但受害者是否能得到公平对待的疑问，并没有随着推文一起消失。

在法律方面，一名66岁的女子[芭芭拉](https://www.mediapart.fr/journal/france/170321/pour-la-justice-refuser-des-relations-sexuelles-son-mari-est-une-faute)（Barbara，化名）因拒绝与前夫发生性关系，被法院以“行为不端”为由判为离婚官司中的责任方。芭芭拉表示，前夫不承担4个儿女的育儿责任，且常年家暴她以及罹患身心障碍的小女儿，她也因此患上精神疾病，需长期服药控制，也以此为理由拒绝和前夫发生性关系。而前夫则以她“未尽婚姻的义务”为由将过错离婚的责任归咎在芭芭拉身上。上诉法院认为她以精神问题为由常年拒绝与丈夫行房有违“婚姻的义务和责任”，且在2020年9月驳回了她的上诉，代表此结果为最终判决。芭芭拉对“婚内责任”判决不服，将此案提交欧洲人权法院。

## 语言、文化，与愈发分化的社会

公众对打破传统与惯有思想的恐惧或许是所有平权运动都会面临的阻碍；法国人对近年兴起的平权运动产生的抵触心理，也出于“保护”自己引以为傲的文化和“法兰西价值”。这种“自我保护机制”，在语言改革和面对“外来文化入侵”时体现得淋漓尽致，也让这个提倡普遍主义、统一国族认同的社会更加分裂。

### “含她书写”（Écriture inclusive）：语言中的性别歧视

对女性的暴力在日常生活中横行，导致这种不平等的根源之一或许藏匿在我们每天使用的语言之中；对于高度性别化的语言来说，法语可能是性别不平等的重灾区。

今年2月，法国费加罗报[指出](https://etudiant.lefigaro.fr/article/a-sciences-po-paris-des-points-bonus-accordes-pour-l-utilisation-de-l-ecriture-inclusive_70f90efe-777e-11eb-930c-df8a7d3f696f/)巴黎政治学院要求学生在一次期末考试中使用“含她书写”（*Écriture inclusive*，另译“包容性书写”）。在考试说明中，教师鼓励使用“含她书写”，不使用的学生并不会被处罚,但使用了该种书写的学生将会获得0.5的加分。<font style="font-size:16px;color:#708090">（注：法国评分系统为20分满分制，0.5分可视为对最终成绩有一定影响的加分）</font>

而什么是“含她书写”？首先要简单了解法语的几点语法规则：
1. 法语所有名词都有阴阳性之分；
2. 由于“阳性先于阴性”（*le masculin l'emporte sur le féminin*，可被阐释为“男尊女卑”）的语法规则，如果同时存在阳性和阴性名词，代词将使用阳性Il（“他”）的复数形式Ils（“他们”），阴性名词的存在在此被抹去；
3. 由于过去女性无权从事许多岗位，所以如教师（professeur）、作家（écrivain）、主席/总统（président）、律师（avocat）等职业名词没有阴性形式。

而“含她书写”则为了改变这种语言性别歧视，提出几条规则：
1. 对女性使用职业名词的新造阴性形式，如professeure, écrivaine, présidente, avocate
<font style="font-size:16px;color:#708090">（注：但此项倡议在提倡改革的群体内部也有争议，指责阴性职业名词从阳性形式演变而来是父权压迫的另一种体现，且部分先前已存在的阴性形式大多含贬义，或指代从事该门职业男性的妻子，有矮化、物化女性之嫌）*</font>
4. 阴性和阳性名词同时存在时，使用包含阳性Il/Ils和阴性Elle/Elles在内的“Iels”作为代词，职业名词也需要做出相应改变，如professeur·e·s
<font style="font-size:16px;color:#708090">（注：Iels为新创代词，所衍生的动词变位问题目前尚未规范化，也成为许多反对者的攻击点之一）</font>
6. 不用“男人”（homme）来代表全体人类，如不使用“*droits de l'homme*”（法语中的“人权”，但也可被阐释为“男权”）而用“*droits humains*”（“人类的权利”）

<center><img src="https://cdn.radiofrance.fr/s3/cruiser-production/2021/02/05627252-c133-46d3-8946-77661c1bf315/838_maxbestof175316.jpg" style="zoom:60%"/></center>
<center style="font-size:14px;color:#C0C0C0">含她书写在年轻人中逐渐流行（图源：France Culture）</center>

而“含她书写”的最大反对者则是负责规范化法语的法兰西学院（*l'Académie française*），[认为](http://www.academie-francaise.fr/actualites/declaration-de-lacademie-francaise-sur-lecriture-dite-inclusive)这种“混乱”的书写系统破坏了法语的“正统性”，导致语言的分裂，加大了阅读和学习法语的困难，将法语置于“致命危险”之中。而今年2月，法国国会60名议员联名签署一份[新法案](https://www.leparisien.fr/societe/ecriture-inclusive-cinq-minutes-pour-comprendre-le-retour-du-debat-sur-son-interdiction-25-02-2021-SQCAVU6Z55EX3FBZPBXYJFEEP4.php)，要求在行政文件和在接受公共服务任务的法人企业中禁止使用这种书写方式，认为其“易引争论”、“不被大多数人接受”、“使信息变得混乱”，并不是“性别平等的载体”。这项新法案由国会副主席弗朗索瓦·若利韦（François Jolivet）及几位马克龙的亲近盟友提出，支持这项法案的大多数是来自LREM（共和国前进党，法国目前的执政党）和右翼党派的议员；若利韦还曾呼吁禁止在学校教授这类书写方式。

而法国政坛对“含她书写”的突然攻击并非空穴来风：今年开始，马克龙政府开始批评在英语世界兴起的性别、种族和后殖思潮对法国高校及社会的不良影响，性别研究、种族研究被政客批评甚至攻击；许多支撑点建基于性别研究、旨在消除语言性别歧视的“含她书写”也在这场风波中成为批判对象，被指责系“矫枉过正”。

### 文化战：性别研究、“Wokism”，“荼毒”大学的舶来品？

今年2月，法国高等教育部长弗雷德里克·维达尔（Frédérique Vidal）接受保守电视新闻媒体CNews访问时[表示](https://www.lefigaro.fr/politique/islamo-gauchisme-une-large-majorite-de-francais-soutient-frederique-vidal-20210224)，“伊斯兰左翼主义在腐蚀我们的社会和大学”，引发巨大争议。她强调来自美国的性别、种族、交叉性和后殖研究关注基于身份的少数群体的歧视，助长了左派的“激进”和“好战”。

<center><img src="https://i.f1g.fr/media/eidos/704x396_cropupscale/2021/02/24/XVM6167e164-76ce-11eb-9ea8-48b0d7a02ee9.jpg" style="zoom:70%" /></center>
<center style="font-size:14px;color:#C0C0C0">法国高等教育部长弗雷德里克·维达尔（图源：GUILLAUME SOUVANT/AFP）</center>

根据著名民意调查机构ifop的[报告](https://www.lexpress.fr/actualite/societe/sondage-exclusif-ecriture-inclusive-privilege-blanc-ces-notions-encore-peu-connues_2145943.html)，“白人特权”（white privilege）、“系统性种族歧视”（systematic racism）、“强奸文化”（rape culture）或“有害的男子气概”（toxic masculinity）之类的词汇在法国依然鲜为大众所知，只在左派进步青年中流行。但在美国，“woke文化”（“觉醒”文化）因Black Lives Matter（“黑命攸关”）运动开始在互联网走红，逐渐被广泛应用于不同领域。“Stay Woke”（“保持觉醒”）意指对社会问题、尤其是各种形式的不平等保持高度警觉的状态。这种“wokeness”（“觉醒性”）已经跨越校园，蔓延到企业和社会各个角落。而在法国高校，越来越多争论开始围绕这些逐渐流行的概念，特别是基于肤色、宗教或性别的歧视相关研究。

研究相关议题的人文社科学者感受到了外界日益紧绷的气氛，学者群体内部的矛盾也越来越激烈。批判性别、种族、殖民等研究的人认为，这些专注于身份的理论和研究是对“共和精神及普遍主义的威胁”。学者被议员要求维护和遵守定义模糊的“共和国价值”，高教部长也声称要在大学内对“伊斯兰左翼主义”展开调查。1月，76名法国学者[成立](https://www.lepoint.fr/politique/appel-de-l-observatoire-du-decolonialisme-et-des-ideologies-identitaires-13-01-2021-2409523_20.php)了“去殖主义及身份意识形态观察所”（*Observatoire du décolonialisme et des idéologies identitaires*），以抵制将个人“简化成种族与性别”的意识形态，马克龙也认为应当对这种美式思潮保持警惕。政府的高调表态某种程度上也助长了社会上对相关研究的[仇恨](https://www.lemonde.fr/societe/article/2021/03/15/a-l-universite-une-guerre-de-tranchees-autour-des-questions-de-race-de-genre-ou-d-ecriture-inclusive_6073126_3224.html)：最近几个月，不断有学者与大学教员被人身攻击或收到死亡威胁，这些人身安全威胁在他们的研究被政府高层指责“挑起社会分裂和冲突”，甚至“助长恐怖主义”后变得更加严重。

### 两极分化的社会，共识难寻

回顾先前提及的MeToo运动、语言改革，很容易察觉支持变革的群体特质：年轻、学生、认知到社会不公、认同平等价值等。但左派青年并非社会主体，社会大众又是如何处理这些议题？

<center><img src="https://static.lexpress.fr/medias_12296/w_1520,c_fill,g_north/sondage-exclusif-les-francais-ne-parlent-pas-woke_6295922.png" style="zoom:30%"/></center>
<center style="font-size:14px;color:#C0C0C0">“觉醒”一代所提倡的观念远未渗透法国社会（图源：Art Presse）</center>


先前提及的民意调查显示，“觉醒”文化远未渗透法国社会。有学者提出，对这些概念的了解程度与年龄和教育水平相关：它们首先在社交网络上开始流行，所以年轻人了解得更多；其次，受过高等教育的人更关心时事，也更加了解会处理这些议题的媒体。此外，向来对公权力保持批判态度的法国人，却对高教部长“伊斯兰左翼主义”的言论[表现](https://www.lefigaro.fr/politique/islamo-gauchisme-une-large-majorite-de-francais-soutient-frederique-vidal-20210224)出少见的支持：超过65%的群众支持高教部长的观点，这个比例在中间派和右翼群众中更高达80%及以上。

这些民调数字或许可以为马克龙面对抗议声浪却坚决不撤回任命决定、近期不断攻击进步思潮的取态提供了一个解读的角度。作为一个自称走中间路线的政客，马克龙为争取连任，企图打破法国政治的左右分野。拍Snapchat、接受主攻年轻群体的网媒Brut采访、玩Tiktok、上Twitch直播，这些宣传策略都是为了塑造年轻亲民形象来拉拢年轻人；另一方面，他又被认为是亲资本、亲富人的中间偏右政客，在税改上的决定引爆持续多年的黄马甲运动。为了拉拢中间选民和拥护传统价值的右翼保守派，他选择放弃难以说服的左派青年而发表各式言论、出台惹争议政策来“安抚”认同国民身份的群众对传统价值被挑战而产生的不安情绪，抵触注重区分你我的外来思潮，借此不断强调一个统一的法兰西身份认同（*l'identité nationale*）。

<center><img src="https://www.leparisien.fr/resizer/3fuuQpG3cWNxaG7qjzHgnl59E3c=/932x582/cloudfront-eu-central-1.images.arcpublishing.com/leparisien/CYUFGYKU744TSXUCRN4SWABK2E.jpg" style="zoom:50%"/></center>
<center style="font-size:14px;color:#C0C0C0">马克龙接受Brut访问（图源： LP/Olivier Corsan）</center>

而莫雷蒂很符合这种典型的“法兰西形象”——他吸烟、喝红酒、直言不讳、百无禁忌，对政治正确不屑一顾，他象征着法兰西的“光荣传统”；作为意大利移民后代，他一路打拼成为著名律师，是一个有说服力的移民融入典范。对“至高无上”法语的改革、对精英主义政坛的控诉，都是对“不朽传统”的冲击。至于波兰斯基，则是在漫改电影潮流和好莱坞文化“入侵”之下艺术电影“过往荣光的再现”。与女性权益相比，维护这门源于法国的第七艺术的“尊严”，对法国艺术界来说或许更加重要。

**互联网的出现和社交网络的发展让同温层效应不断增强，人们在信息茧房的庇护下围炉取暖，免受对立观点的冲击。如果规则与秩序的改变需要建立在社会共识之上，面对这种对性别、阶级和种族不平等视而不见的“自由主义正义观”，进步群体和保守群体之间愈发撕裂的状态，让公众还有机会达成共识、促成改变吗？**

对性侵指控与“调情自由”、无罪推定、言论自由的争论依旧在持续，将艺术作品与人分开对待的要求似乎依然是行内主流；新冠无法挡住左派青年街头抗争的步伐，但政界的攻击、学界的内讧、两极分化的社会现状，让性别研究与平等运动举步维艰。

在陈旧的文化与僵死的制度面前，法兰西的性平之路依然漫长。



>后记：
>笔者在整理、陈列事件时，曾反思过自己在事件的选取和呈现上是否因个人对事件的取态而过度偏颇，为读者刻画了一个在性平议题上“过度极端”的法国。在许多上述的事件中，其实依旧有许多乐观的信号——比如Adèle勇敢发声、凯撒奖上抗议离场，当即都获得了业内和外界的大量支持；比如法国街头上依然有很多推动社会平等的示威游行，高校行政及大学各种协会也会发起各式宣传、成立委员会以保护受害者、杜绝校园性侵行为；比如因心理疾病拒绝与丈夫发生性关系被法院判处“未尽婚姻义务”的女性，有优秀的女性律师团队和媒体为她辩护和发声……
>
>但在抗议文化发达的法国社会，异见、游行是再平凡不过的事情；会不会正是因为这种“抗议变成再普通不过的日常”的心态和现状，让不同的声音难以进入体制内促成改变？甚至在精英把持的艺文作品加持下，外界对法国（主动或被动）的“激进”、“进步”、“平等”的想象，导致掌权者沉浸在这种为外界、也是为自己营造的虚幻泡沫里，无视残酷现实里的不公与反抗？
>
>在这种不平衡的话语结构下“客观叙述”、“均等描述”，或许是另一种加强不平等的“共犯”。所以我们要花更大的力气、用更多的笔墨，来书写不公、直面反差。这可能是笔者某种自我安慰、自圆其说的思考，故以后记的形式将判断的权力留给各位读者。