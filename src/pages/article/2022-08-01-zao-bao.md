---
templateKey: article
title: 8.1 N记早报
slug: zao-bao
date: 2022-08-01T05:12:19Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: NL

---
**1. 解放军在福建平潭海域实弹训练，距台湾 125 公里**

7 月 29 日，解放军在福建平潭附近海域举行[实弹训练](https://china.huanqiu.com/article/492BIKoXtQo)，距离台湾约 125 公里。7 月 30 日，中国空军发言人称有能力派遣多型战机[“绕飞祖国宝岛”](https://m.thepaper.cn/newsDetail_forward_19253565)。同一时间，解放军[宣布](https://www.chinatimes.com/newspapers/20220731000291-260118?chdtv)在南海多个海域进行军事演习，8 月 2 日还将在雷州半岛海域进行射击训练。

解放军频频示威与美国众议员议长南希·佩洛西（Nancy Pelosi）本周开启的亚洲行程相关。佩洛西将访问新加坡、马来西亚、韩国和日本，其[最新行程表](https://www.speaker.gov/newsroom/73122)上并未列出台湾，但并不排除其造访台湾的可能性。 #台湾

**2. 伊拉克萨德尔运动支持者占领议会**

自 7 月 27 日开始，支持伊拉克什叶派领导人穆克塔达·萨德尔（Muqtada al-Sadr）的示威群众[冲进伊拉克议会](https://chinese.aljazeera.net/news/political/2022/7/31/%E8%90%A8%E5%BE%B7%E5%B0%94%E8%BF%90%E5%8A%A8%E6%94%AF%E6%8C%81%E8%80%85%E8%BF%9E%E7%BB%AD%E7%AC%AC%E4%BA%8C%E5%A4%A9%E5%9C%A8%E4%BC%8A%E6%8B%89%E5%85%8B%E8%AE%AE%E4%BC%9A%E6%89%8E%E8%90%A5%E7%A4%BA)，抗议腐败并反对伊朗对伊拉克政治结构的操纵。示威群众在议会花园扎营，直到萨德尔呼吁他们回家。去年 10 月，伊拉克萨德尔集团在穆克塔达·萨德尔的领导下赢得了议会多数席位，但由于各政治派别之间的分歧，一直未能组建新政府。今年 6 月，萨德尔率领 73 名议员集体辞职。7 月，亲伊朗的什叶派“协调框架”推举穆罕默德·希亚·阿尔-苏达尼（Mohammed Shia al-Sudani）为新总理人选，萨德尔[拒绝了他的候选资格](https://chinese.aljazeera.net/news/2022/7/28/%E4%BC%8A%E6%8B%89%E5%85%8B%E6%94%BF%E5%B1%80%E6%B7%B7%E4%B9%B1%E6%8A%97%E8%AE%AE%E8%80%85%E4%B8%BA%E4%BD%95%E5%86%B2%E8%BF%9B%E8%AE%AE%E4%BC%9A)。 #伊拉克

**3. 俄罗斯要求关闭境内犹太组织机构**

据[经济学人](https://www.economist.com/middle-east-and-africa/2022/07/28/israels-russian-conundrum)，7 月 15 日，俄罗斯法院要求关闭在俄境内长期负责犹太人移民事务的犹太机构（Jewish Agency）。报道指出，随着俄乌战争升温，至少有三万名离散犹太人从俄罗斯和乌克兰移居至以色列。据半岛新闻，一个以色列代表团为此前往俄罗斯[交涉](https://chinese.aljazeera.net/news/political/2022/7/22/%E4%BB%A5%E8%89%B2%E5%88%97%E4%BB%A3%E8%A1%A8%E5%9B%A2%E5%89%8D%E5%BE%80%E8%8E%AB%E6%96%AF%E7%A7%91%E8%AE%A8%E8%AE%BA%E6%AD%A4%E4%BA%8B%E4%BF%84%E7%BD%97%E6%96%AF%E8%A6%81%E6%B1%82%E8%A7%A3%E6%95%A3)。在前苏联解体后，超过一百万犹太裔苏联公民回到以色列定居。

以色列和俄罗斯曾在中东地区军事问题上保持紧密合作，自 2015 年俄罗斯向叙利亚派兵以来，俄方让出叙利亚的领空，以便以色列军方持续轰炸在叙利亚境内的伊朗武装势力。直至今年 7 月，以色列新总理拉皮德上台后，他对俄方入侵乌克兰表示谴责，以色列-俄罗斯关系迅速降温。 #俄罗斯 #以色列

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
