---
templateKey: article
title: This Week China Society - Commemorating Liu Xiaobo When Facing the Sea
slug: weekly-newsletter-18-july
date: 2021-07-18T01:28:20.370Z
description: This is NGOCN’s second weekly newsletter. We’re covering the 4th anniversary of Nobel laureate Liu Xiaobo’s death, the censorship against media Elephant Magazine, and other news that happened in Chinese society.
featuredpost: true
trending: 7
isbrief: false
columnist: '英文周报Weekly Newsletter'
contributors:
  - type: Author
    name: 小戌
  - type: Editor
    name: Samuel

---



Happy weekend. This is NGOCN’s second weekly newsletter. We’re covering the 4th anniversary of Nobel laureate Liu Xiaobo’s death, the censorship against media Elephant Magazine, and other news that happened in Chinese society.



## Nobel laureate Liu Xiaobo commemorated on the 4th anniversary of his death



Chinese dissidents, civil rights activists, scholars and writers worldwide gathered online or near China’s overseas authorities, mourning Nobel laureate Liu Xiaobo’s 4th anniversary of death on July 13. 



In Hong Kong, Wong Ho-Ming, president of the League of Social Democrats, a left-wing democratic party, held the commemoration in front of China’s liaison office surrounded by the police. Wong said, “only a democratic and constitutional China can bring happiness to the people.”



In Germany, Liao Tianqi, president of the PEN International Peace Committee, hosted the commemoration in St. Patrick Church. “There is no grave and no tombstone for Liu Xiaobo in his own country, and the public is not allowed to mourn him,” Liang said. “This is not acceptable.”



Liu Xiaobo was described as China's most prominent dissident and the country's famous political prisoner. He was one of the leading pioneers in the 1989 pro-democracy protest at Tiananmen Square, and later co-drafted the Charter 08, a petition calling for democratic reforms in China, resulting in an 11 years’ imprisonment on the grounds of “inciting subversion against state power.” 



In 2010, Liu Xiaobo was awarded the Nobel Peace Prize but was unable to attend the ceremony, where an empty chair became a symbol for him.



On July 13, 2017, Liu Xiaobo died from complications of liver cancer in a hospital, guarded by state security police.



Banned from being commemorated, Liu Xiaobo’s bone ash was spread into the sea, which became a metaphor to mourn him. As a popular saying goes, “Without the grave, without the ashes, you will become a memory of history, a memory of humanity.”



![img](https://raw.githubusercontent.com/syeung19/image/main/20210720001540.jpg)

["Memorial for Liu Xiaobo."](https://www.flickr.com/photos/156898314@N04/35912523751) Source: [etanliam](https://www.flickr.com/photos/156898314@N04) by [CC BY-ND 2.0](https://creativecommons.org/licenses/by-nd/2.0/?ref=ccsearch&atype=rich)

## Independent media Elephant Magazine censored on all platforms



China censored the Elephant Magazine (known as Da Xiang Gong Hui) on July 14, ending the 8-year production of the independent media focusing on political, social and cultural analysis.



Founded by Huang Zhangjin, a veteran journalist at Phoenix Weekly, the Elephant Magazine used a straightway narrative with dense information to convey analysis of eye-catching social and political topics. 



Some articles touched the government’s red line, such as a story about the conflicts of Uyghur identity in China, written by the magazine’s Xinjiang-born staff Ilham Issak in 2014. This story raised the government’s attention and was banned from publishing for seven days. 



In an [interview](https://mp.weixin.qq.com/s/04ibwW773xlX3ammpC77Rw) with professional media BoKeTianXia in 2014, Huang said the 7-days ban was a dangerous sign and he would refrain from doing “politically sensitive” stories. 



The Elephant Magazine focused more on cultural topics in the past few years, such as the food, language and city. However, the editorial team found their media taken down on all platforms on July 14 without previous warning or specific reasons.

 

![](https://raw.githubusercontent.com/syeung19/image/main/20210720001842.jpg)

The Elephant Magazine is unable to access on WeChat. Source: NGOCN 

## MEANWHILE



1. Human rights lawyer Lu Siwei was violently treated when filing an administrative     lawsuit in Chengdu’s Qingyang District Court on July 13 against the suspension of his attorney's license. According to Lu’s statement published by [China Human Rights Defender (CHRD),](https://twitter.com/CHRDnet/status/1414961389425381378?s=20) four court police seized his throat by hands and legs and pinned him down. He said the court refused to place his lawsuit on file. 

 

1. Hong Kong National security police raided the student union and campus media offices at The University of Hong Kong on July 16 after student leaders formally     expressed appreciation for the “sacrifice” of a man who stabbed an officer before killing himself. According to [South China Morning Post,](https://www.scmp.com/news/hong-kong/politics/article/3141386/hong-kong-national-security-police-raid-university-student) detectives were “investigating whether the student leadership had advocated or incited terrorism under Article 27 of the national security law,” which could result in a 10-year imprisonment maximum.

 

 

 

 