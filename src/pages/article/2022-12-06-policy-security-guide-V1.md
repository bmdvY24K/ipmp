---
templateKey: article
title: '应对警察及防止被电子取证指南'
slug: 'policy-security-guide-V1'
date: 2022-12-06T03:21:57.265Z
description: '如果你有被警察上门的风险，而且有可能被带走你的电子设备做进一步取证，甚至更多的风险，希望这本指南对你有用。'
featuredpost: true
featuredimage: /img/secu1.jpg
trending: 10
isbrief: false
columnist: 公民安全行动指南
contributors:
  - type: '编辑'
    name: '公民安全行动指南小组'
tags:
  - "公民安全行动指南"
  - "紧急处理"
  - "警察"
  - "喝茶"
  - "取证处理" 
---

如果你有被警察上门的风险，而且有可能被带走你的电子设备做进一步取证，甚至更多的风险，希望这本指南对你有用。

这份指南主要是应对警察上门的紧急处理，包括防止对电子设备的取证处理，在你将开始一系列操作前，首先要做的事情是要守住你家的大门，争取更多时间进行各种操作。

由于时间紧迫，指南仍有各种不足之处，请给我们多多反馈意见，或者提出你的问题：it.ngocn@protonmail.com，我们将尽力完善。在实际情景中能否争取时间进行各种紧急处理，直接关系到你或身边的亲友的安全，请给予重视。

最后，愿大家一切平安，念念不忘，必有回响。

**墙内链接**

PDF版
https://mcusercontent.com/da19421f050388da556911ffe/files/c66303b2-39c1-816e-b074-8ee133a063ff/当警察来敲门_V1.01.pdf

PDF🔐版（1206）
https://mcusercontent.com/da19421f050388da556911ffe/files/3f53eb04-79bb-c3ce-197e-3dbc7db75d27/doSO9md98_3doDD9kdOL.pdf
