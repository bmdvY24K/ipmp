---
templateKey: article
title: '告读者：关于长毛象账号的一些问题说明'
slug: 'NGOCN-mastodon-note'
date: 2023-03-19T03:21:57.265Z
description: '我们在长毛象上面也发现了两个同为NGOCN的bot账号。感谢网友备份支持，但我们也同时说明@ ngocn@mastodon.ngo是我们在长毛象上唯一官方账号，其余账号以及其公告上的筹款活动与我们无任何关联。特此说明，以免造成误会。'
featuredpost: false
featuredimage: /img/hua-1.png
trending: 1
isbrief: false
columnist: 告读者
contributors:
  - type: '作者'
    name: 'NGOCN'
tags:
  - "长毛象"
  - "官方账号"
  - "机制"
---

各位读者好，我们收到长毛象所在站点的管理员来信，表示由于站点容量有限，只能为非营利机构和人权捍卫者提供该站点服务。大家可以在长毛象其他站点注册后，搜索我们的账户名“[@ngocn@ mastodon.ngo](https://mastodon.ngo/@ngocn)”同样可以关注我们动态。

同时，我们在长毛象上面也发现了两个同为NGOCN的bot账号。针对备份bot账号，我们也在此说明@ ngocn@mastodon.ngo是我们在长毛象上唯一官方账号，其余账号以及其公告上的筹款活动与我们无任何关联。特此说明，以免造成误会。

最后，我们也收到网友反馈，关注了我们账户后，无法直接显示我们的以往推文。这种情况需要在我们的账户主页点击“在原始个人资料页面上浏览详情”（英文版为Browse more on the original profile）才能显示全部推文。我们进行了测试，发现新关注的用户只能看到关注后的新推文，无法直接浏览往期推文————这个应该属于长毛象站点的机制问题，特此提示，也感谢读者的经验反馈。