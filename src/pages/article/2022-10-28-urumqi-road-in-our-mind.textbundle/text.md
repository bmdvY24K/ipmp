---
templateKey: article
title: '在伦敦乌鲁木齐火灾悼念活动上的路牌'
slug: 'urumqi-road-in-our-mind'
date: 2022-11-28T03:21:57.265Z
description: '上海乌鲁木齐中路已立在人的心中，千千万万块路牌将会不断涌现。'
featuredpost: true
featuredimage: /img/new10.jpeg
trending: 10
isbrief: false
contributors:
  - type: '作者'
    name: '悼念乌鲁木齐火灾逝者'
  - type: '设计'  
    name: '不自由毋宁死'
tags:
  - "乌鲁木齐"
  - "抗议"
  - "上海"
---
![22](/Users/admin/Documents/GitHub/ipmp/static/img/5-new3.jpg)

伦敦当地时间11月27日晚上7时，大批中国留学生来到中国驻英大使馆门前，参与乌鲁木齐火灾的悼念仪式，在场人数远超近期相关中国留学生的抗议活动。除了点点烛光，现场还有不少人举着各种自制“乌鲁木齐中路”的路牌，回应当晚网友视频拍摄在上海被拆除的“乌鲁木齐中路”路牌，并且高喊各种表达诉求的口号。

根据[星岛网等媒体报道](https://std.stheadline.com/realtime/article/1889082/%E5%8D%B3%E6%99%82-%E4%B8%AD%E5%9C%8B-%E4%B8%8A%E6%B5%B7%E6%8B%86-%E7%83%8F%E9%AD%AF%E6%9C%A8%E9%BD%8A%E4%B8%AD%E8%B7%AF-%E8%B7%AF%E7%89%8C%E6%92%B2%E6%9C%94%E8%BF%B7%E9%9B%A2-%E5%82%B3%E6%9B%B4%E6%8F%9B%E5%B7%A5%E7%A8%8B%E5%B7%B2%E9%87%8D%E8%A3%9D)，上海乌鲁木齐中路路牌在当地时间28日凌晨又被重新装回去。也有网友爆料，[乌鲁木齐中路正在进行整治工程](https://twitter.com/whyyoutouzhele/status/1597112626051371008?s=21)，摘路牌可能是施工中的正常行为，与近期的抗争事件无关。

但也有上海网友观察表示，28日，包括乌鲁木齐中路以及周边路段的马路两边，都装上了比人还高的蓝色铁皮，可把两边的人行道上的人群彻底分隔，站在一边人行道上看不到对面，只有中间马路可以行车。而且每个路口都部署了大批警察把守。

我们汇总了在11月27日，在伦敦大使馆悼念活动现场拍摄到的各种自制“乌鲁木齐中路”的路牌，制作出一个长图海报。