---
templateKey: article
title: "This Week China Society: Flowers and Walls in Zhengzhou subway memorial"
slug: weekly-newsletter-01-august
date: 2021-08-01T01:28:20.370Z
description: "This is NGOCN’s fourth weekly newsletter. We’re covering the flowers and walls in front of the Zhengzhou subway station when people mourned for victims, and other news that happened in Chinese society."
featuredpost: true
trending: 7
isbrief: false
columnist: '英文周报Weekly Newsletter'
contributors:
  - type: "Author"
    name: "小戌"
  - type: "Editor"
    name: "Samuel"

---



Happy Monday. This is NGOCN’s fourth weekly newsletter. We’re covering the flowers and walls in front of the Zhengzhou subway station when people mourned for victims, and other news that happened in Chinese society.

 

**Flowers and walls: a metaphor for state-society relations in China**

 

People started to lay flowers and light candles in front of subway entrances last weekend for the 14 passengers who died in one train of Zhengzhou subway submerged by the historical floods last week in central China. 

 

However, on Monday, they found entrances to the Shakou Road station — where the train stopped by floods — were blocked by yellow makeshift barriers. 

 ![圖片 1](/Users/samuelyeung/Documents/GitHub/ipmp/static/img/圖片 1.jpg)

*Flowers blocked by barriers (Source: Weibo)*

 

[Videos](https://m.weibo.cn/status/4663373546127960?) circulated on social media showed several men moved three front barrier boards away, leaving a way where people could get into the surrounding barriers and lay flowers in front of the subway entrance.

 

Anger and grievance swarmed online with 150 million views on two related hashtags. Monday was “the first seventh day of the death,” a critical day for condolences in Chinese tradition, which intensified the sentiments. People criticized the authority for barring people from mourning and cheered for the ones who moved the barrier boards.

 

“Don’t block the dead’s way home,” one [commented.](https://m.weibo.cn/status/4663475609537657?) “The grievance should be unconditionally allowed,” another [said.](https://m.weibo.cn/status/4663475609537657?)

 

![圖片 2](/Users/samuelyeung/Documents/GitHub/ipmp/static/img/圖片 2.jpg)

*Memorial cards saying “Truth won’t drift away with floods” (Source: anonymous sources at the scene)*

 

However, on Tuesday morning, makeshift barriers were put back and guarded a dozen of men who clashed with the mourning crowds, according to witnesses on the scene who didn’t want to identify themselves due to the fear of repercussion. 

 

They said those guards again took several front barriers away and left those on sides in the afternoon. People were allowed to lay flowers and take photos, but had to leave right away.

 

On Tuesday night, photos showed that all the barriers were taken away.

 

However, NGOCN learned that two Chinese reporters were briefly detained by the police on Tuesday for flying drones to capture the scene of memorial flowers, and a freelance photographer was choked and violently assaulted by several men wearing black shirts. 

 

![圖片 3](/Users/samuelyeung/Documents/GitHub/ipmp/static/img/圖片 3.jpg)

*A freelance photographer choked and attacked by several men (Source: anonymous sources at the scene)*

 

“It offers the perfect metaphor for state-society relations in China,” [said](https://twitter.com/xuxibai/status/1419960145262497792?s=20) Xu Xibai on Twitter, a researcher on China’s NGO and civil society.

 

“The condolence is sensitive because it can be seen that people hold the government accountable in a moral way,” said Lin, a 26-year-old woman in Beijing who didn’t want to identify her full name due to the fear of repercussion.

 

“The local government may also be afraid that people will gather together to express their opinions, which would be noted by the central government to hold it accountable,” Lin said.

 ![圖片 4](/Users/samuelyeung/Documents/GitHub/ipmp/static/img/圖片 4.jpg)

*Flowers laying in front of the subway station (Source: Chen Liang via Caixin)*

  

MEANWHILE



1. Chinese-Canadian idol Kris Wu Yifan has been [criminally detained on suspicion of rape](https://weibo.com/5556545776/KrkQb4Zmi?type=comment#_rnd1627882057061), Beijing police announced late Saturday. The decision came after an investigation into online complaints that Wu had “repeatedly tricked young women into sex” and multiple women shared their stories of Wu’s sexual assault.

 

1. Chinese “liberal tycoon” Sun Dawu [was sentenced to 18 years](http://www.xinhuanet.com/2021-07/28/c_1127705825.htm) on charges ranging from provoking trouble to illegal fundraising. Sun had been vocal in criticizing the government, including support for those caught up in the “[709 Crackdown](https://en.wikipedia.org/wiki/709_crackdown)”, when over 300 human rights lawyers and activists were detained.
2. Hong Kong government cut ties with the [Professional Teachers' Union](https://en.wikipedia.org/wiki/Hong_Kong_Professional_Teachers%27_Union) after Chinese state media People’s Daily and Xinhua News [slammed](http://hm.people.com.cn/n1/2021/0731/c42272-32176846.html) the Union as a “poisonous tumor” that must be “eradicated”.

  

 

 

 