---
templateKey: article
title: 12.22 N记日报
slug: zao-bao
date: 2022-12-22T14:07:57Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: Xiao Xu

---
**1. 《纽约时报》调查：Twitter 机器账号色情内容淹没中国抗议推文**

11 月，当中国公民走上街头抗议“清零“和习近平统治时，数以千计的视频被分享在 Twitter 以规避中国政府言论审查。但在 Twitter 搜索这些内容时，却是源源不断的色情内容和博彩广告。

根据[纽约时报](https://www.nytimes.com/interactive/2022/12/19/technology/twitter-bots-china-protests-elon-musk.html)分析和采访，这些垃圾贴绝大多数与商业自动发帖程序网络有关。这些网站在抗议活动开始前就已在 Twitter 运行。

一名曾在 Twitter 安全与诚信部门工作的前员工称，政府可以在出售自动程序的市场上购买发垃圾贴的帐号，并对其进行重新利用，因此很难判断垃圾内容潮是商业性的还是政治性的。 #虚假消息

**2. 为什么民众抢不到退烧药？**

[每日人物](https://chinadigitaltimes.net/chinese/691094.html?utm_source = dlvr.it&utm_medium = twitter)采访发现，中国退烧药产能充足，但由于三年“清零“严格限制“四类药“（ 退热、止咳、抗菌、抗病毒药品）销售，药企只能尽快压缩产品库存，加大周转率，导致整个产业链工序都非常紧凑。于是，当前需求增加时，药企库存不足，加上工人阳性、上游供应跟不上、物流停滞，让退烧药的生产和运输都发生停滞。

自媒体[赛柏蓝](https://chinadigitaltimes.net/chinese/691079.html?utm_source = dlvr.it&utm_medium = twitter)采访发现，尽管 11 月份已经有了明确的四类药松绑动向，但药企们基本没有接到明确通知。此外，并不是所有省份四类药都放开了，药企根据以往经验，仍然对存货、备货有疑虑。各地疫情爆发下，很多药企四类药甚至出现了停产。 #无序放开

**3. 阿富汗塔利班全面禁止女性上大学**

[阿富汗塔利班教育部](https://www.reuters.com/world/asia-pacific/taliban-led-afghan-administration-says-female-students-suspended-universities-2022-12-20/)称，所有公立和私立大学全面禁止女性上学。

塔利班去年接管阿富汗后，大学教室和出入口便采取性别隔离措施，女学生只能由女教授或老年男子授课。阿富汗女性在大学入学考试中只能选择特定专业，如兽医、工程、经济、农业、新闻等。

联合国、美国、英国等强烈谴责阿富汗塔利班再次违反承诺，并表示塔利班必须改善女性受教育权等基本人权和自由，否则不会考虑承认塔利班政权。 #性别平等

欢迎加入NGOCN的Telegram频道：ngocn01
