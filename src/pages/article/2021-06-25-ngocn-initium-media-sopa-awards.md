---
templateKey: article
title: NGOCN声音计划与端传媒合作报道获亚洲卓越新闻奖
slug: ngocn-initium-media-sopa-awards
date: 2021-06-25T03:21:57.265Z
description: null
featuredpost: true
featuredimage: /img/SOPA.jpeg
trending: 6
isbrief: false
typora-copy-images-to: ../../../static/img
columnist: 
tags:
  - 亚洲卓越新闻奖
  - 李翘楚
  - 声音计划
contributors:
  - type: 作者
    name: NGOCN
typora-root-url: ../../../static

---

\#好消息 #亚洲卓越新闻奖

由端传媒与NGOCN声音计划联合出品的《女性抗争者李翘楚： “我有英雄情结，但我幻想的英雄是我自己”》/《抗争者李翘楚的革命与爱》获亚洲卓越新闻奖（SOPA）颁发[卓越女性议题报道奖（中文组）](https://twitter.com/sopasia/status/1408023658484826121?s=21) / Excellence in Chinese Reporting Women’s Issues。

2021年5月25日，律师收到临沂市公安局消息，李翘楚案件侦查期限已被延期一个月。一个月后的今天，仍然杳无音信。律师三次申请会面均被拒绝。

#释放李翘楚

#释放所有人权捍卫者

【文章回顾】

N记版：[https://ngocn2.org/article/2020-12-22-qi-qiao-chu-revolution-love](https://ngocn2.org/article/2020-12-22-qi-qiao-chu-revolution-love/)

英文版：[https://ngocn2.org/article/2021-05-04-the-life-themes-of-activist-li-qiaochu](https://ngocn2.org/article/2021-05-04-the-life-themes-of-activist-li-qiaochu/)

端传媒版：[https://theinitium.com/article/20201218-mainland-liqiaochu](https://theinitium.com/article/20201218-mainland-liqiaochu/)

