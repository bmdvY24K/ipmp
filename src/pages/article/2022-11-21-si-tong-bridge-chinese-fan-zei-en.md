---
templateKey: article
title: 'Overseas “simplified-Chinese traitors” took to the streets: from standing alone to embracing each other'
slug: 'si-tong-bridge-chinese-fan-zei-en'
date: 2022-11-21T03:21:57.265Z
description: 'From each murmuring the protest slogans with a shaky voice to roaring powerfully in unison by the end of the protest, they had found solidarity in each other.'
featuredpost: true
featuredimage: /img/fan-zei4.jpeg
trending: 10
isbrief: false
contributors:
  - type: 'Translated by'
    name: 'Chloe Chen'
  - type: 'Proofread by'  
    name: 'Tu Huynh'
tags:
  - "四通桥"
  - "海报行动"
  - "海外留学生"
  - "简中反贼"
  - "政治表达"
  - "曼彻斯特"
---

**When Taozi, a self-proclaimed “simplified-Chinese traitor,” stood amongst Cantonese-speaking Hong Kong protesters, shouting protest slogans in Mandarin, she realized for the first time that she wasn’t alone. After the Beijing Sitong Bridge incident, a group of Chinese students studying overseas started to protest in the streets and, thus, found their own voices. They identify each other by showing protest slogans written in simplified Chinese characters. They hope to “learn to be courageous and free” and to carry on the resistance started in Beijing one small act at a time.**

[The Chinese version is here](https://ngocn2.org/article/2022-11-03-si-tong-bridge-chinese-fan-zei/), please help us to share it.

![22](/Users/admin/Documents/GitHub/ipmp/static/img/si-tong-bridge3.jpg)
<center><font color=gray> Image:@citizensdailycn instagram</font></center>

<br>Sunday October 23, 2022. Several protests were held in the U.K. to support the Hong Kong protesters, who were attacked by staff members of the Chinese Consulate in Manchester. In London, despite the bad weather, over 500 protesters marched 1.6 km from Downing Street to the Chinese Embassy.

Many of the protesters were from Hong Kong. Among them were youths from mainland China. They are mostly students or recent graduates in the U.K., searching for people who are similar to them, despite fear and repression.
<font color=blue>From each murmuring the protest slogans with a shaky voice to roaring powerfully in unison by the end of the protest, they had found solidarity in each other.</font>

### 1    The “simplified-Chinese traitors” that used to stand alone

During Fall, it’s chilly and windy in London. At the protest for solidarity with the Hong Kong protesters, held in the front of the Chinese Embassy, the 26-year-old protester, Taozi, chose to not leave early but to participate until the end of the event. 

Taozi is from mainland China and a PhD student at one of the universities in the U.K. She refers to herself as a “simplified-Chinese traitor (Jianzhong fanzei),” a derogatory name used by Chinese “patriots” to attack those who originate in mainland China but do not display “love and loyalty to the party and the country.” For Taozi and her friends, appropriating this label is partly self-deprecating and partly challenging the stigma associated with it. Most importantly, to self-identify as a “simplified-Chinese traitor” allows them to secretly identify and connect with those who share the same political views.

Taozi was born and raised in northern China. Her only idea about protest and demonstration came from her high school politics textbook. It states in the textbook: “as stated in our constitution, the citizens of People’s Republic of China are entitled to the rights of free speech, free press, free association, and free assembly, as well as to protest.” She used to memorize this passage by heart to pass the exams. But, as with most texts, she knew that what was written were “just pretty lies.” It only took a glance at the environment for her to know that this isn’t “a country of the people and for the people” as described in the politics textbook.

![22](/Users/admin/Documents/GitHub/ipmp/static/img/fan-zei6.png)
<center><font color=gray> Image:A screenshot of the digital version of the politics textbook.</font></center>

<br>Since high school, Taozi developed an instinctual “disgust for lies and hypocrisy.” She has been searching for truths. After watching a documentary on the 1989 Tiananmen Massacre, a desire rose in her to take to the streets, to speak out loud, not only about the lies that she has been fed, but also the truth that was withheld from her. 

After Taozi went overseas, she started to attend vigils for the 1989 Tiananmen Massacre organized by people from Hong Kong. But she found it hard to engage because the meetings were all in Cantonese. At these meetings, Taozi found herself standing in a corner, trying to understand the speech by guessing. She felt like an onlooker most of the times. At the same time, it did make her wonder if there were other people from mainland China in the room.

It wasn’t until the vigil in 2022, when she spotted a woman standing by the side of the street holding a “free the chained woman” protest placard. It instantly made her feel elated and ashamed. She thought to herself: she also cares deeply about the issues related to Chinese women’s rights, why hasn’t she thought about speaking up on these issues at these vigils? Nonetheless, she continued to participate in the vigil. At the end of the event, Taozi shouted her protest slogan in Mandarin. 

Her voice didn’t get much attention at the beginning. But, as she recalled, she slowly found herself surrounded by several Mandarin-speakers who immediately started conversations about China’s politics with her. It was at that moment that she realized that she was never alone: there has always been other “simplified-Chinese traitors” who, like her, stood alone at the peripheries of these events. 

 
### 2   The Beijing Sitong Bridge protest: “It’s about passing on the courage”

The Beijing Sitong Bridge protest took place just three days before the start of the 20th National Congress of the Chinese Community Party. To Taozi, the black smoke that rose from the bridge was as if a beacon was lit. It instilled in her the commitment to be a political activist. She thought: “the courage was now passed on to me.” She not only joined in passing around the protest posters, but also planned to speak out during in-person protest events to connect with more mainland Chinese activists.

On the late afternoon of October 23, Taozi and her friends began to set up the protest banners and posters that they had made in the previous day in front of the Chinese Embassy – the location for protesting the attack on Hong Kong protesters. Every word on their banners and posters was written in simplified Chinese characters, intended to draw the attention of other “simplified-Chinese traitors.” Given the prevalence of the “little pinks” (xiao fenhong) patrol of the Chinese internet, it hasn’t been easy to connect with other “simplified-Chinese traitors” on the internet.

The rain came unexpectedly. Taozi began to worry that the banners and posters would be damaged by the rain. So, she carefully packed her protest materials into her bags before she put on a raincoat. As the rain got heavier, Taozi’s shoes and clothes became drenched. The thought of leaving crossed her mind, but she thought about how hard it is to come by an opportunity to connect with other “simplified-Chinese traitors” and decided to stay.

The rain gradually stopped around 6pm, and more protesters arrived at the Chinese Embassy. Taozi stood at the same spot where the woman holding the “free the chained woman” placard had stood during the Tiananmen Massacre vigil a few months ago. She held up the poster about the Sitong Bridge protest, as well as the posters calling for the liberation of the people in other oppressed regions of China. Taozi was waiting for other “simplified-Chinese traitors.”

She did indeed attract allies. When the soggy poster that she was holding started to slide down, a man came up to her and handed her a roll of tape. She hesitantly asked: “are you a simplified-Chinese person?” When he answered with a “yes,” Taozi instantly felt like she has gained a teammate.

It was probably her “go on strike at school and work, remove dictator and national traitor Xi Jinping” banner that did the job for her. In the next hours, Taozi has attracted more than ten “simplified-Chinese traitors.” They were mostly women, also holding posters about the Sitong Bridge protest. After a few exchanges, Taozi was already sure that these women were the “simplified-Chinese traitors” that she’s been looking for. They started to discuss when to interject Mandarin protest slogans about the Sitong Bridge protest slogans in between rounds of Cantonese slogans. 

They experienced setbacks in the beginning due to the lack of experience. Their voices were scattered and shaky. But gradually, with more practice, their voices started to synchronize and became more and more powerful, to the point that they felt like they were “roaring.” That was when Taozi realized that it was such a cathartic moment for them to finally be able to be heard after years of living in silence and repression. She also noticed that all the other Hong Kong protesters and passerby have gone quiet, as if they were making room for them to be heard. 

They were greeted with rounds of cheers and applauses when they finished their slogans. “I would never forget the warm feelings that I felt in my heart at that moment,” Taozi said. 

![23](/Users/admin/Documents/GitHub/ipmp/static/img/fan-zei7.jpg)
<center><font color=gray> Image: photo taken at the protest. Source: @citizensdailycn, Instagram.com</font></center>

  
### 3  “The Chinese Communist Party does not represent all of those who use simplified Chinese characters”

Taozi used to worry about being alienated or even discriminated against for using simplified Chinese characters and speaking Mandarin among other activists. They could be used to identify activists from mainland China. But for people from outside of mainland China, however, simplified Chinese characters and Mandarin are almost synonymous with Chinese Communist Party ideologies. They not only signify the CCP’s political hegemony, but could also trigger trauma for some people.

“Professor Xi-humiliator” (Rubao Jiaoshou) is another Chinese student, who participated in the protest with Taozi. She is not only a “simplified-Chinese traitor,” who is against the oppression of the Chinese government, but also someone who cares about human rights movements in other countries, such as anti-war and anti-nuclear weapon protests. “Professor Xi-humiliator” said that even though she has felt welcomed and supported at some of these protest events, there were times when she was mistaken as a supporter of the CCP by other protesters because she used simplified Chinese characters and Mandarin. 

Though she understood where the side-eyes came from, the assumptions made about her, based on the language that she uses, frustrated her. “We cannot let simplified Chinese characters and Mandarin be tainted by the CCP,” she said, “let alone tolerate activists from outside of mainland China think that everyone in mainland China – all 1.4 billion of us, are supportive of the CCP.” For this reason, she is even more keen to identify as a “simplified-Chinese traitor” than before. “Professor Xi-humiliator” wanted more people to know that not everyone, who uses simplified Chinese characters, is a supporter of the CCP and that the people from mainland China do resist and have individuality. The protest on October 23 made her realized, for the first time, that there were other “simplified-Chinese traitors” who also have courage to stand up and act.

![23](/Users/admin/Documents/GitHub/ipmp/static/img/fanzei8.jpg)
<center><font color=gray>Image: a photo taken at the protest. Source: @citizensdailycn, Instagram.com.</font></center>

### 4   The mainland Chinese activists who can’t find belonging

Ada, who was also at the protest that night, noticed that the Hong Kong protestors and mainland Chinese protestors also diverged on the issues that they protest against. Originally, the protest was about standing in solidarity with the Hong Kong protesters who were attacked by the staff members of the Chinese Consulate at Manchester. But, the mainland Chinese protester all showed up to show support for the Beijing Sitong Bridge protest. 

Ada has participated in many in-person protests. As an activist from the Guangdong Province, Ada is fluent in both Cantonese and Mandarin, unlike most of the activists from mainland China. She could easily blend in at the protests and demonstrations organized by Cantonese-speaking activists. However, as a mainland Chinese activist, she always felt that she didn’t belong.

According to Ada, there are more obstacles to finding belonging than the language difference. She found that few other protesters know or are interested in learning about the issues in mainland China, whereas she’s always ready to show solidarity with Hong Kong and Taiwanese protesters by joining their protests and engaging in dialogues with them. Moreover, due to concerns for personal safety, there have been very few events initiated by mainland Chinese activists. This has led to the fact that there is very little public space dedicated to discussing and acting on the issues relevant to mainland Chinese, such as women’s rights in mainland China or the latest news. 

In Ada’s opinion, many of the “simplified-Chinese traitors” often choose to remain invisible out of fear for their safety, even when they are overseas. As a result, mainland Chinese issues are more marginalized at these protest events. 

When Ada showed up with the posters for the Sitong Bridge protest written in simplified Chinese characters and shouted her slogans in Mandarin on October 23, she felt that she has finally done what she had always wanted to do: to let the real voice of a mainland Chinese be heard and to show others that not every Chinese student studying overseas is a “little pink.”

[The videos](https://www.instagram.com/p/CkGnpKgJ2t2/?igshid=YmMyMTA2M2Y%25253D) that show the slogans written in simplified-Chinese and shouted in Mandarin were submitted to social media websites. Chestnut was one of those who watched the videos.

![23](/Users/admin/Documents/GitHub/ipmp/static/img/fan-zei5.png)
<center><font color=gray> Image: a screen cap of the video. Source: @northern_square, instagram.com</font></center>


<br>Chestnut is also a mainland Chinese student currently studying in the U.K. After watching the videos, she immediately shared them to group chats that are not based in mainland China. Knowing the high risk for mainland Chinese people to engage in political activism, Chestnut said that she was worried for the activists who not only showed up in person, but also used their own voices. In Chestnut’s view, to convey messages by voice is particularly powerful. “When I listened to their voices breaking as if they were about to cry, it was an overwhelming experience. I almost broke out in tears. I could hear in their voices my own frustration and anger from being oppressed by authoritarian power and the censorship machines.”

Chestnut said to her friends, “I have not felt this free in a long time.” To Chestnut, speaking out with real voices is especially important and powerful and should occur more often in physical spaces to facilitate more connections between people. “We need to believe and make sure that our individual voices will not be erased by history.”

### 5  “Women have no choice but to start revolutions”

A noteworthy phenomenon that followed the online circulation of the videos of the protest was people commenting on the fact that almost all of the protesters in the videos were women. Some asked in the comment section: “Why are all the protesters women?” To which another commenter replied: “Because women have no choice but to start revolutions from the moment they are born.”

![23](/Users/admin/Documents/GitHub/ipmp/static/img/fan-zei10.jpg)
<center><font color=gray>Image: a screen shot of the social media comments under the videos. Source: Internet.</font></center>
 
<br>“Professor Xi-humiliator” explained that the reason why it seems easier for women to become disenchanted by the status quo was because in almost every aspect of life, women experience the lack of fundamental rights and access to resources at a much greater degree. As a feminist, Taozi also made similar observations. She noticed that even among the activists, women’s voices and issues were often ignored. More than often, women activists are asked to make compromises to form a unified front with their male counterparts. Taozi had firsthand experience of such in an online activist group. One time, a person posted a picture in which the face of a CCP leader was photoshopped to the naked body of a woman - the point of the picture was to insult the CCP leader. When Taozi pointed out the misogynistic message in this picture and demanded an apology, she was scolded by the male members of the group for being a shortsighted “provincial feminist” (tianyuan nvquan). Not only so, she then received condescending, seemingly “neutral” advice: “women’s rights and issues are important, but when we are engaged in political activism, it’s more important to figure out which is the top priority.”

But Taozi and the other female members of the group didn’t back down. After fierce debate, “zero tolerance of sexism, objectification and degradation of women’s body, and femininity” was added to the group’s code of conduct. But Taozi thinks that there are more to be done.

“In order for these male-dominated and male-centered spaces to meet the bare minimum of sustaining a respectful and inclusive environment for women, women always have to do the extra labor to explain and educate,” said Taozi, who is creating an all-women activist group. She hopes to create a safe and comfortable space for women to speak out on the issues that are important to them and to support one another in political activism.
 
### 6    “Not to favor and promote sacrificing oneself, but to take each individual’s fragility and concerns seriously”
 
For many activists, one of the ways to show their commitment is through speaking out against silencing and erasure. However, this could also come with tremendous risks, as the act of speaking out could attract “the searchlight of the hunter in the darkness” or the attention of the state.

Taozi is not one to shy away from any opportunity to speak up despite censorship. She shared her thoughts and comments on all kinds of social issues on her WeChat moment. When it comes to coping with online censorship, her practice has been to not begin by self-censoring, but always to write down her thoughts normally, and to only make adjustments to her wordings when a post is rejected. Even though sometimes she did compromise and “castrate” the language to get her messages to pass the censorship algorithm, she constantly reminds herself to never be complacent with using “castrated” versions of the language.   

However, some of Taozi’s friends have experienced being questioned by the police over the phone for discussing the so-called sensitive topics in WeChat groups – even their parents have received threats from the police. Now that some of Taozi’s friends face graduation and the possibility of returning to China, they have no choice but to consider their own safety and career prospects in China, which has prompted Taozi to question if she was only intrepid because she was not yet scarred by the system. 

Since becoming an activist, Taozi also became more and more aware of the importance of personal safety. The Chinese government’s attempts to surveil and harass overseas Chinese and to ceaselessly monitor their online activities, as well as the risks of being reported and attacked by the “little pinks,” have all made Taozi and her friends hyper-vigilant. Taozi doesn’t share the view that being an activist meant one must embrace a life of sacrifices and martyrdom. What Taozi believes is that every activist’s concern and fragility should be considered and respected, and precautions should always be taken to ensure everyone’s safety. 

Furthermore, Taozi also believes that “the political dominance of the current government can be shattered by accumulations of small acts of defiance by ordinary people,” as she puts it. As such, she would encourage friends who are interested in participating to not dive headfirst into street protest, but rather to participate in any shape or form that is available to them. She would tell them: “it’s not only the street demonstration that counts, but also the planning and strategizing that precede it are equally important.” She found that this has encouraged more participation, and people have learned how to cope with fear and risks in the process. 

As a seasoned protester, Ada is particularly experienced in safeguarding personal information that could lead to identification. She would go to great length to avoid using mainland Chinese social media apps. For the protest, Ada not only created a guideline for protecting personal safety for the protesters, but also helped other protesters to cover their faces and kept reminding them to check their covers during the protest. 

For “Professor Xi-humiliator,” she believes that the more people join the movement, the safer it would be for all activists. Though the increased exposure of their activism would attract the attention of the Chinese government, it would also attract the attention of other activist groups. As the social movement garners influence and external support, along with mutual support among activist groups, there could be more protection for mainland Chinese activists.
 
### 7 “We face the same elephant in the same room”
 
C, from Hong Kong, also went to the London protest. She used to have many friends from mainland China before the 2019 Hong Kong anti-extradition protest. She also used to be able to discuss political topics with her mainland Chinese activist friends. After the anti-extradition protest, though she still reads news about mainland Chinese politics, she has deleted most of the mainland Chinese social media apps that she previously used. She gradually stopped communicating with her mainland Chinese friends. 

C recalled feeling very moved when she heard the protest slogans in Mandarin at the protest. The voices of the mainland Chinese activists not only seemed louder than their Hong Kong counterparts, but also rekindled a sense of comradeship - C felt that she’s no longer alone. Just like what Ada said, “we faced the same elephant in the room.” C is also looking forward to participating in protest events organized by mainland Chinese activists and speaking up against issues that are relevant to them. 

C is an experienced activist compared to most of the mainland Chinese activists, who are new to political protest. Participating in political activism has not only become part of her daily life, but also her lifelong commitment. For mainland Chinese activists, who are unsure where to start, C suggested that they could start from something small while always being mindful of their own safety. 

For Taozi, the experiences and insights of democratic movements, gained by hardened activists, are invaluable lessons worth learning from. “We are not only hoping to unite more mainland Chinese activists, but also anyone who has ever experienced oppression from patriarchal authoritarianism,” said Taozi. As for the future, Taozi is a prudent optimist: “We are not expecting the status quo to immediately change by a protest slogan, a poster, or a protest. What we are hoping is that, by one small act at a time, more and more people could learn to be courageous and feel free to join political activism, so that resistance can continue.”

(To protect the identity of the interviewees, all of the names appeared in this article have been changed to pseudonyms.)