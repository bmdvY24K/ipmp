---
templateKey: article
title: 'Wanton Hatred in the Name of Patriotism: A Decade Since the Assault of a Man Whose Skull was Smashed by a U-lock in 2012'
slug: 'Ulock-ten-years-en'
date: 2022-10-31T03:21:57.265Z
description: 'In April 2022, Wang Juling received the news that Cai Yang was released from prison. Cai Yang was sentenced to ten years of imprisonment for assaulting Li Jianli with a U-lock, which left him disabled for the rest of his life. She was upset, “To this day, I cannot understand how someone could violently beat up a completely random stranger.”'
featuredpost: true
featuredimage: /img/lock-10-1.jpg
trending: 10
isbrief: false
contributors:
  - type: 'Author'
    name: 'Jiang Xue'
  - type: 'Translated by'
    name: 'Chloe Chen'
  - type: 'Proofread by'
    name: 'Tu Huynh'      
tags:
  - "西安"
  - "反日游行"
  - "U型锁"
  - "民族主义"
---
*In September 2012, the Japanese government purchased three of the disputed Senkaku islands, including the Diaoyu Island, from a private owner. This promoted large-scale anti-Japanese demonstrations across China. On September 15, 2012, during one of such demonstrations in Xi’an City, numerous businesses and cars were vandalized as protesters became violent. On that day, Li Jianli, a Xi’an resident, suffered severe head trauma when a “patriotic youth” assaulted him for driving a Japanese car. Li Jianli's skull was broken with a heavy duty motorcycle U-lock. Ten years later, when the perpetrator of the crime was released from prison, Li Jianli still has to undergo physical therapy treatments for the injuries that he sustained ten years ago. In the meantime, the U-lock came to signify extreme Chinese nationalism. Thus, by following up with the family of the victim at the incident's 10th-year anniversary, this article aims to reveal the ravage caused by extreme Chinese nationalism on ordinary Chinese people's lives.*

![234](/Users/admin/Documents/GitHub/ipmp/static/img/lock-10-1.jpg)
Illustrator: Wilson Tsang

**[editor’s note] This article was co-published by Initium Media and the NGOCN Voice Program. [The Chinese version](https://ngocn2.org/article/2022-09-27-Ulock-ten-years/) first appeared on Initium Media, and the English version first appeared on NGOCN.**

The bell of the North Street News Building was ringing again. The melody of “Red East” (dong fang hong) reverberated and could be heard from the hospital’s buildings.  The News Building was put into use in 1965, when Li Jianli, who was born below the city wall, was four-years old. The News Building’s bell has been playing the melody of “Red East” for many decades. Li Jianli, now 61-years old, was lying on a rehabilitation table at Xi’an Central Hospital listening to the tune while two young physical therapy interns in white coats were helping him with arms and legs exercises.

“It was…today…ten years ago…that my incident happened.” He told the two young interns while struggling with stutters. Both looked to be in their early 20s, a wide-eyed innocence on their faces. They seemed to be listening to him, but had no idea, nor did they care, what he was saying.

It was a few minutes past ten o’clock in the morning of September 15, 2022. Li Jianli is the unfortunate Chinese man whose skull was broken by a U-lock ten years ago. The U-lock not only shattered part of his skull, but also his peaceful and ordinary life. 

![24](/Users/admin/Documents/GitHub/ipmp/static/img/lock-2.jpg)
On September 15, 2012, anti-Japanese demonstrations broke out in major Chinese cities, such as Beijing, Shenzhen, and Xi’an. Image: Internet.

**1    “It’s hard work for the police, too.”**

A decade ago, Wang Juling was 48 years old. She was carefree and spoke with a booming voice. She is tall and enjoyed wearing dresses with big floral prints, with her hair tied up in a bun. On that ill-fated early autumn afternoon, she found her dress soaked in blood from her husband’s head.

“I don’t know how I got through the last ten years. Can’t put it into words,” she said.

Ten years ago, on September 15, her husband was rushed to the Xi’an Central Hospital. The operation lasted through the night: some of his brain matters fell out when the left side of his skull was broken by Cai Yang with a U-lock. He survived, thanks to the timely operation. However, the part of him that was inventive and liked to work on his car did not. And, neither did the part of him that enjoyed traveling with his wife.

He and his wife spent most of their time in the last decade in the Xi’an Central Hospital.

Initially, he was in bed number 45 when he was checked into the hospital on September 15, 2012. Then, he was moved to bed number 43 before he was moved again to a room that was at the very end of a long hallway. The hospital had become their home for the past eight years.

Over the last ten years, they witnessed the food stands at the hospital entrance that sell roasted sweet yams or Chinese pancakes change ownership several times, and the hospital newspaper booth removed. It wasn’t until October 2020, when the hospital inpatient building was due for renovation, that they were discharged from the hospital.

During the first two years of his hospitalization, Li Jianli and Wang Juling were closely watched by six policemen. Over the years, the number of the policemen assigned to watch them dwindled to four, and then to two. During the day, the two young policemen usually stayed in Li Jianli’s hospital room and browsed on their cellphones. At night, they unfolded their portable beds and slept in the hallway. When Wang Juling needed to go to the government buildings or the Courthouse for errands, they would give her a ride in their police car. “It’s hard work for the police, too,” she said.

Both Li Jianli and Wang Juling understand that the police’s job is to make sure that no reporters can get through and interview them. Their other job is to make sure that Li Jianli and Wang Juling won’t go to the government for petitions.

Many reporters were interested in interviewing them during the first two years of the incident. They swarmed to the hospital, wanting an interview. One time, as Wang Juling recalls, there was a Japanese reporter that made it to their hospital room, but his real identity was quickly discovered by the police when he bowed in between sentences out of habit by virtue of the Japanese culture. He was immediately asked to leave. 

In another time, there was an American reporter who was particularly determined. Because no reporters were allowed to enter the building, she camped outside of the hospital entrance for several days. When Wang Juling’s older sister left the hospital after bringing food to Li Jianli and Wang Juling, the reporter followed her all the way to the bus stop.  “I didn’t dare to say anything to her,” Wang Juling’s older sister told her later.

Wang Juling believed what the young policeman had told her, “Your husband’s case is influential, so the government won’t abandon you when you’re in need. Just rest assured and focus on getting better.” She thought to herself that as long as her husband is receiving treatments and the medical bills are being taken care of, she shouldn’t worry too much.

Li Jianli’s rehabilitation went well at the beginning. He was able to slowly walk again and even to slowly go downstairs. It was the best time of the last ten years, Wang Juling thought. In between hospital visits, she used to rush home to help take care of her newborn grandson.

In the second year of her husband’s incident, Wang Juling had pushed her son to get married as soon as possible. She was worried that her son’s fiancée would get cold feet, considering the turmoil that the family was going through. In 2014, the grandson was born, bringing to the family hope and much laughter.

Their family car, then a newly purchased white Toyota Corolla, was stuck in a traffic jam on South Labor Avenue when all its windows were smashed by demonstrators during the Anti-Japanese demonstration in Xi’an on September 15, 2012. Li Jianli, who was inside the car, was later found injured and unconscious on the ground outside his car and rushed to the hospital. The car also survived the demonstration. 

Hundreds of cars were damaged by the demonstrators in Xi’an that day. The government offered free repairs for all of these cars. The repaired car, along with their small family business, were passed onto their son. Since then, Wang Juling devoted herself to taking care of her husband. Her family relied on her strength.

![29](/Users/admin/Documents/GitHub/ipmp/static/img/lock-3.jpg)
The once able Li Jianli still can’t accept that he must use a wheelchair now. Image: courtesy of the Interviewee.

**2   The medical bill is 1.9 million RMB**

﻿
On the morning of September 15th, 2022, with Wang Juling’s help, Li Jianli slowly walked to the bus stop right outside of their gated community, where they waited to take the bus Line 6.

Li Jianli was discharged from the hospital two years ago due to the hospital’s renovations. However, he still went to the hospital every day for physical therapy. They haven’t missed a day except on days of extremely bad weather.

As a result of consistent physical therapy, Wang Juling considers her husband’s condition as “not bad.” But even so, she still notices her husband’s worsening sequelae due to the head trauma.

One time, on their return from the hospital, she asked her husband to walk slowly home while she went to buy groceries. She remembered receiving her husband’s phone call when she was picking the veggies. He told her that he tumbled over a small pit and fell to the ground. She rushed to his side and found him helplessly lying on the ground. No one tried to help him. At that moment, she felt devastated. 

Li Jianli had two more falls after that, one of which was on the bus. He refused to walk by himself and insisted that Wang Juling help him ever since. Not only that, but he also suffers from nerve damages as the result of his craniocerebral injury: there can be no delays whatsoever as soon as he feels the need to use the toilet. He has embarrassed himself many times in the elevator or in the car.

His thinking is still clear, but he struggles to speak without stutters, especially when he’s in a hurry.

Wang Juling is already worried about the future. With her husband’s worsening health and her own aging, she knows that she wouldn’t be able to keep taking care of him. Sooner or later, he will need a caregiver.

She is also worried about his treatments. Despite the hospital personnel constantly telling them to discharge from the hospital, they insisted on remaining as an inpatient for years, out of fear that they treatments would discontinue when they were discharged. 

Physical therapy is very expensive. As early retirees of a collectively-owned enterprise, Li Jianli and Wang Juling wouldn’t have been able to afford physical therapy with their meager pensions, if not for the government’s promise to take care of the medical bills.

The costs add up. His medical bill from the Xi’an Central Hospital is 1.9 million renminbi (about 264,000 USD).

On September 14, 2022, she received a phone call from the secretary of the political and legal committee of their district, who told her that he would like to visit them the next day. Li Jianli and Wang Juling went to physical therapy early the next morning in order to come home early to wait for the secretary’s visit. To their disappointment, the secretary didn’t show, but only sent his kezhang, a subordinate.

Nonetheless, she told the kezhang what they need—that they would like to receive a lump sum of their promised government subsidy. The kezhang simply told her that he had already sent in the paperwork, and all they needed to do was wait.

“But people working at the government come and go all the time. There have been four secretaries in the last years. The new superiors may not even know about my family’s case. I’m worried that we would be forgotten,” Wang Juling sighed. As she became more and more aware of her worsening memory due to aging, she is also feeling anxious about having to repeatedly explain her husband’s situation every time she has to deal with the government, “I’m afraid that one day I would not be able to clearly explain the situation.”

![202](/Users/admin/Documents/GitHub/ipmp/static/img/lock-4.jpg)
On September 15, 2022, Li Jianli and Wang Juling took the bus to go home after his physical therapy. Picture: courtesy of the interviewee.

**3     Other things that have disappeared from the city in the last ten years**

The line 6 bus route from Li Jianli’s home to the Xi’an Central Hospital goes through South Labor Avenue, where Li Jianli was “U-locked” ten year ago, and passes by the bell tower, one of Xi’an’s landmarks. These streets were the most crowded on the day of the anti-Japanese demonstration ten years ago.

On that day, September 15, 2012, Laohu, a photographer, was also there. He had worked for the local newspaper and is a very experienced live photographer. He remembers leaving home in a rush that day to get downtown when he heard that the demonstrators had started smashing cars. 

He recalls the traffic being completely jammed by crowds of demonstrators at North Avenue. He parked his car in an alleyway and went into the crowd.

He saw a few policemen guarding the entrance of the Bell Tower Hotel, which was rumored to be at risk of being broken into by the demonstrators. The Bell Tower Hotel was famous for housing overseas tourists. The police might be concerned about any disturbance to the Japanese guests staying at the hotel. 

He heard the crowd fanatically shouting in unison, “defeat the Japs” and “return the Diaoyu Island (the Senkaku Island) to China!” Suddenly, there was a single voice that yelled “no to corruption,” which no other voice joined. 

He captured the image of the overzealous crowds with his Nikon camera, a Japanese brand. He felt worried that his camera would be taken by the demonstrators, which luckily didn’t happen. It may have been his local accent that saved his camera and perhaps somehow the demonstrators could tell that he was a reporter. 

Standing amongst the demonstrators, the frenzied atmosphere worried him. “I think most of the people that day just used it as an opportunity to let loose their emotions. It may not had much to do with the actual anti-Japan sentiment,” he said as he recalled that day.

Still worried about loosing his camera, he left the scene of the demonstration after two hours. He was equally relieved to find that his car, which is of German make, wasn’t vandalized.

The hundreds of photos that Laohu took that day at the demonstration did not make it to the newspaper the next day. Nor did the interviews that Laohu’s colleagues did with some of the demonstrators, for a ban was immediately issued that day to the newspaper.

In fact, nothing was written about the demonstration of the previous day on the largest local newspaper on September 16. However, just a little bit to the right and below the front-page main headline, there was a reprinted news article titled: “Waves of Anti-American sentiments erupted around the world.”

There was, however, an opinion on the editorial page that day, bearing the title “Expressions of patriotism should be rational and lawful.” Another opinion appeared on the editorial page on September 18, titled “Patriotism is meaningless, if it tramples on the law.”

The end of that opinion article reads, “when patriotism is more violent than violent acts, then it is hypocritical, absurd, and sinful in nature. The rule of law is the soul of a country. If the rule of law is trampled on, patriotism is meaningless.”

“At that time, the media environment wasn’t as bad as it is today. Although we couldn’t report on the news about the demonstration, we were still able to publish these two opinions after painstaking negotiations,” said Ma Jiuqi, who was senior commentator and the author of the two opinions. Since then, he has left the news industry in 2015 as press freedom gradually deteriorated. Still, he found that as a commentator, he has “nowhere to be” in this country.

![202](/Users/admin/Documents/GitHub/ipmp/static/img/lock-5.jpg)
On December 26, 2021, a city-wide lockdown was enforced in Xi’an to curb the spread of Covid-19. Photography: Zhang Yuan/China News Service via Getty Images.

If it weren’t for a friend’s reminder, Ma Jiuqi, who now resides overseas, has almost forgotten about that day from ten years ago. Not unlike him, Laohu, who’s a grandfather now, also decided to leave the news industry in 2015.

Still, too little was documented about the violence committed in Xi’an on September 15, 2012. Journalist Wang Xing from Southern Metropolis Daily once went to Xi’an and conducted in-depth interview. 

Meanwhile, I was the director of the opinion department of a local newspaper. I produced an independent piece outside of my job obligations, titled “915, The Pain of Xi’an.”

After that, a scholar and I decided to apply to the Xi’an Police Bureau for government information disclosure, in accordance with the ‘Regulations on Disclosure of Information,’ and requested the Police Bureau to publish the results of the investigation into the ‘Anti-Japan Demonstration’ that turned into violence as well as the record of whether a safety protocol was put in place before the police approved the demonstration. But to this day, we still haven’t received anything from our application. While all I know for sure is that the government ‘was very concerned,’ I received a lot of pressures to stop my inquiry, both explicit and implicit.”


Another Xi’an citizen, who experienced the “915” demonstration in person, recalled that a popular Xi’an grassroots independent media (zi meiti) called INXIAN was around in 2012. INXIAN was relatively new but already had a reputation for its witty and thoughtful contents. On the day of September 15, 2012, many Xi’an citizens, who had just witnessed their city falling into fanaticism, submitted entries to INXIAN in order to leave records of what they had witnessed.


According to *INXIAN*’s weibo, they received tens of thousands of submissions about the violence and vandalism that occurred during the demonstration: “Today is the busiest day for our team since the founding of *INXIAN*. But, for reasons we all know, we can’t publish a lot of the submissions we have received. The ones that were published don’t survive the censorship either.”

It was a glorious time for *INXIAN*. In the following years, *INXIAN*’s online presence was blocked by censorship and had to reestablish their presence from overseas.


**4   “Grateful to the government”**

On December 23, 2021, Xi’an city was in lockdown for Covid-19. As a result, Li Jianli couldn’t go to physical therapy for a whole month. During that time, Wang Juling put finger splints on Li Jianli’s right hand for two hours each day to prevent further deterioration to his fingers.

When they finally saw the physical therapist after a month of lockdown, they were told that Li Jianli had lost some of his progresses from missing a whole month of physical therapy.

In April 2022, Wang Juling received the news that Cai Yang was released from prison. Cai Yang was sentenced to ten years of imprisonment for assaulting Li Jianli with a U-lock, which left him disabled for the rest of his life. Now, his sentence was reduced by half a year, and he was released on parole. When Wang Juling was interviewed by a journalist and asked about how she felt about Cai Yang’s release, she was upset, “To this day, I cannot understand how someone could violently beat up a completely random stranger.”

She has only seen Cai Yang twice. Once was on the day when Cai Yang’s trial started, and the other was during his sentencing. “Nonetheless, the fact that there hasn’t been another anti-Japan demonstration in all these years must indicate that people may have actually learned a thing or two,” she said.

![202](/Users/admin/Documents/GitHub/ipmp/static/img/lock-6.jpg)
September 2022, Wang Juling and Li Jianli went out for a walk. Image: courtesy of the interviewee. 

On September 15, 2022, Wang Juling took Li Jianli for a COVID test after his physical therapy. They went home afterwards to pack for their trip the next day, a four-day tour for seniors that only costed 299 RMB.

Life can be miserable at times, which is why Wang Juling looks for ways to have fun. She has prepared a wheelchair for this trip. For all these years, Li Jianli has refused to even use a walking stick because of his ego. But, a wheelchair is necessary for long distance trips.

Wang Juling hopes that one day they will be able to receive a lump sum from the government that would not only save them many trips to the government, but also enable them to access the hospital nearest to their home or even to see non-local physicians of their choosing.

On the afternoon of September 15, when they were visited by the district kezhang, Wang Juling again told him their request, to which the kezhang simply asked them to “wait with patience.” When the kezhang was leaving, she heard her husband say, “I’m grateful to the government.”
