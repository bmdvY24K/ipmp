---
templateKey: article
title: 12.31 N记早报
slug: zao-bao
date: 2021-12-31T02:30:09Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: M

---
**1. 《立场新闻》钟沛权和林绍桐正式被起诉，保释申请被拒**

根据[香港自由新闻报道](https://hongkongfp.com/2021/12/30/ex-editors-of-stand-news-officially-charged-under-hong-kongs-anti-sedition-law-denied-bail/)，《立场新闻》前总编辑钟沛权、署理总编辑林绍桐和《立场新闻》母公司 Best Pencil（Hong Kong）Limited 的案件在周四下午于西九龙法院开审，林绍桐因身体原因未能出庭。两人的保释申请均被拒绝，总裁判官罗德泉表示他不认为有足够的理由相信两人在保释后不会继续实施危害国家安全的行为。钟沛权和林绍桐的律师都表示放弃每八日保释复核的权利。三位被告被控[串谋他人发布或复制煽动刊物](https://www.hkcnews.com/article/49783/%E7%AB%8B%E5%A0%B4%E6%96%B0%E8%81%9E-%E7%85%BD%E5%8B%95%E5%88%8A%E7%89%A9%E7%BD%AA-%E5%9C%8B%E5%AE%89%E8%99%95-49783/%E3%80%90%E7%AB%8B%E5%A0%B4%E6%96%B0%E8%81%9E%E6%A1%88%E3%80%91%E5%89%8D%E7%B8%BD%E7%B7%A8%E8%BC%AF%E9%8D%BE%E6%B2%9B%E6%AC%8A%E3%80%81%E5%89%8D%E7%BD%B2%E7%90%86%E7%B8%BD%E7%B7%A8%E8%BC%AF%E6%9E%97%E7%B4%B9%E6%A1%90%E8%A2%AB%E6%8B%92%E4%BF%9D%E9%87%8B-%E9%82%84%E6%9F%99%E8%87%B3%E6%98%8E%E5%B9%B42%E6%9C%8825%E6%97%A5%E5%86%8D%E8%A8%8A)，导致了包括引起香港市民间的不满及叛离以及对香港司法的憎恨和藐视在内的危害国家安全的后果。该案被延期至明年 2 月 25 日，因警方需要时间进行进一步调查，并决定是否有更多人被起诉。一位匿名前员工表示，他虽然知道《立场新闻》一直是当局的潜在目标，但没有意料到这一天来得这么快。#香港 #新闻自由

**2. 缅甸军政府在除夕夜前对全民盟领导人和活动家进行判决**

据缅甸[独立媒体 Irrawaddy 报道](https://www.irrawaddy.com/news/burma/myanmar-junta-sentences-nld-leaders-and-activists-ahead-of-new-years-eve.html)，在本周四至少有 62 名全国民主联盟（NLD）的领导人和参加反政府抗议的活动家被判刑。这其中包括了因六项腐败指控而将面临 18 年监禁的马圭地区首席部长昂莫努博士（Dr. Aung Moe Nyo），以及因煽动、叛乱和叛国等四项指控被判处 38 年监禁的掸邦计划和财政部长吴梭纽伦（U Soe Nyunt Lwin）。军政府法庭也对参加反对活动的名人和学生活动家进行了判决。包括女演员 Eaindra Kyaw Zin 和演员 Lu Min 在内的 7 位名人面临 3 年的劳改，而其他参与了抗议的学生的最低刑期均为两年。

[政治犯援助协会（AAPP）](https://aappb.org/)统计，距 2 月 1 日的军事政变以来，共有 8331 人被逮捕、指控或判刑，1964 人身负逮捕令并正在逃避捉捕，487 人被确认判刑。 除此之外，已有 1382 人被军政府杀害，这其中包括了在[平安夜屠杀](https://www.theguardian.com/world/2021/dec/28/myanmar-massacre-two-save-the-children-staff-among-dead)丢失性命的妇女儿童以及两名国际组织“救助儿童会”（Save the Children）的员工。#缅甸

**3. 拜登与普京通话，就乌克兰危机以及两国外交交换意见**

据[德国之声报道](https://www.dw.com/en/joe-biden-and-vladimir-putin-hold-constructive-phone-call/a-60299985)，美国总统拜登与俄罗斯总统普京于本周四通话了近一个小时，两人同意在明年初的日内瓦会议之前继续并推进他们的对话。两位总统的通话始于西方国家对俄罗斯在乌克兰附近集结的 10 万军队的担忧。普京表示，如果拜登在俄罗斯入侵乌克兰的情况下进行制裁，两国的关系将完全破裂。对此拜登回应道，如果出现入侵乌克兰的情况，美国及其盟国将作出果断反应，但其仍希望俄罗斯可以缓和与乌克兰的紧张关系。两国的代表将于明年一月十日于日内瓦进行进一步沟通与谈判，以期缓解剑拔弩张的局势。#乌克兰 #美国 #俄罗斯

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
