---
templateKey: article
title: 3.29 N记日报
slug: zao-bao
date: 2023-03-28T17:22:30Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: 松山

---
**1. 洪都拉斯宣布与台湾断交，与中华人民共和国建交**

[根据中华人民共和国驻新潟总领事馆新闻稿](http://niigata.china-consulate.gov.cn/fyrth/202303/t20230326_11049258.htm)，3 月 26 日，中国同洪都拉斯签署《中华人民共和国和洪都拉斯共和国关于建立外交关系的联合公报》，建立外交关系。公报指出，[洪都拉斯共和国政府承认世界上只有一个中国](https://sreci.gob.hn/node/1171)，并与台湾断绝外交关系。[据台湾媒体报导者消息](https://www.facebook.com/twreporter/posts/pfbid02nqjZGEk4UvJSKLmi8WSxyetoLTRwfqVqryqsHHM5sASUXPzbBAZYbG8XYb273gUfl?locale = zh_TW)，洪都拉斯于 1941 年与中华民国建立公使级外交关系，至今已超过 80 年。断交后，台湾的邦交国剩余 13 个。

[据中央社报道](https://www.cna.com.tw/news/aipl/202303265004.aspx)，3 月 13 日，台湾外交部长吴钊燮收到洪都拉斯外交部长寄出的纸本信件，信件署名日期为 3 月 7 日。信件内容提及洪都拉斯希望推动的 3 项合作案，涉及金额逾 20 亿美元。当地时间 3 月 14 日，[洪都拉斯总统](https://twitter.com/XiomaraCastroZ/status/1635780945096704000?cxt = HHwWgIC8rcqGu7MtAAAA)[Xiomara ](https://twitter.com/XiomaraCastroZ/status/1635780945096704000?cxt = HHwWgIC8rcqGu7MtAAAA)[Castro](https://twitter.com/XiomaraCastroZ/status/1635780945096704000?cxt = HHwWgIC8rcqGu7MtAAAA)[即在推特发表声明](https://twitter.com/XiomaraCastroZ/status/1635780945096704000?cxt = HHwWgIC8rcqGu7MtAAAA)，称已指示外交部长处理与中国建交事宜。

**2. 河北沧州一废弃冷库在拆除过程中发生火灾，致 11 人死亡**

[据沧县人民政府发布公告](http://www.cangxian.gov.cn/cangxian/c100850/202303/db383c9d7c754ae6872cb02399da4f8b.shtml)，3 月 27 日 14 时 30 分许，河北省沧州市沧县崔尔庄镇东村一废弃冷库在拆除过程中发生火灾。22 时 55 分火灾被彻底扑灭，共搜救出 11 人，均无生命体征。官方并未透漏事故的可能原因，有网友认为可能与制冷剂有关。

根据[《](http://m.weibo.cn/status/4884394751557900?)[经理人](http://m.weibo.cn/status/4884394751557900?)[杂志》](http://m.weibo.cn/status/4884394751557900?)消息，火灾事发地为崔尔庄红枣市场冷库，冷库关联的沧州崔尔庄枣业有限公司，曾于 2021 年因公示信息隐瞒真实情况，被沧州市场监督管理局列为经营异常。

**3. 谷歌应用市场因安全问题移除拼多多**

[路透社消息](https://www.reuters.com/technology/google-suspends-chinas-pinduoduo-app-due-malware-issues-2023-03-21/)，当地时间 3 月 21 日，Google 宣布因检测出拼多多的多个版本存在恶意软件问题，目前将其从应用市场移除。海外版拼多多“Temu”不受影响。据推特用户@李老师不是你老师收到投稿，github 上匿名用户发布[《](https://github.com/davincifans101/pinduoduo_backdoor_detailed_report/blob/main/report_cn.pdf)[拼多多后门详细报告》](https://github.com/davincifans101/pinduoduo_backdoor_detailed_report/blob/main/report_cn.pdf)。报告列举拼多多恶意行为，指出其利用手机厂商和云服务的漏洞获取用户隐私、强迫用户安装，控制超过 4 亿用户的电子设备。

**4. 在香港警方严格监视下，将军澳民众展开香港自 2020 年以来的第一次游行**

[据](https://www.bbc.com/news/world-asia-65080083)[BBC](https://www.bbc.com/news/world-asia-65080083)[新闻报道](https://www.bbc.com/news/world-asia-65080083)，3 月 26 日，约 80 名香港居民在将军澳地区游行，反对香港政府在将军澳 132 区进行填海、削坡工程，并重置填料转运等公共设施。据悉，这是香港自去年 12 月撤销限聚令以来，第一个获得警方不反对通知书的游行。警方对游行实施严格管制，要求游行人数不得超过 100 人，沿途应拉起胶带隔绝参加游行者与媒体、群众。参与游行者需在颈间佩戴号码牌，其他人不可以中途加入。[将军澳民生关注组召集人陈展浚接受香港媒体](https://news.rthk.hk/rthk/ch/component/k2/1693791-20230327.htm)[RTHK](https://news.rthk.hk/rthk/ch/component/k2/1693791-20230327.htm)[采访时表示](https://news.rthk.hk/rthk/ch/component/k2/1693791-20230327.htm)，在游行时要挂卡牌感到极大侮辱，下次举办相关活动时不会再接受类似要求。

欢迎加入NGOCN的Telegram频道：ngocn01
