---
templateKey: article
title: 11.10 N记早报
slug: zao-bao
date: 2020-11-10T08:01:25.607Z
featuredpost: false
featuredimage: /img/11.10.jpg
trending: 3
isbrief: true
tags:
  - ""
---
1.拜登公布过渡政府疫情顾问委员会名单，特朗普开除国防部长


据纽约时报，当地时间11月9日上午，美国当选总统拜登（Joe Biden）与副总统贺锦丽（Kamala Harris）于公布了过渡政府的疫情顾问委员会名单。拜登发推文表示，“直到1月20日我才会成为总统，但我今天就要告诉每个人：佩戴口罩”。另一方面，美国总统特朗普继续拒绝承认败选，并于周一上午发推宣布，开除国防部长埃斯珀（Mark Esper），由国家反恐中心主任克里斯托弗·米勒（Christopher C. Miller）担任代理国防部长一职。6月，埃斯珀曾就“是否派遣现役军人来控制美国城市的抗议浪潮”一事与特朗普公开表示异议。


2.浙江衢州一工厂发生火灾，据称无人员伤亡


据微博“衢州发布”消息，11月9日中午11点30分左右，浙江中天东方氟硅材料有限公司露天桶装高沸物发生火灾事故，并于晚间发生二次燃爆。截止10日7时，火势基本得到控制，只剩一处明火处于受控燃烧状态，目前没有人员伤亡和被困，救援还在进行中。据悉，事故中的燃烧物质主要是氯硅烷，燃烧产物有毒。据网传的现场视频，起火处有大量浓烟，且升起“蘑菇云”，与衢州市毗邻的玉山县有多名群众反映称，玉山县上空有大片“黑云”，天空变暗，且空气弥漫着刺鼻气味。晚间，衢州市广播电视总台称，“经环保部门现场检测，此次火灾事故环境影响基本可控。”


3.美国辉瑞公司宣布新冠疫苗在第三阶段试验中90%有效


法新社消息，11月9日，美国辉瑞公司宣布，其与德国BioNTech公司联手研发的新冠疫苗，在进行中的第三期临床试验显示，预防COVID-19的保护效力高达90%。据联合报，美国国家过敏和传染病研究所所长佛奇表示，疫苗只要达到50%至60%的有效率即可接受。辉瑞和BioNTech疫苗的有效率远超市场预期。此消息一出，美股道琼斯指数大涨，美国总统特朗普和当选总统的拜登都发推文指这是一则好消息。


4.据报道人大常委会将会取消多名泛民议员资格，泛民称若属实则会总辞


据香港01独家消息，香港建制派高层圈子昨夜突然传出“北京将重手‘对付’泛民派复会后的拉布抗争行动”（“拉布”指泛民派议员在10月中香港立法会复会后，多次提出清点法定人数，至今导致3次流会）。报道称，于10日在北京召开的人大常委会将加入针对香港立法会“拉布”问题的议程，预料泛民派多名议员将会因此被取消（DQ）议员资格。据《南华早报》报道，香港唯一人大常委谭耀宗于9日上午表示，反对派议员在立法会会议上采取拖延战术，他引用民调结果，指泛民不适合担任立法会议员。泛民派会议召集人胡志伟表示，“一旦人大常委会落实取消资格的决定，我们一定会义无反顾总辞”以表达抗争之意。


5.缅甸大选投票结束，昂山素季领导的执政党预计胜出


BBC消息，缅甸在当地时间周日（11月8日）举行国会选举，这是缅甸自2011年脱离军事统治后迎来的第二次民主大选。据澎湃新闻数据，此次大选投票率高达88%，超过了2015年80%的投票率。11月9日，昂山素季领导的执政党全国民主联盟发言人表示，根据内部非正式选举结果预测，他们已经赢得足够议席，可以筹备组建新政府。选举委员会目前暂未公布任何结果。


6.港科大学生周梓乐逝世一年，警方举紫旗驱散悼念群众


2019年11月8日，香港科技大学学生周梓乐在警民冲突的现场、将军澳附近的停车场中坠楼，经送医不治身亡。据端传媒报道，前天（11月8日）晚间，大量市民携带蜡烛和鲜花，前往将军澳参与悼念。約20時左右，警方指聚集人数过多，宣布施行人潮管制，拉起封锁线，呼吁到场市民离开，引发市民不满。期间，有市民大喊“光复香港 时代革命”等口号，现场警方立刻举起紫旗和蓝旗，警告市民喊口号涉嫌违反港版《国安法》。周梓乐坠楼事件目前仍然存疑，11月16日，法院将展开周梓乐的死因研讯。


7.牛津词典修订对“woman（女性）”的定义


据《卫报》报道，11月9日，作为对网上联署的回应，牛津大学出版社修订了《牛津词典》中对“woman（女性）”一词的定义：由“a man’s wife, girlfriend, or female lover”，改为“a person’s wife, girlfriend, or female lover”，认为一位女性可以是一个人（而不必要是一位男性）的妻子、女友或女情人。据悉，今年的国际妇女节，女权运动家Maria Beatrice Giovanardi提出修改《牛津字典》中对“woman”定义的联署，获得近3.5万人的支持。Maria Beatrice Giovanardi对修改的结果表示“非常满意”，但对字典中一些贬损女性的字词（如“bitch”）与女性关联表达失望。对此，牛津大学出版社解释说，字典是为了反映人们在日常生活中如何使用英语，而出版社已经在修订中为这些字词加上了“过时”等注释。


早报🐱：不许为难的朋朋


欢迎加入NGOCN的Telegram频道：ngocn01


微信好友：njiqirenno2

![](/img/11.10.jpg)