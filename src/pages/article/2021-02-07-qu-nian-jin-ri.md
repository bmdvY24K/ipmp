---
templateKey: article
title: 2020:"共享富裕，共享恐惧"(一月)
slug: qu-nian-jin-ri-jan
date: 2021-02-02T03:21:57.265Z
description: null
featuredpost: true
featuredimage: /img/20191230 2.jpg
trending: 9
isbrief: false
typora-copy-images-to: ../../../static/img
columnist: 2020：“共享富裕，共享恐惧”
tags:
  - "2020"
  - 去年今日
  - covid-19
  - 武汉
  - 李文亮
contributors:
  - type: 插图
    name: 糖糖
  - type: 作者
    name: laitek
  - type: 作者
    name: lizi
typora-root-url: ../../../static
---

![](/img/20191230 2.jpg)

![](/img/20191231 2.jpg)

![](/img/20200101 2.jpg)

![](/img/20200102 2.jpg)

![](/img/20200103 2.jpg)

![](/img/20200117 2.jpg)

![](/img/20200118 2.jpg)

![](/img/20200120 2.jpg)

![](/img/20200121 2.jpg)

![](/img/20200122 2.jpg)

![](/img/20200123 2.jpg)

![](/img/20200124 2.jpg)

![](/img/20200125 2.jpg)

![](/img/20200126 2.jpg)

![](/img/20200127 2.jpg)

![](/img/20200128 2.jpg)

![](/img/20200129 2.jpg)

![](/img/20200130 2.jpg)