---
templateKey: article
title: 'This Week China Society - Chaos, fears, and desperation: Afghanistan after the Taliban takeover'
slug: 'weekly-newsletter-24-august'
date: 2021-08-24T01:28:20.370Z
description: 'This is NGOCN‘s seventh weekly newsletter. We are covering the chaotic scenes at Kabul International Airport, women and girls in the Taliban-led Afghanistan, and other news that happened in Chinese society.'
featuredpost: true
trending: 7
isbrief: false
columnist: '英文周报Weekly Newsletter'
contributors:
  - type: 'Author'
    name: 'Ming'
  - type: 'Editor'
    name: 'laitek, cookie'


---

Happy Tuesday. This is NGOCN's seventh weekly newsletter. We are covering the chaotic scenes at Kabul International Airport, women and girls in the Taliban-led Afghanistan, and other news that happened in Chinese society.

**Chaos, fears, and desperation: Afghanistan after the Taliban takeover**

It has been over a week since Afghanistan fell to the Taliban.

Kabul International Airport, currently almost the only route out of the country, has seen days of chaos since the Taliban swiftly took control of the capital on August 15.

[Videos](https://edition.cnn.com/videos/world/2021/08/16/kabul-clinging-to-airplane-taking-off-tarmac-afghanistan-ward-vpx.cnn) circulating widely on social media showed hundreds of people pouring into the tarmac of Kabul's International Airport, forcing their way onto any airplane leaving the country. Another horrifying [video](https://www.youtube.com/watch?v=OrJxqEwIcxU) appeared to show two Afghans falling from the sky after desperately clinging to a military plane as it took off. 

At least 20 people have died in last week near the airport, a [Nato official](https://www.reuters.com/world/asia-pacific/least-20-deaths-last-week-during-kabul-airport-evacuation-effort-nato-official-2021-08-22/) said on Sunday.

![](https://raw.githubusercontent.com/syeung19/image/main/20210824085752.png)

*Screenshot from Twitter showing people run on tarmac of Kabul airport as plane attempts to take off (Source:* [*The New York Times*](https://twitter.com/nytimes/status/1427280264133230598?s=20)*)*



The country is in chaos, but for the women and girls in Afghanistan, the future is particularly terrifying.

A Taliban spokesman said on Monday that under the Sharia law, women would be allowed to work and study. "We are committed to women's rights, to education, to work, and to freedom of speech, in the light of our Islamic rules," said Suhail Shaheen.

But when the Taliban was in power 20 years ago, women were not allowed to study, work, and hang out without the company of a male chaperone. They have to wear head-to-toe burqas. The punishments for violating these rules were brutal: women were beaten for attempts to study and stoned to death if were found guilty of adultery.

![](https://raw.githubusercontent.com/syeung19/image/main/20210824085841.png)

*Images of women severely defaced in Kabul beauty salon (Source: AFP/Wakil Kohsar)*



"Everything that I have worked so hard to build as a filmmaker in my country is at risk of falling," Sahraa Karimi, the first female president of the Afghan state film company, wrote in [an open letter](https://twitter.com/sahraakarimi/status/1426161540818997250?ref_src=twsrc^tfw|twcamp^tweetembed|twterm^1426161540818997250|twgr^|twcon^s1_&ref_url=https%3A%2F%2Fwww.dw.com%2Fen%2Fthe-taliban-will-ban-all-art-an-afghan-female-filmmakers-plea%2Fa-58877655) calling for international attention on the Taliban's takeover of Afghanistan.

"If the Taliban take over they will ban all art. I and other filmmakers could be next on their hit list," she wrote.

A [translated Chinese version](https://chinadigitaltimes.net/chinese/669677.html) of this open letter circulated on Chinese social medias and was reposted widely. But it has soon been deleted by the official WeChat platform due to "[violation on certain regulations](https://mp.weixin.qq.com/s/xvVl1e9raOtpnJG-lvVe7g?fbclid=IwAR38dvBifPxz6r5zbO5lNYXuq7-H-VUzUist6NWmRpaCV9EhgUOuOfEFyDw)".

Chinese state media attempts to describe the Taliban as “people’s choice”, probably a sign Beijing is in support of the Taliban-led government, which has received criticism. “The situation in Afghanistan has already undergone major changes. We respect the will and choice of the Afghan people,” [said](https://www.fmprc.gov.cn/mfa_eng/xwfw_665399/s2510_665401/2511_665403/t1899785.shtml) the Chinese Foreign Ministry on August 16.

 

**MEANWHILE**

1. Li Mei, a high school student, has [accused](https://www.weibo.com/5267090159/KtLBXwL6t?from  = page_1005055267090159_profile&wvr  = 6&mod  = weibotime&type  = comment#_rnd1629167656568) her math teacher Duan Xiangyang on August 16 of sexual assault. The teacher has acknowledged the fact of sexual assault and promised that he would not continue teaching at his school; however, people [found out](https://mp.weixin.qq.com/s/AlWS5-Vi-k_VCt7__TjILA) that he is still at school for teaching after the case. Changsha police [announced](https://weibo.com/5747180584/KtZNBzoIK?type = comment#_rnd1629301525175) on August 18 that Duan has been detained on suspicion of sexual assault and they are currently investigating into the case.

2. Echeng, Hubei [announced](https://news.sina.com.cn/c/2021-08-17/doc-ikqcfncc3433457.shtml) on August 15 that covid vaccination is required for everyone 12 years of age and older, except for those who have a contraindication to vaccination. People who are unvaccinated without a valid reason will leave a record in their personal credit report.