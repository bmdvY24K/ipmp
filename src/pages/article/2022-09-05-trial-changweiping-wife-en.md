---
templateKey: article
title: 'High (official) crime on the highway: Chen Zijuan’s long journey to a trial of her human rights lawyer husband'
slug: 'trial-changweiping-wife-en'
date: 2022-09-05T03:21:57.265Z
description: 'He had wanted to appear in court in his suit. But he wasn’t even allowed to. He was in this absurd trial in a white hazmat suit.'
featuredpost: true
featuredimage: /img/chang-wife1.jpg
trending: 10
isbrief: false
contributors:
  - type: 'Author'
    name: 'Jiang Xue'
  - type: 'Translator'
    name: 'Chloe Chen'
  - type: 'Proofreader'
    name: 'Tu Huynh' 
tags:
  - "human rights lawyer"
  - "Chang Weiping"
  - "Chen Zijuan"
  - "Xiamen Case"
---

![234](/Users/admin/Documents/GitHub/ipmp/static/img/chang-wife1.jpg)
插画：Wilson Tsang

**[Editor’s note] This original article in Chinese ([陈紫娟千里“开庭记”:从深圳到凤县，去守候一位人权律师的庭审](https://ngocn2.org/article/2022-08-26-trial-changweiping-wife/)) was co-published by Initium Media and the NGOCN Voice Program, and this English version first appeared on NGOCN.**

> Since July 26, 2022, when Chang Wei-ping‘s trial on the charge of "suspicion of subversion of state power" was held in secret, there has been no updates on his case. This is worrisome. We have translated this report into English and call on the community at home and abroad to continue to pay attention to Mr. Chang’s case.

> We hope that you will forward the Chinese or English version of this report to people who are concerned about the story of human rights lawyers in China, in general, and Mr. Chang’s case, in particular. A seemingly small action, such as introducing people to this case and the plight of other human rights lawyers, is a form of support that can go a long way (see [*Intolerable to the State: Lawyer Chang Weiping*](https://ngocn2.org/article/2021-09-07-lawyer-chang-weiping/)). All support right now is important to Mr. Chang and for his family.


In July 2022, the trial date of Chang Weiping’s case was announced. His wife, Chen Zijuan, decided to travel with her family to Fengxian, Shaanxi from Shenzhen, Guangdong. Her plan was to wait outside the courthouse on the day of her husband’s trial. The story below is an account of what Chen Zijuan, her son, and her mother endured on that trip.

Chen Zijuan rarely wore her emerald green suit and skirt set. However, on July 26, 2022, she decided to wear this to “hover and watch”(weiguan) her husband’s trial.

The court of Fengxian,in Baoji City, had announced that Chang Weiping’s case of “suspicion of inciting subversion of state power” would not be publicly tried. This announcement deprived Chen Zijuan of the opportunity to sit in on the trial as a family member. Nonetheless, she held out hope and planned to bring her mother and son there. She wanted to stand outside of the courthouse in hope of Chang Weiping catching a glimpse of them through the window of the prison van when he was being transferred from the detention center to the courthouse.

Chen Zijuan works at a hospital in Shenzhen. She is the same age as Chang Weiping. Both turned 38 years old this year. She has a Ph.D. in biology. Their life could have been well-off; the days could have been “nice and peaceful.” But, since her husband was arrested again in October 2020, her life has changed.

Despite pressure, she has tirelessly spoken out, appealed, and raised attention to her husband’s case for one year and eight months. What came out of it was a trial that she “no longer has hope for.”

**1**

In July 2022, after a week-long “pre-trial meetings,” the court date for Chang Weiping’s case was set for July 26. Before its announcement, Chen Zijuan was already frustrated by a series of actions made by the court: the trial was declared a “non-public” hearing and Chang’s lawyer was required to sign a non-disclosure agreement.


Inter-city travel is still somewhat restricted under China’s “Zero-Covid” policy and never-ending controls over travel in the name of pandemic prevention. Shenzhen has had a few Covid outbreaks since the beginning of this year. As a result, the city has instructed its residents to “remain in Shenzhen unless absolutely necessary.” To be able to leave Shenzhen for Fengxian, Shaanxi, as a medical staff, Chen Zijuan is required to apply for leave from her employer in advance.

She feels grateful towards her employer. Since Chang Weiping lost his freedom in October 2020, the Shaanxi Provincial police have traveled to Shenzhen many times to interrogate her. They sometimes went directly to the hospital where she works to look for her. During one of those times, the police wanted to take her into the station for questioning, but her superior stopped them. Her superior negotiated with them: “I’ll make available an office room for you. You can question her there.” In that office room, she was questioned for two full hours.

She is glad that she lives in Shenzhen. Compared to inland provinces like Shaanxi, Shenzhen, as an economically developed city in southern China, can still secure some flexibility in terms of its local culture.

In fact, the managerial staff at the hospital that Chen Zijuan works at, which is affiliated to a university, are appalled by the police who would appear at will to harass their employees in the name of “police work,” even though they rarely expressed it.

After waiting for so long, Chang’s case is finally going to trial. For Chen Zijuan, this is a big event that she must attend. She felt relieved that her application for leave was approved.

She decided to bring her son and mother to Fengxian, which is more than 1,000 kilometers away. For the family, this is a historic event that needs everyone to be present.

She booked a flight for 6:30am on July 25th, so that they could arrive in Baoji early to transfer planes to Fengxian. Besides, early morning tickets are cheaper, too.

To catch their flight, they had to leave home before 4am. Chen Zijuan barely slept that night. Unexpectedly, the early morning flight was cancelled. They didn’t arrive at Xianyang International Airport until after 1pm.

A friend had rented a car for her to pick up at the airport. Before she left there, she searched for a flower stand at the airport until she finally found one. She bought a bouquet of champagne roses and bright sunflowers. These are the flowers that she and Chang adore.

“I worried that Fengxian might be too rural to have flower shops,” she thought.

The bouquet inside the car filled the space with its delicate fragrance.

![224](/Users/admin/Documents/GitHub/ipmp/static/img/chang-wife2.jpg)
Picture: many police cars and policemen at the exit of the highway. Source: courtesy of the interviewee.

**2**

She was driving on the highway. It takes about three hours to get to Fengxian. The Feng-tai Highway that she must take was inaugurated at the end of last year, which was good news for her.

Fengxian County is part of Baoji City, but it is located deep within the Qinling mountains. When the weather condition is good, it takes about three hours to drive there from Baoji. To get to Fengxian, Chen Zijuan had to fly to Xi’an to catch a connecting flight to Baoji, and then drive through the Qinling mountains. In the past, the journey took 5 to 6 hours.

With the newly inaugurated highway, her travel time is cut.

The mountains in the distance were contoured by the gleaming twilight of a setting summer sun. The words “Fengxian” appeared under the blue sky, they were almost near their exit on the highway. Even from a distance, Chen Zijuan could tell the inspections at the exit was particularly strict. Normally, highway inspections for Covid prevention would only require people to show their health code. But this time both drivers and passengers were asked to show their photo IDs besides their health codes.

So, she showed the inspectors her family members’ health codes and photo IDs. A person in a white hazmat suit approached her, informing her about the “police inspection” and, subsequently, denied her entry into Fengxian. The reason given for this was that “Dongguan” showed up on her past itinerary. “But Dongguan is not even a high-risked area at the moment!” she bargained.

She had previously checked the local Covid policy by calling the Baoji Municipal hotline 12345. The hotline unequivocally informed her that her condition was clear, and she would be able to enter Baoji. So, she called the number again in the car and was told the same thing.

However, the so-called “law enforcement officers,” who hid their faces behind their masks and in white hazmat suits, insisted that they had to turn back or else be forced to a quarantine facility. 

She started to quarrel with them, [which was recorded on video](https://www.youtube.com/watch?v=kNMtXwH7hfk&feature=youtu.be). A chubby young police officer, who wore glasses, persistently yelled at her to leave and stop recording. 

She noticed that many more police cars appeared within the next 10 mins. Some police cars even had the word “SWAT” on them. Two ambulances also arrived in the next few minutes.

She counted the number of police officers from the inside of her car. There was more than a dozen of them. A SWAT police car parked right in front of her car. She did not realize that they were barricading her car. Before long, a white SUV appeared and parked immediately to her left, blocking her side view. Another car arrived and parked behind her. The right side of her car was already against the guardrail of the highway.

She was completely surrounded. 

![224](/Users/admin/Documents/GitHub/ipmp/static/img/chang-wife3.jpg)
Picture: surrounded by the police at a highway exit. Courtesy of the interviewee.

**3**

When the ambulances arrived, Chen Zijuan started to feel anxious. She worried that they would send her, her child, and her mother to a quarantine facility.

She decided that she would not get out of her car anymore. A standoff began. Every once in a short while someone would come and knock on her window to ask her to step out of her car. She refused each time.

The night fell and the mountains in the distance blended into the night. She had not have dinner. Neither had her child nor mother.

Someone knocked at her window again. She yelled at them, “how do I know if you are really who you say you are? For all I know, you could very well be local gangs!” What she said might have worked, because two women were sent to knock on her window from then on.

She wondered to herself, “How did these people know that she was coming for the trial?” She had turned on her phone’s flight mode since she was at the Xiaoyang Airport to prevent tracking. It proved to be futile. How could one escape an all-encompassing surveillance state?

The police rotated the people sent to ask her to step out of her car. Some in police uniform, some in street clothes, and some in white hazmat suits. They told her, “You are not allowed to enter Fengxian for the sake of the public health of the people in Fengxian.” As a doctor, Chen Zijuan almost laughed when she heard this.

It was past 9pm when Chen Zijuan snapped. She yelled at them, “You are detaining me illegally! I have an 8-year-old and a 60-year-old in my car. If anything happens to them, I will hold all of you responsible!”

No one answered. It was hard for her to make out their faces in the dark. All those people inside and outside of the cars that surrounded hers remained silent.

Her lawyer brought food, but could not get it to her. She heard those shadowy figures telling the lawyer, “She is dangerous. You cannot approach her.”

![124](/Users/admin/Documents/GitHub/ipmp/static/img/chang-wife4.jpg)
Picture: Police and the food that had to be left outside of her car door. Courtesy of the interviewee.

She knew that they were trying to force her to come out of her car.

She felt watching eyes from the white car parked to her left, even though she could not make out the faces. A camera was placed on her car hood and pointed at her.

The night deepened. Her child grew tired. She was glad that the rental car was a 7-seater. Her child was able to lie down in the back seat to get some sleep.

“If this was a 5-seater, I think I might have to surrender to them,” she said.

She grew extremely tired past 2am and was just about to dose off when Li Dawei, a friend who traveled from another province to “hover and watch” the trial, called her. He told her that he was staying at a hotel room, and national security agents knocked on his door. They told him to go back to where he came from. He refused and started to quarrel with them.

Chen Zijuan did not think she could fall asleep listening to the quarrel over her phone. But she was so tired. Eventually, she passed out for about two hours.


**4**

The day dawned. It was only past 6am, but the weather was already hot. Chen Zijuan recorded another short video from inside her car after she woke up: the mountains in the distance, the inspection station, the food that has been sitting on her car hood - but she could not get to it.

Her family and her have not eaten any food since the previous day’s lunch, which only consisted of take-out roujiamo (Chinese hamburger) and liangpi (spicy “cold skin” noodle).

Only two hours left before the trial began. She checked her phone; she was only about 4.8km away from the courthouse. She wondered if she should get out of the car and walk to the courthouse.

But, she quickly thought to herself, their goal was to stop her from getting to the courthouse. If she stepped out of her car, she might come into physical conflict with them, which she did not want to happen.

Deep down, she knew that even if she made it to the courthouse, she would likely not be allowed near the courtroom. She only wanted to stand outside of the courthouse and watch the prison van pull up. If Chang Weiping was able to look out of his car window and saw her, their child, and her mother, it might bring him some solace, she thought.

After spending some time entertaining the idea of walking to the courthouse, she gave up the idea. She knew it was impossible that she would be allowed to do that.

![125](/Users/admin/Documents/GitHub/ipmp/static/img/chang-wife5.jpg)
Picture: the SWAT car that blocked Chen Zijuan’s car from the front. Courtesy of the interviewee.

It was 9 o’clock in the morning, the trial began. The car that was blocking her car from behind drove away. She was told that she could turn around and head back to Baoji, but she still could not go to Fengxian.

She refused, “My destination is in Fengxian, not in Baoji.”

It has been 16 hours since she was besieged. Bottled water, cucumbers, and tomatoes were brought to her but left by her car. They wanted her to step out to get them. She said to them, “I came here for the trial, not for this food,” and remained in her car.

Li Dawei arrived some time after 11am. He was followed by national security agents. He brought mantou (Chinese steamed bun) and watermelons, but was instructed to leave them on the road by the car.

Meanwhile, the trial went on for about two hours at the courthouse that was only 4.8km away from her.

At 12 o’clock, she called her lawyer and learned that the trial had concluded. Her lawyer informed her that Chang Weiping looked okay except for having lost some weight.

“I will call you back in a moment,” the lawyer said before he hung up, “I want to watch him get on the prison van.”

Chen Zijuan could no longer hold back her tears. She bursted out crying.


**5**

“He had wanted to appear in court in his suit. But he wasn’t even allowed to. He was in this absurd trial in a white hazmat suit.” She felt so sad at the thought of this.

“I know it in my heart that he has suffered. This is his fate, but also his achievement. To be charged with ‘suspicion of inciting subversion of state power’in a time like this, it is an honor. But I also think he was martyred by his time. On the other hand, he has made history.” When Chen Zijuan thought about these to herself, she could feel more than just sadness. In fact, she deeply understood him.

It was for this reason that she wanted to appear at his trial in her nice emerald green suit. “Because I wanted to stand witness to this process dressed dignified.”

The videos that she recorded from inside her car during the besiegement was for her 8-year-old son, who is a particularly naughty and clever little boy. In the video captured by her mobile phone, her son was acting silly and loudly shouting: “My dad’s trial has begun! My dad’s trial has begun! All rise!”

At the end of the video, her son asked, “Why can’t we go to Fengxian?”

“Some people said that the videos I recorded for my son did not show the suffering we endured on that trip. But I think that sometimes life and death are not so important. We should let go a little, and just live with integrity. In any case, we still have the moral high ground, and we have nothing to apologize for,” she said.

“Although I am also angry and sad because they maltreated us, I loathe these people from the bottom of my heart. Those who are evil’s accomplices are, in my opinion, beasts in human skins.”

“They are morally incomparable to us,” she said.

It was noon on July 26, Chang Weiping’s trial ended in a courtroom 4.8km away. Chen Zijuan, after being barricaded near an exit on the Fengxian Highway, finally opened the car door and stepped out, carrying the bouquet of flower that she had bought at the airport.

Her son and mother also stepped out of the car.

She stood near the highway exit with the bouquet in her arms. She passed her phone to her son and asked him to capture the moments that followed.

She heard her 60-year-old mother sob while berating those that stood around them: “You have all wronged and treated Chang Weiping unjustly! Dare you tell the public about this so-called case that you are handling?”

None of the people who surrounded them said a word.

The summer breeze gently moved Chen Zijuan’s hair. Her 8-year-old son recorded her speech: “Chang Weiping, a trial is held for your case today. Today you suffered, but today is also a glorious day for you…… your child and I will always support you, and we await the day of your return.”

[![chang-wife](https://res.cloudinary.com/marcomontalbano/image/upload/v1661508087/video_to_markdown/images/youtube--hQMvvvmtOy4-c05b58ac6eb4c4700831b2b3070cd403.jpg)](https://www.youtube.com/watch?v=hQMvvvmtOy4 "chang-wife")

Those who stood around them only seemed to snap out of their trance at the moment she that finished recording the video, which was one minute and four seconds long. A man in a hazmat suit approached her and said: “Put on your mask. You need to comply with pandemic prevention measures.”

She replied, “This is a crime that you are committing. Does it mean that I have to cooperate, too?”

She walked up to each of them and announced loudly, “Please show me your ID before talking to me.” The people around them seemed discouraged. No one dared to reveal their names to her.

Chen Zijuan started her car at half past one in the afternoon. She left the highway exit and drove back to Baoji. She went back to her work in Shenzhen the next day.

They had something to eat after she recorded her speech. “We don’t need to suffer,” she laughed. This was the first meal they had after being barricaded on the highway for 18 hours.

![126](/Users/admin/Documents/GitHub/ipmp/static/img/chang-wife5.jpg)
Picture: a family photo of Chang Weiping, Chen Zijuan, and their son. Courtesy of the interviewee. 

**6**

On the way back, she found herself missing him a lot. He is a good person, she thought, someone who is worthy of her fighting for.

He has always been reliable. He is extraordinary and always strives to be better. He never stops learning. After the two of them graduated from college, they stayed in Beijing while he worked on becoming certified on multiple things, including a certificate for securities broker. He can succeed at whatever he set his mind to. He is the best among his peers.

They were high school classmates. She recalled the two brightest students in her class. One was Chang Weiping, the other one went on to become a professor at one of the most prestigious universities in China, joined the Chinese Community Party, thrived, and have a life that is envious to the many. “He made himself fit to everything the system requires.”

Today, she knew from comparison what qualities that she admires and aspires to.

“I’m not doing any of this just because I’m Chang Weiping’s wife. I love him. But I’m my own person too. I’m outraged by what these people have done to us. They have been bullying us, and I’ve been tolerant,” she said.

In October 2020, Chang Weiping was arrested again during his bail period. The Shenzhen police were the first to knock on her door and tried to harass her. Both times they came during the night. In response, she applied for release of information through the government. Later, the Shaanxi police made 9 trips to Shenzhen, all to threaten and intimidate her. But each time she would write down what happened and publicize it on Weibo.

She said she was never going to be silent. Even as an ordinary person, she cannot tolerate injustices and evil wrongdoings committed in the name of law enforcement.

She recalled their high school days in Chongqing. They had just started dating, she would always laugh because of his sense of humor.

He has a cheerful appearance and has aged into an unwavering look. But he still has a sensitive and vulnerable side. When they got into a fight, he was always the one who would open the door and wanted to leave. And she was always the one who would have to woo him back.

He has lots of soft spots in him and would become emotional and cry often.

In her opinion, he is someone who cannot tolerate injustices, someone who is passionate about law. He wanted to help people and push for social progresses by taking on legal cases. He is too passionate about being a lawyer.

She also recalled some of the arguments they had when they just got married. They would argue about trivial things, such as whose family they would spend time with during the Spring Festival. But he has changed. He has become more respectful of her over the years. As he spent more time with friends who worked on gender equality, he also became more mindful of his own gender politics. In her opinion, this is quite natural because he already has deep sympathy for the disadvantaged.

She believes he has insight, ideas, actions, and courage. “He is worthy of all the good words,” she said.

But she did not really get to know him until his second arrest. He had shielded her in the past. He did not even allow her to use his laptop and see what he was working on.

She recalled the last time she saw him. It was the October of 2019; he went away again after he just returned to his home in Shenzhen. He called her from Xinjiang in December. She saw him standing in a snow-covered field in the video call.

She received a call from him soon afterwards, telling her that he needed to lay low for a while. He attended something in Xiamen, and several people were arrested for it. He could not be back for the New Year. After a few days, she could no longer find him. He was arrested.

He was released after 10 days. He told her about the torture he suffered in the prison. They both cried.

Since then, she lived in Shenzhen with their son and her mother, while he stayed at his hometown. It was not until much later that she learned how lonely and scared he felt living alone.

He suffered the pain left by the torture, the home surveillance, and humiliation during his bail period. But he still tried to find joy in sorrows.

He did not want her to worry and shielded her from knowing many things. Instead, he confided in his friends: the coffins in the attics that his parents had prepared for themselves, in the fashion of local custom, terrified him. He had dodged the national security agents assigned to watch him and drove to the city to watch the city lights. Because the night in rural areas were too quiet, too dark, and too terrifying.

He yearned to return to his home in the city, to his family, and to his work. But he was not allowed. He struggled in loneliness.

On October 16, 2020, for unknown reasons, he suddenly decided to [publicize an account of the torture he had endured in the prison](https://www.youtube.com/watch?v=1dvDjbHr85k&t=1s). He was arrested again in October 24.

“Can’t he just hold on until the one-year bail period expires,” some said. Chen Zijuan wondered that, too.

Another friend of theirs thought that, perhaps, there was an epiphanic moment when he realized he could no longer endure the humiliation. He wanted to expose it all and end the suffering he endured in silence.

But other signs indicated that it might have been because he had learned that the authorities would continue to manufacture a case against him.

He left many videos during the 10-month bail period. In one of the videos, he yelled out loud: “Let the storm come!”

“Many people have lost touch with their conscience and only care about their own losses and gains. But not Chang,” Chen Zijuan said.

In the summer of 2022, the 38-year-old Chang Weiping is weathering the thunderstorm of his life. His resilience makes Chen Zijuan proud.