---
templateKey: article
title: '成都“老油条”：我目睹了警察冲进人群的那一刻'
slug: 'white-paper-movement-cheng-du-2'
date: 2022-12-08T13:21:57.265Z
description: '上百个身穿黑色衣服的便衣直接冲进了人群之中进行驱赶，聚集的人群瞬间往两边跑，整个场面开始乱了起来。'
featuredpost: true
featuredimage: /img/chengd-21.jpg
trending: 10
isbrief: false
contributors:
  - type: '整理'
    name: '林十三'
  - type: '编辑'  
    name: 'Tan'
tags:
  - "白纸运动"
  - "成都"
  - "乌鲁木齐火灾"
  - "疫情封控"
  - "抗议"
  - "望平街" 
---

**“去望平街之前，我一度以为没有多少人会来，最后又变成和朋友吃饭、喝茶、聊天——就像2013年，我去参加反对彭州石化的抗议一样。”以下是成都抗议现场，一位目击者对NGOCN的讲述。**

![22](/Users/admin/Documents/GitHub/ipmp/static/img/chengd-21.jpg)
<center><font color=gray>11月27日晚，大批成都市民聚集在望平街一带响应白纸运动。图片由受访者提供</font></center>

周日我本来是要上班的，不过武侯区刚好发了社会面疫情防控的通告，“严格重点场所管理”、“暂停营业”。后来我也会想，是不是因为恰好是周末，才有了这场我从未遇见过的大规模抗议。

前一天和朋友看了世界杯，一边还在关注上海乌鲁木齐中路的抗议，所以第二天我一直睡到中午才起来，下午就去了玉林路，打算逛逛街，或是找朋友一起晒晒太阳、聊聊天。恰好也是这天的三点左右，就在玉林路小酒馆外，几个女生站在路边手持白纸，无声抗议。网上看到这张照片之后，我决定去看一看。那是下午四点。

去到的时候，几个女生还在，现场的人不多，甚至可以说是没人管她们。路过的司机有些会鸣笛，还对她们说，“我支持你们”。后来，站在那里拿着白纸的人又多了两三个——有人自发地加入。我本来也想加入，但又很担心会被警察找上门。后来我也得知，几个女生都被警察找去做了笔录。

![22](/Users/admin/Documents/GitHub/ipmp/static/img/chengd-22.jpg)
<center><font color=gray>11月27日，几个成都女生站在路边手持白纸无声抗议。图片由受访者提供</font></center>

待到五点左右举白纸的人都撤了。我在那里遇到了几个认识的朋友，就打算一起去望平街。

去望平街之前，我一度以为没有多少人会来，最后又变成和朋友吃饭、喝茶、聊天——就像2013年，我去参加反对彭州石化的抗议一样。因为那天成都还发生了不少事，早上就传出有两个男生在太古里悼念乌鲁木齐的遇难同胞，最后被警察带走了。等到下午的时候，又有消息传出来，望平街已经有不少警察严阵以待。

从玉林路到望平街差不多有6公里的路，我们骑了半个小时的共享单车，到现场的第一眼，是沿着河边的一排警车和大批警察。

望平街算是成都比较出名的一条文创网红街，周末一般都很多人。按照之前来的经验，进街只要出示健康码就行，但这次不一样，卡口的保安严格地执行扫场所码的规定，说实话我挺怀疑是警察为了收集入场人士的身份信息。

所以我们绕着街区走了一圈，想看看有没有别的办法能够溜进去，后来还是有个现场的朋友给我们指了一条路。进去之后我们先去吃了饭，巧的是，旁边的一桌年轻人后来也去了现场——晚上离开的时候我们又碰上了。

吃完饭去到现场已经是七点多，那是亚朵酒店前面的空地，看到现场的第一眼我是震惊的，因为路上、花坛上密密麻麻都已经站满了人，中间则是已经摆满了蜡烛，保安已经在拿栏杆准备把路封起来。这是我完全没有预料到的场景。

![22](/Users/admin/Documents/GitHub/ipmp/static/img/chengd-23.jpg)
<center><font color=gray>11月27日晚，大批成都市民聚集在望平街一带响应白纸运动。图片由受访者提供</font></center>

一开始我还在观望，我在附近走了一圈，又在一旁找了个空地抽了一根烟。也就是在这个时候，一个女生过来问我，“你需要A4纸吗？”，一个男生哭得稀里哗啦地在我面前走过。

我以为自己只是来凑热闹的，我以为如同往常参与的活动，不过也是抽烟、喝酒、聊天。直到这一刻，耳旁是人群的呐喊，“不自由，毋宁死”。我觉得，我还是要进去看一看。

地上是蜡烛和鲜花，有个新疆的男生讲了一段话，我现在记不清他究竟说了什么，只记得他没有戴口罩。人们在唱《国际歌》、《你要跳舞吗》，旁边的女生问我，“你要蜡烛吗，我带了一百多支蜡烛”。

我没有设想过自己处于这种场景下的状态。在看乌鲁木齐的视频时，我是伤心；在看上海的视频时，我是愤怒。但在这一刻，我只是想哭。

想哭的也不止我一个，哭出来的则是更多，在现场的时候，总会不久就能看到一个人哭着离开人群。

[人群的口号继续响彻天空](https://www.youtube.com/watch?v=ny7nMfklx5M)，“不自由，毋宁死”，“不要核酸要自由”，“要生存”，“要新闻自由”。一个人喊下一句口号，一群人呼应一句口号。

那种场景之下是没有办法感受到时间流逝的，直到喊到嗓子都沙哑，已经感到疲累的我开始慢慢走到一边，拍视频、发朋友圈。我当时心里冒出的念头是：这群人的身体怎么能这么好！

警察也开始做事了。

他们在人群的外围排成了人墙，有警察进来试图拉起警戒线，想把最中心的鲜花和蜡烛围住。有人试图上前和警察理论，“你为什么要做这个事，你可以不做这个的”。有几个男生拿起了火机，烧掉了隔离带，现场响起一片欢呼。

警察也没有再尝试封锁，只是在反复地对人群喊道，“快散咯”，“快散咯”。

那是九点左右的时候。我也不确定是什么原因，可能是觉得喊得也差不多了，可能是觉得警察开始组成人墙驱散，有被捕的危险。人群之中开始慢慢传递一个信息：“有序撤离，不要落单，和朋友一起走”。

但事实上大家并没有真正的撤离。在望平街出来的东风路上、桥头上，依旧是黑压压的一片人，大家都想确认后面的人都安全离开之后才撤离。有人甚至也在外面继续喊起了口号。

![22](/Users/admin/Documents/GitHub/ipmp/static/img/chengd-24.jpg)
<center><font color=gray>尽管警察开始驱散人群，在望平街一带仍有不少成都市民留守确保大家安全有序撤离。图片由受访者提供</font></center>

在我所能看到的地方，警察开始增援，一辆中巴车，又是一辆中巴车，有的上面坐满了警察，有的则是空车，是要来抓人的吗？我们都有些担心。

接下来就是整个晚上我冲击最深的一刻之一——**上百个身穿黑色衣服的便衣直接冲进了人群之中进行驱赶，聚集的人群瞬间往两边跑，整个场面开始乱了起来**。因为那天晚上，据说是附近通知不允许网约车运行，很多人都是骑共享单车过来的，所以路边基本停了很多单车，警察冲过来的时候，单车倒了一地，有人也摔倒在了单车上。而在望平街里面，大批警察也开始对尚未走出来的人群大声喊，“走咯”、“走咯”。

事实上那一瞬间我竟然没有太多反感的情绪，甚至当时朋友跟我说“这群警察好暴力喔”的时候，我还试图“解释”：清场不都是这样子的嘛。

但我还是没办法忘记那个画面对我带来的冲击，**[一群警察蜂拥而上，无助的人群四散而逃，好像一群豹子冲击迁徙的羊群](https://www.youtube.com/watch?v=lrhWZX8Sm8M)。后来，朋友也告诉我，有很多人当时已经被抓了，只是我没有看到。**

也是在这次冲击之后，我和人群分隔开，和朋友也失散了，我在旁边等了一段时间朋友，最后决定回家。那大概是晚上十点多。——后来我知道，另一头的人们还在高喊口号到十一点。

回想那个晚上，我感慨的是，年轻人们实在太值得敬佩，现场的主力都是年轻人——那天晚上也来了不少成都当地的知识分子，他们更多是站在一旁记录和观察，很少喊口号。尤其让我感受到力量的是成都的女生，我后来听说，在人群撤离望平街的时候，警察原本要求扫健康码，是几个女生上前理论，最后才作罢。

这场抗议给我的感觉，也并非是争取什么宏大的权利、言论自由、新闻自由，只是没办法过日子了，争取活下去的权利。

但这也是我气愤的地方，因为回到家我刷微博，看到了很多质疑的声音，“境外势力”、“港独、台独”。不少人还说现场没有成都人，网上也没有成都人说话。没错，现场有人质疑有些口号是不是太过激烈。但我没有怀疑过所有人，因为我就在那里。**至于发声，发微博可能会被网暴，发朋友圈可能被警察找，并不是没有成都人发声，而是成都人说不了话，因为成都人害怕**.我也害怕，我在成都生活了十年，算不算一个成都人？

至于现在，我其实也不知道未来会怎样。一边担忧着后续的影响，有朋友建议我删掉手机里拍下的几十个视频，我只好慢慢在处理。

一边也在想，如果有下一次，我一定还会去。如果被抓了人，要声援，我也一定会去。


### **紧急关注：**

近日中国多地有大量曾参与白纸运动的公民被警察带有问过，有网友在社交媒体分享[成都被捕的年轻抗争者仍在看守所](https://chinadigitaltimes.net/chinese/690615.html)，也有网友表示[广州仍有年轻抗争者失联](https://twitter.com/xlxnoodles/status/1600117820452327424)。我们呼吁大家持续关注他们的情况。另外，应对紧急情况，我们特别制作了一份[《针对警察上门及防止被电子取证指南》](https://ngocn2.org/article/2022-12-06-policy-security-guide-V1/)。如果你有相关风险，希望这本指南对你有用。

也欢迎你给我们提供反馈意见或者提出你的问题，请邮箱发送至：it.ngocn@protonmail.com。我们将根据情况和需求推出系列《公民安全行动指南》，祝愿大家平安健康。