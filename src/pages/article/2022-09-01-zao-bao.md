---
templateKey: article
title: 9.1 N记早报
slug: zao-bao
date: 2022-09-01T15:44:58Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: 小咚

---
**1. 联合国发布新疆人权报告，指相关行为可能构成反人类罪**

日内瓦时间 8 月 31 日深夜，联合国发布一份[关于中国新疆维吾尔自治区人权问题的报告](https://www.ohchr.org/en/documents/country-reports/ohchr-assessment-human-rights-concerns-xinjiang-uyghur-autonomous-region)，称新疆地区发生了严重侵犯人权事件。据[德国之声](https://www.dw.com/zh/%E8%81%AF%E5%90%88%E5%9C%8B%E7%99%BC%E5%B8%83%E6%96%B0%E7%96%86%E5%A0%B1%E5%91%8A-%E7%A8%B1%E4%B8%AD%E5%9C%8B%E6%81%90%E7%8A%AF%E4%B8%8B%E5%8F%8D%E4%BA%BA%E9%A1%9E%E7%BD%AA/a-62987574?maca = chi-rss-chi-all-1127-rdf)，联合国报告对被关押在所谓“职业教育和培训中心”的人表示慰问，称酷刑、虐待、性暴力、强迫劳动及恶劣的居留条件等指控均属实。

据[美国之音](https://www.voachinese.com/a/china-to-stop-un-report-20220831/6724682.html)消息，中国驻联合国大使张军于同日报告发布前宣称，北京“坚决反对”该报告的评估结果。张军坚称中国并没有看过该报告，而此前巴切莱特（Michelle Bachelet）曾说，报告初稿已经依流程提交给北京并遭受北京方面的压力，因此不断延缓发布。报告的最终发布时间为发布者联合国人权办公室高级官员巴切莱特离任前 10 小时。#人权 #新疆

**2. 香港八三一事件三周年，市民自发于太子站外纪念**

据[香港独立媒体](https://www.inmediahk.net/node/%E7%A4%BE%E9%81%8B/831%E4%B8%89%E5%91%A8%E5%B9%B4-%E5%B8%82%E6%B0%91%E5%A6%82%E5%B8%B8%E5%88%B0%E5%A4%AA%E5%AD%90%E7%AB%99%E5%A4%96%EF%BC%9A%E5%9C%A8%E8%8D%92%E8%AC%AC%E9%BB%91%E6%9A%97%E6%99%82%E4%BB%A3%E4%BB%8D%E8%A6%81%E5%A0%85%E6%8C%81)报道，2022 年 8 月 31 日，香港市民于地铁太子站外聚集，以手持白花、民主女神像等方式自发默哀、纪念“八三一事件”三周年。下午起，太子站 B1 出口开始有警力聚集，入夜后警力增加。期间，部分想要将白花放置在出口地面上的民众遭到警察威胁，称若不带走会被控“乱丢垃圾”。据[香港 01 消息](https://www.hk01.com/%E7%AA%81%E7%99%BC/810096/831%E4%B8%89%E5%91%A8%E5%B9%B4-%E8%AD%A6%E5%93%A1%E5%A4%AA%E5%AD%90%E7%AB%99%E5%A4%96%E6%88%AA%E6%9F%A5%E6%8C%81%E7%99%BD%E8%8A%B1%E5%B8%82%E6%B0%91-%E5%85%A9%E7%94%B7%E5%8C%85%E6%8B%AC%E4%B8%80%E5%B0%91%E5%B9%B4%E8%A2%AB%E6%8D%95)，另有三名民众因持白花被截查。

据[端传媒报道](https://theinitium.com/article/20190905-hongkong-prince-edward-mtr-station-police/?utm_source = facebook&utm_medium = facebook&utm_campaign = fbpost&fbclid = IwAR2hjU0Z0sqKKWVQCRdhvLuructMqkQtZCxYY-qytWKtCqwH7Sptv8cjGv4)，2019 年 8 月 31 日晚，大量香港警察于香港九龙旺角太子站对市民、乘客使用暴力。在网传视频中，警察使用警棍、枪及胡椒喷雾等武器致使市民流血受伤，亦有警察用警枪对准车厢中乘客，被认为是无差别攻击。#香港

**3. 成都宣布自 9 月 1 日起封城**

据[天津日报](https://finance.sina.com.cn/wm/2022-09-01/doc-imizmscv8649780.shtml?cref = cj)，9 月 1 日中国新增本土病例共 307 例，四川省新增 132 例。据[公众号成都发布消息](https://mp.weixin.qq.com/s/Wn7kVavg6969z82xX_yGZQ)，自 9 月 1 日 18 时起，成都市全体居民“原则居家”，9 月 1 日至 4 日将展开为期四日的全员核酸检测，波及近 2120 万成都市民。据悉，封城前两日（8 月 30 日），成都发布公众号曾发布以《不！用！囤！》为标题的推文提醒市民不需要担忧物资供应。#封城 #疫情

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
