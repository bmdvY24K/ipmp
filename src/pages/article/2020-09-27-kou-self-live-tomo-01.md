---
templateKey: article
title: 自给生活 | 从明天起，关心粮食和蔬菜
slug: kou-self-live-tomo-01
date: 2020-05-23T22:52:19.163Z
description: 必须承认，海子给我出了一个难题，我向来算数不好，但非常努力地算了一下，并欢迎帮忙挑错：中国13亿人，6.6亿吨粮食，不计贸易进出口，简单约合每人0.5吨。一千斤呀！！
featuredpost: false
featuredimage: /img/自给生活01.jpeg
trending: 6
isbrief: false
tags:
  - 自给生活，粮食，蔬菜，海子
contributors:
  - type: 寇延丁
    name: 鹤苦蛙
---

> 编者按：一去数年再归来，一回便遇上了新冠疫情，除了保持写《封城·记忆》系列日记的习惯，寇姐更不断思考着对生命的其他观照——世事蜩螗，如何“自处”成为普通百姓更愿意关心的事情。除了以看书观影等方式在精神世界里找到属于自己的小角落外，寇姐更用实际行动为我们带来了新一种安顿自我的实践模式：自给生活。

一直喜欢海子的诗：“从明天起，做一个幸福的的人……”谁不想幸福呢？

接下一句“关心粮食和蔬菜”。这很重要，特别切合现在因粮食问题人心惶惶的网络背景。

先关心粮食，国家统计局：“2019年全国粮食总产量66384万吨。”

必须承认，海子给我出了一个难题，我向来算数不好，但非常努力地算了一下，并欢迎帮忙挑错：中国13亿人，6.6亿吨粮食，不计贸易进出口，简单约合每人0.5吨。一千斤呀！！

必须承认我被吓到：“这么多，吃不完怎么办？”

## 乐死人

此前在一个民间组织盛会上，参与农业分论坛，某民间机构专家做主旨发言：“我们粮食自给82%低于国际公认粮食安全线90%，但是，有必要乐观进言中央领导：完全不用担心。还有大量撂荒地，遇到什么风吹草动，种起来就是……”呃哦，不乐观，会死人么？

懵圈两秒钟之后，稳住了自己环顾四周，先确认没有什么“领导”，更是连“中央”的影子都见不到。再确认这位专家身份，民间人士无误。素来对这种身在江湖心在朝堂不知道自己是谁的砖家高山仰止，如此这般不用普通人口气说话，那是何等不食人间烟火仙气飘飘。

但是，如此统计数字当前，不得不比那位专家更乐观，直至乐极生悲：粮食太多，吃不完怎么办？

因为从事体力劳动，一直饭量可观，拥有让人不好意思的好胃口，但我的粮食消耗，却很有限。我对添加剂敏感，极少外购外食，很容易计算粮食消耗。极少精致淀粉高碳水，五谷杂粮粥是我的主食百吃不厌，100克混合杂粮煮粥一大锅，要吃两天，如此一年最多消耗36斤，再加上其他类别50斤封顶。虽然粮食太多有必要努力为国分担，再努力也不可能超过60斤；我不吃肉，那就努力吃鸡蛋，一天两个一年700，90斤，饲料与鸡蛋料蛋比1.8-2.5：1，按最高值2.5，又可以间接消耗粮食225斤。但因之贡献的三只老母鸡只能请他人代劳。

我国人均千斤粮，作为一个从事体力劳动的资深吃货，我直接吃、加间接消耗，全年不到300斤，不抵三分之一。

这样的数字，乐观到乐死人。

## 吓死人

接下来的算数任务更艰巨。全球化时代，进出口因素不能不计。我不吃肉，2019年517.8万吨各种肉类进口无法为国分担，只算我能吃的：2019谷物进口1791.8万吨，出口323.6万吨，净进口1468.2万吨；还有食糖339.0万吨；大豆8851.1万吨；油菜籽273.7万吨；奶粉139.5万吨，总计11071.5万吨，13亿人平均每人超过0.08吨，160斤！这还没算1152.7万吨植物油。直接开吃要吃三年多，变饲料转换成鸡蛋要吃半年多……跪求高人指点：我没算错吧？

不由“乐死人”变成了——“吓死人”。

我国粮食产量统计数字不妄加评论，但能确信进出口数字没有水分（只会少不会多，因为婴儿奶粉代购和各种走私均不在其列）。

净进口数字无论大小，都是真金白银买进来的，不会存在库里烧化，最终都要成为我们的生命能量。如果我的算数没出问题，是否说明：以我的食量，在2019年，维持这条小命， 至少一半来自进口？

中国13亿人，饭量比我小的大有人在，比我大的同样不少。**不管吃多吃少，我们的生命，都相当比例依赖进口。一天不吃没问题，一周不吃就会死人，不敢想像再久怎样。一旦国际粮食贸易有变，那可不是什么“风吹草动”，而是性命攸关。这也充分说明了，为什么少数国家因肺炎限制出口、稍有风吹草动，立即有人闻风丧胆开始抢购囤积。**

## 麻木不仁

事实上，我既不是乐死人，也不是吓死人，而是——麻木不仁。不管是官方辟谣再三强调粮食安全，还是民间人心浮动开始抢购存粮，一概对我没影响，麻木不仁。这种麻木不仁，既有自带底气，还要加上有粮者无畏。

先说底气，我之于粮食的底气，源于2018。新手农夫第一年，亲手种出的稻谷，又亲手日晒收袋，称量干谷750公斤——有这么多粮食垫底，还怕什么呢？虽然我的收获发生在遥远的台湾，拥有这样的劳动能力，对人的心理影响至关重要。

再说有粮。因为对添加剂敏感，长期自炊，粮食都有日常储备。五谷杂粮，量不多但样样有，现下储备，够用半年。

各种豆类是素食者重要蛋白质来源，红豆绿豆黑豆花豆不厌其繁，顺便分享吃货的储粮心得：用过的透明塑料瓶盛粮，隔潮、防虫、节约收纳空间一目了然，当然黄豆鹰嘴豆因为还会用来打豆浆消耗量更大，需要用更大一些的瓶子，花生也一样。

谷物，同样是原粒为主，比如我会优先选原粒的麦子，或者糙米、小米，一则浅加工保留维生素族群，二则原形原粒食物耐储存。离家四年，存下的黑豆红豆照样好吃，豌豆甚至还有相当的发芽率。花生容易走油存不久，米也不宜久存，但麦粒没有问题。我习惯每种保持一瓶储量，不会等坐吃山空才做补充，七七八八加起来，常规储量二十斤左右。瓶中有粮，自然心里不慌，自然对与粮食有关的风吹草动麻木不仁。

![](https://assets.matters.news/embed/a653927d-0853-49ec-a779-b48c95640d1a.jpeg)

收藏的可爱豆类



## 自给生活，从蔬菜开始

说完了粮食，再说蔬菜。

有存粮，就能保证最低生命需求，至少不被饿死，才有可能考虑救人一类理想，和生命品质一类奢侈。

我是蔬菜水果不限量型选手，主食消耗低，与此有关。蔬果的品种与数量，关乎生命质量幸福指数。作为水果依赖症重度患者不敢轻言自给自足，没有一片山林纯属痴人说梦，但可以试试蔬菜。

我的泰山小窝有接近三十平方小小院落，正在大兴土木做阳光房——建一个蔬菜自给自足的四季菜园。而建阳光房，我们这里又称“封阳台”。本来一直反对封阳台，我喜欢院子里来去自由的鸟儿和来去自由的风，一地芳草满目鲜花也像我一样，需要自由自在的阳光和风。**这次一去数年，归来芳草凋零、玫瑰凋零，我亦凋零。新生的我已是农民，需要新天新地新菜园，要在冰封冬季实践自给自足。**



诗人说得好，做一个幸福的人，关心粮食和蔬菜。在宜兰有宽广农田开阔菜园、完全自给自足，是农村奢华版，如今则是城市折衷克难版本。不管怎样，都是自己动手争取食物主权的尝试，是在权力系统围追堵截之下重建生命自主的努力。

**即将开启“自给生活”实践，业余时间、方寸之地，螺丝壳里做道场。**目标：蔬菜为主，争取实现自给自足，水果为辅，能收多少随缘随喜，但能保证快乐爆棚自给有余，并乐于与大家分享。

![](https://assets.matters.news/embed/fd893973-978e-47de-a826-99b5d31444b5.jpeg)

\    欢迎扫码支持扣子姐的写作