---
templateKey: article
title: 1.4 N记早报
slug: zao-bao
date: 2021-01-04T02:56:31Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: 馬來貘貘

---
**1. 发哨人艾芬因右眼遭误诊失明问责爱尔眼科，后者称与手术无关**

12 月 31 日，[武汉市中心医院急诊科主任艾芬在微博通过视频自述](http://t.cn/A6q8ZDAF?m = 4588320120649213&u = 2662574464)，自己于 2020 年 5 月向爱尔眼科医院咨询眼睛视力问题，在没有接受相关检查的情况下被建议进行白内障晶体置换手术，术后右眼几乎失明。艾芬质疑爱尔医院存在不规范行为，并要求爱尔眼科医院承担相关手术责任。爱尔医院当晚发布声明回应称已展开自查。针对该声明，[艾芬告知](https://finance.sina.com.cn/china/gncj/2021-01-01/doc-iiznctke9683760.shtml)《每日经济新闻》记者认为对方在撒谎，随后艾芬继续在网络平台公开更多治疗细节并维权。1 月 4 日，爱尔眼科公布手术核查报告并称，艾女士情况与手术无直接关联。艾芬是疫情吹哨人李文亮的同事，同为最早传出武汉疫情相关消息的第一批人，被媒体称为“发哨人”。三月份接受《人物》[杂志采访时表示](https://chinadigitaltimes.net/chinese/2020/03/%e4%ba%ba%e7%89%a9%ef%bd%9c%e5%8f%91%e5%93%a8%e5%ad%90%e7%9a%84%e4%ba%ba/)：“我管他批评不批评，老子到处说”，这句话成了艾芬、李文亮一群敢于讲真相医生的代表性“名言”。

**2. 大连理工大学发生校内车祸，肇事者曾任副校长**

12 月 30 日下午，大连理工大学校内发生一起严重车祸，一名研究生被撞离世，网传车祸因超速驾驶而起。[据网友微博内容](https://weibo.com/7238361153/JBaUztrah?type = repost#_rnd1609722050892)，死者为该校研究生，是家中独女，刚过完生日不久。事件发生后网络相关讨论一度被压制，直至 1 月 1 日下午该事件相关词第一次进入热搜榜。随后，[大连理工大学官微发布通报回应称](https://weibo.com/3082822153/JBbM3bs8Z?type = comment#_rnd1609722610673)，肇事者为该校教师，[网上资料显示曾于](https://www.163.com/dy/article/FVE67F350537A693.html)[2010](https://www.163.com/dy/article/FVE67F350537A693.html)[年被免去副校长职务](https://www.163.com/dy/article/FVE67F350537A693.html)；通报内容简述了车祸发生的时间与地点，并提及事发当日的雨雪天气和道路结冰等情况，而未提“超速驾驶”等字眼。

**3. 国产疫苗：老人不能打，效力引怀疑**

1 月 3 日，[央视发布](https://weibo.com/2656274875/JBr0y0VGQ?type = comment)[“](https://weibo.com/2656274875/JBr0y0VGQ?type = comment)[新冠病毒疫苗接种须知](https://weibo.com/2656274875/JBr0y0VGQ?type = comment)[”](https://weibo.com/2656274875/JBr0y0VGQ?type = comment)，内容指明目前疫苗接种年龄限制为 18 至 59 岁，其余年龄段人群暂时不予接种。这当中提到的疫苗，是指 12 月 31 日被当局批准上市的尚未完成第三阶段实验的国药集团研发的新冠灭活疫苗。与国外做法不同的是，疫苗既然已获批上市，最需要优先接种的老年人群反而不在接种范围之内。有分析认为，该疫苗可接种人群简而言之即是身体健康的青中年人群，在接种范围有限的情况下，疫苗原本的目的——群体免疫的实现和高危人群的保护都看似失去意义。目前正在美欧接种的辉瑞疫苗，年龄设定为 16 岁至 85 岁，优先接种老弱人群和危险人群。中国国产疫苗因为没有完成第三期实验，加之没有公布完整数据，以致引起外界对其疫苗安全性和有效性的怀疑。

**4. 湖北村支书当街持刀捅人，因被村民举报变卖集体财产**

[据民生观察网消息](https://www.msguancha.com/a/lanmu4/2021/0104/20607.html)，2020 年 12 月 31 日 14 时许，湖北襄阳市樊城区太平店镇一李姓村书记被举报后，当街持刀捅人，被警方制服后威胁村民说“老子一定要杀了你！”村民们反映，村里有一片约 300 亩的河滩树林，属于村集体财产，却被村党支部书记私自承包给了个人，在村民不知情的情况下被私人变卖。村民找村部反映多次仍未得到解决，于是向上级部门反应。事发当天，村书记的家人辱骂群众，引起民愤，双方撕扯在一起，村支书掏出一把长刀后有村民报警，目前村支书已被行政拘留。

**5. 特朗普电话录音流出，要求佐治亚洲官员寻找更多选票以扭转大选结果**

[《华盛顿邮报》周日曝光了其获得的一段电话录音指](https://www.washingtonpost.com/politics/trump-raffensperger-call-georgia-vote/2021/01/03/d45acb92-4dc4-11eb-bda4-615aaefd0555_story.html)，美国总统特朗普给佐治亚共和党人州务卿布拉德·拉芬斯佩尔（Brad Raffensperger）打了一小时电话，要求对方“找到”11780 张选票，以推翻自己在该州总统大选中败选的结果。特朗普在通话中坚持自己胜出大选，指摘拉芬斯佩尔 ，后者则直言特朗普的指控是基于没有根据的传言。有法律专家指特朗普这次与拉芬斯佩尔等人的通话可能触犯法律，透过敦促州务卿“寻找”选票及部署“想找出答案”的调查人员，特朗普似乎在鼓励他窜改选举结果。

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
