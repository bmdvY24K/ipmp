---
templateKey: article
title: 1.26 N记早报
slug: zao-bao
date: 2022-01-26T08:17:06Z
featuredpost: false
trending: 0
isbrief: true
contributors:
  - type: 作者
    name: NL

---
**1. 王建兵、黄雪琴失联超过 4 个月，分别被关押在广州市第一、第二看守所**

职业病权益倡导者王建兵、独立记者黄雪琴于 2021 年 9 月 19 日下午失联后，至今已超过 4 个月。10 月 27 日，广州公安局发出“煽动颠覆国家政权罪”逮捕令。据[关注雪饼](https://free-xueq-jianb.github.io/?fbclid = IwAR2z80NeFOYord6I_oMJ0DNCfYyhbxsI9ldCEt2e2bpW9QqZUJLIFkemt6A)，黄雪琴目前正被关押于广州市第二看守所(白云区石井镇潭岗村)，此前已确认王建兵被关押于广州市第一看守所。广州警方拒绝律师会见和取保，家属也无法向二人汇款，至今不知其身心健康状况。 #黄雪琴 #王建兵

**2. 日本 #Metoo 运动代表人物伊藤诗织胜诉，获赔 332 万日元**

据[日本时报](https://www.japantimes.co.jp/news/2022/01/25/national/crime-legal/shiori-ito-awarded-damages-rape-case/)，东京高等法院当地时间 1 月 25 日判决伊藤诗织指控前 TBS 电视台记者山口敬之性侵的损害赔偿诉讼维持一审判决，要求山口敬之赔偿 332 万日元；同时，法院认定伊藤诗织在记者会和书中的被下药猜测构成妨害名誉，需要赔偿山口 55 万日元。此项裁决在日本 #Metoo 运动中具有重要意义。

2015 年 4 月，路透社东京分部实习记者伊藤诗织与承诺帮她找工作的前 TBS 高级记者山口敬之共进晚餐后，在昏迷中于一家酒店内被强奸。2017 年 5 月，伊藤诗织实名亲自出席记者会，宣布她控告山口性侵的刑事诉讼遭到警方阻挠。伊藤继续提起[民事诉讼](https://global.udn.com/global_vision/story/8662/4234475)，东京地方法院于 2019 年 12 月 18 日做出裁决，认同伊藤的主张。回避出庭的山口敬之随后表示不服判决提起上诉，并反告伊藤损毁名誉。东京高院昨天维持一审判决。伊藤于 2017 年出版[《黑箱》](https://book.douban.com/subject/30485848/)一书，描述她遭到性侵与司法不公对待的过程。这起事件让伊藤诗织成为日本 #Metoo 运动的代表人物。 #Metoo #日本

**3. 蔡英文出席蒋经国图书馆开幕典礼，遭批评“违背转型正义”**

台湾领导人蔡英文于 1 月 22 日[出席蒋经国总统图书馆开幕典礼](https://www.president.gov.tw/NEWS/26510)，并称蒋经国“反共保台”的立场是“当前台湾人民最大的共识”。此举在台湾朝野引发争议，时代力量党团[批评蔡英文](https://www.upmedia.mg/news_info.php?SerialNo = 136349)出现在“纪念威权统治者的场合”，“完全违背转型正义政策目标”。

转型正义是民主国家对过去威权政府实施的违法和不正义行为的弥补，通常具有司法、历史、行政、赔偿等面向，为民进党政府上台后的主要政见承诺。蔡英文任内推动成立的“促转会”（促进转型正义委员会）[曾发布报告](https://www.ly.gov.tw/Pages/Detail.aspx?nodeid = 6590&pid = 211827)，提出将中正纪念堂转型为“反省威权历史公园”，并移除蒋介石塑像。 #台湾

**4. 乌克兰局势恶化，恐爆发战争**

俄罗斯与乌克兰的战争一触即发，引发多国关注。据[半岛电视台](https://www.aljazeera.com/news/2022/1/25/five-things-to-know-about-russia-ukraine-tensions)，俄罗斯指责美国及其北约盟国向乌克兰提供武器并举行联合演习，反对乌克兰加入北约的计划。北约表示，纳入乌克兰成为其成员国之前，乌克兰需根除本国的腐败等问题。

1 月 25 日，就俄罗斯阻挠乌克兰加入欧盟及北约的决定，乌克兰国会向联合国、欧盟及各国政府[重申立场](https://www.ukrinform.net/rubric-polytics/3391292-ukrainian-parliament-addresses-intl-organizations-over-russias-military-blackmail.html)，乌克兰国防部[指责](https://www.ukrinform.net/rubric-polytics/3391095-russia-betting-on-ukraines-destabilization-nsdc-secretary-believes.html)俄罗斯破坏乌克兰内部稳定。1 月 24 日，五角大楼已派出约[8500 名士兵](https://www.aljazeera.com/news/2022/1/24/uk-pulls-staff-from-ukraine-as-fears-of-war-rise-liveblog)待命。同日，欧盟委员会[表示](https://ec.europa.eu/commission/presscorner/detail/en/STATEMENT_22_545)，将提供 12 亿欧元紧急援助支持乌克兰。 #乌克兰

欢迎加入NGOCN的Telegram频道：ngocn01

微信好友：njiqirenno2
