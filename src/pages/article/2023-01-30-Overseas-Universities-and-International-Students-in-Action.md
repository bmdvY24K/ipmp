---
templateKey: article
title: '快讯｜多个海外高校及留学生公开行动关注白纸运动被捕校友'
slug: 'Overseas-Universities-and-International-Students-in-Action'
date: 2023-01-30T03:21:57.265Z
description: '尽管已经过了两个月，但当局仍在不断搜捕白纸运动的参与者。一些被捕者的海外母校或留学生也正在公开发声，呼吁当局释放他们的校友。'
featuredpost: true
featuredimage: /img/siqi.jpg
trending: 10
isbrief: false
contributors:
  - type: '作者'
    name: '小辉'
tags:
  - "被捕者"
  - "海外母校"
  - "留学生"
  - "白纸运动"
---

尽管已经过了两个月，但当局仍在不断搜捕白纸运动的参与者。这场和平抗争行动是为了悼念乌鲁木齐火灾的逝者和呼吁停止过度防疫措施，参与者为来自各行各业的普通市民。随着越来越多被捕者信息被公开，让外界了解到部分被捕者有海外留学经历。这些海外母校或者留学生也开始公开行动，表达对被捕校友的关注，并呼吁中国当局能尽快释放他们和其他白纸运动被捕者。希望更多海外大学能为他们的被捕校友挺身而出，进行更多公开发声表达支持。

近日，英国大学Goldsmiths（中文一般称“金匠大学”）在给学生的回信中表示，他们已经向中国驻伦敦大使馆发信表达对被捕的校友李思琪的关注，呼吁尽快释放她，并且1月25日已在[学校网志The Tab发表了公开报道](https://thetab.com/uk/london/2023/01/25/a-goldsmiths-graduate-has-been-detained-in-china-after-attending-public-demonstration-49313)，同时也正在研究如何保护在校中国学生的自由表达权利和人身安全等。 

1月19日，芝加哥大学东亚研究中心[就校友秦梓奕被捕事件发表公开声明](https://myemail.constantcontact.com/Statement-on-Recent-Blank-Paper-Protests-in-China.html?soid=1125351042727&aid=8lgd3aMEPF0)（秦梓奕2017年毕业于社科文学硕士课程），深切关注秦同学和其他白纸运动被捕者情况，并呼吁尽快释放他们。同时表示将在未来几周针对白纸运动展开相关讨论，欢迎师生就这一事件向他们提出更好的应对想法。在公开声明发出后，根据多个知情人士证实，秦梓奕目前已被取保候审。

1月26日，被捕者李元婧曾就读的澳洲新南威尔士大学（英文简称UNSW）[在校学生和往届校友发起公开联署](https://www.change.org/p/free-unsw-s-detained-alumna-li-yuanjing?redirect=false%0A)，希望征集500名UNSW学生或者校友的署名共同向学校发出公开信，希望学校能仿效芝加哥大学或者其他海外高校一样发出公开声明声援他们的校友，敦促中国驻澳州悉尼大使馆公布这些被捕者的情况，并且呼吁中国当局立即无条件释放他们。截至报道时间，这个联署行动已经获得265个联署。

