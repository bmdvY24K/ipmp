---
templateKey: article
title: 'N记快讯 ｜ 关注中国女工权益平台“尖椒部落”宣布注销'
slug: 'jian-jiao-bu-luo-close-statement'
date: '2021-08-10T03:21:57.265Z'
description: null
featuredpost: true
featuredimage: /img/IMG_2082.JPG
trending: 8
isbrief: false
tags:
  - '尖椒部落'
  - '女权'
  - '女工'
contributors:
  - type: '作者'
    name: 'Harry'


---

**\#N记快讯 ｜ 关注中国女工权益平台“尖椒部落”宣布注销**



8 月 9 日，中国女工权益和生活组织“尖椒部落”在其[微信公众号](https://mp.weixin.qq.com/s/6YPcMv0jN2S-aCFoMwEQ1g)上发表声明，宣布将停止更新，各官方账号将注销关闭。

尖椒部落于2014年创立，聚焦女性工人的权益，7年来发布超过1400篇文章，举办了超过30场线下活动，有超过100位女工为其撰稿。根据其微信公众号文章，尖椒部落关注女性与工人的交叉性问题，对于性别暴力、月经羞耻、女同性恋工人发表多篇文章，也关注性教育和女工情感等话题，

2020 年 2 月 6 日，尖椒部落前员工王小嗨[发文](https://matters.news/@Leftsupervision/尖椒部落-我被尖椒部落非法开除-为什么劳工机构的官僚比资本家还要凶狠-bafyreidgme2yoiz6sxqzc35vaouynvwwbsctvxiox5pi4thodzxgxb2o5u)，指控尖椒部落违反《劳动法》，将其非法开除，尖椒部落随后[发布声明](https://zhuanlan.zhihu.com/p/107301165)称解除劳动合同因该员工“工作表现依然未能达到要求”。随后事件双方各自撰写多篇文章支持己方观点。2020 年 4 月 15 日，深圳市龙华区劳动人事争议仲裁委员会以尖椒部落“双方协商一致解除合同”与邮件中“不能胜任工作”的辞退理由矛盾，且尖椒部落无法举证王小嗨不能胜任工作为由，裁定劳动合同属于违法解除，尖椒部落需支付赔偿金 12140 元。4 月 27 日，女权活动者大兔[发文](https://matters.news/@solidkillian/毁誉-告密-消声-三步可毁一个女工网站-bafyreigwtljey4lepuxf6lf63kxmstyvd2fdabrkc4olse2fwhgi2bk6c4)，指责王小嗨及其支持者利用尖椒部落面临的政治风险，公开爆料使得警察上门搜查，员工被带走盘问。该事件引起了左翼人士与女权主义活动者之间的大量讨论和争执。

从 2014 年成立到 2021 年注销，尖椒部落见证了中国剧烈变化的几年。在不断紧缩的环境下，相关领域的行动者面对的是更加复杂、分裂的劳工与性别状况。